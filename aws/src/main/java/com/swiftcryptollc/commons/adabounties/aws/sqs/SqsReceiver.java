package com.swiftcryptollc.commons.adabounties.aws.sqs;

import com.swiftcryptollc.commons.adabounties.aws.sqs.data.SqsData;
import com.swiftcryptollc.commons.adabounties.aws.sqs.data.SqsRegistration;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import software.amazon.awssdk.services.sqs.SqsClient;
import software.amazon.awssdk.services.sqs.model.DeleteMessageRequest;
import software.amazon.awssdk.services.sqs.model.Message;
import software.amazon.awssdk.services.sqs.model.ReceiveMessageRequest;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
public class SqsReceiver implements Runnable {

    protected final SqsService sqsService;
    private final Set<SqsListener> listeners = new HashSet<>();
    private Boolean keepAlive = true;
    private final SqsClient sqsClient;
    protected final String queueUrl;
    private final Object LOCK = new Object();
    private static Logger logger = LoggerFactory.getLogger(SqsReceiver.class);

    /**
     *
     * @param sqsService
     * @param sqsClient
     */
    public SqsReceiver(SqsService sqsService, SqsClient sqsClient, String queueUrl) {
        this.sqsService = sqsService;
        this.sqsClient = sqsClient;
        this.queueUrl = queueUrl;
        Thread me = new Thread(this);
        me.start();
    }

    /**
     *
     */
    @Override
    public void run() {
        logger.info("Receiving messages on Queue URL [" + queueUrl + "]");
        while (keepAlive) {
            try {
                ReceiveMessageRequest receiveMessageRequest = ReceiveMessageRequest.builder()
                        .queueUrl(queueUrl)
                        .maxNumberOfMessages(1)
                        .build();
                List<Message> messages = sqsClient.receiveMessage(receiveMessageRequest).messages();
                if (!messages.isEmpty()) {
                    logger.info("Processing [" + messages.size() + "] messages");
                    synchronized (LOCK) {
                        Set<SqsData> data = new HashSet<>();
                        for (Message message : messages) {
                            SqsData sqsData = SqsData.fromJson(message.body());
                            if (sqsData.getType().equals("SQSREGISTRATION")) {
                                logger.info("Processing SQS Registration");
                                SqsRegistration queueInfo = (SqsRegistration) sqsData.getObject(SqsRegistration.class);
                                if (queueInfo.getIsDelete()) {
                                    sqsService.removeQueueUrl(queueInfo.getQueueUrl());
                                } else {
                                    sqsService.addQueueUrl(queueInfo.getQueueUrl());
                                }
                            } else {
                                data.add(sqsData);
                            }
                            DeleteMessageRequest delMsgReq = DeleteMessageRequest.builder()
                                    .receiptHandle(message.receiptHandle())
                                    .queueUrl(queueUrl)
                                    .build();
                            sqsClient.deleteMessage(delMsgReq);
                        }
                        for (SqsListener listener : listeners) {
                            listener.addAllToQueue(data);
                        }
                    }
                } else {
                    try {
                        Thread.sleep(1000);
                    } catch (Exception e) {
                    }
                }
            } catch (Exception ex) {
                logger.error("Exception occured [" + ex.getMessage() + "]", ex);
            }
        }
    }

    /**
     *
     * @param listener
     */
    public void addListener(SqsListener listener) {
        synchronized (LOCK) {
            logger.info("Adding listener [" + listener.getId() + "]");
            this.listeners.add(listener);
        }
    }

    /**
     *
     * @param listener
     */
    public void removeListener(SqsListener listener) {
        synchronized (LOCK) {
            logger.info("Removing listener [" + listener.getId() + "]");
            this.listeners.remove(listener);
        }
    }

    /**
     *
     */
    public void stop() {
        keepAlive = false;
    }

    /**
     * @return the queueUrl
     */
    public String getQueueUrl() {
        return queueUrl;
    }
}
