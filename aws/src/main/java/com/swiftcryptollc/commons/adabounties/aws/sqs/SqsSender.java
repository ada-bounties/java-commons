package com.swiftcryptollc.commons.adabounties.aws.sqs;

import com.swiftcryptollc.commons.adabounties.aws.sqs.data.SqsData;
import com.swiftcryptollc.commons.adabounties.utilities.handler.ObjectHandler;
import software.amazon.awssdk.services.sqs.SqsClient;
import software.amazon.awssdk.services.sqs.model.SendMessageRequest;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
public class SqsSender extends ObjectHandler<SqsData> {

    protected final Boolean isLambda;
    protected final SqsService sqsFac;
    protected final SqsClient sqsClient;
    protected String serverQueueUrl;
    protected String myQueueName;

    /**
     *
     * @param sqsService
     * @param sqsClient
     * @param isLambda
     * @param myQueueName
     */
    public SqsSender(SqsService sqsService, SqsClient sqsClient, Boolean isLambda, String myQueueName) {
        super();
        this.isLambda = isLambda;
        this.sqsFac = sqsService;
        this.myQueueName = myQueueName;
        this.sqsClient = sqsClient;
        if (this.isLambda) {
            this.serverQueueUrl = sqsService.getServerQueueUrl();
        }
        logger.info("Created new SqsSender with ID [" + id + "]");
    }

    /**
     *
     * @param sqsData
     */
    protected void processObject(SqsData sqsData) {
        Boolean error = false;
        logger.info("Processing [" + sqsData.getType() + "]");
        if ((!this.isLambda) || (sqsData.getSendToAll())) {
            String[] urls = sqsFac.getQueueUrls().toArray(new String[0]);
            logger.info("Sending message to [" + (urls.length - 1) + "] URLs");
            for (String queueUrl : urls) {
                try {
                    if (!queueUrl.endsWith(myQueueName)) {
                        sqsClient.sendMessage(SendMessageRequest.builder()
                                .queueUrl(queueUrl)
                                .messageGroupId("1")
                                .messageBody(sqsData.toJson())
                                .build());
                    }
                } catch (Exception ex) {
                    logger.error("Exception [" + ex.getMessage() + "] with queue URL [" + queueUrl + "]");
                    error = true;
                }
                if (error) {
                    sqsFac.refreshQueues();
                }
            }
        } else {
            try {
                logger.info("Sending message to [" + serverQueueUrl + "]");
                sqsClient.sendMessage(SendMessageRequest.builder()
                        .queueUrl(serverQueueUrl)
                        .messageGroupId("1")
                        .messageBody(sqsData.toJson())
                        .build());
            } catch (Exception ex) {
                logger.error("Exception [" + ex.getMessage() + "] with queue URL [" + serverQueueUrl + "]");
                error = true;
            }
        }
    }

    /**
     * @return the sqsClient
     */
    public SqsClient getSqsClient() {
        return sqsClient;
    }

}
