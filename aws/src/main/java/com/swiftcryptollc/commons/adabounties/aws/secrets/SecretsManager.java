package com.swiftcryptollc.commons.adabounties.aws.secrets;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.swiftcryptollc.commons.adabounties.aws.data.CardanoNetwork;
import com.swiftcryptollc.commons.adabounties.utilities.timers.ScheduledTask;
import com.swiftcryptollc.commons.adabounties.utilities.timers.TimeoutInterface;
import com.swiftcryptollc.commons.adabounties.utilities.timers.TimeoutTask;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;
import java.util.concurrent.ScheduledExecutorService;
import org.apache.commons.lang3.SerializationUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import software.amazon.awssdk.auth.credentials.ContainerCredentialsProvider;
import software.amazon.awssdk.auth.credentials.ProfileCredentialsProvider;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.secretsmanager.SecretsManagerClient;
import software.amazon.awssdk.services.secretsmanager.model.GetSecretValueRequest;
import software.amazon.awssdk.services.secretsmanager.model.GetSecretValueResponse;
import software.amazon.awssdk.services.secretsmanager.model.UpdateSecretRequest;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
public class SecretsManager implements TimeoutInterface {

    private static Map<UUID, SecretsManagerListener> listenerMap = new HashMap<>();
    private static Map<UUID, ScheduledExecutorService> scheduleMap = new HashMap<>();
    protected static Map<String, String> secretMap = new HashMap<>();
    private final static int[] UPDATE_HOURS = {1};
    private final static String[] PREVIEW_SECRET_NAMES = {"ADABounties-Preview"};
    private final static String[] MAINNET_SECRET_NAMES = {"ADABounties-Mainnet", "jwt"};
    private static String[] SECRET_NAMES;
    private static Region region;
    private final static Region PREVIEW_REGION = Region.of("us-east-2");
    private final static Region MAINNET_REGION = Region.of("us-east-1");
    private final static String PREVIEW_PROFILE = "adabounties-preview";
    private final static String MAINNET_PROFILE = "adabounties";
    private static String PROFILE;
    protected static Properties props = new Properties();
    private Boolean isLambda = Boolean.FALSE;
    private static String network;
    public final static Object LOCK = new Object();
    private static Logger logger = LoggerFactory.getLogger(SecretsManager.class);

    /**
     *
     */
    public SecretsManager() {
        this.init();
    }

    /**
     *
     * @param isLambda
     */
    public SecretsManager(Boolean isLambda) {
        this.isLambda = isLambda;
        this.init();
    }

    /**
     *
     */
    public void init() {
        synchronized (LOCK) {
            if (network == null) {
                network = System.getenv("cardano_network");
                logger.info("Using network [" + network + "]");

                if ((CardanoNetwork.MAINNET.label.equalsIgnoreCase(network))
                        || (network == null)) {
                    network = CardanoNetwork.MAINNET.label;
                    PROFILE = MAINNET_PROFILE;
                    SECRET_NAMES = MAINNET_SECRET_NAMES;
                    region = MAINNET_REGION;
                } else {
                    PROFILE = PREVIEW_PROFILE;
                    SECRET_NAMES = PREVIEW_SECRET_NAMES;
                    region = PREVIEW_REGION;
                }
                if (props.isEmpty()) {
                    try {
                        getSecrets();
                    } catch (Exception ex) {

                    }
                    if (!isLambda) {
                        kickoffSchedule();
                    }
                }
            }
        }
    }

    /**
     *
     * @param listener
     */
    public static void subscribe(SecretsManagerListener listener) {
        synchronized (LOCK) {
            listenerMap.put(listener.getId(), listener);
            if (!secretMap.isEmpty()) {
                listener.updateSecrets(secretMap);
            }
        }
    }

    /**
     *
     * @param listener
     */
    public static void unsubscribe(SecretsManagerListener listener) {
        synchronized (LOCK) {
            listenerMap.remove(listener.getId());
        }
    }

    /**
     *
     */
    public void kickoffSchedule() {
        synchronized (LOCK) {
            TimeoutTask timeoutTask = new TimeoutTask(this);
            for (int hour : UPDATE_HOURS) {
                UUID uuid = UUID.randomUUID();
                ScheduledTask task = new ScheduledTask(timeoutTask, uuid);
                scheduleMap.put(uuid, task.schedule(hour));
            }
        }
    }

    @Override
    public void processTimeout(UUID uuid) {
        try {
            logger.info("Updating secrets..");
            getSecrets();
        } catch (Exception ex) {

        }

        try {
            scheduleMap.remove(uuid);
            if (scheduleMap.isEmpty()) {
                kickoffSchedule();
            }
        } catch (Exception e) {
        }
    }

    /**
     * Secret name refers to a set of secrets within AWS Secrets Manager
     *
     * @return
     * @throws JsonProcessingException
     */
    private void getSecrets() throws JsonProcessingException {
        synchronized (LOCK) {
            secretMap.clear();
            for (String secretName : SECRET_NAMES) {
                try {
                    logger.info("Pulling secrets for [" + secretName + "] from [" + region.toString() + "]");
                    // Create a Secrets Manager client
                    Boolean success = false;
                    try {
                        SecretsManagerClient client = SecretsManagerClient.builder()
                                .region(region)
                                .build();
                        GetSecretValueRequest getSecretValueRequest = GetSecretValueRequest.builder()
                                .secretId(secretName)
                                .build();
                        GetSecretValueResponse getSecretValueResponse = client.getSecretValue(getSecretValueRequest);
                        String rawJson = getSecretValueResponse.secretString();
                        secretMap.putAll(new ObjectMapper().readValue(rawJson, HashMap.class));
                        logger.info("Found [" + secretMap.keySet().size() + "] secrets");
                        for (Map.Entry<String, String> entrySet : secretMap.entrySet()) {
                            props.setProperty(entrySet.getKey(), entrySet.getValue());
                            System.setProperty(entrySet.getKey(), entrySet.getValue());
                        }
                        success = true;
                        client.close();
                    } catch (Exception e) {
                        logger.warn("Could not pull secrets with default credential handling");
                    }
                    if (!success) {
                        try {
                            SecretsManagerClient client = SecretsManagerClient.builder()
                                    .region(region)
                                    .credentialsProvider(ContainerCredentialsProvider.builder().build())
                                    .build();
                            GetSecretValueRequest getSecretValueRequest = GetSecretValueRequest.builder()
                                    .secretId(secretName)
                                    .build();
                            GetSecretValueResponse getSecretValueResponse = client.getSecretValue(getSecretValueRequest);
                            String rawJson = getSecretValueResponse.secretString();
                            secretMap.putAll(new ObjectMapper().readValue(rawJson, HashMap.class));
                            logger.info("Found [" + secretMap.keySet().size() + "] secrets");
                            for (Map.Entry<String, String> entrySet : secretMap.entrySet()) {
                                props.setProperty(entrySet.getKey(), entrySet.getValue());
                                System.setProperty(entrySet.getKey(), entrySet.getValue());
                            }
                            client.close();
                        } catch (Exception e) {
                            try {
                                logger.warn("Could not connect with the ContainerCredentialsProvider! [" + e.getMessage() + "]");
                                SecretsManagerClient client = SecretsManagerClient.builder()
                                        .region(region)
                                        .credentialsProvider(ProfileCredentialsProvider.create(PROFILE))
                                        .build();
                                GetSecretValueRequest getSecretValueRequest = GetSecretValueRequest.builder()
                                        .secretId(secretName)
                                        .build();
                                GetSecretValueResponse getSecretValueResponse = client.getSecretValue(getSecretValueRequest);
                                String rawJson = getSecretValueResponse.secretString();
                                secretMap.putAll(new ObjectMapper().readValue(rawJson, HashMap.class));
                                logger.info("Found [" + secretMap.keySet().size() + "] secrets");
                                for (Map.Entry<String, String> entrySet : secretMap.entrySet()) {
                                    props.setProperty(entrySet.getKey(), entrySet.getValue());
                                    System.setProperty(entrySet.getKey().replace("\\.", "_"), entrySet.getValue());
                                }
                                client.close();
                            } catch (Exception e2) {
                                logger.warn("Could not connect with the ProfileCredentialsProvider! [" + e2.getMessage() + "]");
                            }
                        }
                    }
                } catch (Exception e) {
                    logger.error("Could not pull [" + secretName + "] from [" + region.toString() + "]! [" + e.getMessage() + "]", e);
                    throw e;
                }
            }
            for (SecretsManagerListener listener : listenerMap.values()) {
                listener.updateSecrets(secretMap);
            }
        }
    }

    /**
     *
     * @param secretName
     * @param secretValue
     */
    public static void putSecrets(String secretName, String secretValue) throws Exception {
        try {
            logger.info("Updating secrets for [" + secretName + "] from [" + region.toString() + "]");
            // Create a Secrets Manager client
            Boolean success = false;
            try {
                SecretsManagerClient client = SecretsManagerClient.builder()
                        .region(region)
                        .build();
                UpdateSecretRequest secretRequest = UpdateSecretRequest.builder()
                        .secretId(secretName)
                        .secretString(secretValue)
                        .build();

                client.updateSecret(secretRequest);
                success = true;
                client.close();
            } catch (Exception e) {
                logger.warn("Could not update secrets with default credential handling");
            }
            if (!success) {
                try {
                    SecretsManagerClient client = SecretsManagerClient.builder()
                            .region(region)
                            .credentialsProvider(ContainerCredentialsProvider.builder().build())
                            .build();
                    UpdateSecretRequest secretRequest = UpdateSecretRequest.builder()
                            .secretId(secretName)
                            .secretString(secretValue)
                            .build();

                    client.updateSecret(secretRequest);
                    client.close();
                } catch (Exception e) {
                    try {
                        logger.warn("Could not connect with the ContainerCredentialsProvider! [" + e.getMessage() + "]");
                        SecretsManagerClient client = SecretsManagerClient.builder()
                                .region(region)
                                .credentialsProvider(ProfileCredentialsProvider.create(PROFILE))
                                .build();
                        UpdateSecretRequest secretRequest = UpdateSecretRequest.builder()
                                .secretId(secretName)
                                .secretString(secretValue)
                                .build();

                        client.updateSecret(secretRequest);
                        client.close();
                    } catch (Exception e2) {
                        logger.warn("Could not connect with the ProfileCredentialsProvider! [" + e2.getMessage() + "]");
                    }
                }
            }
        } catch (Exception e) {
            logger.error("Could not update [" + secretName + "] from [" + region.toString() + "]! [" + e.getMessage() + "]", e);
            throw e;
        }
    }

    /**
     * @return the props
     */
    public static Properties getProps() {
        return props;
    }

    /**
     * @return the secretMap
     */
    public static Map<String, String> getSecretMap() {
        Map<String, String> deepCopy = new HashMap<>();
        synchronized (LOCK) {
            deepCopy = SerializationUtils.clone((HashMap<String, String>) secretMap);
        }
        return deepCopy;
    }
}
