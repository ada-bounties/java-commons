package com.swiftcryptollc.commons.adabounties.aws.sqs;

import com.swiftcryptollc.commons.adabounties.aws.sqs.data.SqsData;
import com.swiftcryptollc.commons.adabounties.utilities.handler.ObjectHandler;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
public abstract class SqsListener extends ObjectHandler<SqsData> {

    public SqsListener() {
        super();
    }
}
