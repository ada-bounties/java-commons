package com.swiftcryptollc.commons.adabounties.aws.sqs.data;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.swiftcryptollc.commons.adabounties.utilities.data.Base64Id;
import java.text.DateFormat;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
public class SqsData {

    protected boolean isFifo = true;
    protected String id;
    protected Object object;
    protected static Gson gson = new GsonBuilder()
            .setDateFormat(DateFormat.FULL, DateFormat.FULL)
            .serializeSpecialFloatingPointValues()
            .create();
    // Always Upper Case
    protected String type;
    protected Boolean sendToAll = Boolean.FALSE;

    /**
     *
     */
    public SqsData() {
    }

    /**
     *
     * @param object
     */
    public SqsData(Object object) {
        this.object = object;
        if (object != null) {
            setType(object.getClass().getSimpleName());
        } else {
            setType("NULL");
        }
        this.id = Base64Id.getNewId();
    }

    /**
     *
     * @return
     */
    public String toJson() {
        return gson.toJson(this);
    }

    /**
     * Get the Object from the JSON
     *
     * @param dataString
     * @return
     */
    public static SqsData fromJson(String dataString) {
        return gson.fromJson(dataString, SqsData.class);
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the object
     */
    public Object getObject() {
        return object;
    }

    /**
     *
     * @param c
     * @return
     */
    public Object getObject(Class c) {
        Object obj = gson.fromJson(gson.toJsonTree(object).getAsJsonObject(), c);
        return obj;
    }

    /**
     * @param object the object to set
     */
    public void setObject(Object object) {
        this.object = object;
        if (object != null) {
            setType(object.getClass().getSimpleName());
        } else {
            setType("NULL");
        }
    }

    /**
     * @param type the type to set
     */
    public final void setType(String type) {
        this.type = type.toUpperCase();
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     *
     * @param queueName
     * @return
     */
    private String processQueueName(String queueName) {
        if (isFifo) {
            if (!queueName.endsWith(".fifo")) {
                queueName = queueName.concat(".fifo");
            }
        }
        return queueName;
    }

    /**
     * @return the isFifo
     */
    public boolean isIsFifo() {
        return isFifo;
    }

    /**
     * @param isFifo the isFifo to set
     */
    public void setIsFifo(boolean isFifo) {
        this.isFifo = isFifo;
    }

    /**
     * @return the sendToAll
     */
    public Boolean getSendToAll() {
        return sendToAll;
    }

    /**
     * @param sendToAll the sendToAll to set
     */
    public void setSendToAll(Boolean sendToAll) {
        this.sendToAll = sendToAll;
    }
}
