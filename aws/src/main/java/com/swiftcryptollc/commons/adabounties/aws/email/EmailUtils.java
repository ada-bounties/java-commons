package com.swiftcryptollc.commons.adabounties.aws.email;

import com.swiftcryptollc.commons.adabounties.aws.data.CardanoNetwork;
import com.swiftcryptollc.commons.adabounties.aws.email.data.NotificationSummary;
import com.swiftcryptollc.commons.adabounties.utilities.data.ActionType;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import software.amazon.awssdk.auth.credentials.ContainerCredentialsProvider;
import software.amazon.awssdk.auth.credentials.ProfileCredentialsProvider;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.ses.SesClient;
import software.amazon.awssdk.services.ses.model.Body;
import software.amazon.awssdk.services.ses.model.Content;
import software.amazon.awssdk.services.ses.model.Destination;
import software.amazon.awssdk.services.ses.model.Message;
import software.amazon.awssdk.services.ses.model.SendEmailRequest;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
public class EmailUtils {

    protected static SesClient client = null;
    private final static String NETWORK;
    private final static String PROFILE = "adabounties-email";
    protected final static String MAINNET_URL = "https://adabounties.io";
    protected final static String PREVIEW_URL = "https://preview.adabounties.io";
    protected static String URL;
    protected final static String SENDER_ADDRESS = "\"ADA Bounties\" <info@adabounties.io>";
    protected final static String SENDER_NAME = "ADA Bounties";
    protected final static String ACCOUNT_VERIFICATION_SUBJECT = "Account verification";
    protected final static String NEW_CLAIM_SUBJECT = "New claim received";
    protected final static String CLAIM_UPDATE_SUBJECT = "Claim update";
    protected final static Logger logger = LoggerFactory.getLogger(EmailUtils.class);

    static {
        NETWORK = System.getenv("cardano_network");
        if (CardanoNetwork.MAINNET.label.equalsIgnoreCase(NETWORK)) {
            URL = MAINNET_URL;
        } else {
            URL = PREVIEW_URL;
        }
        try {
            client = SesClient.builder()
                    .region(Region.US_EAST_2)
                    .build();
            // Forces a connection check
            client.getSendQuota();
            logger.info("Created EmailUtils");
        } catch (Exception e) {
            client = null;
            logger.error("Could not create the SES Client with the default credentials");
        }
        if (client == null) {
            try {
                client = SesClient.builder()
                        .region(Region.US_EAST_2)
                        .credentialsProvider(ContainerCredentialsProvider.builder().build())
                        .build();
                // Forces a connection check
                client.getSendQuota();
                logger.info("Created EmailUtils");
            } catch (Exception e) {
                client = null;
                logger.error("Could not create the SES Client with the Container Credentials");
                try {
                    client = SesClient.builder()
                            .region(Region.US_EAST_2)
                            .credentialsProvider(ProfileCredentialsProvider.create(PROFILE))
                            .build();
                    // Forces a connection check
                    client.getSendQuota();
                    logger.info("Created EmailUtils");
                } catch (Exception e2) {
                    logger.error("Could not create the SES Client with the Profile Credentials");
                }
            }
        }
    }

    public EmailUtils() {
    }

    private static String HEADER = "<!DOCTYPE html>"
            + "<html lang=\"en\">"
            + "<head>"
            + "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">"
            + "<!--<base href=\"/\">-->"
            + "<base href=\".\">"
            + "<link rel=\"icon\" href=\"https://adabounties.s3.us-east-2.amazonaws.com/imgs/icon.png\">"
            + "<title>ADA Bounties</title>"
            + "<meta name=\"viewport\""
            + "content=\"width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no\">"
            + "<script>"
            + "document.title = \"ADA Bounties\";"
            + "</script>"
            + "<style>"
            + "p {"
            + "word-break: normal!important;"
            + "white-space: normal!important;"
            + "}"
            + "body {"
            + "width: 100vw!important;"
            + "}"
            + "</style>"
            + "</head>"
            + "<body>"
            + "<div style=\"margin-left: 5vw;margin-right: 5vw;\">"
            + "<div style=\"margin-top: 2em;margin-bottom: 1em\">"
            + "<a href=\"URL\" style=\"text-decoration: none;display: flex!important\">"
            + "<img style=\"display: flex!important\" src=\"https://adabounties.s3.us-east-2.amazonaws.com/imgs/logo-light.png\" height=\"64\""
            + "alt=\"\"></a>"
            + "</div>"
            + "<hr>";
    private static String REG_BODY = "<div style=\"margin-bottom: 2em\">"
            + "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">"
            + "<tbody>"
            + "<tr>"
            + "<td height=\"38\" valign=\"middle\""
            + "style=\"border:0;margin:0;padding:0;\">"
            + "Please verify your email to complete your registration."
            + "</td>"
            + "</tr>"
            + "<tr>"
            + "<td height=\"38\" valign=\"middle\""
            + "style=\"border:0;margin:0;padding:0;\">"
            + "<a href=\"" + URL + "/activation/verify/CHALLENGE\" target=\"_blank\">"
            + "<div><div style=\"padding-top: 10px;padding-bottom: 10px;\">Verify</div></div>"
            + "</a>"
            + "</td>"
            + "</tr>"
            + "<tr>"
            + "<td height=\"38\" valign=\"middle\""
            + "style=\"border:0;margin:0;padding:0;\">"
            + "If you have received this email in error, you can click 'Remove' to have your email address erased from our system."
            + "</td>"
            + "</tr>"
            + "<tr>"
            + "<td height=\"38\" valign=\"middle\""
            + "style=\"border:0;margin:0;padding:0;\">"
            + "<a href=\"" + URL + "/activation/remove/CHALLENGE\" target=\"_blank\">"
            + "<div><div style=\"padding-top: 10px;padding-bottom: 10px;\">Remove</div></div>"
            + "</a>"
            + "</td>"
            + "</tr>"
            + "</tbody>"
            + "</table>";
    private static String REG_SUBJECT = "Account Verification";
    private static String CONTACT_US_BODY = "<div style=\"margin-bottom: 2em\">"
            + "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">"
            + "<tbody>"
            + "<tr>"
            + "<td height=\"38\" valign=\"middle\""
            + "style=\"border:0;margin:0;padding:0;\">"
            + "ID: MSGID"
            + "</td>"
            + "</tr>"
            + "<tr>"
            + "<td height=\"38\" valign=\"middle\""
            + "style=\"border:0;margin:0;padding:0;\">"
            + "Name: USERNAME"
            + "</td>"
            + "</tr>"
            + "<tr>"
            + "<td height=\"38\" valign=\"middle\""
            + "style=\"border:0;margin:0;padding:0;\">"
            + "Subject: USERSUBJECT"
            + "</td>"
            + "</tr>"
            + "<tr>"
            + "<td height=\"38\" valign=\"middle\""
            + "style=\"border:0;margin:0;padding:0;\">"
            + "Email: USEREMAIL"
            + "</td>"
            + "</tr>"
            + "<tr>"
            + "<td height=\"38\" valign=\"middle\""
            + "style=\"border:0;margin:0;padding:0;\">"
            + "Message: USERMESSAGE"
            + "</td>"
            + "</tr>"
            + "</tbody>"
            + "</table>"
            + "</div>";
    private static String CONTACT_US_TO_USER_BODY = "<div style=\"margin-bottom: 2em\">"
            + "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">"
            + "<tbody>"
            + "<tr>"
            + "<td height=\"38\" valign=\"middle\""
            + "style=\"border:0;margin:0;padding:0;\">"
            + "We've received your message and will respond as quickly as possible. You'll find your message details below."
            + "</td>"
            + "</tr>"
            + "</tbody>"
            + "</table>"
            + "</div>"
            + "<div style=\"margin-bottom: 2em\">"
            + "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">"
            + "<tbody>"
            + "<tr>"
            + "<td height=\"38\" valign=\"middle\""
            + "style=\"border:0;margin:0;padding:0;\">"
            + "ID: MSGID"
            + "</td>"
            + "</tr>"
            + "<tr>"
            + "<td height=\"38\" valign=\"middle\""
            + "style=\"border:0;margin:0;padding:0;\">"
            + "Name: USERNAME"
            + "</td>"
            + "</tr>"
            + "<tr>"
            + "<td height=\"38\" valign=\"middle\""
            + "style=\"border:0;margin:0;padding:0;\">"
            + "Subject: USERSUBJECT"
            + "</td>"
            + "</tr>"
            + "<tr>"
            + "<td height=\"38\" valign=\"middle\""
            + "style=\"border:0;margin:0;padding:0;\">"
            + "Email: USEREMAIL"
            + "</td>"
            + "</tr>"
            + "<tr>"
            + "<td height=\"38\" valign=\"middle\""
            + "style=\"border:0;margin:0;padding:0;\">"
            + "Message: USERMESSAGE"
            + "</td>"
            + "</tr>"
            + "</tbody>"
            + "</table>"
            + "</div>";
    private static String NEW_CLAIM = "<div style=\"margin-bottom: 2em\">"
            + "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">"
            + "<tbody>"
            + "<tr>"
            + "<td height=\"38\" valign=\"middle\""
            + "style=\"border:0;margin:0;padding:0;\">"
            + "You have received a <a href=\"URL/claims/view/CLAIMID\" style=\"color: #1e79f7\">new claim</a> for your bounty titled \"TITLE\"."
            + "</td>"
            + "</tr>"
            + "</tbody>"
            + "</table>"
            + "</div>";
    private static String CLAIM_UPDATE = "<div style=\"margin-bottom: 2em\">"
            + "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">"
            + "<tbody>"
            + "<tr>"
            + "<td height=\"38\" valign=\"middle\""
            + "style=\"border:0;margin:0;padding:0;\">"
            + "Your <a href=\"URL/claims/view/CLAIMID\" style=\"color: #1e79f7\">claim</a> for bounty \"TITLE\" has been STATUS."
            + "</td>"
            + "</tr>"
            + "</tbody>"
            + "</table>"
            + "</div>";
    private static String SUM_NOTIFICATION_OPEN = "<div style=\"margin-bottom: 2em\">"
            + "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">"
            + "<tbody>"
            + "<tr>"
            + "<td height=\"38\" valign=\"middle\""
            + "style=\"border:0;margin:0;padding:0;\">"
            + "This is what you may have missed over the last 24 hours:"
            + "</td>"
            + "</tr>"
            + "</tbody>"
            + "</table>"
            + "</div>"
            + "<div style=\"margin-bottom: 2em\">"
            + "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">"
            + "<tbody>";
    private static String SUM_ROW = "<tr>"
            + "<td height=\"38\" valign=\"middle\""
            + "style=\"border:0;margin:0;padding:0;\">"
            + "MSG"
            + "</td>"
            + "</tr>";
    private static String SUM_NOTIFICATION_CLOSE = "<tr><td><br></td><td><br></td></tr>"
            + "<tr><td><a href=\"URL\" style=\"color: #1e79f7\">Login</a> to check your notifications!</td></tr>"
            + "</tbody>"
            + "</table>"
            + "</div>";
    private static String FOOTER = "<div style=\"margin-top: 2em;\">"
            + "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">"
            + "<tbody>"
            + "<tr>"
            + "<td height=\"38\" valign=\"middle\""
            + "style=\"border:0;margin:0;padding:0;\">"
            + "V/r,"
            + "</td>"
            + "</tr>"
            + "<tr>"
            + "<td height=\"38\" valign=\"middle\""
            + "style=\"border:0;margin:0;padding:0;\">"
            + "ADA Bounties"
            + "</td>"
            + "</tr>"
            + "</tbody>"
            + "</table>"
            + "</div>"
            + "<div style=\"margin-top: 2em;\">"
            + "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">"
            + "<tbody>"
            + "<tr>"
            + "<td height=\"38\" valign=\"middle\""
            + "style=\"border:0;margin:0;padding:0;\">"
            + "Need additional help? Email us at&nbsp;<a href=\"mailto:info@adabounties.io\""
            + "style=\"color: #1e79f7\">info@adabounties.io</a></td>"
            + "</tr>"
            + "</tbody>"
            + "</table>"
            + "</div>"
            + "<div style=\"margin-top: 1em;\">"
            + "<hr>"
            + "<div style=\"margin-top: 2em;\">"
            + "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">"
            + "<tbody>"
            + "<tr>"
            + "<td align=\"center\" height=\"28\" valign=\"middle\""
            + "style=\"border:0;margin:0;padding:0;text-align:center\">"
            + "Connect with Us"
            + "</td>"
            + "</tr>"
            + "<tr>"
            + "<td align=\"center\" height=\"28\" valign=\"middle\""
            + "style=\"border:0;margin:0;padding:0;text-align:center\">"
            + "<a href=\"https://www.linkedin.com/company/swift-crypto-llc/\" target=\"_blank\""
            + "style=\"color: #1e79f7;margin-left: .5em;\">LinkedIn</a>"
            + "<a href=\"https://twitter.com/adabounties\" target=\"_blank\""
            + "style=\"color: #1e79f7;margin-left: .5em;\">Twitter</a>"
            + "<a href=\"https://discord.gg/rHcETUhkjq\" target=\"_blank\""
            + "style=\"color: #1e79f7;margin-left: .5em;\">Discord</a>"
            + "<a href=\"https://t.me/+NLP2pl59bvE4ODQx\" target=\"_blank\""
            + "style=\"color: #1e79f7;margin-left: .5em;\">Telegram</a>"
            + "</td>"
            + "</tr>"
            + "<tr>"
            + "<td align=\"center\" height=\"38\" valign=\"middle\""
            + "style=\"border:0;margin:0;padding:0;text-align:center;font-size:xx-small;\">"
            + "If you no longer wish to receive these emails, click <a href=\"" + URL + "/unsubscribe/CHALLENGE\" target=\"_blank\">"
            + "unsubscribe</a>.</td>"
            + "</tr>"
            + "<tr>"
            + "<td align=\"center\" height=\"38\" valign=\"middle\""
            + "style=\"border:0;margin:0;padding:0;text-align:center\">"
            + "&copy;2023 Swift Crypto LLC. All rights reserved."
            + "</td>"
            + "</tr>"
            + "</tbody>"
            + "</table>"
            + "</div>"
            + "</div>"
            + "</div>"
            + "</body>"
            + "</html>";

    /**
     *
     * @param notificationSummary
     * @return
     */
    public boolean sendSummaryNotification(NotificationSummary notificationSummary, String challenge) {
        boolean success = false;
        try {
            Map<String, Long> statusMap = notificationSummary.getStatusCountMap();
            if (!statusMap.isEmpty()) {
                StringBuilder bodyBuilder = new StringBuilder(HEADER);
                bodyBuilder.append(SUM_NOTIFICATION_OPEN.replace("URL", URL));

                // Process Bookmarks
                if ((statusMap.containsKey(ActionType.BOOKMARKED_BOUNTY_COMMENT.label))
                        || (statusMap.containsKey(ActionType.BOOKMARKED_BOUNTY_COMMENT_UPDATE.label))) {
                    Long numComments = statusMap.get(ActionType.BOOKMARKED_BOUNTY_COMMENT.label);
                    Long numCommentUpdates = statusMap.get(ActionType.BOOKMARKED_BOUNTY_COMMENT_UPDATE.label);
                    Long total = numComments != null ? numComments : 0L;
                    total += numCommentUpdates != null ? numCommentUpdates : 0L;
                    if (total == 1) {
                        bodyBuilder.append(SUM_ROW.replace("MSG", "One new or updated bookmarked bounty comment."));
                    } else if (total > 1) {
                        bodyBuilder.append(SUM_ROW.replace("MSG", total.toString().concat(" new or updated bookmarked bounty comments.")));
                    }
                }
                if (statusMap.containsKey(ActionType.BOOKMARKED_BOUNTY_UPDATE.label)) {
                    Long numUpdates = statusMap.get(ActionType.BOOKMARKED_BOUNTY_UPDATE.label);
                    if (numUpdates == 1) {
                        bodyBuilder.append(SUM_ROW.replace("MSG", "One bookmarked bounty update."));
                    } else if (numUpdates > 1) {
                        bodyBuilder.append(SUM_ROW.replace("MSG", numUpdates.toString().concat(" bookmarked bounty updates.")));
                    }
                }

                if (statusMap.containsKey(ActionType.BOOKMARKED_BOUNTY_CLOSED.label)) {
                    Long numClosed = statusMap.get(ActionType.BOOKMARKED_BOUNTY_CLOSED.label);
                    if (numClosed == 1) {
                        bodyBuilder.append(SUM_ROW.replace("MSG", "One bookmarked bounty was closed."));
                    } else if (numClosed > 1) {
                        bodyBuilder.append(SUM_ROW.replace("MSG", numClosed.toString().concat(" bookmarked bounties have been closed.")));
                    }
                }
                if (statusMap.containsKey(ActionType.BOOKMARKED_BOUNTY_CLAIMED.label)) {
                    Long numClaimed = statusMap.get(ActionType.BOOKMARKED_BOUNTY_CLAIMED.label);
                    if (numClaimed == 1) {
                        bodyBuilder.append(SUM_ROW.replace("MSG", "One bookmarked bounty has been claimed."));
                    } else if (numClaimed > 1) {
                        bodyBuilder.append(SUM_ROW.replace("MSG", numClaimed.toString().concat(" bookmarked bounties have been claimed.")));
                    }
                }
                // Process any bounty type updates
                if ((statusMap.containsKey(ActionType.BOUNTY_COMMENT.label))
                        || (statusMap.containsKey(ActionType.BOUNTY_COMMENT_UPDATE.label))) {
                    Long numComments = statusMap.get(ActionType.BOUNTY_COMMENT.label);
                    Long numCommentUpdates = statusMap.get(ActionType.BOUNTY_COMMENT_UPDATE.label);
                    Long total = numComments != null ? numComments : 0L;
                    total += numCommentUpdates != null ? numCommentUpdates : 0L;
                    if (total == 1) {
                        bodyBuilder.append(SUM_ROW.replace("MSG", "One new or updated comment on one of your bounties."));
                    } else if (total > 1) {
                        bodyBuilder.append(SUM_ROW.replace("MSG", total.toString().concat(" new or updated comments on your bounties.")));
                    }
                }

                if (statusMap.containsKey(ActionType.BOUNTY_COMMENT_REPLY.label)) {
                    Long numUpdates = statusMap.get(ActionType.BOUNTY_COMMENT_REPLY.label);
                    if (numUpdates == 1) {
                        bodyBuilder.append(SUM_ROW.replace("MSG", "One reply to one of your bounty comments."));
                    } else if (numUpdates > 1) {
                        bodyBuilder.append(SUM_ROW.replace("MSG", numUpdates.toString().concat(" replies to your bounty comments.")));
                    }
                }

                // Process any claim type updates
                if (statusMap.containsKey(ActionType.CLAIM_NEW.label)) {
                    Long numUpdates = statusMap.get(ActionType.CLAIM_NEW.label);
                    if (numUpdates == 1) {
                        bodyBuilder.append(SUM_ROW.replace("MSG", "One new claim on one of your bounties."));
                    } else if (numUpdates > 1) {
                        bodyBuilder.append(SUM_ROW.replace("MSG", numUpdates.toString().concat(" new claims on your bounties.")));
                    }
                }

                if (statusMap.containsKey(ActionType.CLAIM_UPDATE.label)) {
                    Long numUpdates = statusMap.get(ActionType.CLAIM_UPDATE.label);
                    if (numUpdates == 1) {
                        bodyBuilder.append(SUM_ROW.replace("MSG", "One updated claim on one of your bounties."));
                    } else if (numUpdates > 1) {
                        bodyBuilder.append(SUM_ROW.replace("MSG", numUpdates.toString().concat(" updated claims on your bounties.")));
                    }
                }

                if ((statusMap.containsKey(ActionType.CLAIM_COMMENT.label))
                        || (statusMap.containsKey(ActionType.CLAIM_COMMENT_UPDATE.label))) {
                    Long numComments = statusMap.get(ActionType.CLAIM_COMMENT.label);
                    Long numCommentUpdates = statusMap.get(ActionType.CLAIM_COMMENT_UPDATE.label);
                    Long total = numComments != null ? numComments : 0L;
                    total += numCommentUpdates != null ? numCommentUpdates : 0L;
                    if (total == 1) {
                        bodyBuilder.append(SUM_ROW.replace("MSG", "One new or updated comment on one of your claims."));
                    } else if (total > 1) {
                        bodyBuilder.append(SUM_ROW.replace("MSG", total.toString().concat(" new or updated comments on your claims.")));
                    }
                }

                if (statusMap.containsKey(ActionType.CLAIM_COMMENT_REPLY.label)) {
                    Long numUpdates = statusMap.get(ActionType.CLAIM_COMMENT_REPLY.label);
                    if (numUpdates == 1) {
                        bodyBuilder.append(SUM_ROW.replace("MSG", "One reply to one of your claim comments."));
                    } else if (numUpdates > 1) {
                        bodyBuilder.append(SUM_ROW.replace("MSG", numUpdates.toString().concat(" replies to your claim comments.")));
                    }
                }
                if (statusMap.containsKey(ActionType.CLAIM_ACCEPT.label)) {
                    Long numUpdates = statusMap.get(ActionType.CLAIM_ACCEPT.label);
                    if (numUpdates == 1) {
                        bodyBuilder.append(SUM_ROW.replace("MSG", "One of your claims has been accepted!"));
                    } else if (numUpdates > 1) {
                        bodyBuilder.append(SUM_ROW.replace("MSG", numUpdates.toString().concat(" of your claims have been accepted!")));
                    }
                }
                if (statusMap.containsKey(ActionType.CLAIM_REJECT.label)) {
                    Long numUpdates = statusMap.get(ActionType.CLAIM_REJECT.label);
                    if (numUpdates == 1) {
                        bodyBuilder.append(SUM_ROW.replace("MSG", "One of your claims was rejected."));
                    } else if (numUpdates > 1) {
                        bodyBuilder.append(SUM_ROW.replace("MSG", numUpdates.toString().concat(" of your claims were rejected.")));
                    }
                }
                if (statusMap.containsKey(ActionType.CLAIM_CLOSED.label)) {
                    Long numUpdates = statusMap.get(ActionType.CLAIM_CLOSED.label);
                    if (numUpdates == 1) {
                        bodyBuilder.append(SUM_ROW.replace("MSG", "One of you claims was closed."));
                    } else if (numUpdates > 1) {
                        bodyBuilder.append(SUM_ROW.replace("MSG", numUpdates.toString().concat(" or more of your claims were closed.")));
                    }
                }

                bodyBuilder.append(SUM_NOTIFICATION_CLOSE.replace("URL", URL));
                bodyBuilder.append(FOOTER.replace("URL", URL).replace("CHALLENGE", challenge));
                String newSubject = "Notification updates";
                sendEmail(notificationSummary.getEmail(), null, newSubject, bodyBuilder.toString());
            } else {
                logger.warn("Not sending empty summary email");
            }
            success = true;
        } catch (Exception ex) {
            logger.error("Could not send a summary notification email [" + ex.getMessage() + "]", ex);
        }

        return success;
    }

    /**
     *
     * @param email
     * @param name
     * @param subject
     * @param msg
     * @param msgId
     * @return
     */
    public boolean sendContactUs(String email, String name, String subject, String msg, String msgId, String challenge) {
        boolean success = false;
        try {
            if (!email.equalsIgnoreCase(SENDER_ADDRESS)) {
                StringBuilder bodyBuilder = new StringBuilder(HEADER);
                String body = CONTACT_US_TO_USER_BODY.replace("USEREMAIL", email);
                body = body.replace("USERNAME", name);
                body = body.replace("USERSUBJECT", subject);
                body = body.replace("USERMESSAGE", msg);
                body = body.replace("MSGID", msgId);
                bodyBuilder.append(body);
                bodyBuilder.append(FOOTER.replace("URL", URL).replace("CHALLENGE", challenge));
                String newSubject = "RE: " + subject;
                sendEmail(email, null, newSubject, bodyBuilder.toString());

                body = CONTACT_US_BODY.replace("USEREMAIL", email);
                body = body.replace("USERNAME", name);
                body = body.replace("USERSUBJECT", subject);
                body = body.replace("USERMESSAGE", msg);
                body = body.replace("MSGID", msgId);
                newSubject = "CONTACT US: " + subject;
                sendEmail(SENDER_ADDRESS, null, newSubject, body);

                success = true;
            }
        } catch (Exception ex) {
            logger.error("Could not send a contact us email with id [" + msgId + "] [" + ex.getMessage() + "]", ex);
        }

        return success;
    }

    /**
     *
     * @param email
     * @param challenge
     * @return
     */
    public boolean sendAccountVerificationEmail(String email, String challenge) {
        logger.info("Sending verification email to [" + email + "] Challenge [" + challenge + "]");
        boolean success = false;
        try {
            StringBuilder bodyBuilder = new StringBuilder(HEADER);
            bodyBuilder.append(REG_BODY.replace("CHALLENGE", challenge));
            bodyBuilder.append(FOOTER.replace("URL", URL).replace("CHALLENGE", challenge));
            sendEmail(email, null, REG_SUBJECT, bodyBuilder.toString());
            success = true;
        } catch (Exception ex) {
            logger.error("Exception occured during send! [" + ex.getMessage() + "]", ex);
        }
        return success;
    }

    /**
     *
     * @param email
     * @param claimId
     * @param btyTitle
     * @param challenge
     * @return
     */
    public boolean sendNewClaimEmail(String email, String claimId, String btyTitle, String challenge) {
        logger.info("Sending new claim email to [" + email + "] Bounty title: [" + btyTitle + "]");
        boolean success = false;
        try {
            StringBuilder bodyBuilder = new StringBuilder(HEADER);
            bodyBuilder.append(NEW_CLAIM.replace("URL", URL).replace("CLAIMID", claimId).replace("TITLE", btyTitle));
            bodyBuilder.append(FOOTER.replace("URL", URL).replace("CHALLENGE", challenge));
            sendEmail(email, null, NEW_CLAIM_SUBJECT, bodyBuilder.toString());
            success = true;
        } catch (Exception ex) {
            logger.error("Exception occured during send! [" + ex.getMessage() + "]", ex);
        }
        return success;
    }

    /**
     *
     * @param email
     * @param claimId
     * @param status
     * @param btyTitle
     * @param challenge
     * @return
     */
    public boolean sendClaimUpdateEmail(String email, String claimId, String status, String btyTitle, String challenge) {
        logger.info("Sending new claim email to [" + email + "] Bounty title: [" + btyTitle + "]");
        boolean success = false;
        try {
            StringBuilder bodyBuilder = new StringBuilder(HEADER);
            bodyBuilder.append(CLAIM_UPDATE.replace("URL", URL)
                    .replace("CLAIMID", claimId)
                    .replace("STATUS", status.toLowerCase())
                    .replace("TITLE", btyTitle));
            bodyBuilder.append(FOOTER.replace("URL", URL).replace("CHALLENGE", challenge));
            sendEmail(email, null, CLAIM_UPDATE_SUBJECT, bodyBuilder.toString());
            success = true;
        } catch (Exception ex) {
            logger.error("Exception occured during send! [" + ex.getMessage() + "]", ex);
        }
        return success;
    }

    /**
     *
     * @param recipient
     * @param sendToName
     * @param subject
     * @param bodyHTML
     * @throws Exception
     */
    protected void sendEmail(String recipient, String sendToName, String subject, String bodyHTML)
            throws Exception {

        Destination destination = Destination.builder()
                .toAddresses(recipient)
                .build();

        Content content = Content.builder()
                .data(bodyHTML)
                .build();

        Content sub = Content.builder()
                .data(subject)
                .build();

        Body body = Body.builder()
                .html(content)
                .build();

        Message msg = Message.builder()
                .subject(sub)
                .body(body)
                .build();

        SendEmailRequest emailRequest = SendEmailRequest.builder()
                .destination(destination)
                .message(msg)
                .source(SENDER_ADDRESS)
                .build();
        client.sendEmail(emailRequest);
        logger.info(bodyHTML);
    }
}
