package com.swiftcryptollc.commons.adabounties.aws.data;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
public enum CardanoNetwork {
    MAINNET("mainnet"),
    PREVIEW("preview");

    public final String label;

    private CardanoNetwork(String label) {
        this.label = label;
    }

    public static CardanoNetwork valueOfLabel(String label) {
        for (CardanoNetwork type : values()) {
            if (type.label.equalsIgnoreCase(label)) {
                return type;
            }
        }
        return null;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return label;
    }

}
