package com.swiftcryptollc.commons.adabounties.aws.secrets;

import java.util.Map;
import java.util.UUID;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
public interface SecretsManagerListener {

    public UUID getId();
    
    public void updateSecrets(Map<String, String> secretMap);
}
