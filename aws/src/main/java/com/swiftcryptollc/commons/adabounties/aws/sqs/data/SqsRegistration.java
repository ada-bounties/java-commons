package com.swiftcryptollc.commons.adabounties.aws.sqs.data;

import java.io.Serializable;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
public class SqsRegistration implements Serializable {

    private static final long serialVersionUID = 100099L;

    protected String queueUrl;
    protected Boolean isDelete = false;

    public SqsRegistration() {

    }

    /**
     *
     * @param queueUrl
     */
    public SqsRegistration(String queueUrl) {
        this.queueUrl = queueUrl;
    }

    /**
     *
     * @param queueUrl
     * @param isDelete
     */
    public SqsRegistration(String queueUrl, Boolean isDelete) {
        this.queueUrl = queueUrl;
        this.isDelete = isDelete;
    }

    /**
     * @return the queueUrl
     */
    public String getQueueUrl() {
        return queueUrl;
    }

    /**
     * @param queueUrl the queueUrl to set
     */
    public void setQueueUrl(String queueUrl) {
        this.queueUrl = queueUrl;
    }

    /**
     * @return the isDelete
     */
    public Boolean getIsDelete() {
        return isDelete;
    }

    /**
     * @param isDelete the isDelete to set
     */
    public void setIsDelete(Boolean isDelete) {
        this.isDelete = isDelete;
    }
}
