package com.swiftcryptollc.commons.adabounties.aws.email.data;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
public class NotificationSummary {

    protected Long numNotifications = 0L;
    protected String email;
    protected Map<String, Long> statusCountMap;

    public NotificationSummary() {

    }

    /**
     * @return the numNotifications
     */
    public Long getNumNotifications() {
        return numNotifications;
    }

    /**
     * @param numNotifications the numNotifications to set
     */
    public void setNumNotifications(Long numNotifications) {
        this.numNotifications = numNotifications;
    }

    /**
     *
     * @param status
     * @param count
     */
    public void addToStatusMap(String status, Long count) {
        if (statusCountMap == null) {
            statusCountMap = new HashMap<>();
        }
        statusCountMap.put(status, count);
    }

    /**
     * @return the statusCountMap
     */
    public Map<String, Long> getStatusCountMap() {
        if (statusCountMap == null) {
            statusCountMap = new HashMap<>();
        }
        return statusCountMap;
    }

    /**
     * @param statusCountMap the statusCountMap to set
     */
    public void setStatusCountMap(Map<String, Long> statusCountMap) {
        this.statusCountMap = statusCountMap;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }
}
