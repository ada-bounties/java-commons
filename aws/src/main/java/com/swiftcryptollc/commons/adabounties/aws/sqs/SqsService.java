package com.swiftcryptollc.commons.adabounties.aws.sqs;

import com.swiftcryptollc.commons.adabounties.aws.data.CardanoNetwork;
import com.swiftcryptollc.commons.adabounties.aws.sqs.data.SqsData;
import com.swiftcryptollc.commons.adabounties.aws.sqs.data.SqsRegistration;
import com.swiftcryptollc.commons.adabounties.utilities.system.HostnameUtility;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import software.amazon.awssdk.auth.credentials.ContainerCredentialsProvider;
import software.amazon.awssdk.auth.credentials.ProfileCredentialsProvider;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.sqs.SqsClient;
import software.amazon.awssdk.services.sqs.model.CreateQueueRequest;
import software.amazon.awssdk.services.sqs.model.DeleteQueueRequest;
import software.amazon.awssdk.services.sqs.model.GetQueueUrlRequest;
import software.amazon.awssdk.services.sqs.model.GetQueueUrlResponse;
import software.amazon.awssdk.services.sqs.model.ListQueuesResponse;
import software.amazon.awssdk.services.sqs.model.QueueAttributeName;
import software.amazon.awssdk.services.sqs.model.SendMessageRequest;
import software.amazon.awssdk.services.sqs.model.SqsException;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
public class SqsService {

    private static String network;
    protected final static String PREVIEW_SERVER_QUEUE_NAME = "ADABOUNTIES-SERVERS-preview.fifo";
    protected final static String SERVER_QUEUE_NAME = "ADABOUNTIES-SERVERS-mainnet.fifo";
    private static String QUEUE_NAME;
    protected static String serverQueueUrl;
    protected static String myQueueName;
    private static String myQueueUrl;
    private static Set<String> queueUrls = new HashSet<>();
    protected static SqsReceiver receiver;
    // subscribes to the SERVER queue for messages from the Lambda processes
    protected static SqsReceiver lambdaReceiver;
    protected static SqsSender sender;
    protected static SqsClient sqsClient;
    protected static Boolean isLambda = false;
    private static Region region;
    private final static Region PREVIEW_REGION = Region.of("us-east-2");
    private final static Region MAINNET_REGION = Region.of("us-east-1");
    private final static String PREVIEW_PROFILE = "adabounties-preview";
    private final static String MAINNET_PROFILE = "adabounties";
    private static String PROFILE;
    private static Logger logger = LoggerFactory.getLogger(SqsService.class);

    /**
     *
     */
    public SqsService() {
        if (myQueueName == null) {
            init();
            logger.info("Creating My Queue [" + myQueueName + "]");
            sender = new SqsSender(this, createClient(), isLambda, myQueueName);
            receiver = createReceiver();
            createServerQueueIfNotExists();
            sendNotice();
        }
    }

    /**
     *
     * @param isLambda
     */
    public SqsService(Boolean isLambda) {
        if (myQueueName == null) {
            this.isLambda = isLambda;
            init();
            logger.info("Creating My Queue [" + myQueueName + "]");
            sender = new SqsSender(this, createClient(), isLambda, myQueueName);
            if (!isLambda) {
                receiver = createReceiver();
                createServerQueueIfNotExists();
                sendNotice();
            }
        }
    }

    /**
     *
     */
    private final static void init() {
        network = System.getenv("cardano_network");
        logger.info("Using network [" + network + "]");
        if (CardanoNetwork.MAINNET.label.equalsIgnoreCase(network)) {
            QUEUE_NAME = SERVER_QUEUE_NAME;
            PROFILE = MAINNET_PROFILE;
            region = MAINNET_REGION;
        } else {
            QUEUE_NAME = PREVIEW_SERVER_QUEUE_NAME;
            PROFILE = PREVIEW_PROFILE;
            region = PREVIEW_REGION;
        }

        myQueueName = HostnameUtility.getHostname().replace("-", "").replace(".", "").concat("-").concat(network).concat(".fifo");
    }

    /**
     *
     * @return
     */
    private synchronized SqsClient createClient() {
        if (sqsClient == null) {
            if (isLambda) {
                try {
                    sqsClient = SqsClient.builder()
                            .region(region)
                            .build();
                    ListQueuesResponse queueResponse = sqsClient.listQueues();
                    setQueueUrls(queueResponse);
                } catch (Exception e) {
                    logger.warn("Could not connect to SQS with default credential providers [" + e.getMessage() + "]");
                }
            } else {
                try {
                    sqsClient = SqsClient.builder()
                            .region(region)
                            .credentialsProvider(ContainerCredentialsProvider.builder().build())
                            .build();
                    ListQueuesResponse queueResponse = sqsClient.listQueues();
                    queueUrls = new HashSet(queueResponse.queueUrls());
                    logger.info("Found [" + queueUrls.size() + "] Queues");
                } catch (Exception ex) {
                    try {
                        logger.warn("Could not connect with the ContainerCredentialsProvider! [" + ex.getMessage() + "]");
                        sqsClient = SqsClient.builder()
                                .region(region)
                                .credentialsProvider(ProfileCredentialsProvider.create(PROFILE))
                                .build();
                        ListQueuesResponse queueResponse = sqsClient.listQueues();
                        setQueueUrls(queueResponse);
                    } catch (Exception e2) {
                        logger.warn("Could not connect with the ProfileCredentialsProvider! [" + e2.getMessage() + "]");
                    }
                }
            }
            if (myQueueUrl != null) {
                queueUrls.remove(myQueueUrl);
            }
        }
        return sqsClient;
    }

    /**
     * Filter queue URLs by
     *
     * @param queueResponse
     */
    protected void setQueueUrls(ListQueuesResponse queueResponse) {
        queueUrls = new HashSet();
        String urlEnd = network.concat(".fifo");
        logger.info("Looking for queue's that end in [" + urlEnd + "]");
        for (String url : queueResponse.queueUrls()) {
            if (url.endsWith(urlEnd)) {
                queueUrls.add(url);
            }
        }
        logger.info("Found [" + queueUrls.size() + "] Queues");
    }

    /**
     *
     * @return
     */
    protected String createServerQueueIfNotExists() {
        if ((lambdaReceiver == null) || (serverQueueUrl == null)) {
            try {
                SqsClient sqsClient = createClient();
                logger.info("Creating server queue named [" + QUEUE_NAME + "] (if not existing)");
                Map<QueueAttributeName, String> queueAttributes = new HashMap<>();
                queueAttributes.put(QueueAttributeName.FIFO_QUEUE, "true");
                queueAttributes.put(QueueAttributeName.CONTENT_BASED_DEDUPLICATION, "true");
                queueAttributes.put(QueueAttributeName.RECEIVE_MESSAGE_WAIT_TIME_SECONDS, "20");
                CreateQueueRequest createQueueRequest = CreateQueueRequest.builder()
                        .queueName(QUEUE_NAME)
                        .attributes(queueAttributes)
                        .build();
                sqsClient.createQueue(createQueueRequest);
                GetQueueUrlResponse getQueueUrlResponse = sqsClient.getQueueUrl(GetQueueUrlRequest.builder().queueName(QUEUE_NAME).build());
                serverQueueUrl = getQueueUrlResponse.queueUrl();
                if (!isLambda) {
                    lambdaReceiver = new SqsReceiver(this, sqsClient, serverQueueUrl);
                }
                logger.info("Created queue with URL [ " + serverQueueUrl + "]");
            } catch (SqsException e) {
                logger.error(e.awsErrorDetails().errorMessage(), e);
            }
        }
        return serverQueueUrl;
    }

    /**
     *
     * @return
     */
    private SqsReceiver createReceiver() {
        if (receiver == null) {
            try {
                SqsClient sqsClient = createClient();
                logger.info("Creating queue receiver named [" + myQueueName + "]");
                Map<QueueAttributeName, String> queueAttributes = new HashMap<>();
                queueAttributes.put(QueueAttributeName.FIFO_QUEUE, "true");
                queueAttributes.put(QueueAttributeName.CONTENT_BASED_DEDUPLICATION, "true");
                queueAttributes.put(QueueAttributeName.RECEIVE_MESSAGE_WAIT_TIME_SECONDS, "20");
                CreateQueueRequest createQueueRequest = CreateQueueRequest.builder()
                        .queueName(myQueueName)
                        .attributes(queueAttributes)
                        .build();
                sqsClient.createQueue(createQueueRequest);
                GetQueueUrlResponse getQueueUrlResponse = sqsClient.getQueueUrl(GetQueueUrlRequest.builder().queueName(myQueueName).build());
                myQueueUrl = getQueueUrlResponse.queueUrl();
                receiver = new SqsReceiver(this, sqsClient, myQueueUrl);
                logger.info("Created receiver queue with URL [ " + myQueueUrl + "]");
            } catch (SqsException e) {
                logger.error(e.awsErrorDetails().errorMessage(), e);
            }
        }
        return receiver;
    }

    /**
     *
     */
    private final void sendNotice() {
        logger.info("Sending Queue notice for [" + myQueueUrl + "]");
        SqsRegistration info = new SqsRegistration(myQueueUrl);
        SqsData data = new SqsData(info);
        try {
            sqsClient.sendMessage(SendMessageRequest.builder()
                    .queueUrl(myQueueUrl)
                    .messageGroupId("1")
                    .messageBody(data.toJson())
                    .build());
        } catch (Exception e) {
            logger.error("Could not initialize my queue! [" + e.getMessage() + "]", e);
        }
        addToQueue(data);
    }

    /**
     *
     * @param data
     */
    public void addToQueue(SqsData data) {
        sender.addToQueue(data);
    }

    /**
     *
     * @param listener
     */
    public void addListener(SqsListener listener) {
        receiver.addListener(listener);
        if (lambdaReceiver != null) {
            lambdaReceiver.addListener(listener);
        }
    }

    /**
     *
     */
    public void destroy() {
        if (myQueueUrl != null) {
            SqsData data = new SqsData(new SqsRegistration(myQueueUrl, true));
            sender.addToQueue(data);
        }

        try {
            receiver.stop();
        } catch (Exception e) {
        }
        try {
            sender.stop(true);
        } catch (Exception e) {
        }
        if (isLambda) {
            try {
                DeleteQueueRequest deleteQueueRequest = DeleteQueueRequest.builder()
                        .queueUrl(myQueueUrl)
                        .build();
                sqsClient.deleteQueue(deleteQueueRequest);
            } catch (Exception ex) {

            }
        }
    }

    /**
     *
     */
    public void refreshQueues() {
        try {
            ListQueuesResponse queueResponse = sqsClient.listQueues();
            queueUrls = new HashSet(queueResponse.queueUrls());
            if (myQueueUrl != null) {
                queueUrls.remove(myQueueUrl);
            }
            if (serverQueueUrl != null) {
                queueUrls.remove(serverQueueUrl);
            }
        } catch (Exception e) {
            logger.error("Could not refresh the queue list! [" + e.getMessage() + "]");
        }
    }

    /**
     *
     * @param listener
     */
    public void removeListener(SqsListener listener) {
        receiver.removeListener(listener);
        if (lambdaReceiver != null) {
            lambdaReceiver.removeListener(listener);
        }
    }

    /**
     * @return the queueUrls
     */
    public Set<String> getQueueUrls() {
        return queueUrls;
    }

    /**
     * @param queueUrls the queueUrls to set
     */
    public void setQueueUrls(Set<String> queueUrls) {
        this.queueUrls = queueUrls;
    }

    /**
     *
     * @param queueUrl
     */
    public void addQueueUrl(String queueUrl) {
        logger.info("Attempting to add Queue URL [" + queueUrl + "]");
        if ((queueUrl != null)
                && (!queueUrl.equals(myQueueUrl))
                && (!queueUrl.equals(serverQueueUrl))) {
            logger.info("Adding Queue URL [" + queueUrl + "]");
            this.queueUrls.add(queueUrl);
        }
    }

    /**
     *
     * @param queueUrl
     */
    public void removeQueueUrl(String queueUrl) {
        logger.info("Removing Queue URL [" + queueUrl + "]");
        this.queueUrls.remove(queueUrl);
    }

    /**
     * @return the myQueueUrl
     */
    public String getMyQueueUrl() {
        return myQueueUrl;
    }

    /**
     * @param myQueueUrl the myQueueUrl to set
     */
    public void setMyQueueUrl(String myQueueUrl) {
        this.myQueueUrl = myQueueUrl;
    }

    /**
     * @return the myQueueName
     */
    public String getMyQueueName() {
        return myQueueName;
    }

    /**
     * @param myQueueName the myQueueName to set
     */
    public void setMyQueueName(String myQueueName) {
        this.myQueueName = myQueueName;
    }

    /**
     * @return the receiver
     */
    public SqsReceiver getReceiver() {
        return receiver;
    }

    /**
     * @param receiver the receiver to set
     */
    public void setReceiver(SqsReceiver receiver) {
        this.receiver = receiver;
    }

    /**
     * @return the sender
     */
    public SqsSender getSender() {
        return sender;
    }

    /**
     * @param sender the sender to set
     */
    public void setSender(SqsSender sender) {
        this.sender = sender;
    }

    /**
     * @return the sqsClient
     */
    public SqsClient getSqsClient() {
        return sqsClient;
    }

    /**
     * @param sqsClient the sqsClient to set
     */
    public void setSqsClient(SqsClient sqsClient) {
        this.sqsClient = sqsClient;
    }

    /**
     * @return the serverQueueUrl
     */
    public String getServerQueueUrl() {
        if (serverQueueUrl == null) {
            serverQueueUrl = createServerQueueIfNotExists();
        }
        return serverQueueUrl;
    }

    /**
     * @param aServerQueueUrl the serverQueueUrl to set
     */
    public void setServerQueueUrl(String aServerQueueUrl) {
        serverQueueUrl = aServerQueueUrl;
    }
}
