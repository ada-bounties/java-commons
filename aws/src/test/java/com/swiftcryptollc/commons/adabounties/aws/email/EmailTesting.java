package com.swiftcryptollc.commons.adabounties.aws.email;

import com.swiftcryptollc.commons.adabounties.aws.email.data.NotificationSummary;
import com.swiftcryptollc.commons.adabounties.utilities.data.ActionType;
import java.util.concurrent.TimeUnit;
import org.junit.jupiter.api.Test;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
public class EmailTesting {

    public EmailTesting() {

    }

    @Test
    public void testSendEmail() {
        try {
            new ProcessBuilder().command("export", "cardano_network=preview").start().waitFor(5L, TimeUnit.SECONDS);
            NotificationSummary notSum = new NotificationSummary();
            notSum.setEmail("info@adabounties.io");
            notSum.addToStatusMap(ActionType.BOOKMARKED_BOUNTY_COMMENT.label, 1L);
            notSum.addToStatusMap(ActionType.BOOKMARKED_BOUNTY_COMMENT_UPDATE.label, 2L);
            notSum.addToStatusMap(ActionType.BOOKMARKED_BOUNTY_CLAIMED.label, 3L);
            notSum.addToStatusMap(ActionType.BOOKMARKED_BOUNTY_CLOSED.label, 4L);
            notSum.addToStatusMap(ActionType.BOOKMARKED_BOUNTY_UPDATE.label, 5L);

            notSum.addToStatusMap(ActionType.CLAIM_ACCEPT.label, 1L);
            notSum.addToStatusMap(ActionType.CLAIM_CLOSED.label, 2L);
            notSum.addToStatusMap(ActionType.CLAIM_COMMENT.label, 3L);
            notSum.addToStatusMap(ActionType.CLAIM_COMMENT_REPLY.label, 4L);
            notSum.addToStatusMap(ActionType.CLAIM_COMMENT_UPDATE.label, 5L);
            notSum.addToStatusMap(ActionType.CLAIM_NEW.label, 6L);
            notSum.addToStatusMap(ActionType.CLAIM_REJECT.label, 7L);
            notSum.addToStatusMap(ActionType.CLAIM_UPDATE.label, 8L);

            notSum.addToStatusMap(ActionType.BOUNTY_CLAIMED.label, 1L);
            notSum.addToStatusMap(ActionType.BOUNTY_CLOSED.label, 2L);
            notSum.addToStatusMap(ActionType.BOUNTY_COMMENT.label, 3L);
            notSum.addToStatusMap(ActionType.BOUNTY_COMMENT_REACT.label, 4L);
            notSum.addToStatusMap(ActionType.BOUNTY_COMMENT_REPLY.label, 5L);
            notSum.addToStatusMap(ActionType.BOUNTY_COMMENT_UPDATE.label, 6L);
            notSum.addToStatusMap(ActionType.BOUNTY_NEW.label, 7L);
            notSum.addToStatusMap(ActionType.BOUNTY_REACT.label, 8L);
            notSum.addToStatusMap(ActionType.BOUNTY_UPDATE.label, 9L);

            EmailUtils emailUtils = new EmailUtils();
            Boolean sent = emailUtils.sendSummaryNotification(notSum, "fakechallenge");
            if (sent) {
                System.out.println("Email sent!");
            } else {
                System.out.println("Email NOT sent!");
            }
            try {
                Thread.sleep(3000);
            } catch (Exception e) {
            }
        } catch (Exception ex) {
            System.out.println("EXCEPTION [" + ex.getMessage() + "]");
            ex.printStackTrace();
        }
    }
}
