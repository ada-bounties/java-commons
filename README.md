# ADA Bounties Java Commons

These common modules are used by the ADA Bounties Spring Boot server and the ADA Bounties Lambda functions. 

## Modules and layouts
<b>Utilities Commons</b>
<ul>
<li><i class="fas fa-caret-right"></i>Data utilities: com.swiftcryptollc.commons.adabounties.utilities.data</li>
<li><i class="fas fa-caret-right"></i>Security utilities: com.swiftcryptollc.commons.adabounties.utilities.security</li>
<li><i class="fas fa-caret-right"></i>Generic timer utilities: com.swiftcryptollc.commons.adabounties.utilities.timers</li>
</ul>

<b>AWS Commons</b>
<ul>
<li><i class="fas fa-caret-right"></i>AWS specific classes: com.swiftcryptollc.commons.adabounties.aws</li>
</ul>

<b>Data Commons</b>
<ul>
<li><i class="fas fa-caret-right"></i>Basic data classes: com.swiftcryptollc.commons.adabounties.data</li>
<li><i class="fas fa-caret-right"></i>Cardano specific data classes: com.swiftcryptollc.commons.adabounties.data.cardano</li>
<li><i class="fas fa-caret-right"></i>Base package for Elasticsearch: com.swiftcryptollc.commons.adabounties.es</li>
<li><i class="fas fa-caret-right"></i>Elasticsearch data/index classes: com.swiftcryptollc.commons.adabounties.es.data</li>
<li><i class="fas fa-caret-right"></i>Elasticsearch repository classes: com.swiftcryptollc.commons.adabounties.es.repository</li>
</ul>

