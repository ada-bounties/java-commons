#!/bin/bash

FILE=/opt/adabounties/tokens/cardano-token-registry/README.md
if test -f "$FILE"; then
    echo "Updating the registry";
    cd /opt/adabounties/tokens/cardano-token-registry
    git pull
else
    echo "Cloning the registry";
    mkdir /opt/adabounties/tokens
    cd /opt/adabounties/tokens
    git clone https://github.com/cardano-foundation/cardano-token-registry.git
fi

