package com.swiftcryptollc.commons.adabounties.integrations.cardano;

import co.elastic.clients.elasticsearch.core.BulkRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.swiftcryptollc.commons.adabounties.data.cardano.tokens.TokenRegistration;
import com.swiftcryptollc.commons.adabounties.es.data.Token;
import com.swiftcryptollc.commons.adabounties.es.repository.TokenRepository;
import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.TimeUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
public class TokenLoader implements Runnable {

    private TokenRepository tokenRepo;

    private final TokenLoaderListener listener;
    private final static String TOKEN_DIR = "/opt/adabounties/tokens/cardano-token-registry/mappings/";
    private final static String TOKEN_SCRIPT = "/opt/adabounties/bin/tokens.sh";
    private final Gson gson = new GsonBuilder().create();
    protected final static Logger logger = LoggerFactory.getLogger(TokenLoader.class);

    public TokenLoader(TokenRepository tokenRepo, TokenLoaderListener listener) {
        this.tokenRepo = tokenRepo;
        this.listener = listener;
    }

    @Override
    public void run() {
        try {
            Process tokenGit = new ProcessBuilder(TOKEN_SCRIPT).start();
            tokenGit.waitFor(5, TimeUnit.MINUTES);
            File tokenDir = new File(TOKEN_DIR);
            BulkRequest.Builder br = new BulkRequest.Builder();
            for (String file : tokenDir.list()) {
                if (file.endsWith("json")) {
                    String json = new String(Files.readAllBytes(Paths.get(TOKEN_DIR.concat(file))), StandardCharsets.UTF_8);
                    Token token = Token.generateToken(gson.fromJson(json, TokenRegistration.class));
                    if (token.getSubject() != null) {
                        if (token.getTicker() == null) {
                            String hex = token.getSubject().substring(56);
                            if (hex.length() > 0) {
                                StringBuffer sb = new StringBuffer();
                                char[] charArray = hex.toCharArray();
                                for (int i = 0; i < charArray.length; i = i + 2) {
                                    String st = "" + charArray[i] + "" + charArray[i + 1];
                                    char ch = (char) Integer.parseInt(st, 16);
                                    sb.append(ch);
                                }
                                String ticker = sb.toString().trim();
                                logger.info("Generated Ticker [" + ticker + "]");
                                token.setTicker(ticker);
                            } else {
                                logger.error("No ticker..");
                            }
                        }
                        if (token.getTicker() != null) {
                            token.setTicker(token.getTicker().trim());
                            token.setName(token.getName().trim());
                            logger.info("Loading [" + token.getTicker() + "]");
                            tokenRepo.save(token);
                        }
                    } else {
                        logger.error("Skipping file [" + file + "]");
                    }
                }
            }
        } catch (Exception ex) {
            logger.error("Could not load the tokens! [" + ex.getMessage() + "]", ex);
        }
        this.listener.loaderFinished();
    }
}
