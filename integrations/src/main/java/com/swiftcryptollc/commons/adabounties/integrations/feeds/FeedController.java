package com.swiftcryptollc.commons.adabounties.integrations.feeds;

import com.rometools.rome.feed.atom.Category;
import com.rometools.rome.feed.atom.Content;
import com.rometools.rome.feed.atom.Entry;
import com.rometools.rome.feed.atom.Feed;
import com.rometools.rome.feed.atom.Link;
import com.rometools.rome.feed.atom.Person;
import com.rometools.rome.feed.rss.Channel;
import com.rometools.rome.feed.rss.Description;
import com.rometools.rome.feed.rss.Guid;
import com.rometools.rome.feed.rss.Image;
import com.rometools.rome.feed.rss.Item;
import com.rometools.rome.feed.synd.SyndPerson;
import com.swiftcryptollc.commons.adabounties.es.data.Alias;
import com.swiftcryptollc.commons.adabounties.es.data.Timeline;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
@RestController
public class FeedController {

    private final static String BASE_URL = "https://adabounties.io/";
    // RSS
    public Channel rssChannel = new Channel();
    public Map<Guid, Item> rssMap = new HashMap<>();

    // Atom
    public Feed feed = new Feed();
    public Map<String, Entry> feedMap = new HashMap<>();
    protected final static Logger logger = LoggerFactory.getLogger(FeedController.class);

    public FeedController() {

    }

    @PostConstruct
    public void init() {
        rssChannel.setFeedType("rss_2.0");
        rssChannel.setTitle("ADA Bounties Timeline Feed");
        rssChannel.setDescription("ADA Bounties timeline of events.");
        rssChannel.setLink(BASE_URL);
        rssChannel.setUri(BASE_URL);
        rssChannel.setCopyright("© Swift Crypto LLC - 2023. All Rights Reserved.");
        rssChannel.setGenerator("auto");

        Image image = new Image();
        image.setUrl(BASE_URL.concat("assets/layout/images/logo-light.png"));
        image.setTitle("ADA Bounties Timeline Feed");
        image.setHeight(32);
        image.setWidth(32);
        image.setLink(BASE_URL);
        rssChannel.setImage(image);

        feed.setFeedType("atom_1.0");
        feed.setTitle("ADA Bounties");
        feed.setLogo(BASE_URL.concat("assets/layout/images/logo-light.png"));
        feed.setIcon(BASE_URL.concat("assets/layout/images/logo-light.png"));
        feed.setId(BASE_URL);
        feed.setCopyright("© Swift Crypto LLC - 2023. All Rights Reserved.");
        Content subtitle = new Content();
        subtitle.setType("text/plain");
        subtitle.setValue("ADA Bounties Timeline of Events");
        feed.setSubtitle(subtitle);
    }

    /**
     *
     * @param timeline
     * @param alias
     */
    public void addTimeline(Timeline timeline, Alias alias) {

        Date postDate = new Date();

        String title = "";
        String url = "";
        if (timeline.getAction().equals("btynew")) {
            title = alias.getName() + " created a new bounty";
            url = "bounties/view/" + timeline.getBountyId();
        } else if (timeline.getAction().equals("btyupd")) {
            title = alias.getName() + " updated a bounty";
            url = "bounties/view/" + timeline.getBountyId();
        } else if (timeline.getAction().equals("btycmt")) {
            title = alias.getName() + " added a bounty comment";
            url = "bounties/view/" + timeline.getBountyId() + "/" + timeline.getCommentId();
        } else if (timeline.getAction().equals("btycmtupd")) {
            title = alias.getName() + " updated a bounty comment";
            url = "bounties/view/" + timeline.getBountyId() + "/" + timeline.getCommentId();
        } else if (timeline.getAction().equals("btycmtrpy")) {
            title = alias.getName() + " replied to a bounty comment";
            url = "bounties/view/" + timeline.getBountyId() + "/" + timeline.getCommentId();
        } else if (timeline.getAction().equals("btycmtrct")) {
            title = alias.getName() + " reacted to a bounty comment";
            url = "bounties/view/" + timeline.getBountyId() + "/" + timeline.getCommentId();
        } else if (timeline.getAction().equals("btyrct")) {
            title = alias.getName() + " reacted to a bounty";
            url = "bounties/view/" + timeline.getBountyId();
        } else if (timeline.getAction().equals("btycls")) {
            title = alias.getName() + " closed a bounty";
            url = "bounties/view/" + timeline.getBountyId();
        } else if (timeline.getAction().equals("btyclm")) {
            title = alias.getName() + " claimed a bounty";
            url = "bounties/view/" + timeline.getBountyId();
        } else if (timeline.getAction().equals("clmnew")) {
            title = alias.getName() + " created a new claim";
            url = "claims/view/" + timeline.getClaimId();
        } else if (timeline.getAction().equals("clmupd")) {
            title = alias.getName() + " updated a claim";
            url = "claims/view/" + timeline.getClaimId();
        } else if (timeline.getAction().equals("clmcmt")) {
            title = alias.getName() + " added a claim comment";
            url = "claims/view/" + timeline.getClaimId() + "/" + timeline.getCommentId();
        } else if (timeline.getAction().equals("clmcmtupd")) {
            title = alias.getName() + " updated a claim comment";
            url = "claims/view/" + timeline.getClaimId() + "/" + timeline.getCommentId();
        } else if (timeline.getAction().equals("clmcmtrpy")) {
            title = alias.getName() + " replied to a claim comment";
            url = "claims/view/" + timeline.getClaimId() + "/" + timeline.getCommentId();
        } else if (timeline.getAction().equals("clmcls")) {
            title = alias.getName() + " closed a claim";
            url = "claims/view/" + timeline.getClaimId();
        } else if (timeline.getAction().equals("clmacpt")) {
            title = alias.getName() + " accepted a claim";
            url = "claims/view/" + timeline.getClaimId();
        } else if (timeline.getAction().equals("clmrjt")) {
            title = alias.getName() + " rejected a claim";
            url = "claims/view/" + timeline.getClaimId();
        } else if (timeline.getAction().equals("accnew")) {
            title = alias.getName() + " joined ADABounties";
            url = "profile/" + timeline.getAccountId();
        }

        url = BASE_URL.concat(url);
        Item item = new Item();
        item.setTitle(title);
        item.setLink(url);
        item.setUri(url);
        Guid guid = new Guid();
        guid.setValue(url);
        item.setGuid(guid);

        com.rometools.rome.feed.rss.Category category = new com.rometools.rome.feed.rss.Category();
        category.setValue("Timeline");
        item.setCategories(Collections.singletonList(category));

        Description descr = new Description();
        descr.setValue(title);
        item.setDescription(descr);
        item.setPubDate(new Date(timeline.getUpdateMs()));
        rssMap.put(guid, item);
        rssChannel.setPubDate(postDate);
        if (rssMap.values().size() > 50) {
            Guid oldestId = null;
            Long oldestTime = Long.MAX_VALUE;
            for (Map.Entry<Guid, Item> rssEntry : rssMap.entrySet()) {
                Item rItem = rssEntry.getValue();
                Long timeMs = rItem.getPubDate().getTime();
                if (timeMs < oldestTime) {
                    oldestId = rItem.getGuid();
                    oldestTime = timeMs;
                }
            }
            if (oldestId != null) {
                rssMap.remove(oldestId);
            }
        }

        rssChannel.setItems(new ArrayList<>(rssMap.values()));

        feed.setUpdated(postDate);
        Entry entry = new Entry();
        Link link = new Link();
        link.setHref(url);
        entry.setAlternateLinks(Collections.singletonList(link));
        SyndPerson author = new Person();
        author.setName("ADA Bounties");
        entry.setAuthors(Collections.singletonList(author));
        entry.setCreated(postDate);
        entry.setPublished(new Date(timeline.getUpdateMs()));
        entry.setUpdated(new Date(timeline.getUpdateMs()));
        entry.setId(url);
        entry.setTitle(title);

        Category atomCategory = new Category();
        atomCategory.setTerm("Timeline");
        entry.setCategories(Collections.singletonList(atomCategory));

        Content summary = new Content();
        summary.setType("text/plain");
        summary.setValue(title);
        entry.setSummary(summary);

        feedMap.put(url, entry);
        if (feedMap.values().size() > 50) {
            String oldestId = null;
            Long oldestTime = Long.MAX_VALUE;
            for (Map.Entry<String, Entry> feedEntry : feedMap.entrySet()) {
                Entry fEntry = feedEntry.getValue();
                Long timeMs = fEntry.getPublished().getTime();
                if (timeMs < oldestTime) {
                    oldestId = fEntry.getId();
                    oldestTime = timeMs;
                }
            }
            if (oldestId != null) {
                feedMap.remove(oldestId);
            }
        }
        feed.setEntries(new ArrayList<>(feedMap.values()));
    }

    @GetMapping(path = "/rss")
    public Channel rss() {
        return rssChannel;
    }

    @GetMapping(path = "/atom")
    public Feed atom() {

        return feed;
    }
}
