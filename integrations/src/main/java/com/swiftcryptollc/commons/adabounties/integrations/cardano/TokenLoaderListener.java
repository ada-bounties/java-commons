package com.swiftcryptollc.commons.adabounties.integrations.cardano;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
public interface TokenLoaderListener {

    public void loaderFinished();
}
