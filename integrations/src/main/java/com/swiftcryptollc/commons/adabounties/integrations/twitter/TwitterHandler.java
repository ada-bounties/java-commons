package com.swiftcryptollc.commons.adabounties.integrations.twitter;

import com.swiftcryptollc.commons.adabounties.aws.secrets.SecretsManager;
import com.swiftcryptollc.commons.adabounties.aws.secrets.SecretsManagerListener;
import com.swiftcryptollc.commons.adabounties.data.Category;
import com.swiftcryptollc.commons.adabounties.es.data.Bounty;
import com.swiftcryptollc.commons.adabounties.es.data.Claim;
import java.util.Map;
import java.util.UUID;
import javax.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import twitter4j.CreateTweetResponse;
import twitter4j.Twitter;
import twitter4j.TwitterFactory;
import twitter4j.TwitterV2;
import twitter4j.TwitterV2ExKt;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
@Service
public class TwitterHandler implements SecretsManagerListener {

    private final UUID id;
    private static String cardanoNetwork;
    private static String twitterAPIKey;
    private static String twitterAPISecret;
    private static String twitterAccessToken;
    private static String twitterAccessTokenSecret;
    private static String bountyUrl;
    private static String claimUrl;
    private static String lbUrl;
    private static String profileUrl;
    private static String bountyTags;
    private static String claimTags;
    private static String leaderboardTags;
    private static String userTags;
    private final static Integer titleLength = 80;
    private final static Object LOCK = new Object();
    private final static String BOUNTIES_URL = "https://adabounties.io/";
    private final static String PREVIEW_BOUNTIES_URL = "https://preview.adabounties.io/";
    protected final static Logger logger = LoggerFactory.getLogger(TwitterHandler.class);

    public TwitterHandler() {
        id = UUID.randomUUID();
    }

    /**
     * @return the id
     */
    public UUID getId() {
        return id;
    }

    @PostConstruct
    public void init() {
        SecretsManager.subscribe(this);
    }

    /**
     *
     * @param secretMap
     */
    @Override
    public void updateSecrets(Map<String, String> secretMap) {
        synchronized (LOCK) {
            if (secretMap != null) {
                if (secretMap.containsKey("cardano_network")) {
                    cardanoNetwork = secretMap.get("cardano_network");
                }
                if (secretMap.containsKey("twitterAPIKey")) {
                    twitterAPIKey = secretMap.get("twitterAPIKey");
                }
                if (secretMap.containsKey("twitterAPISecret")) {
                    twitterAPISecret = secretMap.get("twitterAPISecret");
                }
                if (secretMap.containsKey("twitterAccessToken")) {
                    twitterAccessToken = secretMap.get("twitterAccessToken");
                }
                if (secretMap.containsKey("twitterAccessTokenSecret")) {
                    twitterAccessTokenSecret = secretMap.get("twitterAccessTokenSecret");
                }
                if (secretMap.containsKey("bountyTags")) {
                    bountyTags = secretMap.get("bountyTags");
                }
                if (secretMap.containsKey("claimTags")) {
                    claimTags = secretMap.get("claimTags");
                }
                if (secretMap.containsKey("leaderboardTags")) {
                    leaderboardTags = secretMap.get("leaderboardTags");
                }
                if (secretMap.containsKey("userTags")) {
                    userTags = secretMap.get("userTags");
                }

            }

            if ((cardanoNetwork == null)
                    || (cardanoNetwork.equals("preview"))) {
                bountyUrl = PREVIEW_BOUNTIES_URL.concat("bounties/view/");
                claimUrl = PREVIEW_BOUNTIES_URL.concat("claims/view/");
                lbUrl = PREVIEW_BOUNTIES_URL.concat("metrics/leaderboards");
                profileUrl = PREVIEW_BOUNTIES_URL.concat("profile/");
            } else {
                bountyUrl = BOUNTIES_URL.concat("bounties/view/");
                claimUrl = BOUNTIES_URL.concat("claims/view/");
                lbUrl = BOUNTIES_URL.concat("metrics/leaderboards");
                profileUrl = BOUNTIES_URL.concat("profile/");
            }
            System.setProperty("oauth.consumerKey", twitterAPIKey);
            System.setProperty("oauth.consumerSecret", twitterAPISecret);
            System.setProperty("oauth.accessToken", twitterAccessToken);
            System.setProperty("oauth.accessTokenSecret", twitterAccessTokenSecret);
            System.setProperty("debug", "false");
            System.setProperty("http.prettyDebug", "true");

            logger.info("Using bounty URL [" + bountyUrl + "]");
        }
    }

    /**
     *
     * @param bounty
     * @return
     */
    public Long sendNewBountyTweet(Bounty bounty) {
        return sendNewBountyTweet(bounty, Boolean.TRUE);
    }

    /**
     *
     * @param bounty
     * @param send
     * @return
     */
    public Long sendNewBountyTweet(Bounty bounty, Boolean send) {
        try {
            String title = bounty.getTitle();
            if (title.length() > titleLength) {
                title = title.substring(0, titleLength).concat("..");
            }
            String url = bountyUrl.concat(bounty.getId());
            StringBuilder data = new StringBuilder("A new bounty has been posted:\n\n");
            data.append("\"");
            data.append(title);
            data.append("\"");
            data.append("\n\n");
            data.append("Reward: ");
            data.append(bounty.getAmount());
            data.append(" ");
            data.append(bounty.getToken());
            data.append("\n\n");
            data.append(url);
            data.append("\n\n");
            data.append(bountyTags);
            if (!bounty.getCategory().equals(Category.OTHER.label)) {
                data.append(" #");
                data.append(bounty.getCategory().replace(" ", ""));
            }
            if (send) {
                return sendTweet(data.toString());
            } else {
                logger.info("Tweet [" + data.toString() + "]");
                return 9L;
            }
        } catch (Exception e) {
            logger.error("Could not send a tweet! [" + e.getMessage() + "]");
        }
        return null;
    }

    /**
     *
     * @param bounty
     * @return
     */
    public Long sendOpenBountyTweet(Bounty bounty) {
        return sendOpenBountyTweet(bounty, Boolean.TRUE);
    }

    /**
     *
     * @param bounty
     * @param send
     * @return
     */
    public Long sendOpenBountyTweet(Bounty bounty, Boolean send) {
        try {
            String title = bounty.getTitle();
            if (title.length() > titleLength) {
                title = title.substring(0, titleLength).concat("..");
            }
            String url = bountyUrl.concat(bounty.getId());
            StringBuilder data = new StringBuilder("Bounty Highlight:\n\n");
            data.append("\"");
            data.append(title);
            data.append("\"");
            data.append("\n\n");
            data.append("Reward: ");
            data.append(bounty.getAmount());
            data.append(" ");
            data.append(bounty.getToken());
            data.append("\n\n");
            data.append(url);
            data.append("\n\n");
            data.append(bountyTags);
            if (!bounty.getCategory().equals(Category.OTHER.label)) {
                data.append(" #");
                data.append(bounty.getCategory().replace(" ", ""));
            }
            if (send) {
                return sendTweet(data.toString());
            } else {
                logger.info("Tweet [" + data.toString() + "]");
                return 2L;
            }
        } catch (Exception e) {
            logger.error("Could not send a tweet! [" + e.getMessage() + "]");
        }
        return null;
    }

    /**
     *
     * @param bounty
     * @return
     */
    public Long sendBountyUpdatedTweet(Bounty bounty) {
        return sendBountyUpdatedTweet(bounty, Boolean.TRUE);
    }

    /**
     *
     * @param bounty
     * @param send
     * @return
     */
    public Long sendBountyUpdatedTweet(Bounty bounty, Boolean send) {
        try {
            String title = bounty.getTitle();
            if (title.length() > titleLength) {
                title = title.substring(0, titleLength).concat("..");
            }
            String url = bountyUrl.concat(bounty.getId());
            StringBuilder data = new StringBuilder("A bounty has been updated:\n\n");
            data.append("\"");
            data.append(title);
            data.append("\"");
            data.append("\n\n");
            data.append(url);
            data.append("\n\n");
            data.append(bountyTags);
            if (!bounty.getCategory().equals(Category.OTHER.label)) {
                data.append(" #");
                data.append(bounty.getCategory().replace(" ", ""));
            }
            if (send) {
                return sendTweet(data.toString());
            } else {
                logger.info("Tweet [" + data.toString() + "]");
                return 3L;
            }
        } catch (Exception e) {
            logger.error("Could not send a tweet! [" + e.getMessage() + "]");
        }
        return null;
    }

    /**
     *
     * @param claim
     * @return
     */
    public Long sendNewClaimTweet(Claim claim) {
        return sendNewClaimTweet(claim, Boolean.TRUE);
    }

    /**
     *
     * @param claim
     * @param send
     * @return
     */
    public Long sendNewClaimTweet(Claim claim, Boolean send) {
        try {
            Bounty bounty = claim.getBounty();
            String url = claimUrl.concat(claim.getId());
            StringBuilder data = new StringBuilder("A new claim has been created:\n\n");
            if (bounty != null) {
                String title = bounty.getTitle();
                if (title.length() > titleLength) {
                    title = title.substring(0, titleLength).concat("..");
                }
                data.append("\"");
                data.append(title);
                data.append("\"");
                data.append("\n\n");
            } else if (claim.getBountyTitle() != null) {
                String title = claim.getBountyTitle();
                if (title.length() > titleLength) {
                    title = title.substring(0, titleLength).concat("..");
                }
                data.append("\"");
                data.append(title);
                data.append("\"");
                data.append("\n\n");
            }
            data.append(url);
            data.append("\n\n");
            data.append(claimTags);
            if (send) {
                return sendTweet(data.toString());
            } else {
                logger.info("Tweet [" + data.toString() + "]");
                return 4L;
            }
        } catch (Exception e) {
            logger.error("Could not send a tweet! [" + e.getMessage() + "]");
        }
        return null;
    }

    /**
     *
     * @param claim
     * @return
     */
    public Long sendAcceptedClaimTweet(Claim claim) {
        return sendAcceptedClaimTweet(claim, Boolean.TRUE);
    }

    /**
     *
     * @param claim
     * @param send
     * @return
     */
    public Long sendAcceptedClaimTweet(Claim claim, Boolean send) {
        try {
            Bounty bounty = claim.getBounty();
            String url = claimUrl.concat(claim.getId());
            StringBuilder data = new StringBuilder("A bounty has been claimed:\n\n");
            if (bounty != null) {
                String title = bounty.getTitle();
                if (title.length() > titleLength) {
                    title = title.substring(0, titleLength).concat("..");
                }
                data.append("\"");
                data.append(title);
                data.append("\"");
                data.append("\n\n");
            } else if (claim.getBountyTitle() != null) {
                String title = claim.getBountyTitle();
                if (title.length() > titleLength) {
                    title = title.substring(0, titleLength).concat("..");
                }
                data.append("\"");
                data.append(title);
                data.append("\"");
                data.append("\n\n");
            }
            data.append(url);
            data.append("\n\n");
            data.append(claimTags);
            if (send) {
                return sendTweet(data.toString());
            } else {
                logger.info("Tweet [" + data.toString() + "]");
                return 5L;
            }
        } catch (Exception e) {
            logger.error("Could not send a tweet! [" + e.getMessage() + "]");
        }
        return null;
    }

    /**
     *
     * @return
     */
    public Long sendLeaderboardTweet() {
        return sendLeaderboardTweet(Boolean.TRUE);
    }

    /**
     *
     * @param send
     * @return
     */
    public Long sendLeaderboardTweet(Boolean send) {
        try {
            String url = lbUrl;
            StringBuilder data = new StringBuilder("New leaderboards are up!\n\n");
            data.append("See who's moved up and down the ranks here:");
            data.append("\n\n");
            data.append(url);
            data.append("\n\n");
            data.append(leaderboardTags);
            if (send) {
                return sendTweet(data.toString());
            } else {
                logger.info("Tweet [" + data.toString() + "]");
                return 6L;
            }
        } catch (Exception e) {
            logger.error("Could not send a tweet! [" + e.getMessage() + "]");
        }
        return null;
    }

    /**
     *
     * @param accountId
     * @return
     */
    public Long sendNewUserTweet(String accountId) {
        return sendNewUserTweet(accountId, Boolean.TRUE);
    }

    /**
     *
     * @param accountId
     * @param send
     * @return
     */
    public Long sendNewUserTweet(String accountId, Boolean send) {
        try {
            String url = profileUrl.concat(accountId);
            StringBuilder data = new StringBuilder("Please welcome a new user!\n\n");
            data.append("\n\n");
            data.append(url);
            data.append("\n\n");
            data.append(userTags);
            if (send) {
                return sendTweet(data.toString());
            } else {
                logger.info("Tweet [" + data.toString() + "]");
                return 7L;
            }
        } catch (Exception e) {
            logger.error("Could not send a tweet! [" + e.getMessage() + "]");
        }
        return null;
    }

    /**
     *
     * @param leaderboard
     * @param names
     * @param values
     * @return
     */
    public Long sendTopThree(String leaderboard, String[] names, Double[] values) {
        return sendTopThree(leaderboard, names, values, Boolean.TRUE);
    }

    /**
     *
     * @param leaderboard
     * @param names
     * @param values
     * @param send
     * @return
     */
    public Long sendTopThree(String leaderboard, String[] names, Double[] values, Boolean send) {
        try {
            String url = lbUrl;
            StringBuilder data = new StringBuilder("Here are your current top 3 \"");
            data.append(leaderboard);
            data.append("\" leaders:\n\n");
            for (int i = 0; i < names.length; ++i) {
                int index = i + 1;
                data.append(index);
                data.append(") ");
                data.append(names[i]);
                data.append(": ");
                if (leaderboard.startsWith("rep")) {
                    data.append(String.format("%.2f", values[i]));
                } else {
                    data.append(String.format("%.0f", values[i]));
                }

                data.append("\n\n");
            }
            data.append(url);
            data.append("\n\n");
            data.append(leaderboardTags);
            if (send) {
                return sendTweet(data.toString());
            } else {
                logger.info("Tweet [" + data.toString() + "]");
                return 8L;
            }
        } catch (Exception e) {
            logger.error("Could not send a tweet! [" + e.getMessage() + "]");
        }
        return null;
    }

    /**
     *
     * @param data
     * @return
     */
    private Long sendTweet(String data) {
        Long id = null;
        try {

            Twitter twitter = new TwitterFactory().getInstance();
            final TwitterV2 v2 = TwitterV2ExKt.getV2(twitter);
            CreateTweetResponse resp = v2.createTweet(null, null, null, null, null, null, null, null, null, null, null, data);
            id = resp.getId();
            //     Status status = twitter.v1().tweets().updateStatus(data);
            //    id = status.getId();
            logger.info("Tweet sent with ID [" + id + "]");
        } catch (Exception tex) {
            logger.error("Exception occured while trying to send a tweet! [" + tex.getMessage() + "]", tex);
        }
        return id;
    }
}
