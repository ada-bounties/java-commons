package com.swiftcryptollc.commons.adabounties.integrations;

import co.elastic.clients.elasticsearch.ElasticsearchClient;
import com.swiftcryptollc.commons.adabounties.aws.secrets.SecretsManager;
import com.swiftcryptollc.commons.adabounties.es.LambdaElasticsearchClientConfig;
import com.swiftcryptollc.commons.adabounties.es.data.Bounty;
import com.swiftcryptollc.commons.adabounties.es.query.BountyQueryHandler;
import com.swiftcryptollc.commons.adabounties.integrations.twitter.TwitterHandler;
import java.util.HashSet;
import java.util.Set;
import org.junit.jupiter.api.Test;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
public class TwitterTest {

    protected final static TwitterHandler twitterHandler;
    protected final static BountyQueryHandler bountyQuery;

    private final static LambdaElasticsearchClientConfig esClientConfig;
    private final static ElasticsearchClient client;

    static {
        twitterHandler = new TwitterHandler();
        esClientConfig = new LambdaElasticsearchClientConfig();
        client = esClientConfig.elasticSearchClient();
        bountyQuery = new BountyQueryHandler();
        bountyQuery.setClient(client);
        twitterHandler.updateSecrets(SecretsManager.getSecretMap());
    }

    @Test
    public void sendTweet() {
        Set<String> ids = new HashSet<>();
        ids.add("H2Hm_fsfR46_2TEVIVN3mg");
        Set<Bounty> btySet = bountyQuery.findAllById(ids);
        btySet.forEach(bounty -> {
            Long id = twitterHandler.sendNewBountyTweet(bounty);
            if (id != null) {
                System.out.println("Bounty Tweet ID [" + id + "]");
            } else {
                System.out.println("Could not send the tweet!");
            }
        });
    }
}