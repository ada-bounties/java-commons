package com.swiftcryptollc.commons.adabounties.aws.sqs;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.swiftcryptollc.commons.adabounties.aws.sqs.data.SqsData;
import com.swiftcryptollc.commons.adabounties.utilities.data.ActionType;
import com.swiftcryptollc.commons.adabounties.es.data.Timeline;
import com.swiftcryptollc.commons.adabounties.utilities.data.Base64Id;

import java.util.Date;
import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;

import org.junit.jupiter.api.Test;
import org.slf4j.LoggerFactory;

import software.amazon.awssdk.regions.Region;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
public class TestMultiples {

    private final Gson gson = new GsonBuilder().create();
    private final static Region region = Region.of("us-east-2");

    @Test
    public void testSqsService() {
        Logger root = (Logger) LoggerFactory.getLogger(org.slf4j.Logger.ROOT_LOGGER_NAME);
        root.setLevel(Level.INFO);

        SqsService service1 = new SqsService();
        MyOtherListener listener1 = new MyOtherListener("LISTENER1");
        service1.addListener(listener1);
        try {
            Thread.sleep(5000);
        } catch (Exception e) {
        }
        SqsService service2 = new SqsService();
        MyOtherListener listener2 = new MyOtherListener("LISTENER2");
        service2.addListener(listener2);
        //  for (int i = 0; i < 10; ++i) {
        try {
            Thread.sleep(5000);
        } catch (Exception e) {
        }
        Timeline timeline = new Timeline();
        timeline.setAccountId(Base64Id.getNewId());
        timeline.setAction(ActionType.CLAIM_ACCEPT.label);
        timeline.setClaimId(Base64Id.getNewId());
        timeline.setBountyId(Base64Id.getNewId());
        timeline.setId(Base64Id.getNewId());
        timeline.setStartMs(new Date().getTime());
        timeline.setUpdateMs(new Date().getTime());
        System.out.println("Service 1 Sending Id [" + timeline.getId() + "]");
        SqsData data = new SqsData(timeline);
        service1.addToQueue(data);
        try {
            Thread.sleep(5000);
        } catch (Exception e) {
        }

        Timeline timeline2 = new Timeline();
        timeline2.setAccountId(Base64Id.getNewId());
        timeline2.setAction(ActionType.CLAIM_ACCEPT.label);
        timeline2.setClaimId(Base64Id.getNewId());
        timeline2.setBountyId(Base64Id.getNewId());
        timeline2.setId(Base64Id.getNewId());
        timeline2.setStartMs(new Date().getTime());
        timeline2.setUpdateMs(new Date().getTime());
        SqsData data2 = new SqsData(timeline2);
        System.out.println("Service 2 Sending Id [" + timeline2.getId() + "]");
        service2.addToQueue(data2);

        try {
            Thread.sleep(5000);
        } catch (Exception e) {
        }
        service1.destroy();
        service2.destroy();
        try {
            Thread.sleep(2000);
        } catch (Exception e) {
        }
    }
}

class MyOtherListener extends SqsListener {

    public String id;

    public MyOtherListener(String id) {
        this.id = id;
    }

    protected void processObject(SqsData sqsData) {
        if ((sqsData.getObject() != null) && (sqsData.getType().equals("TIMELINE"))) {
            Timeline timeline = (Timeline) sqsData.getObject(Timeline.class);
            System.out.println("[" + id + "] RECEIVED Id [" + timeline.getId() + "]");
        } else {
            System.out.println("[" + id + "] NOT A TIMELINE!");
        }
    }

    /**
     *
     */
    public void stop() {

    }

}
