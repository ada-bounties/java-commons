package com.swiftcryptollc.commons.adabounties.aws.sqs;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.swiftcryptollc.commons.adabounties.aws.sqs.data.SqsData;
import com.swiftcryptollc.commons.adabounties.utilities.data.ActionType;
import com.swiftcryptollc.commons.adabounties.es.data.Timeline;
import com.swiftcryptollc.commons.adabounties.utilities.data.Base64Id;
import java.text.DateFormat;
import java.util.Date;
import org.junit.jupiter.api.Test;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
public class TestSqs {

    private final Gson gson = new GsonBuilder().create();

    @Test
    public void testSqsService() {
        Logger root = (Logger) LoggerFactory.getLogger(org.slf4j.Logger.ROOT_LOGGER_NAME);
        root.setLevel(Level.INFO);
        SqsService sqsService = new SqsService();
        sqsService.addListener(new MyListener());

        for (int i = 0; i < 10; ++i) {
            Timeline timeline = new Timeline();
            timeline.setAccountId(Base64Id.getNewId());
            timeline.setAction(ActionType.CLAIM_ACCEPT.label);
            timeline.setClaimId(Base64Id.getNewId());
            timeline.setBountyId(Base64Id.getNewId());
            timeline.setId(Base64Id.getNewId());
            timeline.setStartMs(new Date().getTime());
            timeline.setUpdateMs(new Date().getTime());
            System.out.println("Sending Id [" + timeline.getId() + "]");
            SqsData data = new SqsData(timeline);
            sqsService.addToQueue(data);
            try {
                Thread.sleep(2000);
            } catch (Exception e) {
            }
        }
        try {
            Thread.sleep(10000);
        } catch (Exception e) {
        }
        System.out.println("Finished!");
        sqsService.destroy();
        try {
            Thread.sleep(2000);
        } catch (Exception e) {
        }

    }
}

class MyListener extends SqsListener {

    Gson gson = new GsonBuilder()
            .setDateFormat(DateFormat.FULL, DateFormat.FULL)
            .serializeSpecialFloatingPointValues()
            .create();

    public MyListener() {

    }

    protected void processObject(SqsData sqsData) {
        System.out.println("Type [" + sqsData.getType() + "]");
        if ((sqsData.getObject() != null) && (sqsData.getType().equals("TIMELINE"))) {
            Timeline timeline = (Timeline) sqsData.getObject(Timeline.class);
            System.out.println("[" + id + "] Received Id [" + timeline.getId() + "]");
        } else {
            System.out.println("[" + id + "] NOT A TIMELINE!");
        }

    }

    /**
     *
     */
    public void stop() {

    }

}
