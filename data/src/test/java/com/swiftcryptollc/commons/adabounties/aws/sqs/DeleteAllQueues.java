package com.swiftcryptollc.commons.adabounties.aws.sqs;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import java.util.Set;
import org.junit.jupiter.api.Test;
import org.slf4j.LoggerFactory;
import software.amazon.awssdk.services.sqs.model.DeleteQueueRequest;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
public class DeleteAllQueues {

    @Test
    public void testSqsService() {
        Logger root = (Logger) LoggerFactory.getLogger(org.slf4j.Logger.ROOT_LOGGER_NAME);
        root.setLevel(Level.INFO);
        SqsService sqsService = new SqsService();
        Set<String> queueUrls = sqsService.getQueueUrls();

        for (String queueUrl : queueUrls) {
            try {
                DeleteQueueRequest deleteQueueRequest = DeleteQueueRequest.builder()
                        .queueUrl(queueUrl)
                        .build();
                sqsService.getSqsClient().deleteQueue(deleteQueueRequest);

            } catch (Exception e) {
            }
        }
    }
}
