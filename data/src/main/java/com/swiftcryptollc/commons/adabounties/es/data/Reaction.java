package com.swiftcryptollc.commons.adabounties.es.data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.swiftcryptollc.commons.adabounties.utilities.data.Base64Id;
import java.io.Serializable;
import java.util.Date;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
@Document(indexName = "reaction")
@JsonIgnoreProperties({"_class", "_id"})
public class Reaction implements Serializable {

    @Transient
    private static final long serialVersionUID = 100016L;

    @Id
    protected String id;
    // Person who made the comment
    @Field(type = FieldType.Keyword)
    protected String accountId;
    @Field(type = FieldType.Keyword)
    protected String oAccountId;
    @Field(type = FieldType.Keyword)
    protected String bountyId;
    @Field(type = FieldType.Keyword)
    protected String commentId;
    @Field(type = FieldType.Keyword)
    protected String type;
    @Field
    protected Long postedMs;
    @Transient
    public final transient static String INDEX_NAME = "reaction";

    public Reaction() {
        id = Base64Id.getNewId();
        commentId = "null";
    }

    /**
     * @return the id
     */
    public String getId() {
        if (id == null) {
            id = Base64Id.getNewId();
        }
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the accountId
     */
    public String getAccountId() {
        return accountId;
    }

    /**
     * @param accountId the accountId to set
     */
    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the bountyId
     */
    public String getBountyId() {
        return bountyId;
    }

    /**
     * @param bountyId the bountyId to set
     */
    public void setBountyId(String bountyId) {
        this.bountyId = bountyId;
    }

    /**
     * @return the commentId
     */
    public String getCommentId() {
        if (commentId == null) {
            commentId = "null";
        }

        return commentId;
    }

    /**
     * @param commentId the commentId to set
     */
    public void setCommentId(String commentId) {
        this.commentId = commentId;
    }

    /**
     * @return the postedMs
     */
    public Long getPostedMs() {
        if (postedMs == null) {
            postedMs = new Date().getTime();
        }
        return postedMs;
    }

    /**
     * @param postedMs the postedMs to set
     */
    public void setPostedMs(Long postedMs) {
        this.postedMs = postedMs;
    }

    /**
     * @return the oAccountId
     */
    public String getoAccountId() {
        return oAccountId;
    }

    /**
     * @param oAccountId the oAccountId to set
     */
    public void setoAccountId(String oAccountId) {
        this.oAccountId = oAccountId;
    }

}
