package com.swiftcryptollc.commons.adabounties.es.repository;

import com.swiftcryptollc.commons.adabounties.es.data.Reaction;
import java.util.List;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
@Repository
public interface ReactionRepository extends ElasticsearchRepository<Reaction, String> {

    public List<Reaction> findByAccountId(String accountId);

    public List<Reaction> findByBountyId(String bountyId);

    public List<Reaction> findByCommentId(String commentId);

    public void deleteByCommentId(String commentId);
}
