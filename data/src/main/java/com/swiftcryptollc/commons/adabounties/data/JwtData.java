package com.swiftcryptollc.commons.adabounties.data;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
public class JwtData {

    protected String previousJwtString;
    protected String jwtString;

    public JwtData() {

    }

    /**
     * @return the previousJwtString
     */
    public String getPreviousJwtString() {
        return previousJwtString;
    }

    /**
     * @param previousJwtString the previousJwtString to set
     */
    public void setPreviousJwtString(String previousJwtString) {
        this.previousJwtString = previousJwtString;
    }

    /**
     * @return the jwtString
     */
    public String getJwtString() {
        return jwtString;
    }

    /**
     * @param jwtString the jwtString to set
     */
    public void setJwtString(String jwtString) {
        this.jwtString = jwtString;
    }
}
