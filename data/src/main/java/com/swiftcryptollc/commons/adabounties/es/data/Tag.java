package com.swiftcryptollc.commons.adabounties.es.data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.swiftcryptollc.commons.adabounties.utilities.data.Base64Id;
import java.io.Serializable;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
@Document(indexName = "tag")
@JsonIgnoreProperties({"_class", "_id"})
public class Tag implements Serializable {

    @Transient
    private static final long serialVersionUID = 100019L;

    @Id
    private String id;
    @Field
    private String category;
    @Field(type = FieldType.Wildcard)
    private String value;
    @Transient
    private double score;
    @Transient
    private Long count;
    @Transient
    public final transient static String INDEX_NAME = "tag";

    /**
     *
     */
    public Tag() {
        id = Base64Id.getNewId();
    }

    /**
     * @return the id
     */
    public String getId() {
        if (id == null) {
            id = Base64Id.getNewId();
        }
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the category
     */
    public String getCategory() {
        return category;
    }

    /**
     * @param category the category to set
     */
    public void setCategory(String category) {
        this.category = category;
    }

    /**
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * @return the weight
     */
    public double getScore() {
        return score;
    }

    /**
     * @param score the weight to set
     */
    public void setScore(double score) {
        this.score = score;
    }

    /**
     * @return the count
     */
    public Long getCount() {
        return count;
    }

    /**
     * @param count the count to set
     */
    public void setCount(Long count) {
        this.count = count;
    }
}
