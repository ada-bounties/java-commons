package com.swiftcryptollc.commons.adabounties.es.query;

import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.elasticsearch._types.FieldSort;
import co.elastic.clients.elasticsearch._types.SortOptions;
import co.elastic.clients.elasticsearch._types.SortOrder;
import co.elastic.clients.elasticsearch._types.query_dsl.BoolQuery;
import co.elastic.clients.elasticsearch._types.query_dsl.MatchQuery;
import co.elastic.clients.elasticsearch._types.query_dsl.Query;
import co.elastic.clients.elasticsearch._types.query_dsl.RangeQuery;
import co.elastic.clients.elasticsearch.core.SearchResponse;
import co.elastic.clients.elasticsearch.core.search.Hit;
import co.elastic.clients.elasticsearch.core.search.HitsMetadata;
import co.elastic.clients.json.JsonData;
import com.swiftcryptollc.commons.adabounties.es.ElasticsearchClientConfig;
import com.swiftcryptollc.commons.adabounties.es.ElasticsearchClientListener;
import com.swiftcryptollc.commons.adabounties.redis.cache.AliasCache;
import com.swiftcryptollc.commons.adabounties.es.data.UserMetric;
import com.swiftcryptollc.commons.adabounties.utilities.data.SyncArrayDeque;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import javax.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
@Service
public class UserMetricQueryHandler implements ElasticsearchClientListener {

    @Autowired
    protected ElasticsearchClientConfig esClient;
    @Autowired
    protected AliasCache aliasCache;
    protected ElasticsearchClient client;
    protected UUID id;
    protected final static Integer MAX_USERS = 500;

    protected final static Logger logger = LoggerFactory.getLogger(UserMetricQueryHandler.class);

    public UserMetricQueryHandler() {

    }

    /**
     *
     * @param client
     */
    public UserMetricQueryHandler(ElasticsearchClient client) {
        this.client = client;
    }

    /**
     * @return the id
     */
    public UUID getId() {
        if (id == null) {
            id = UUID.randomUUID();
        }
        return id;
    }

    @Override
    public void setClient() {
        if (esClient != null) {
            client = esClient.elasticSearchClient();
        }
    }

    /**
     * @param esClient the esClient to set
     */
    public void setEsClient(ElasticsearchClientConfig esClient) {
        this.esClient = esClient;
    }

    /**
     * @param client the client to set
     */
    public void setClient(ElasticsearchClient client) {
        this.client = client;
    }

    /**
     *
     */
    @PostConstruct
    public void init() {
        if (client == null) {
            ElasticsearchClientConfig.subscribe(this);
        }
    }

    /**
     *
     * @param totalBty
     * @param perClmdBty
     */
    public Set<UserMetric> searchOwnersForTotalBtyAndPerClmdBty(Long totalBty, Integer perClmdBty) {
        Set<UserMetric> userMetrics = new HashSet<>();
        List<Query> queryList = new ArrayList<>();
        Boolean sortByPerClmd = Boolean.TRUE;
        if ((totalBty != null) || (perClmdBty != null)) {
            queryList.add(MatchQuery.of(fn -> fn.field("lastXDays").query("-1"))._toQuery());
            if (totalBty != null) {
                queryList.add(RangeQuery.of(fn -> fn.field("totalBounties").gte(JsonData.of(totalBty)))._toQuery());
            }
            if (perClmdBty != null) {
                queryList.add(RangeQuery.of(fn -> fn.field("perClmdBty").gte(JsonData.of(perClmdBty)))._toQuery());
            } else {
                sortByPerClmd = Boolean.FALSE;
            }

            Query bQuery = BoolQuery.of(fn -> fn.must(queryList))._toQuery();
            final Query finalBQuery = bQuery;
            SortOptions sortOptions;
            if (sortByPerClmd) {
                //logger.info("Sorting search by _score");
                sortOptions = SortOptions.of(fn -> fn.field(FieldSort.of(fn2 -> fn2.field("perClmdBty").order(SortOrder.Desc))));
            } else {
                //logger.info("Sorting search by postedMs");
                sortOptions = SortOptions.of(fn -> fn.field(FieldSort.of(fn2 -> fn2.field("totalBounties").order(SortOrder.Desc))));
            }

            try {
                SearchResponse<UserMetric> response = client.search(t -> t.index(UserMetric.INDEX_NAME)
                        .query(finalBQuery)
                        .sort(sortOptions)
                        .from(0)
                        .size(MAX_USERS), UserMetric.class);
                HitsMetadata<UserMetric> searchHits = response.hits();

                for (Hit<UserMetric> hit : searchHits.hits()) {
                    UserMetric userMetric = hit.source();
                    if (aliasCache != null) {
                        userMetric.setAlias(aliasCache.getAlias(userMetric.getAccountId()));
                    }
                    userMetrics.add(userMetric);
                }
            } catch (Exception ex) {
                logger.error("Exception during user metric search! [" + ex.getMessage() + "]", ex);
            }
            logger.info("Found [" + userMetrics.size() + "] accounts for the search");
        }
        return userMetrics;
    }

    /**
     *
     * @param lastXDays
     * @param sortField
     * @param pageable
     * @return
     */
    public SyncArrayDeque<UserMetric> searchUserMetrics(final String lastXDays, final String sortField, PageRequest pageable) {
        SyncArrayDeque<UserMetric> userMetrics = new SyncArrayDeque<>(pageable.getPageSize());
        List<Query> queryList = new ArrayList<>();
        queryList.add(MatchQuery.of(fn -> fn.field("lastXDays").query(lastXDays))._toQuery());
        final SortOptions sortOptions;
        final Boolean trackScores;
        if (sortField == null) {
            trackScores = Boolean.FALSE;
            sortOptions = SortOptions.of(fn -> fn.field(FieldSort.of(fn2 -> fn2.field("_score").order(SortOrder.Desc))));
        } else {
            trackScores = Boolean.TRUE;
            queryList.add(RangeQuery.of(fn -> fn.field(sortField).gt(JsonData.of("0")))._toQuery());
            sortOptions = SortOptions.of(fn -> fn.field(FieldSort.of(fn2 -> fn2.field(sortField).order(SortOrder.Desc))));
        }
        final Query bQuery = BoolQuery.of(fn -> fn.must(queryList))._toQuery();
        try {
            SearchResponse<UserMetric> response = client.search(t -> t.index(UserMetric.INDEX_NAME)
                    .query(bQuery)
                    .trackScores(trackScores)
                    .sort(sortOptions)
                    .from((pageable.getPageNumber() - 1) * pageable.getPageSize())
                    .size(pageable.getPageSize()), UserMetric.class);
            HitsMetadata<UserMetric> searchHits = response.hits();
            for (Hit<UserMetric> hit : searchHits.hits()) {
                UserMetric userMetric = hit.source();
                if (aliasCache != null) {
                    userMetric.setAlias(aliasCache.getAlias(userMetric.getAccountId()));
                }
                if (trackScores) {
                    userMetric.setSearchScore(hit.score());
                }
                userMetrics.addLast(userMetric);
            }
        } catch (Exception ex) {
            logger.error("Exception during user metric search! [" + ex.getMessage() + "]", ex);
        }

        return userMetrics;
    }

    /**
     *
     * @param lastXDays
     * @param sortField
     * @return
     */
    public Long getTotalUserMetrics(final String lastXDays, final String sortField) {
        Long totalMetrics = 0L;
        List<Query> queryList = new ArrayList<>();
        queryList.add(MatchQuery.of(fn -> fn.field("lastXDays").query(lastXDays))._toQuery());
        if (sortField != null) {
            queryList.add(RangeQuery.of(fn -> fn.field(sortField).gt(JsonData.of("0")))._toQuery());
        }
        final Query bQuery = BoolQuery.of(fn -> fn.must(queryList))._toQuery();
        try {
            SearchResponse<UserMetric> response = client.search(t -> t.index(UserMetric.INDEX_NAME)
                    .query(bQuery)
                    .size(0), UserMetric.class);
            HitsMetadata<UserMetric> searchHits = response.hits();
            totalMetrics = searchHits.total().value();
        } catch (Exception ex) {
            logger.error("Exception during user metric total search! [" + ex.getMessage() + "]", ex);
        }

        return totalMetrics;
    }

}
