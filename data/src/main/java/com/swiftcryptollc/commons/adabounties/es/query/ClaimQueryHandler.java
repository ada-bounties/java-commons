package com.swiftcryptollc.commons.adabounties.es.query;

import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.elasticsearch._types.FieldSort;
import co.elastic.clients.elasticsearch._types.FieldValue;
import co.elastic.clients.elasticsearch._types.SortOptions;
import co.elastic.clients.elasticsearch._types.SortOrder;
import co.elastic.clients.elasticsearch._types.aggregations.Aggregate;
import co.elastic.clients.elasticsearch._types.aggregations.MultiBucketBase;
import co.elastic.clients.elasticsearch._types.aggregations.StringTermsBucket;
import co.elastic.clients.elasticsearch._types.query_dsl.BoolQuery;
import co.elastic.clients.elasticsearch._types.query_dsl.MatchQuery;
import co.elastic.clients.elasticsearch._types.query_dsl.Operator;
import co.elastic.clients.elasticsearch._types.query_dsl.Query;
import co.elastic.clients.elasticsearch._types.query_dsl.RangeQuery;
import co.elastic.clients.elasticsearch.core.BulkRequest;
import co.elastic.clients.elasticsearch.core.BulkResponse;
import co.elastic.clients.elasticsearch.core.SearchResponse;
import co.elastic.clients.elasticsearch.core.bulk.BulkResponseItem;
import co.elastic.clients.elasticsearch.core.search.Hit;
import co.elastic.clients.elasticsearch.core.search.HitsMetadata;
import co.elastic.clients.json.JsonData;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.swiftcryptollc.commons.adabounties.data.ClaimStatus;
import com.swiftcryptollc.commons.adabounties.es.ElasticsearchClientConfig;
import com.swiftcryptollc.commons.adabounties.es.ElasticsearchClientListener;
import com.swiftcryptollc.commons.adabounties.es.data.Alias;
import com.swiftcryptollc.commons.adabounties.redis.cache.BountyCache;
import com.swiftcryptollc.commons.adabounties.es.data.Bounty;
import com.swiftcryptollc.commons.adabounties.es.data.Claim;
import com.swiftcryptollc.commons.adabounties.es.data.SystemMetric;
import com.swiftcryptollc.commons.adabounties.es.data.UserMetric;
import com.swiftcryptollc.commons.adabounties.redis.cache.AliasCache;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
@Service
public final class ClaimQueryHandler implements ElasticsearchClientListener {

    @Autowired
    private BountyCache bountyCache;
    @Autowired
    private AliasCache aliasCache;

    @Autowired
    protected ElasticsearchClientConfig esClient;
    protected ElasticsearchClient client;
    protected UUID id;
    private final static Integer PAGE_SIZE = 500;
    protected final SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
    protected final static Logger logger = LoggerFactory.getLogger(ClaimQueryHandler.class);

    public ClaimQueryHandler() {
    }

    /**
     *
     * @param client
     */
    public ClaimQueryHandler(ElasticsearchClient client) {
        this.client = client;
    }

    /**
     * @return the id
     */
    @Override
    public UUID getId() {
        if (id == null) {
            id = UUID.randomUUID();
        }
        return id;
    }

    @Override
    public void setClient() {
        if (esClient != null) {
            client = esClient.elasticSearchClient();
        }
    }

    /**
     *
     */
    @PostConstruct
    public void init() {
        if (client == null) {
            ElasticsearchClientConfig.subscribe(this);
        }
    }

    /**
     *
     * @param accountId
     * @param status
     * @param pageable
     * @return
     */
    public Set<Claim> findClaims(String accountId, String status, PageRequest pageable) {
        return findClaims(accountId, null, status, pageable);
    }

    /**
     * @param cutoffMs
     * @param pageable
     * @return
     */
    public Set<Claim> findPendingClaims(Long cutoffMs, PageRequest pageable) {
        Set<Claim> claims = new HashSet<>();
        List<Query> queryList = new ArrayList<>();
        queryList.add(MatchQuery.of(fn -> fn.field("status").query(ClaimStatus.PENDING.label))._toQuery());
        queryList.add(RangeQuery.of(fn -> fn.field("updatedMs").lte(JsonData.of(cutoffMs)))._toQuery());
        Query bQuery = BoolQuery.of(fn -> fn.must(queryList))._toQuery();

        try {
            SearchResponse<Claim> response = client.search(t -> t.index(Claim.INDEX_NAME)
                    .query(bQuery)
                    .from((pageable.getPageNumber() - 1) * pageable.getPageSize())
                    .size(pageable.getPageSize()), Claim.class);
            HitsMetadata<Claim> searchHits = response.hits();

            for (Hit<Claim> hit : searchHits.hits()) {
                Claim claim = hit.source();
                if (bountyCache != null) {
                    String bountyTitle = bountyCache.getBountyTitle(claim.getBountyId());
                    claim.setBountyTitle(bountyTitle);
                }
                claims.add(claim);
            }
        } catch (Exception ex) {
            logger.error("Exception during claim search! [" + ex.getMessage() + "]", ex);
        }

        return claims;
    }

    /**
     *
     * @param bountyId
     */
    public Set<Claim> closeOtherClaims(String bountyId) {
        ObjectMapper mapper = new ObjectMapper();
        Set<Claim> updatedClaims = new HashSet<>();
        List<Query> queryList = new ArrayList<>();
        queryList.add(MatchQuery.of(fn -> fn.field("bountyId").query(bountyId))._toQuery());
        queryList.add(MatchQuery.of(fn -> fn.field("status").query("Open"))._toQuery());
        Query bQuery = BoolQuery.of(fn -> fn.must(queryList))._toQuery();

        do {
            try {
                BulkRequest.Builder br = new BulkRequest.Builder();
                SearchResponse<Claim> response = client.search(t -> t.index(Claim.INDEX_NAME)
                        .query(bQuery), Claim.class);
                HitsMetadata<Claim> searchHits = response.hits();
                if (!searchHits.hits().isEmpty()) {
                    for (Hit<Claim> hit : searchHits.hits()) {
                        Claim claim = hit.source();
                        claim.setStatus(ClaimStatus.CLOSED.label);
                        updatedClaims.add(claim);
                        ObjectNode node = mapper.convertValue(claim, ObjectNode.class);
                        node.put("_class", "com.swiftcryptollc.commons.adabounties.es.data.Claim");
                        br.operations(op -> op
                                .index(idx -> idx
                                .index(Claim.INDEX_NAME)
                                .id(claim.getId())
                                .document(node)
                                )
                        );
                    }
                    BulkResponse result = client.bulk(br.build());
                    if (result.errors()) {
                        logger.error("Bulk update errors encountered");
                        for (BulkResponseItem item : result.items()) {
                            if (item.error() != null) {
                                logger.error(item.error().reason());
                            }
                        }
                    }

                } else {
                    break;
                }
            } catch (Exception ex) {
                if (ex.getMessage().contains("all shards failed")) {
                    logger.error("Exception while closing other claims [" + ex.getMessage() + "]");
                } else {
                    logger.error("Exception while closing other claims [" + ex.getMessage() + "]", ex);
                }
                break;
            }
        } while (true);

        return updatedClaims;
    }

    /**
     *
     * @param accountId
     * @param bountyId
     * @param status
     * @param pageable
     * @return
     */
    public Set<Claim> findClaims(String accountId, String bountyId, String status, PageRequest pageable) {
        Set<Claim> claims = new HashSet<>();
        List<Query> queryList = new ArrayList<>();
        if (accountId != null) {
            queryList.add(MatchQuery.of(fn -> fn.field("accountId").query(accountId))._toQuery());
        }
        if (bountyId != null) {
            queryList.add(MatchQuery.of(fn -> fn.field("bountyId").query(bountyId))._toQuery());
        }
        if ((status != null) && (!status.equals("All"))) {
            queryList.add(MatchQuery.of(fn -> fn.field("status").query(status))._toQuery());
        }
        Query bQuery = BoolQuery.of(fn -> fn.must(queryList))._toQuery();
        SortOptions sortOptions = SortOptions.of(fn -> fn.field(FieldSort.of(fn2 -> fn2.field("postedMs").order(SortOrder.Desc))));

        try {
            SearchResponse<Claim> response = client.search(t -> t.index(Claim.INDEX_NAME)
                    .query(bQuery)
                    .sort(sortOptions)
                    .from((pageable.getPageNumber() - 1) * pageable.getPageSize())
                    .size(pageable.getPageSize()), Claim.class);
            HitsMetadata<Claim> searchHits = response.hits();

            for (Hit<Claim> hit : searchHits.hits()) {
                Claim claim = hit.source();
                String bountyTitle = bountyCache.getBountyTitle(claim.getBountyId());
                claim.setBountyTitle(bountyTitle);
                claims.add(claim);
            }
        } catch (Exception ex) {
            logger.error("Exception during claim search! [" + ex.getMessage() + "]", ex);
        }

        return claims;
    }

    /**
     *
     * @param accountId
     * @param pageable
     * @return
     */
    public Set<Claim> findClaimsToReview(String accountId, PageRequest pageable) {
        Set<Claim> claims = new HashSet<>();
        List<Query> queryList = new ArrayList<>();
        if (accountId != null) {
            queryList.add(MatchQuery.of(fn -> fn.field("btyAccountId").query(accountId))._toQuery());
            queryList.add(MatchQuery.of(fn -> fn.field("status").query(ClaimStatus.OPEN.label))._toQuery());
            Query bQuery = BoolQuery.of(fn -> fn.must(queryList))._toQuery();
            SortOptions sortOptions = SortOptions.of(fn -> fn.field(FieldSort.of(fn2 -> fn2.field("postedMs").order(SortOrder.Desc))));

            try {
                SearchResponse<Claim> response = client.search(t -> t.index(Claim.INDEX_NAME)
                        .query(bQuery)
                        .sort(sortOptions)
                        .from((pageable.getPageNumber() - 1) * pageable.getPageSize())
                        .size(pageable.getPageSize()), Claim.class);
                HitsMetadata<Claim> searchHits = response.hits();

                for (Hit<Claim> hit : searchHits.hits()) {
                    Claim claim = hit.source();
                    String bountyTitle = bountyCache.getBountyTitle(claim.getBountyId());
                    claim.setBountyTitle(bountyTitle);
                    Alias alias = aliasCache.getAlias(claim.getAccountId());
                    claim.setAlias(alias);
                    claims.add(claim);
                }
            } catch (Exception ex) {
                logger.error("Exception during claim search! [" + ex.getMessage() + "]", ex);
            }
        }
        return claims;
    }

    /**
     *
     * @param bounties
     * @return
     */
    public Map<String, Long> getClaimCountListForBounties(Set<Bounty> bounties) {
        Set<String> ids = new HashSet<>();
        bounties.forEach(bounty -> {
            ids.add(bounty.getId());
        });
        return getClaimCountList(ids);
    }

    /**
     * Get the number of claims for each bounty
     *
     * @param bountyIds
     * @return
     */
    public Map<String, Long> getClaimCountList(Set<String> bountyIds) {
        Map<String, Long> aggMap = new HashMap<>();
        List<Query> idQueryList = new ArrayList<>();
        for (String bountyId : bountyIds) {
            idQueryList.add(MatchQuery.of(fn -> fn.field("bountyId").query(bountyId))._toQuery());
        }
        Query idQuery = BoolQuery.of(fn -> fn.should(idQueryList))._toQuery();
        Query bQuery = BoolQuery.of(fn -> fn.must(idQuery))._toQuery();

        try {
            SearchResponse<Object> response = client.search(t
                    -> t.index(Claim.INDEX_NAME).query(bQuery)
                            .aggregations("bountyId", a -> a
                            .terms(h -> h
                            .field("bountyId")
                            )),
                    Object.class);
            Aggregate aggregate = response.aggregations().get("bountyId");
            List<StringTermsBucket> buckets = aggregate.sterms().buckets().array();
            Map<FieldValue, Long> mapAggsDirector = buckets.stream().collect(Collectors.toMap(StringTermsBucket::key, MultiBucketBase::docCount));

            for (Map.Entry<FieldValue, Long> entry : mapAggsDirector.entrySet()) {
                if (aggMap.containsKey(entry.getKey().stringValue())) {
                    // Because we are paging
                    Long curVal = aggMap.get(entry.getKey().stringValue());
                    curVal += entry.getValue();
                    aggMap.put(entry.getKey().stringValue(), curVal);
                } else {
                    aggMap.put(entry.getKey().stringValue(), entry.getValue());
                }
            }
        } catch (Exception ex) {
            if (ex.getMessage().contains("all shards failed")) {
                logger.error("Exception during getClaimCountList! [" + ex.getMessage() + "]");
            } else {
                logger.error("Exception during getClaimCountList! [" + ex.getMessage() + "]", ex);
            }
        }

        return aggMap;
    }

    /**
     * Find all claims for the given bounty ids
     *
     * @param bountyIds
     * @return
     */
    public Set<Claim> findAllByBountyId(Set<String> bountyIds) {
        Set<Claim> claims = new HashSet<>();
        List<Query> idQueryList = new ArrayList<>();
        if (bountyIds != null) {
            for (String id : bountyIds) {
                idQueryList.add(MatchQuery.of(fn -> fn.field("bountyId").query(id).operator(Operator.Or))._toQuery());
            }
        }
        Query bQuery = BoolQuery.of(fn -> fn.should(idQueryList))._toQuery();
        try {
            SearchResponse<Claim> response;
            HitsMetadata<Claim> searchHits;
            if (bountyIds != null) {
                response = client.search(t
                        -> t.index(Claim.INDEX_NAME)
                                .query(bQuery)
                                .size(bountyIds.size()),
                        Claim.class);
            } else {
                response = client.search(t
                        -> t.index(Claim.INDEX_NAME)
                                .query(bQuery),
                        Claim.class);
            }
            searchHits = response.hits();
            for (Hit<Claim> hit : searchHits.hits()) {
                claims.add(hit.source());
            }
        } catch (Exception ex) {
            logger.error("Exception during findAllById! [" + ex.getMessage() + "]");
        }
        return claims;
    }

    /**
     *
     * @param accountId
     * @param oldestTimeMs
     * @param userMetric
     */
    public void getStatusCountForClaimByAccount(String accountId, Long oldestTimeMs, UserMetric userMetric) {
        Aggregate aggregate = aggregateStatesForClaimByAccount(accountId, oldestTimeMs);
        userMetric.setRejectedClaims(0L);
        userMetric.setOpenClaims(0L);
        userMetric.setClosedClaims(0L);
        userMetric.setAcceptedClaims(0L);
        userMetric.setTotalClaims(0L);
        if (aggregate != null) {
            List<StringTermsBucket> buckets = aggregate.sterms().buckets().array();
            Long totalClaims = 0L;

            Map<FieldValue, Long> mapAggsDirector = buckets.stream().collect(Collectors.toMap(StringTermsBucket::key, MultiBucketBase::docCount));
            for (Map.Entry<FieldValue, Long> entry : mapAggsDirector.entrySet()) {
                String status = entry.getKey().stringValue();
                Long count = entry.getValue();
                if (count != null) {
                    switch (status) {
                        case "Rejected":
                            userMetric.setRejectedClaims(count);
                            totalClaims += count;
                            //logger.info("Account [" + accountId + "] Rejected Bounty [" + count + "]");
                            break;
                        case "Open":
                            userMetric.setOpenClaims(count);
                            totalClaims += count;
                            //logger.info("Account [" + accountId + "] Open Claims [" + count + "]");
                            break;
                        case "Closed":
                            userMetric.setClosedClaims(count);
                            totalClaims += count;
                            //logger.info("Account [" + accountId + "] Closed Claims [" + count + "]");
                            break;
                        case "Accepted":
                            userMetric.setAcceptedClaims(count);
                            totalClaims += count;
                            //logger.info("Account [" + accountId + "] Accepted Claims [" + count + "]");
                            break;
                    }
                }
            }
            if (totalClaims > 0L) {
                userMetric.setTotalClaims(totalClaims);
            }
        }
    }

    /**
     *
     * @param accountId
     * @param oldestTimeMs
     * @return
     */
    protected Aggregate aggregateStatesForClaimByAccount(String accountId, Long oldestTimeMs) {
        Aggregate aggregate = null;
        List<Query> queryList = new ArrayList<>();

        queryList.add(MatchQuery.of(fn -> fn.field("accountId").query(accountId))._toQuery());
        if (oldestTimeMs > 0L) {
            queryList.add(RangeQuery.of(fn -> fn.field("postedMs").gte(JsonData.of(oldestTimeMs)))._toQuery());
        }
        Query bQuery = BoolQuery.of(fn -> fn.must(queryList))._toQuery();
        //logger.info("Executing query [" + bQuery.toString() + "]");

        try {
            SearchResponse<Object> response = client.search(t
                    -> t.index(Claim.INDEX_NAME).query(bQuery)
                            .aggregations("status", a -> a
                            .terms(h -> h
                            .field("status"))),
                    Object.class);
            aggregate = response.aggregations().get("status");
        } catch (Exception ex) {
            logger.error("Exception during aggregateStatesForClaimByAccount! [" + ex.getMessage() + "]");
        }

        return aggregate;
    }

    /**
     *
     * @param lessThanTimeMs
     * @param greaterThanTimeMs
     * @param timeframeMs
     * @param systemMetric
     */
    public synchronized void getStatusCountForClaim(Long lessThanTimeMs, Long greaterThanTimeMs, Long timeframeMs, SystemMetric systemMetric) {
        String from = dateTimeFormat.format(new Date(greaterThanTimeMs));
        String to = dateTimeFormat.format(new Date(lessThanTimeMs));
        Aggregate aggregate = aggregateStatesForClaim(lessThanTimeMs, greaterThanTimeMs);
        systemMetric.setRejectedClaims(0L);
        systemMetric.setOpenClaims(0L);
        systemMetric.setClosedClaims(0L);
        systemMetric.setAcceptedClaims(0L);
        systemMetric.setTotalClaims(0L);
        if (aggregate != null) {
            List<StringTermsBucket> buckets = aggregate.sterms().buckets().array();
            Long totalClaims = 0L;
            Map<FieldValue, Long> mapAggsDirector = buckets.stream().collect(Collectors.toMap(StringTermsBucket::key, MultiBucketBase::docCount));
            for (Map.Entry<FieldValue, Long> entry : mapAggsDirector.entrySet()) {
                String status = entry.getKey().stringValue();
                Long count = entry.getValue();
                if (count != null) {
                    switch (status) {
                        case "Rejected":
                            systemMetric.setRejectedClaims(count);
                            totalClaims += count;
                            break;
                        case "Open":
                            systemMetric.setOpenClaims(count);
                            totalClaims += count;
                            break;
                        case "Closed":
                            systemMetric.setClosedClaims(count);
                            totalClaims += count;
                            break;
                        case "Accepted":
                            systemMetric.setAcceptedClaims(count);
                            totalClaims += count;
                            break;
                    }
                }
            }
            if (totalClaims > 0L) {
                systemMetric.setTotalClaims(totalClaims);
            }
            if (timeframeMs > 0) {
                logger.info("[" + systemMetric.getLastXDays() + "] Calculate the previous timeframe for percent increase/decrease");
                Long newOldestTimeMs = greaterThanTimeMs - timeframeMs;
                from = dateTimeFormat.format(new Date(newOldestTimeMs));
                to = dateTimeFormat.format(new Date(greaterThanTimeMs));
                aggregate = aggregateStatesForClaim(greaterThanTimeMs, newOldestTimeMs);
                if (aggregate != null) {
                    Long totalClaims2 = 0L;
                    List<StringTermsBucket> buckets2 = aggregate.sterms().buckets().array();

                    Map<FieldValue, Long> mapAggsDirector2 = buckets2.stream().collect(Collectors.toMap(StringTermsBucket::key, MultiBucketBase::docCount));
                    if (!mapAggsDirector2.isEmpty()) {
                        for (Map.Entry<FieldValue, Long> entry : mapAggsDirector2.entrySet()) {
                            String status = entry.getKey().stringValue();
                            Long count = entry.getValue();
                            if (count != null) {
                                switch (status) {
                                    case "Rejected":
                                        Long currentRejected = systemMetric.getRejectedClaims();
                                        logger.info("[" + systemMetric.getLastXDays() + "] Prev total rejected claims [" + from + "] to [" + to + "] [" + count + "]");
                                        totalClaims2 += count;
                                        if (count > 0L) {
                                            Double perGrowth = ((currentRejected / count) - 1d) * 100d;
                                            systemMetric.setGrowthRejectedClaims(perGrowth);
                                            logger.info("[" + systemMetric.getLastXDays() + "] Per growth rejected claims [" + from + "] to [" + to + "] [" + perGrowth + "%]");
                                        } else {
                                            Double perGrowth = currentRejected * 100d;
                                            systemMetric.setGrowthRejectedClaims(perGrowth);
                                            logger.info("[" + systemMetric.getLastXDays() + "] Per growth rejected claims [" + from + "] to [" + to + "] [" + perGrowth + "%]");
                                        }
                                        break;
                                    case "Open":
                                        Long currentOpen = systemMetric.getOpenClaims();
                                        logger.info("[" + systemMetric.getLastXDays() + "] Prev total open claims [" + from + "] to [" + to + "] [" + count + "]");
                                        totalClaims2 += count;
                                        if (count > 0L) {
                                            Double perGrowth = ((currentOpen / count) - 1d) * 100d;
                                            systemMetric.setGrowthOpenClaims(perGrowth);
                                            logger.info("[" + systemMetric.getLastXDays() + "] Per growth open claims [" + from + "] to [" + to + "] [" + perGrowth + "%]");
                                        } else {
                                            Double perGrowth = currentOpen * 100d;
                                            systemMetric.setGrowthOpenClaims(perGrowth);
                                            logger.info("[" + systemMetric.getLastXDays() + "] Per growth open claims [" + from + "] to [" + to + "] [" + perGrowth + "%]");
                                        }
                                        break;
                                    case "Closed":
                                        Long currentClosed = systemMetric.getClosedClaims();
                                        logger.info("[" + systemMetric.getLastXDays() + "] Prev total closed claims [" + from + "] to [" + to + "] [" + count + "]");
                                        totalClaims2 += count;
                                        if (count > 0L) {
                                            Double perGrowth = ((currentClosed / count) - 1d) * 100d;
                                            systemMetric.setGrowthClosedClaims(perGrowth);
                                            logger.info("[" + systemMetric.getLastXDays() + "] Per growth closed claims [" + from + "] to [" + to + "] [" + perGrowth + "%]");
                                        } else {
                                            Double perGrowth = currentClosed * 100d;
                                            systemMetric.setGrowthClosedClaims(perGrowth);
                                            logger.info("[" + systemMetric.getLastXDays() + "] Per growth closed claims [" + from + "] to [" + to + "] [" + perGrowth + "%]");
                                        }
                                        break;
                                    case "Accepted":
                                        Long currentAccepted = systemMetric.getAcceptedClaims();
                                        logger.info("[" + systemMetric.getLastXDays() + "] Prev total accepted claims [" + from + "] to [" + to + "] [" + count + "]");
                                        totalClaims2 += count;
                                        if (count > 0L) {
                                            Double perGrowth = ((currentAccepted / count) - 1d) * 100d;
                                            systemMetric.setGrowthAcceptedClaims(perGrowth);
                                            logger.info("[" + systemMetric.getLastXDays() + "] Per growth accepted claims [" + from + "] to [" + to + "] [" + perGrowth + "%]");
                                        } else {
                                            Double perGrowth = currentAccepted * 100d;
                                            systemMetric.setGrowthAcceptedClaims(perGrowth);
                                            logger.info("[" + systemMetric.getLastXDays() + "] Per growth accepted claims [" + from + "] to [" + to + "] [" + perGrowth + "%]");
                                        }
                                        break;
                                }
                            }
                        }
                    } else {
                        Long currentRejected = systemMetric.getRejectedClaims();
                        Double perGrowthRejected = currentRejected * 100d;
                        systemMetric.setGrowthRejectedClaims(perGrowthRejected);
                        logger.info("[" + systemMetric.getLastXDays() + "] Per growth rejected claims [" + from + "] to [" + to + "] [" + perGrowthRejected + "%]");
                        Long currentOpen = systemMetric.getOpenClaims();
                        Double perGrowthOpen = currentOpen * 100d;
                        systemMetric.setGrowthOpenClaims(perGrowthOpen);
                        logger.info("[" + systemMetric.getLastXDays() + "] Per growth open claims [" + from + "] to [" + to + "] [" + perGrowthOpen + "%]");
                        Long currentClosed = systemMetric.getClosedClaims();
                        Double perGrowthClosed = currentClosed * 100d;
                        systemMetric.setGrowthClosedClaims(perGrowthClosed);
                        logger.info("[" + systemMetric.getLastXDays() + "] Per growth closed claims [" + from + "] to [" + to + "] [" + perGrowthClosed + "%]");
                        Long currentAccepted = systemMetric.getAcceptedClaims();
                        Double perGrowthAccepted = currentAccepted * 100d;
                        systemMetric.setGrowthAcceptedClaims(perGrowthAccepted);
                        logger.info("[" + systemMetric.getLastXDays() + "] Per growth accepted claims [" + from + "] to [" + to + "] [" + perGrowthAccepted + "%]");
                    }
                    Long currentTotal = systemMetric.getTotalClaims();
                    if (totalClaims2 > 0L) {
                        Double perGrowth = ((currentTotal / totalClaims2) - 1d) * 100d;
                        systemMetric.setGrowthTotalClaims(perGrowth);
                        logger.info("[" + systemMetric.getLastXDays() + "] Per growth total claims [" + from + "] to [" + to + "] [" + perGrowth + "%]");
                    } else {
                        Double perGrowth = currentTotal * 100d;
                        systemMetric.setGrowthTotalClaims(perGrowth);
                        logger.info("[" + systemMetric.getLastXDays() + "] Per growth total claims [" + from + "] to [" + to + "] [" + perGrowth + "%]");
                    }
                }
            }
        }
    }

    /**
     * @param lessThanTimeMs
     * @param greaterThanTimeMs
     * @return
     */
    protected synchronized Aggregate aggregateStatesForClaim(Long lessThanTimeMs, Long greaterThanTimeMs) {
        Aggregate aggregate = null;
        if (greaterThanTimeMs > 0L) {
            List<Query> queryList = new ArrayList<>();
            queryList.add(RangeQuery.of(fn -> fn.field("postedMs").lt(JsonData.of(lessThanTimeMs)))._toQuery());
            queryList.add(RangeQuery.of(fn -> fn.field("postedMs").gte(JsonData.of(greaterThanTimeMs)))._toQuery());
            Query bQuery = BoolQuery.of(fn -> fn.must(queryList))._toQuery();
            //logger.info("Executing query [" + bQuery.toString() + "]");

            try {
                SearchResponse<Object> response = client.search(t
                        -> t.index(Claim.INDEX_NAME).query(bQuery)
                                .aggregations("status", a -> a
                                .terms(h -> h
                                .field("status"))),
                        Object.class);
                aggregate = response.aggregations().get("status");
            } catch (Exception ex) {
                logger.error("Exception during aggregateStatesForClaimByAccount! [" + ex.getMessage() + "]");
            }
        } else {
            try {
                SearchResponse<Object> response = client.search(t
                        -> t.index(Claim.INDEX_NAME)
                                .aggregations("status", a -> a
                                .terms(h -> h
                                .field("status"))),
                        Object.class);
                aggregate = response.aggregations().get("status");
            } catch (Exception ex) {
                logger.error("Exception during aggregateStatesForClaimByAccount! [" + ex.getMessage() + "]");
            }
        }

        return aggregate;
    }

    /**
     * @return the esClient
     */
    public ElasticsearchClientConfig getEsClient() {
        return esClient;
    }

    /**
     * @param esClient the esClient to set
     */
    public void setEsClient(ElasticsearchClientConfig esClient) {
        this.esClient = esClient;
    }

    /**
     * @return the client
     */
    public ElasticsearchClient getClient() {
        return client;
    }

    /**
     * @param client the client to set
     */
    public void setClient(ElasticsearchClient client) {
        this.client = client;
    }
}
