package com.swiftcryptollc.commons.adabounties.redis.cache;

import static java.util.Arrays.asList;
import org.springframework.boot.autoconfigure.cache.CacheManagerCustomizer;
import org.springframework.cache.concurrent.ConcurrentMapCacheManager;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
@Configuration
public class CacheConfiguration implements CacheManagerCustomizer<ConcurrentMapCacheManager> {

    @Override
    public void customize(ConcurrentMapCacheManager cacheManager) {
        cacheManager.setCacheNames(asList("token", "alias", "bounty", "bountyTitle", "bountyAccountId", "account", "commentMsg", "claim", "commentAccountId", "optionList"));
    }
}
