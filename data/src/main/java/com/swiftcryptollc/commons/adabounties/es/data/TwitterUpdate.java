package com.swiftcryptollc.commons.adabounties.es.data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
@Document(indexName = "twitter-update")
@JsonIgnoreProperties(ignoreUnknown = true)
public class TwitterUpdate implements Serializable {

    @Transient
    private static final long serialVersionUID = 100035L;

    // the tweet ID
    @Id
    protected String id;
    @Field(type = FieldType.Keyword)
    protected String bountyId;
    @Field(type = FieldType.Keyword)
    protected String claimId;
    @Field(type = FieldType.Keyword)
    protected String leaderboard;
    protected Long updateTimeMs;
    @Transient
    public final transient static String INDEX_NAME = "twitter-update";

    public TwitterUpdate() {

    }

    /**
     * @return the bountyId
     */
    public String getBountyId() {
        return bountyId;
    }

    /**
     * @param bountyId the bountyId to set
     */
    public void setBountyId(String bountyId) {
        this.bountyId = bountyId;
    }

    /**
     * @return the claimId
     */
    public String getClaimId() {
        return claimId;
    }

    /**
     * @param claimId the claimId to set
     */
    public void setClaimId(String claimId) {
        this.claimId = claimId;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the leaderboard
     */
    public String getLeaderboard() {
        return leaderboard;
    }

    /**
     * @param leaderboard the leaderboard to set
     */
    public void setLeaderboard(String leaderboard) {
        this.leaderboard = leaderboard;
    }

    /**
     * @return the updateTimeMs
     */
    public Long getUpdateTimeMs() {
        return updateTimeMs;
    }

    /**
     * @param updateTimeMs the updateTimeMs to set
     */
    public void setUpdateTimeMs(Long updateTimeMs) {
        this.updateTimeMs = updateTimeMs;
    }
}
