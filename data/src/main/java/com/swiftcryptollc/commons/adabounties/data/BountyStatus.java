package com.swiftcryptollc.commons.adabounties.data;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
public enum BountyStatus {
    All("All"),
    OPEN("Open"),
    CLAIMED("Claimed"),
    PENDING("Pending"),
    DRAFT("Draft"),
    EXPIRED("Expired"),
    CLOSED("Closed"),
    STALE("Stale");

    public final String label;

    private BountyStatus(String label) {
        this.label = label;
    }

    public static BountyStatus valueOfLabel(String label) {
        for (BountyStatus type : values()) {
            if (type.label.equalsIgnoreCase(label)) {
                return type;
            }
        }
        return null;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return label;
    }

}
