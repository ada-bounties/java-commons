package com.swiftcryptollc.commons.adabounties.redis.cache;

import com.swiftcryptollc.commons.adabounties.es.data.Comment;
import com.swiftcryptollc.commons.adabounties.es.repository.CommentRepository;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
@Service
public class CommentCache {

    @Autowired
    protected CommentRepository commentRepo;

    protected final static Logger logger = LoggerFactory.getLogger(CommentCache.class);

    public CommentCache() {

    }

    /**
     *
     * @param comment
     * @return
     */
    @CachePut(value = "commentMsg", key = "#comment.id")
    public String updateCachedCommentMsg(Comment comment) {
        return comment.getMsg();
    }

    /**
     *
     * @param id
     * @return
     */
    @Cacheable(value = "commentMsg", key = "#id")
    public String getCommentMsg(String id) {
        if (id == null) {
            throw new IllegalArgumentException("Id cannot be null");
        }
        Optional<Comment> commentOpt = commentRepo.findById(id);
        if (commentOpt.isPresent()) {
            return commentOpt.get().getMsg();
        } else {
            logger.warn("Comment [" + id + "] does not exist");
        }
        return null;
    }

    /**
     *
     * @param id
     * @return
     */
    @Cacheable(value = "commentAccountId", key = "#id")
    public String getCommentAccountId(String id) {
        if (id == null) {
            throw new IllegalArgumentException("Id cannot be null");
        }
        Optional<Comment> commentOpt = commentRepo.findById(id);
        if (commentOpt.isPresent()) {
            return commentOpt.get().getAccountId();
        }
        return null;
    }
}
