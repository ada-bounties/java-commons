package com.swiftcryptollc.commons.adabounties.data;

import java.util.UUID;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
public class ServerMessage {

    private String id;
    private String type;
    private String action;
    private UUID sessionId;
    private String object;
    private String token;

    public ServerMessage() {

    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    public ServerMessageType getServerMessageType() {
        return ServerMessageType.valueOfLabel(this.type);
    }

    /**
     * @return the action
     */
    public String getAction() {
        return action;
    }

    /**
     * @param action the action to set
     */
    public void setAction(String action) {
        this.action = action;
    }

    public ServerMessageAction getServerMessageAction() {
        return ServerMessageAction.valueOfLabel(this.action);
    }

    /**
     * @return the object
     */
    public String getObject() {
        return object;
    }

    /**
     * @param object the object to set
     */
    public void setObject(String object) {
        this.object = object;
    }

    /**
     * @return the sessionId
     */
    public UUID getSessionId() {
        return sessionId;
    }

    /**
     * @param sessionId the sessionId to set
     */
    public void setSessionId(UUID sessionId) {
        this.sessionId = sessionId;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the token
     */
    public String getToken() {
        return token;
    }

    /**
     * @param token the token to set
     */
    public void setToken(String token) {
        this.token = token;
    }
}
