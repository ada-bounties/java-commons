package com.swiftcryptollc.commons.adabounties.data;

import com.swiftcryptollc.commons.adabounties.es.data.Option;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.data.annotation.Transient;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
public class OptionList implements Serializable {

    @Transient
    private static final long serialVersionUID = 100007L;

    private String accountId;
    private List<Option> options;
    private Map<String, String> optionMap;

    public OptionList() {

    }

    /**
     * @return the accountId
     */
    public String getAccountId() {
        return accountId;
    }

    /**
     * @param accountId the accountId to set
     */
    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    /**
     * @return the options
     */
    public List<Option> getOptions() {
        return options;
    }

    /**
     * @param options the options to set
     */
    public void setOptions(List<Option> options) {
        this.options = options;
    }

    /**
     *
     */
    public void clearMap() {
        optionMap = null;
    }

    /**
     *
     */
    public void setMap() {
        optionMap = new HashMap<>();
        for (Option option : options) {
            optionMap.put(option.getKey(), option.getValue());
        }
    }

    /**
     *
     * @param key
     * @return
     */
    public String getOptionValue(String key) {
        if (optionMap == null) {
            setMap();
        }

        return optionMap.get(key);
    }
}
