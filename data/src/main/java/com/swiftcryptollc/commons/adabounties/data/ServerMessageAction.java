package com.swiftcryptollc.commons.adabounties.data;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
public enum ServerMessageAction {
    LOGIN("login"),
    NEW("new"),
    NEWALL("newall"),
    RESENDVERIFY("rsdv"),
    RESUBMIT("resub"),
    UPDATE("upd"),
    REQUEST("req"),
    SEARCH("srch"),
    FINDBYID("id"),
    LISTBYID("lid"),
    TX("tx"),
    SIGN("sign"),
    LIST("list"),
    FUZZY("fuz"),
    BOOKMARKS("bkmrk"),
    AGGRAGATE("agg"),
    CLOSE("cls"),
    BYBOUNTY("bbty"),
    DELETE("del"),
    DELETEALL("delall"),
    READ("rd"),
    UNREAD("unr"),
    ALLREAD("ard"),
    ALLUNREAD("aunr");

    public final String label;

    private ServerMessageAction(String label) {
        this.label = label;
    }

    public static ServerMessageAction valueOfLabel(String label) {
        for (ServerMessageAction type : values()) {
            if (type.label.equalsIgnoreCase(label)) {
                return type;
            }
        }
        return null;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return label;
    }

}
