package com.swiftcryptollc.commons.adabounties.es.repository;

import com.swiftcryptollc.commons.adabounties.es.data.UserAgent;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
@Repository
public interface UserAgentRepository extends ElasticsearchRepository<UserAgent, String> {

}
