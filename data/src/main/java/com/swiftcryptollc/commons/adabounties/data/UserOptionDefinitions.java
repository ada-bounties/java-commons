package com.swiftcryptollc.commons.adabounties.data;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
public enum UserOptionDefinitions {

    BOUNTY_UPDATE("btyupd"),
    COMMENT("cmt"),
    REACT("rct"),
    CLAIM_UPDATE("clmupd"),
    USE_DARK_THEME("dark");

    public final String label;

    private UserOptionDefinitions(String label) {
        this.label = label;
    }

    public static UserOptionDefinitions valueOfLabel(String label) {
        for (UserOptionDefinitions type : values()) {
            if (type.label.equalsIgnoreCase(label)) {
                return type;
            }
        }
        return null;
    }

    /**
     *
     * @return
     */
    public String toString() {
        return label;
    }
}
