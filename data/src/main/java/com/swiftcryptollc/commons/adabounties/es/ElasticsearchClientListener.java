package com.swiftcryptollc.commons.adabounties.es;

import java.util.UUID;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
public interface ElasticsearchClientListener {

    public UUID getId();

    public void setClient();
}
