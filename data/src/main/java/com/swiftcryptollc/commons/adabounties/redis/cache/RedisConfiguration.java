package com.swiftcryptollc.commons.adabounties.redis.cache;

import com.swiftcryptollc.commons.adabounties.aws.secrets.SecretsManager;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.JdkSerializationRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.stereotype.Component;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
@Component
public class RedisConfiguration {

    protected static Boolean initialized = Boolean.FALSE;
    protected static String password;
    protected static String host;
    protected static Integer port;

    protected final static Logger logger = LoggerFactory.getLogger(RedisConfiguration.class);

    static {
        do {
            Map<String, String> secretMap = SecretsManager.getSecretMap();
            if (!secretMap.isEmpty()) {
                updateStaticSecrets(secretMap);
                initialized = Boolean.TRUE;
            } else {
                try {
                    Thread.sleep(5000);
                } catch (Exception e) {
                }
            }
        } while (!initialized);
        logger.info("RedisConfiguration initialized");
    }

    @Bean
    public JedisConnectionFactory redisConnectionFactory() {
        while (!initialized) {
            try {
                Thread.sleep(1000);
            } catch (Exception e) {
            }
        }
        RedisStandaloneConfiguration redisConfiguration = new RedisStandaloneConfiguration(host, port);
        redisConfiguration.setPassword(password);
        return new JedisConnectionFactory(redisConfiguration);
    }

    @Bean
    public RedisTemplate<String, Object> redisTemplate() {
        RedisTemplate<String, Object> template = new RedisTemplate<>();
        template.setConnectionFactory(redisConnectionFactory());
        template.setKeySerializer(new StringRedisSerializer());
        template.setHashKeySerializer(new StringRedisSerializer());
        template.setHashKeySerializer(new JdkSerializationRedisSerializer());
        template.setValueSerializer(new GenericJackson2JsonRedisSerializer());
        template.setEnableTransactionSupport(true);
        template.afterPropertiesSet();
        return template;
    }

    /**
     *
     * @param secretMap
     */
    public static void updateStaticSecrets(Map<String, String> secretMap) {
        logger.info("Updating secrets");
        try {
            if (secretMap.containsKey("redis_password")) {
                password = secretMap.get("redis_password");
            }
        } catch (Exception ex) {
            password = "";
            logger.error("Could not read the redis_password from the SecretsManager [" + ex.getMessage() + "]");
        }
        try {
            if (secretMap.containsKey("redis_host")) {
                host = secretMap.get("redis_host");
            }
        } catch (Exception ex) {
            host = "127.0.0.1";
            logger.error("Could not read the redis_host from the SecretsManager [" + ex.getMessage() + "]");
        }
        try {
            if (secretMap.containsKey("redis_port")) {
                port = Integer.valueOf(secretMap.get("redis_port"));
            }
        } catch (Exception ex) {
            logger.error("Could not read the redis_port from the SecretsManager [" + ex.getMessage() + "]");
            port = 6380;
        }
        logger.info("Using redis host [" + host + "]");
        logger.info("Using redis port [" + port + "]");
    }
}
