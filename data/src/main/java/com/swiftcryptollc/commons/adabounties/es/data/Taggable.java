package com.swiftcryptollc.commons.adabounties.es.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import org.springframework.data.annotation.Transient;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
public abstract class Taggable implements Serializable {

    @Transient
    private static final long serialVersionUID = 100025L;

    @Field
    protected String category;
    @Field(type = FieldType.Keyword)
    protected List<String> tags;
    @Field(type = FieldType.Keyword)
    protected List<String> lcTags;
    @Transient
    protected Double searchScore;

    public Taggable() {

    }

    /**
     * @return the tags
     */
    public List<String> getTags() {
        return tags;
    }

    /**
     * @param tags the tags to set
     */
    public void setTags(List<String> tags) {
        this.tags = tags;
        this.setLcTags(this.tags);
    }

    /**
     * @return the tags
     */
    public List<String> getLcTags() {
        return lcTags;
    }

    /**
     * @param lcTags the lcTags to set
     */
    public void setLcTags(List<String> lcTags) {
        if (lcTags != null) {
            if (this.lcTags == null) {
                this.lcTags = new ArrayList<>();
            } else {
                this.lcTags.clear();
            }
            for (String tag : lcTags) {
                this.lcTags.add(tag.toLowerCase());
            }
        }
    }

    /**
     * @return the weight
     */
    public Double getSearchScore() {
        return searchScore;
    }

    /**
     * @param searchScore the weight to set
     */
    public void setSearchScore(Double searchScore) {
        this.searchScore = searchScore;
    }

    /**
     * @return the category
     */
    public String getCategory() {
        return category;
    }

    /**
     * @param category the category to set
     */
    public void setCategory(String category) {
        this.category = category;
    }

}
