package com.swiftcryptollc.commons.adabounties.es.query;

import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.elasticsearch._types.FieldSort;
import co.elastic.clients.elasticsearch._types.SortOptions;
import co.elastic.clients.elasticsearch._types.SortOrder;
import co.elastic.clients.elasticsearch._types.query_dsl.BoolQuery;
import co.elastic.clients.elasticsearch._types.query_dsl.MatchQuery;
import co.elastic.clients.elasticsearch._types.query_dsl.Query;
import co.elastic.clients.elasticsearch._types.query_dsl.RangeQuery;
import co.elastic.clients.elasticsearch.core.SearchResponse;
import co.elastic.clients.elasticsearch.core.search.Hit;
import co.elastic.clients.elasticsearch.core.search.HitsMetadata;
import co.elastic.clients.json.JsonData;
import com.swiftcryptollc.commons.adabounties.es.ElasticsearchClientConfig;
import com.swiftcryptollc.commons.adabounties.es.ElasticsearchClientListener;
import com.swiftcryptollc.commons.adabounties.redis.cache.AliasCache;
import com.swiftcryptollc.commons.adabounties.es.data.TokenBountyTotal;
import com.swiftcryptollc.commons.adabounties.utilities.data.SyncArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import javax.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
@Service
public final class TBTQueryHandler implements ElasticsearchClientListener {

    @Autowired
    private AliasCache aliasCache;

    @Autowired
    protected ElasticsearchClientConfig esClient;
    protected ElasticsearchClient client;
    protected UUID id;
    protected final static Logger logger = LoggerFactory.getLogger(TBTQueryHandler.class);

    public TBTQueryHandler() {
    }

    /**
     * @return the id
     */
    @Override
    public UUID getId() {
        if (id == null) {
            id = UUID.randomUUID();
        }
        return id;
    }

    @Override
    public void setClient() {
        client = esClient.elasticSearchClient();
    }

    /**
     * @param client the client to set
     */
    public void setClient(ElasticsearchClient client) {
        this.client = client;
    }

    /**
     *
     */
    @PostConstruct
    public void init() {
        if (client == null) {
            ElasticsearchClientConfig.subscribe(this);
        }
    }

    /**
     *
     * @param accountId
     * @param token
     * @param lastXDays
     * @param pageable
     * @return
     */
    public SyncArrayDeque<TokenBountyTotal> findTokenBountyTotals(String accountId, String token, String lastXDays, PageRequest pageable) {
        SyncArrayDeque<TokenBountyTotal> tbts = new SyncArrayDeque<>(pageable.getPageSize());
        List<Query> queryList = new ArrayList<>();
        if (accountId != null) {
            queryList.add(MatchQuery.of(fn -> fn.field("accountId").query(accountId))._toQuery());
        } else {
            queryList.add(RangeQuery.of(fn -> fn.field("amount").gt(JsonData.of("0")))._toQuery());
        }
        if (token != null) {
            queryList.add(MatchQuery.of(fn -> fn.field("token").query(token))._toQuery());
        }
        if (lastXDays != null) {
            queryList.add(MatchQuery.of(fn -> fn.field("lastXDays").query(lastXDays))._toQuery());
        }

        Query bQuery = BoolQuery.of(fn -> fn.must(queryList))._toQuery();
        //logger.info("Executing [" + bQuery.toString() + "]");
        SortOptions sortOptions = SortOptions.of(fn -> fn.field(FieldSort.of(fn2 -> fn2.field("amount").order(SortOrder.Desc))));

        try {
            SearchResponse<TokenBountyTotal> response = client.search(t -> t.index(TokenBountyTotal.INDEX_NAME)
                    .query(bQuery)
                    .sort(sortOptions)
                    .from((pageable.getPageNumber() - 1) * pageable.getPageSize())
                    .size(pageable.getPageSize()), TokenBountyTotal.class);
            HitsMetadata<TokenBountyTotal> searchHits = response.hits();

            for (Hit<TokenBountyTotal> hit : searchHits.hits()) {
                TokenBountyTotal tbt = hit.source();
                if (aliasCache != null) {
                    tbt.setAlias(aliasCache.getAlias(tbt.getAccountId()));
                }
                tbts.addLast(tbt);
            }
        } catch (Exception ex) {
            logger.error("Exception during findTokenBountyTotals! [" + ex.getMessage() + "]", ex);
        }

        return tbts;
    }

}
