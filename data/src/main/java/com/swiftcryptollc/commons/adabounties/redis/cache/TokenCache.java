package com.swiftcryptollc.commons.adabounties.redis.cache;

import com.swiftcryptollc.commons.adabounties.es.data.Token;
import com.swiftcryptollc.commons.adabounties.es.repository.TokenRepository;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
@Service
public class TokenCache {

    @Autowired
    private TokenRepository tokenRepo;

    protected final static Logger logger = LoggerFactory.getLogger(TokenCache.class);

    public TokenCache() {

    }

    /**
     *
     * @param subject
     * @return
     */
    @Cacheable(value = "token", key = "#subject")
    public Token getToken(String subject) {
        if (subject == null) {
            throw new IllegalArgumentException("Id cannot be null");
        }
        Optional<Token> tokenOpt = tokenRepo.findById(subject);
        if (tokenOpt.isPresent()) {
            return tokenOpt.get();
        } else {
            logger.warn("Token [" + subject + "] does not exist");
        }

        return null;
    }

}
