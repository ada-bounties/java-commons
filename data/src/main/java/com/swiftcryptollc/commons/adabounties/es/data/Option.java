package com.swiftcryptollc.commons.adabounties.es.data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
@Document(indexName = "option")
@JsonIgnoreProperties({"_class", "_id"})
public class Option implements Serializable {

    @Transient
    private static final long serialVersionUID = 100006L;

    @Id
    private String id;
    @Field(type = FieldType.Keyword)
    private String accountId;
    @Field
    private String key;
    @Field
    private String value;

    public Option() {
    }

    /**
     *
     * @param accountId
     * @param key
     * @param value
     */
    public Option(String accountId, String key, String value) {
        this.accountId = accountId;
        this.key = key;
        this.value = value;
        this.id = accountId.concat("-").concat(key);
    }

    /**
     * @return the id
     */
    public String getId() {
        if (id == null) {
            id = accountId.concat("-").concat(key);
        }
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the accountId
     */
    public String getAccountId() {
        return accountId;
    }

    /**
     * @param accountId the accountId to set
     */
    public void setAccountId(String accountId) {
        this.accountId = accountId;
        if ((id == null) && (key != null)) {
            id = this.accountId.concat("-").concat(key);
        }
    }

    /**
     * @return the key
     */
    public String getKey() {
        return key;
    }

    /**
     * @param key the key to set
     */
    public void setKey(String key) {
        this.key = key;
        if ((id == null) && (accountId != null)) {
            id = accountId.concat("-").concat(this.key);
        }
    }

    /**
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(String value) {
        this.value = value;
    }
}
