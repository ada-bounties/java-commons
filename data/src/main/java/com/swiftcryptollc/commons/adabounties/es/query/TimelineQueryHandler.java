package com.swiftcryptollc.commons.adabounties.es.query;

import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.elasticsearch._types.FieldSort;
import co.elastic.clients.elasticsearch._types.SortOptions;
import co.elastic.clients.elasticsearch._types.SortOrder;
import co.elastic.clients.elasticsearch._types.query_dsl.BoolQuery;
import co.elastic.clients.elasticsearch._types.query_dsl.Query;
import co.elastic.clients.elasticsearch._types.query_dsl.RangeQuery;
import co.elastic.clients.elasticsearch.core.SearchResponse;
import co.elastic.clients.elasticsearch.core.search.Hit;
import co.elastic.clients.elasticsearch.core.search.HitsMetadata;
import co.elastic.clients.json.JsonData;
import com.swiftcryptollc.commons.adabounties.es.ElasticsearchClientConfig;
import com.swiftcryptollc.commons.adabounties.es.ElasticsearchClientListener;
import com.swiftcryptollc.commons.adabounties.es.data.Timeline;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import javax.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
@Service
public final class TimelineQueryHandler implements ElasticsearchClientListener {

    @Autowired
    protected ElasticsearchClientConfig esClient;
    protected ElasticsearchClient client;
    protected UUID id;
    protected final static Logger logger = LoggerFactory.getLogger(TimelineQueryHandler.class);

    public TimelineQueryHandler() {
    }

    /**
     * @return the id
     */
    @Override
    public UUID getId() {
        if (id == null) {
            id = UUID.randomUUID();
        }
        return id;
    }

    @Override
    public void setClient() {
        client = esClient.elasticSearchClient();
    }

    /**
     *
     */
    @PostConstruct
    public void init() {
        if (client == null) {
            ElasticsearchClientConfig.subscribe(this);
        }
    }

    /**
     *
     * @param startMs
     * @param endMs
     * @return
     */
    public Set<Timeline> findTimeline(Long startMs, Long endMs, Pageable pageable) {
        Set<Timeline> timelines = new HashSet<>();
        List<Query> queryList = new ArrayList<>();

        if (startMs != null) {
            queryList.add(RangeQuery.of(fn -> fn.field("updateMs").gte(JsonData.of(startMs)))._toQuery());
        }
        if (endMs == null) {
            endMs = new Date().getTime();
        }
        final Long endMsFinal = endMs;
        queryList.add(RangeQuery.of(fn -> fn.field("updateMs").lte(JsonData.of(endMsFinal)))._toQuery());

        Query bQuery = BoolQuery.of(fn -> fn.must(queryList))._toQuery();

        SortOptions sortOptions = SortOptions.of(fn -> fn.field(FieldSort.of(fn2 -> fn2.field("updateMs").order(SortOrder.Desc))));
        try {
            SearchResponse<Timeline> response = client.search(t
                    -> t.index(Timeline.INDEX_NAME)
                            .query(bQuery)
                            .sort(sortOptions)
                            .from((pageable.getPageNumber() - 1) * pageable.getPageSize())
                            .size(pageable.getPageSize()), Timeline.class);
            HitsMetadata<Timeline> searchHits = response.hits();

            for (Hit<Timeline> hit : searchHits.hits()) {
                Timeline timeline = hit.source();
                timelines.add(timeline);
            }
        } catch (Exception ex) {
            logger.error("Exception during timeline search! [" + ex.getMessage() + "]", ex);
        }

        return timelines;
    }

}
