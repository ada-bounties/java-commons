package com.swiftcryptollc.commons.adabounties.es.data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.Date;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
@Document(indexName = "bounty-dates")
@JsonIgnoreProperties({"_class", "_id"})
public class BountyDates implements Serializable {

    @Transient
    private static final long serialVersionUID = 100011L;

    @Id
    private String id;
    @Field
    protected Long lastIntMs;
    @Field
    protected Long expireMs;
    @Transient
    public final transient static String INDEX_NAME = "bounty-dates";

    /**
     *
     */
    public BountyDates() {
    }

    /**
     *
     * @param bountyId
     */
    public BountyDates(String bountyId) {
        this.id = bountyId;
        this.lastIntMs = new Date().getTime();
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the lastIntMs
     */
    public Long getLastIntMs() {
        return lastIntMs;
    }

    /**
     * @param lastIntMs the lastIntMs to set
     */
    public void setLastIntMs(Long lastIntMs) {
        this.lastIntMs = lastIntMs;
    }

    /**
     * @return the expireMs
     */
    public Long getExpireMs() {
        return expireMs;
    }

    /**
     * @param expireMs the expireMs to set
     */
    public void setExpireMs(Long expireMs) {
        this.expireMs = expireMs;
    }
}
