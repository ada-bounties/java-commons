package com.swiftcryptollc.commons.adabounties.es.query;

import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.elasticsearch._types.FieldSort;
import co.elastic.clients.elasticsearch._types.SortOptions;
import co.elastic.clients.elasticsearch._types.SortOrder;
import co.elastic.clients.elasticsearch._types.query_dsl.BoolQuery;
import co.elastic.clients.elasticsearch._types.query_dsl.MatchQuery;
import co.elastic.clients.elasticsearch._types.query_dsl.Query;
import co.elastic.clients.elasticsearch._types.query_dsl.RangeQuery;
import co.elastic.clients.elasticsearch.core.SearchResponse;
import co.elastic.clients.elasticsearch.core.search.Hit;
import co.elastic.clients.elasticsearch.core.search.HitsMetadata;
import co.elastic.clients.json.JsonData;
import com.swiftcryptollc.commons.adabounties.es.ElasticsearchClientConfig;
import com.swiftcryptollc.commons.adabounties.es.ElasticsearchClientListener;
import com.swiftcryptollc.commons.adabounties.es.data.Accounting;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import javax.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
@Service
public final class AccountingQueryHandler implements ElasticsearchClientListener {

    @Autowired
    protected ElasticsearchClientConfig esClient;
    protected ElasticsearchClient client;
    protected UUID id;
    protected final static Logger logger = LoggerFactory.getLogger(AccountingQueryHandler.class);

    public AccountingQueryHandler() {
    }

    /**
     * @return the id
     */
    @Override
    public UUID getId() {
        if (id == null) {
            id = UUID.randomUUID();
        }
        return id;
    }

    @Override
    public void setClient() {
        client = esClient.elasticSearchClient();
    }

    /**
     * @param client the client to set
     */
    public void setClient(ElasticsearchClient client) {
        this.client = client;
    }

    /**
     *
     */
    @PostConstruct
    public void init() {
        if (client == null) {
            ElasticsearchClientConfig.subscribe(this);
        }
    }

    /**
     * Accounts that have paid out bounties
     *
     * @param accountId
     * @param token
     * @param startTimeMs
     * @param fromTimeMs
     * @param filterADABounties
     * @param pageable
     * @return
     */
    public Set<Accounting> findBountyAccountingRecords(String accountId, String token, Long startTimeMs, Long fromTimeMs, Boolean filterADABounties, PageRequest pageable) {
        List<Query> queryList = new ArrayList<>();
        List<Query> notQueryList = new ArrayList<>();
        if (accountId != null) {
            queryList.add(MatchQuery.of(fn -> fn.field("srcAccountId").query(accountId))._toQuery());
        }
        if (filterADABounties) {
            notQueryList.add(MatchQuery.of(fn -> fn.field("destAccountId").query(Accounting.ADABOUNTIES_ACCOUNT_ID))._toQuery());
        }
        return findAccountingRecords(queryList, notQueryList, token, startTimeMs, fromTimeMs, pageable);
    }

    /**
     * Accounts that have received bounty rewards
     *
     * @param accountId
     * @param token
     * @param startTimeMs
     * @param fromTimeMs
     * @param pageable
     * @return
     */
    public Set<Accounting> findClaimAccountingRecords(String accountId, String token, Long startTimeMs, Long fromTimeMs, PageRequest pageable) {
        List<Query> queryList = new ArrayList<>();
        List<Query> notQueryList = new ArrayList<>();
        if (accountId != null) {
            queryList.add(MatchQuery.of(fn -> fn.field("destAccountId").query(accountId))._toQuery());
        }
        return findAccountingRecords(queryList, notQueryList, token, startTimeMs, fromTimeMs, pageable);
    }

    /**
     *
     * @param queryList
     * @param notQueryList
     * @param token
     * @param startTimeMs
     * @param fromTimeMs
     * @param pageable
     * @return
     */
    private Set<Accounting> findAccountingRecords(List<Query> queryList, List<Query> notQueryList, String token, Long startTimeMs, Long fromTimeMs, PageRequest pageable) {
        Set<Accounting> accountingSet = new HashSet<>();
        queryList.add(RangeQuery.of(fn -> fn.field("dateTimeMs").lt(JsonData.of(startTimeMs)))._toQuery());
        queryList.add(RangeQuery.of(fn -> fn.field("dateTimeMs").gte(JsonData.of(fromTimeMs)))._toQuery());

        if (token != null) {
            queryList.add(MatchQuery.of(fn -> fn.field("token").query(token))._toQuery());
        }

        Query bQuery;
        if (notQueryList.isEmpty()) {
            bQuery = BoolQuery.of(fn -> fn.must(queryList))._toQuery();
        } else {
            bQuery = BoolQuery.of(fn -> fn.must(queryList).mustNot(notQueryList))._toQuery();
        }
        SortOptions sortOptions = SortOptions.of(fn -> fn.field(FieldSort.of(fn2 -> fn2.field("amount").order(SortOrder.Desc))));

        try {
            SearchResponse<Accounting> response = client.search(t -> t.index(Accounting.INDEX_NAME)
                    .query(bQuery)
                    .sort(sortOptions)
                    .from((pageable.getPageNumber() - 1) * pageable.getPageSize())
                    .size(pageable.getPageSize()), Accounting.class);
            HitsMetadata<Accounting> searchHits = response.hits();

            for (Hit<Accounting> hit : searchHits.hits()) {
                Accounting accounting = hit.source();
                accountingSet.add(accounting);
            }
        } catch (Exception ex) {
            logger.error("Exception during findAccountingRecords! [" + ex.getMessage() + "]", ex);
        }

        return accountingSet;
    }

}
