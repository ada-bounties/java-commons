package com.swiftcryptollc.commons.adabounties.es.query;

import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.elasticsearch._types.FieldSort;
import co.elastic.clients.elasticsearch._types.SortOptions;
import co.elastic.clients.elasticsearch._types.SortOrder;
import co.elastic.clients.elasticsearch._types.query_dsl.BoolQuery;
import co.elastic.clients.elasticsearch._types.query_dsl.MatchQuery;
import co.elastic.clients.elasticsearch._types.query_dsl.Query;
import co.elastic.clients.elasticsearch._types.query_dsl.RangeQuery;
import co.elastic.clients.elasticsearch.core.BulkRequest;
import co.elastic.clients.elasticsearch.core.BulkResponse;
import co.elastic.clients.elasticsearch.core.SearchResponse;
import co.elastic.clients.elasticsearch.core.bulk.BulkResponseItem;
import co.elastic.clients.elasticsearch.core.search.Hit;
import co.elastic.clients.elasticsearch.core.search.HitsMetadata;
import co.elastic.clients.json.JsonData;
import com.swiftcryptollc.commons.adabounties.es.ElasticsearchClientConfig;
import com.swiftcryptollc.commons.adabounties.es.ElasticsearchClientListener;
import com.swiftcryptollc.commons.adabounties.es.data.TwitterUpdate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import javax.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
@Service
public final class TwitterUpdateQueryHandler implements ElasticsearchClientListener {

    @Autowired
    protected ElasticsearchClientConfig esClient;
    protected ElasticsearchClient client;
    protected UUID id;
    protected final static Logger logger = LoggerFactory.getLogger(TwitterUpdateQueryHandler.class);

    /**
     *
     */
    public TwitterUpdateQueryHandler() {
    }

    /**
     *
     * @param client
     */
    public TwitterUpdateQueryHandler(ElasticsearchClient client) {
        this.client = client;
    }

    /**
     * @return the id
     */
    public UUID getId() {
        if (id == null) {
            id = UUID.randomUUID();
        }
        return id;
    }

    @Override
    public void setClient() {
        if (esClient != null) {
            client = esClient.elasticSearchClient();
        }
    }

    /**
     *
     */
    @PostConstruct
    public void init() {
        if (client == null) {
            ElasticsearchClientConfig.subscribe(this);
        }
    }

    public Set<TwitterUpdate> searchTweet(String bountyId, String claimId, String leaderboard, Long fromTimeMs) {
        Set<TwitterUpdate> tweets = new HashSet<>();
        List<Query> queryList = new ArrayList<>();
        if (bountyId != null) {
            queryList.add(MatchQuery.of(fn -> fn.field("bountyId").query(bountyId))._toQuery());
        }
        if (claimId != null) {
            queryList.add(MatchQuery.of(fn -> fn.field("claimId").query(claimId))._toQuery());
        }
        if (leaderboard != null) {
            queryList.add(MatchQuery.of(fn -> fn.field("leaderboard").query(leaderboard))._toQuery());
        }

        queryList.add(RangeQuery.of(fn -> fn.field("updateTimeMs").gte(JsonData.of(fromTimeMs)))._toQuery());
        SortOptions sortOptions = SortOptions.of(fn -> fn.field(FieldSort.of(fn2 -> fn2.field("updateTimeMs").order(SortOrder.Desc))));
        Query bQuery = BoolQuery.of(fn -> fn.must(queryList))._toQuery();

        try {
            SearchResponse<TwitterUpdate> response = client.search(t -> t.index(TwitterUpdate.INDEX_NAME)
                    .query(bQuery)
                    .sort(sortOptions)
                    .size(500), TwitterUpdate.class);
            HitsMetadata<TwitterUpdate> searchHits = response.hits();

            for (Hit<TwitterUpdate> hit : searchHits.hits()) {
                TwitterUpdate tweet = hit.source();
                tweets.add(tweet);
            }
        } catch (Exception ex) {
            logger.error("Exception during tweet search! [" + ex.getMessage() + "]", ex);
        }

        return tweets;
    }

    /**
     *
     * @param upToTimeMs
     */
    public void deleteTwitterUpdates(Long upToTimeMs) {
        List<Query> queryList = new ArrayList<>();
        queryList.add(RangeQuery.of(fn -> fn.field("updateTimeMs").lt(JsonData.of(upToTimeMs)))._toQuery());
        Query bQuery = BoolQuery.of(fn -> fn.must(queryList))._toQuery();

        try {
            do {
                SearchResponse<TwitterUpdate> response = client.search(t -> t.index(TwitterUpdate.INDEX_NAME)
                        .query(bQuery)
                        .size(500), TwitterUpdate.class);
                HitsMetadata<TwitterUpdate> searchHits = response.hits();
                if (!searchHits.hits().isEmpty()) {
                    BulkRequest.Builder brDelete = new BulkRequest.Builder();
                    for (Hit<TwitterUpdate> hit : searchHits.hits()) {
                        TwitterUpdate tweet = hit.source();
                        brDelete.operations(op -> op
                                .delete(idx -> idx
                                .index(TwitterUpdate.INDEX_NAME)
                                .id(tweet.getId())
                                )
                        );
                    }
                    try {
                        BulkResponse result = client.bulk(brDelete.build());
                        if (result.errors()) {
                            logger.error("Bulk delete errors encountered");
                            for (BulkResponseItem item : result.items()) {
                                if (item.error() != null) {
                                    logger.error(item.error().reason());
                                }
                            }
                        }
                    } catch (Exception ex) {
                        logger.error("Exception occured while deleting the old tweets! [" + ex.getMessage() + "]", ex);
                    }
                } else {
                    break;
                }
            } while (true);
        } catch (Exception ex) {
            logger.error("Exception during tweet cleaning! [" + ex.getMessage() + "]", ex);
        }

    }

    /**
     * @param esClient the esClient to set
     */
    public void setEsClient(ElasticsearchClientConfig esClient) {
        this.esClient = esClient;
    }

    /**
     * @param client the client to set
     */
    public void setClient(ElasticsearchClient client) {
        this.client = client;
    }
}
