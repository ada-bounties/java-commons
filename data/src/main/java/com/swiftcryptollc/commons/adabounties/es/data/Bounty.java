package com.swiftcryptollc.commons.adabounties.es.data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.swiftcryptollc.commons.adabounties.data.ReactionSummary;
import com.swiftcryptollc.commons.adabounties.utilities.data.Base64Id;
import com.swiftcryptollc.commons.adabounties.utilities.security.HashUtility;
import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;
import org.springframework.data.elasticsearch.annotations.Setting;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
@Document(indexName = "bounty")
@Setting(settingPath = "es-config/wc.json")
@JsonIgnoreProperties({"_class", "_id"})
public class Bounty extends Taggable implements Serializable {

    @Transient
    private static final long serialVersionUID = 100003L;
 
    @Id
    protected String id;
    @Field(type = FieldType.Keyword)
    protected String accountId;
    @Field(type = FieldType.Keyword)
    protected String claimId;
    @Field
    protected String title;
    @Field
    protected String desc;
    @Field
    protected String criteria;
    @Field
    protected String claiming;
    @Field(type = FieldType.Keyword)
    protected String token;
    @Field
    protected String tokenDisplay;
    @Field
    protected String tokenSubject;
    @Field
    protected Double amount;
    @Field
    protected Long postedMs;
    @Field
    protected String hash;
    @Field
    protected String txHash;
    @Field
    protected Long expireMs;
    @Transient
    protected String expireDateTime;
    @Field
    protected Long updatedMs;
    @Field(type = FieldType.Keyword)
    protected String status;
    @Transient
    protected Boolean bkmrk = false;
    @Transient
    protected Long numClaims;
    @Transient
    protected Alias alias;
    @Transient
    protected Integer page;
    @Transient
    protected Integer numBtys;
    @Transient
    protected Boolean numInTitle;
    @Transient
    protected transient Claim claim;
    @Transient
    protected List<ReactionSummary> reactSumList;
    @Transient
    public final transient static String INDEX_NAME = "bounty";

    public Bounty() {
    }

    /**
     * @return the id
     */
    public String getId() {
        if (id == null) {
            id = Base64Id.getNewId();
        }
        return id;
    }

    /**
     *
     * @throws NoSuchAlgorithmException
     */
    public String calculateHash() throws NoSuchAlgorithmException {
        StringBuilder sb = new StringBuilder(HashUtility.hashSHA256(title));
        sb.append(HashUtility.hashSHA256(desc));
        sb.append(HashUtility.hashSHA256(criteria));
        sb.append(HashUtility.hashSHA256(claiming));
        sb.append(HashUtility.hashSHA256(token));
        sb.append(HashUtility.hashSHA256(String.valueOf(amount)));
        hash = HashUtility.hashSHA256(sb.toString());
        return hash;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the desc
     */
    public String getDesc() {
        return desc;
    }

    /**
     * @param desc the desc to set
     */
    public void setDesc(String desc) {
        this.desc = desc;
    }

    /**
     * @return the criteria
     */
    public String getCriteria() {
        return criteria;
    }

    /**
     * @param criteria the criteria to set
     */
    public void setCriteria(String criteria) {
        this.criteria = criteria;
    }

    /**
     * @return the claiming
     */
    public String getClaiming() {
        return claiming;
    }

    /**
     * @param claiming the claiming to set
     */
    public void setClaiming(String claiming) {
        this.claiming = claiming;
    }

    /**
     * @return the category
     */
    public String getCategory() {
        return category;
    }

    /**
     * @param category the category to set
     */
    public void setCategory(String category) {
        this.category = category;
    }

    /**
     * @return the token
     */
    public String getToken() {
        return token;
    }

    /**
     * @param token the token to set
     */
    public void setToken(String token) {
        this.token = token;
    }

    /**
     * @return the amount
     */
    public Double getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(Double amount) {
        this.amount = amount;
    }

    /**
     * @return the postedMs
     */
    public Long getPostedMs() {
        return postedMs;
    }

    /**
     * @param postedMs the postedMs to set
     */
    public void setPostedMs(Long postedMs) {
        this.postedMs = postedMs;
    }

    /**
     * @return the alias
     */
    public Alias getAlias() {
        return alias;
    }

    /**
     * @param alias the alias to set
     */
    public void setAlias(Alias alias) {
        this.alias = alias;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the hash
     */
    public String getHash() {
        return hash;
    }

    /**
     * @param hash the hash to set
     */
    public void setHash(String hash) {
        this.hash = hash;
    }

    /**
     * @return the accountId
     */
    public String getAccountId() {
        return accountId;
    }

    /**
     * @param accountId the accountId to set
     */
    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    /**
     * @return the page
     */
    public Integer getPage() {
        if (page == null) {
            page = 1;
        }
        return page;
    }

    /**
     * @param page the page to set
     */
    public void setPage(Integer page) {
        this.page = page;
    }

    /**
     * @return the txHash
     */
    public String getTxHash() {
        return txHash;
    }

    /**
     * @param txHash the txHash to set
     */
    public void setTxHash(String txHash) {
        this.txHash = txHash;
    }

    /**
     * @return the reactSumList
     */
    public List<ReactionSummary> getReactSumList() {
        return reactSumList;
    }

    /**
     * @param reactSumList the reactSumList to set
     */
    public void setReactSumList(List<ReactionSummary> reactSumList) {
        this.reactSumList = reactSumList;
    }

    /**
     * @return the claimId
     */
    public String getClaimId() {
        return claimId;
    }

    /**
     * @param claimId the claimId to set
     */
    public void setClaimId(String claimId) {
        this.claimId = claimId;
    }

    /**
     * @return the bkmrk
     */
    public Boolean getBkmrk() {
        return bkmrk;
    }

    /**
     * @param bkmrk the bkmrk to set
     */
    public void setBkmrk(Boolean bkmrk) {
        this.bkmrk = bkmrk;
    }

    /**
     * @return the numClaims
     */
    public Long getNumClaims() {
        return numClaims;
    }

    /**
     * @param numClaims the numClaims to set
     */
    public void setNumClaims(Long numClaims) {
        this.numClaims = numClaims;
    }

    /**
     * @return the expireMs
     */
    public Long getExpireMs() {
        return expireMs;
    }

    /**
     * @param expireMs the expireMs to set
     */
    public void setExpireMs(Long expireMs) {
        this.expireMs = expireMs;
    }

    /**
     * @return the updatedMs
     */
    public Long getUpdatedMs() {
        return updatedMs;
    }

    /**
     * @param updatedMs the updatedMs to set
     */
    public void setUpdatedMs(Long updatedMs) {
        this.updatedMs = updatedMs;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the claim
     */
    public Claim getClaim() {
        return claim;
    }

    /**
     * @param claim the claim to set
     */
    public void setClaim(Claim claim) {
        this.claim = claim;
    }

    /**
     * @return the expireDateTime
     */
    public String getExpireDateTime() {
        return expireDateTime;
    }

    /**
     * @param expireDateTime the expireDateTime to set
     */
    public void setExpireDateTime(String expireDateTime) {
        this.expireDateTime = expireDateTime;
    }

    /**
     * @return the tokenSubject
     */
    public String getTokenSubject() {
        return tokenSubject;
    }

    /**
     * @param tokenSubject the tokenSubject to set
     */
    public void setTokenSubject(String tokenSubject) {
        this.tokenSubject = tokenSubject;
    }

    /**
     * @return the tokenDisplay
     */
    public String getTokenDisplay() {
        return tokenDisplay;
    }

    /**
     * @param tokenDisplay the tokenDisplay to set
     */
    public void setTokenDisplay(String tokenDisplay) {
        this.tokenDisplay = tokenDisplay;
    }

    /**
     * @return the numBtys
     */
    public Integer getNumBtys() {
        return numBtys;
    }

    /**
     * @param numBtys the numBtys to set
     */
    public void setNumBtys(Integer numBtys) {
        this.numBtys = numBtys;
    }

    /**
     * @return the numInTitle
     */
    public Boolean getNumInTitle() {
        return numInTitle;
    }

    /**
     * @param numInTitle the numInTitle to set
     */
    public void setNumInTitle(Boolean numInTitle) {
        this.numInTitle = numInTitle;
    }

}
