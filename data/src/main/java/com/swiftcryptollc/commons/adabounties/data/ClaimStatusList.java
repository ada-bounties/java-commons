package com.swiftcryptollc.commons.adabounties.data;

import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
public class ClaimStatusList {

    protected Set<String> claimStatusList = new HashSet<>();

    public ClaimStatusList() {
        for (ClaimStatus status : ClaimStatus.values()) {
            claimStatusList.add(status.label);
        }
    }

    /**
     * @return the claimStatusList
     */
    public Set<String> getClaimStatusList() {
        return claimStatusList;
    }

    /**
     * @param claimStatusList the claimStatusList to set
     */
    public void setClaimStatusList(Set<String> claimStatusList) {
        this.claimStatusList = claimStatusList;
    }
}
