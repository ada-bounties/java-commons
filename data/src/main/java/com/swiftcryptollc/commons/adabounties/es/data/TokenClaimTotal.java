package com.swiftcryptollc.commons.adabounties.es.data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
@Document(indexName = "token-claim-total")
@JsonIgnoreProperties({"_class", "_id"})
public class TokenClaimTotal implements Serializable {

    @Transient
    private static final long serialVersionUID = 100023L;

    @Id
    protected String id;
    @Field(type = FieldType.Keyword)
    protected String accountId;
    @Field(type = FieldType.Keyword)
    protected String token;
    @Field
    protected Double amount;
    @Field
    protected Double valueUSD;
    @Field(type = FieldType.Keyword)
    protected String lastXDays;
    @Field
    protected Long updateTimeMs;
    @Transient
    protected Double searchScore;
    @Transient
    protected Alias alias;
    @Transient
    protected Integer page;
    @Transient
    public final transient static String INDEX_NAME = "token-claim-total";

    public TokenClaimTotal() {

    }

    /**
     * @return the id
     */
    public String getId() {
        if ((id == null) && (accountId != null) && (lastXDays != null)) {
            id = accountId.concat(lastXDays);
        }
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the accountId
     */
    public String getAccountId() {
        return accountId;
    }

    /**
     * @param accountId the accountId to set
     */
    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    /**
     * @return the token
     */
    public String getToken() {
        return token;
    }

    /**
     * @param token the token to set
     */
    public void setToken(String token) {
        this.token = token;
    }

    /**
     * @return the amount
     */
    public Double getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(Double amount) {
        this.amount = amount;
    }

    /**
     * @return the lastXDays
     */
    public String getLastXDays() {
        return lastXDays;
    }

    /**
     * @param lastXDays the lastXDays to set
     */
    public void setLastXDays(String lastXDays) {
        this.lastXDays = lastXDays;
    }

    /**
     * @return the updateTimeMs
     */
    public Long getUpdateTimeMs() {
        return updateTimeMs;
    }

    /**
     * @param updateTimeMs the updateTimeMs to set
     */
    public void setUpdateTimeMs(Long updateTimeMs) {
        this.updateTimeMs = updateTimeMs;
    }

    /**
     * @return the searchScore
     */
    public Double getSearchScore() {
        return searchScore;
    }

    /**
     * @param searchScore the searchScore to set
     */
    public void setSearchScore(Double searchScore) {
        this.searchScore = searchScore;
    }

    /**
     * @return the alias
     */
    public Alias getAlias() {
        return alias;
    }

    /**
     * @param alias the alias to set
     */
    public void setAlias(Alias alias) {
        this.alias = alias;
    }

    /**
     * @return the page
     */
    public Integer getPage() {
        return page;
    }

    /**
     * @param page the page to set
     */
    public void setPage(Integer page) {
        this.page = page;
    }

    /**
     * @return the valueUSD
     */
    public Double getValueUSD() {
        return valueUSD;
    }

    /**
     * @param valueUSD the valueUSD to set
     */
    public void setValueUSD(Double valueUSD) {
        this.valueUSD = valueUSD;
    }
}
