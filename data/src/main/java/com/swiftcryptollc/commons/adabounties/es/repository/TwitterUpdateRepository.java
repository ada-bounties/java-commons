package com.swiftcryptollc.commons.adabounties.es.repository;

import com.swiftcryptollc.commons.adabounties.es.data.TwitterUpdate;
import java.util.List;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
@Repository
public interface TwitterUpdateRepository extends ElasticsearchRepository<TwitterUpdate, String> {

    public List<TwitterUpdate> findByBountyId(String bountyId);

    public List<TwitterUpdate> findByClaimId(String claimId);

    public List<TwitterUpdate> findByLeaderboard(String leaderboard);
}
