package com.swiftcryptollc.commons.adabounties.data;

import com.swiftcryptollc.commons.adabounties.es.data.Alias;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
public class AliasSet {

    private Set<Alias> aliasSet = new HashSet<>();

    public AliasSet() {

    }

    public AliasSet(Set<Alias> alias) {
        this.aliasSet = alias;
    }

    /**
     *
     * @param newAlias
     */
    public void addAlias(Alias newAlias) {
        for (Alias checkAlias : aliasSet) {
            if (checkAlias.getName().equals(newAlias.getName())) {
                return;
            }
        }
        aliasSet.add(newAlias);
    }

    /**
     * @return the aliasSet
     */
    public Set<Alias> getAliasSet() {
        return aliasSet;
    }

    /**
     * @param aliasSet the aliasSet to set
     */
    public void setAliasSet(Set<Alias> aliasSet) {
        this.aliasSet = aliasSet;
    }

}
