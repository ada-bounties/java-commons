package com.swiftcryptollc.commons.adabounties.es.query;

import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.elasticsearch._types.FieldValue;
import co.elastic.clients.elasticsearch._types.aggregations.Aggregate;
import co.elastic.clients.elasticsearch._types.aggregations.MultiBucketBase;
import co.elastic.clients.elasticsearch._types.aggregations.StringTermsBucket;
import co.elastic.clients.elasticsearch._types.query_dsl.BoolQuery;
import co.elastic.clients.elasticsearch._types.query_dsl.MatchQuery;
import co.elastic.clients.elasticsearch._types.query_dsl.Query;
import co.elastic.clients.elasticsearch._types.query_dsl.WildcardQuery;
import co.elastic.clients.elasticsearch.core.SearchResponse;
import co.elastic.clients.elasticsearch.core.search.Hit;
import co.elastic.clients.elasticsearch.core.search.HitsMetadata;
import com.swiftcryptollc.commons.adabounties.data.TagList;
import com.swiftcryptollc.commons.adabounties.es.ElasticsearchClientConfig;
import com.swiftcryptollc.commons.adabounties.es.ElasticsearchClientListener;
import com.swiftcryptollc.commons.adabounties.es.data.Bounty;
import com.swiftcryptollc.commons.adabounties.es.data.Tag;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
@Service
public final class TagQueryHandler implements ElasticsearchClientListener {

    @Autowired
    protected ElasticsearchClientConfig esClient;
    protected ElasticsearchClient client;
    protected UUID id;
    protected final static Logger logger = LoggerFactory.getLogger(TagQueryHandler.class);

    public TagQueryHandler() {
    }

    /**
     * @return the id
     */
    @Override
    public UUID getId() {
        if (id == null) {
            id = UUID.randomUUID();
        }
        return id;
    }

    @Override
    public void setClient() {
        client = esClient.elasticSearchClient();
    }

    /**
     *
     */
    @PostConstruct
    public void init() {
        if (client == null) {
            ElasticsearchClientConfig.subscribe(this);
        }
    }

    /**
     *
     * @param fuzzy
     * @return
     */
    public Set<Tag> autocompleteSearch(String fuzzy) {
        Set<Tag> tags = new HashSet<>();
        final String finalFuzzy = "*".concat(fuzzy).concat("*");
        Query wcQuery = WildcardQuery.of(fn -> fn.field("value").value(finalFuzzy).caseInsensitive(Boolean.TRUE))._toQuery();
        try {
            SearchResponse<Tag> response = client.search(t -> t.index(Tag.INDEX_NAME).query(wcQuery).size(10), Tag.class);
            HitsMetadata<Tag> searchHits = response.hits();

            for (Hit<Tag> hit : searchHits.hits()) {
                Tag tag = hit.source();
                tag.setScore(hit.score());
                tags.add(tag);

            }
        } catch (Exception ex) {
            logger.error("Exception during autocomplete tag search! [" + ex.getMessage() + "]", ex);
        }
        return tags;
    }

    /**
     *
     * @param status
     * @param category
     * @param mustHaveTags
     * @return
     */
    public TagList getTagCountList(String status, String category, Set<Tag> mustHaveTags) {

        TagList tagList = new TagList();
        tagList.setStatus(status);
        tagList.setCategory(category);
        if (mustHaveTags == null) {
            mustHaveTags = new HashSet<>();
        }

        Aggregate aggregate = findAllTagsFromBounties(status, category, mustHaveTags);
        List<StringTermsBucket> buckets = aggregate.sterms().buckets().array();

        Map<FieldValue, Long> mapAggsDirector = buckets.stream().collect(Collectors.toMap(StringTermsBucket::key, MultiBucketBase::docCount));

        tagList.setMaxCount(0L);
        for (Map.Entry<FieldValue, Long> entry : mapAggsDirector.entrySet()) {
            Tag tag = new Tag();
            tag.setValue(entry.getKey().stringValue());
            tag.setCount(entry.getValue());
            if (tag.getCount() > tagList.getMaxCount()) {
                tagList.setMaxCount(tag.getCount());
            }
            tagList.addTag(tag);
        }
        return tagList;
    }

    /**
     *
     * @param status
     * @param category
     * @param mustContainTags
     * @return
     */
    protected Aggregate findAllTagsFromBounties(String status, String category, Set<Tag> mustContainTags) {
        Aggregate aggregate = null;
        List<Query> queryList = new ArrayList<>();
        List<Query> notList = new ArrayList<>();

        if ((status != null) && (!status.equals("All")) && (!status.equals("null")))  {
            queryList.add(MatchQuery.of(fn -> fn.field("status").query(status))._toQuery());
        } else {
            notList.add(MatchQuery.of(fn -> fn.field("status").query("Draft"))._toQuery());
        }
        if (category != null) {
            queryList.add(MatchQuery.of(fn -> fn.field("category").query(category))._toQuery());
        }
        for (Tag tag : mustContainTags) {
            queryList.add(MatchQuery.of(fn -> fn.field("lcTags.keyword").query(tag.getValue().toLowerCase()))._toQuery());
        }
        Query bQuery;
        if ((!notList.isEmpty()) && (!queryList.isEmpty())) {
            bQuery = BoolQuery.of(fn -> fn.must(queryList).mustNot(notList))._toQuery();
        } else if (!queryList.isEmpty()) {
            bQuery = BoolQuery.of(fn -> fn.must(queryList))._toQuery();
        } else {
            bQuery = BoolQuery.of(fn -> fn.mustNot(notList))._toQuery();
        }

        try {
            SearchResponse<Object> response = client.search(t
                    -> t.index(Bounty.INDEX_NAME)
                            .query(bQuery)
                            .aggregations("lcTags.keyword", a -> a
                            .terms(h -> h
                            .field("lcTags.keyword")
                            .size(50)
                            )),
                    Object.class);
            aggregate = response.aggregations().get("lcTags.keyword");
        } catch (Exception ex) {
            logger.error("Exception during findAllTagsFromBounties! [" + ex.getMessage() + "]", ex);
        }

        return aggregate;
    }

}
