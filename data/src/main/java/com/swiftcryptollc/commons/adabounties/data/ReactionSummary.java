package com.swiftcryptollc.commons.adabounties.data;

import java.io.Serializable;
import org.springframework.data.annotation.Transient;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
public class ReactionSummary implements Serializable {

    @Transient
    private static final long serialVersionUID = 100025L;

    protected String type;
    protected Long count = 0l;
    protected boolean userVoted;

    public ReactionSummary() {

    }

    public ReactionSummary(String type) {
        this.type = type;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     *
     */
    public void increment() {
        ++this.count;
    }

    /**
     * @return the count
     */
    public Long getCount() {
        return count;
    }

    /**
     * @param count the count to set
     */
    public void setCount(Long count) {
        this.count = count;
    }

    /**
     * @return the userVoted
     */
    public boolean isUserVoted() {
        return userVoted;
    }

    /**
     * @param userVoted the userVoted to set
     */
    public void setUserVoted(boolean userVoted) {
        this.userVoted = userVoted;
    }
}
