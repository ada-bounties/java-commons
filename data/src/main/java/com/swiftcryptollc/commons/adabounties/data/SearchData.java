package com.swiftcryptollc.commons.adabounties.data;

import java.util.List;

import org.springframework.data.annotation.Transient;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
public class SearchData {

    protected String name;
    protected String status;
    protected String category;
    protected String token;
    protected String tokenSubject;
    protected Double tknAmt;
    protected Boolean gte;
    protected List<String> tagList;
    protected String txt;
    @Transient
    protected Integer page;
    @Transient
    protected String accountId;
    // Percent success bounties
    protected Integer perClmdBty;
    // Total bounties
    protected Long totalBty;

    public SearchData() {

    }

    /**
     *
     */
    public void trimValues() {
        if ((name != null) && (name.trim().isBlank())) {
            name = null;
        }
        if ((status != null) && ((status.trim().isBlank())
                || (status.equals("null")))) {
            status = null;
        }
        if ((category != null) && ((category.trim().isBlank())
                || (category.equals("null")))) {
            category = null;
        }
        if ((txt != null) && (txt.trim().isBlank())) {
            txt = null;
        }
    }

    /**
     * @return the tagList
     */
    public List<String> getTagList() {
        return tagList;
    }

    /**
     * @param tagList the tagList to set
     */
    public void setTagList(List<String> tagList) {
        this.tagList = tagList;
    }

    /**
     * @return the txt
     */
    public String getTxt() {
        return txt;
    }

    /**
     * @param txt the txt to set
     */
    public void setTxt(String txt) {
        this.txt = txt;
    }

    /**
     * @return the page
     */
    public Integer getPage() {
        return page;
    }

    /**
     * @param page the page to set
     */
    public void setPage(Integer page) {
        this.page = page;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the category
     */
    public String getCategory() {
        return category;
    }

    /**
     * @param category the category to set
     */
    public void setCategory(String category) {
        this.category = category;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the accountId
     */
    public String getAccountId() {
        return accountId;
    }

    /**
     * @param accountId the accountId to set
     */
    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    /**
     * @return the token
     */
    public String getToken() {
        return token;
    }

    /**
     * @param token the token to set
     */
    public void setToken(String token) {
        this.token = token;
    }

    /**
     * @return the tknAmt
     */
    public Double getTknAmt() {
        return tknAmt;
    }

    /**
     * @param tknAmt the tknAmt to set
     */
    public void setTknAmt(Double tknAmt) {
        this.tknAmt = tknAmt;
    }

    /**
     * @return the gte
     */
    public Boolean getGte() {
        return gte;
    }

    /**
     * @param gte the gte to set
     */
    public void setGte(Boolean gte) {
        this.gte = gte;
    }

    /**
     * @return the perClmdBty
     */
    public Integer getPerClmdBty() {
        return perClmdBty;
    }

    /**
     * @param perClmdBty the perClmdBty to set
     */
    public void setPerClmdBty(Integer perClmdBty) {
        this.perClmdBty = perClmdBty;
    }

    /**
     * @return the totalBty
     */
    public Long getTotalBty() {
        return totalBty;
    }

    /**
     * @param totalBty the totalBty to set
     */
    public void setTotalBty(Long totalBty) {
        this.totalBty = totalBty;
    }

    /**
     * @return the tokenSubject
     */
    public String getTokenSubject() {
        return tokenSubject;
    }

    /**
     * @param tokenSubject the tokenSubject to set
     */
    public void setTokenSubject(String tokenSubject) {
        this.tokenSubject = tokenSubject;
    }
}
