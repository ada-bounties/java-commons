package com.swiftcryptollc.commons.adabounties.es.query;

import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.elasticsearch._types.query_dsl.BoolQuery;
import co.elastic.clients.elasticsearch._types.query_dsl.Query;
import co.elastic.clients.elasticsearch._types.query_dsl.RangeQuery;
import co.elastic.clients.elasticsearch.core.SearchResponse;
import co.elastic.clients.elasticsearch.core.search.Hit;
import co.elastic.clients.elasticsearch.core.search.HitsMetadata;
import co.elastic.clients.json.JsonData;
import com.swiftcryptollc.commons.adabounties.data.BountyStatus;
import com.swiftcryptollc.commons.adabounties.es.ElasticsearchClientConfig;
import com.swiftcryptollc.commons.adabounties.es.ElasticsearchClientListener;
import com.swiftcryptollc.commons.adabounties.es.data.Bounty;
import com.swiftcryptollc.commons.adabounties.es.data.BountyDates;
import com.swiftcryptollc.commons.adabounties.redis.cache.BountyCache;
import com.swiftcryptollc.commons.adabounties.es.repository.BountyRepository;
import javax.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.swiftcryptollc.commons.adabounties.es.repository.BountyDatesRepository;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
@Service
public final class BountyDatesQueryHandler implements ElasticsearchClientListener {

 //   protected SqsService sqsService;
    @Autowired
    protected BountyRepository bountyRepo;
    @Autowired
    protected BountyDatesRepository btyDatesRepo;
    @Autowired
    protected BountyCache bountyCache;
    @Autowired
    protected ElasticsearchClientConfig esClient;
    protected ElasticsearchClient client;
    protected UUID id;
    protected final static Logger logger = LoggerFactory.getLogger(BountyDatesQueryHandler.class);

    public BountyDatesQueryHandler() {
    }

    @PostConstruct
    public void init() {
        ElasticsearchClientConfig.subscribe(this);
    }

    @Override
    public void setClient() {
        client = esClient.elasticSearchClient();
//        try {
//            sqsService = new SqsService();
//        } catch (Exception ex) {
//            logger.error("Could not initialize the SQS Service Handler [" + ex.getMessage() + "]");
//        }
    }

    /**
     * @return the id
     */
    public UUID getId() {
        if (id == null) {
            id = UUID.randomUUID();
        }
        return id;
    }

    /**
     *
     * @param lastIntMs
     * @param expireMs
     * @return
     */
    public Set<BountyDates> findStaleOrExpiredBounties(Long lastIntMs, Long expireMs) {

        List<Query> staleQueryList = new ArrayList<>();
        staleQueryList.add(RangeQuery.of(fn -> fn.field("lastIntMs").lte(JsonData.of(lastIntMs)))._toQuery());
        staleQueryList.add(RangeQuery.of(fn -> fn.field("lastIntMs").gte(JsonData.of(0)))._toQuery());
        Query staleQuery = BoolQuery.of(fn -> fn.must(staleQueryList))._toQuery();

        List<Query> expiredQueryList = new ArrayList<>();
        expiredQueryList.add(RangeQuery.of(fn -> fn.field("expireMs").lte(JsonData.of(expireMs)))._toQuery());
        Query expiredQuery = BoolQuery.of(fn -> fn.must(expiredQueryList))._toQuery();
        Query query = BoolQuery.of(fn -> fn.should(staleQuery).should(expiredQuery))._toQuery();
        return performQuery(query);
    }

    /**
     * Perform the query
     *
     * @param bQuery
     * @return
     */
    private Set<BountyDates> performQuery(Query bQuery) {
        Set<BountyDates> bountyDates = new HashSet<>();

        try {
            SearchResponse<BountyDates> response = client.search(t
                    -> t.index(BountyDates.INDEX_NAME)
                            .query(bQuery)
                            .size(500), BountyDates.class
            );
            HitsMetadata<BountyDates> searchHits = response.hits();

            for (Hit<BountyDates> hit : searchHits.hits()) {
                BountyDates btyDates = hit.source();
                bountyDates.add(btyDates);

            }
        } catch (Exception ex) {
            logger.error("Exception during bounty dates search! [" + ex.getMessage() + "]", ex);
        }

        return bountyDates;

    }

    /**
     *
     * @param bountyId
     */
    public void updateBountyInteraction(String bountyId) {
        this.updateBountyInteraction(bountyId, null);
    }

    /**
     *
     * @param bountyId
     * @param expiryMs
     */
    public void updateBountyInteraction(String bountyId, Long expiryMs) {
        this.updateBountyInteraction(bountyId, expiryMs, new Date().getTime());
    }

    /**
     *
     *
     * @param bountyId
     * @param expiryMs
     * @param lastIntMs
     */
    public void updateBountyInteraction(String bountyId, Long expiryMs, Long lastIntMs) {
        Optional<BountyDates> btyDatesOpt = btyDatesRepo.findById(bountyId);
        Bounty bounty = bountyCache.getBounty(bountyId);
        if (bounty != null) {
            if (bounty.getStatus().equals(BountyStatus.STALE.label)) {
                Long now = new Date().getTime();
                if (bounty.getExpireMs() > now) {
                    bounty.setStatus(BountyStatus.OPEN.label);
                } else {
                    bounty.setStatus(BountyStatus.EXPIRED.label);
                }
                bounty = bountyCache.updateCachedBounty(bounty);
//                if (sqsService != null) {
//                    SqsData data = new SqsData(bounty);
//                    data.setSendToAll(Boolean.FALSE);
//                    sqsService.addToQueue(data);
//                }
            }
        }

        if (btyDatesOpt.isPresent()) {
            BountyDates btyDates = btyDatesOpt.get();
            btyDates.setLastIntMs(lastIntMs);
            if (expiryMs != null) {
                btyDates.setExpireMs(expiryMs);
            }
            btyDatesRepo.save(btyDates);
        } else if (bounty != null) {
            BountyDates btyDates = new BountyDates(bountyId);
            btyDates.setLastIntMs(lastIntMs);
            btyDates.setExpireMs(bounty.getExpireMs());
            btyDatesRepo.save(btyDates);
        }
    }

    /**
     * @param esClient the esClient to set
     */
    public void setEsClient(ElasticsearchClientConfig esClient) {
        this.esClient = esClient;
    }

    /**
     * @param client the client to set
     */
    public void setClient(ElasticsearchClient client) {
        this.client = client;
    }
}
