package com.swiftcryptollc.commons.adabounties.es.data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.swiftcryptollc.commons.adabounties.utilities.data.Base64Id;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
@Document(indexName = "accounting")
@JsonIgnoreProperties({"_class", "_id"})
public class Accounting implements Serializable {

    @Transient
    private static final long serialVersionUID = 100009L;

    @Id
    protected String id;
    @Field(type = FieldType.Keyword)
    protected String srcAccountId;
    @Transient
    protected Alias srcAlias;
    @Field(type = FieldType.Keyword)
    protected String token;
    protected String tokenSubject;
    protected String tokenDisplay;
    protected Double amount;
    @Field(type = FieldType.Keyword)
    protected String destAccountId;
    @Transient
    protected Alias destAlias;
    protected Double valueUSD;
    protected Double adaUSDPrice;
    protected Long dateTimeMs;

    @Transient
    public final transient static String INDEX_NAME = "accounting";
    @Transient
    public final transient static String ADABOUNTIES_ACCOUNT_ID = "ADABOUNTIES";

    public Accounting() {
    }

    /**
     * @return the id
     */
    public String getId() {
        if (id == null) {
            id = Base64Id.getNewId();
        }
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the srcAccountId
     */
    public String getSrcAccountId() {
        return srcAccountId;
    }

    /**
     * @param srcAccountId the srcAccountId to set
     */
    public void setSrcAccountId(String srcAccountId) {
        this.srcAccountId = srcAccountId;
    }

    /**
     * @return the token
     */
    public String getToken() {
        return token;
    }

    /**
     * @param token the token to set
     */
    public void setToken(String token) {
        this.token = token;
    }

    /**
     * @return the amount
     */
    public Double getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(Double amount) {
        this.amount = amount;
    }

    /**
     * @return the destAccountId
     */
    public String getDestAccountId() {
        return destAccountId;
    }

    /**
     * @param destAccountId the destAccountId to set
     */
    public void setDestAccountId(String destAccountId) {
        this.destAccountId = destAccountId;
    }

    /**
     * @return the valueUSD
     */
    public Double getValueUSD() {
        return valueUSD;
    }

    /**
     * @param valueUSD the valueUSD to set
     */
    public void setValueUSD(Double valueUSD) {
        BigDecimal bd = new BigDecimal(Double.toString(valueUSD));
        bd = bd.setScale(4, RoundingMode.HALF_UP);
        this.valueUSD = bd.doubleValue();
    }

    /**
     * @return the dateTimeMs
     */
    public Long getDateTimeMs() {
        return dateTimeMs;
    }

    /**
     * @param dateTimeMs the dateTimeMs to set
     */
    public void setDateTimeMs(Long dateTimeMs) {
        this.dateTimeMs = dateTimeMs;
    }

    /**
     * @return the adaUSDPrice
     */
    public Double getAdaUSDPrice() {
        return adaUSDPrice;
    }

    /**
     * @param adaUSDPrice the adaUSDPrice to set
     */
    public void setAdaUSDPrice(Double adaUSDPrice) {
        BigDecimal bd = new BigDecimal(Double.toString(adaUSDPrice));
        bd = bd.setScale(4, RoundingMode.HALF_UP);
        this.adaUSDPrice = bd.doubleValue();
    }

    /**
     * @return the srcAlias
     */
    public Alias getSrcAlias() {
        return srcAlias;
    }

    /**
     * @param srcAlias the srcAlias to set
     */
    public void setSrcAlias(Alias srcAlias) {
        this.srcAlias = srcAlias;
    }

    /**
     * @return the destAlias
     */
    public Alias getDestAlias() {
        return destAlias;
    }

    /**
     * @param destAlias the destAlias to set
     */
    public void setDestAlias(Alias destAlias) {
        this.destAlias = destAlias;
    }

    /**
     * @return the tokenSubject
     */
    public String getTokenSubject() {
        return tokenSubject;
    }

    /**
     * @param tokenSubject the tokenSubject to set
     */
    public void setTokenSubject(String tokenSubject) {
        this.tokenSubject = tokenSubject;
    }

    /**
     * @return the tokenDisplay
     */
    public String getTokenDisplay() {
        return tokenDisplay;
    }

    /**
     * @param tokenDisplay the tokenDisplay to set
     */
    public void setTokenDisplay(String tokenDisplay) {
        this.tokenDisplay = tokenDisplay;
    }
}
