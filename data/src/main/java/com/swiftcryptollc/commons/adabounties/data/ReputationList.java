package com.swiftcryptollc.commons.adabounties.data;

import com.swiftcryptollc.commons.adabounties.es.data.Reputation;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
public class ReputationList {

    protected Integer page;
    protected Long count;
    protected Set<Reputation> reps = new HashSet<>();

    public ReputationList() {

    }

    /**
     * @return the page
     */
    public Integer getPage() {
        return page;
    }

    /**
     * @param page the page to set
     */
    public void setPage(Integer page) {
        this.page = page;
    }

    /**
     * @return the count
     */
    public Long getCount() {
        return count;
    }

    /**
     * @param count the count to set
     */
    public void setCount(Long count) {
        this.count = count;
    }

    /**
     * @return the reps
     */
    public Set<Reputation> getReps() {
        return reps;
    }

    /**
     * @param reps the reps to set
     */
    public void setReps(Set<Reputation> reps) {
        this.reps = reps;
    }

}
