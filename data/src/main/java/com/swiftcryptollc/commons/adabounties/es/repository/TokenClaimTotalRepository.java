package com.swiftcryptollc.commons.adabounties.es.repository;

import com.swiftcryptollc.commons.adabounties.es.data.TokenClaimTotal;
import java.util.List;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
@Repository
public interface TokenClaimTotalRepository extends ElasticsearchRepository<TokenClaimTotal, String> {

    public List<TokenClaimTotal> findByAccountId(String accountId);

    public List<TokenClaimTotal> findByAccountIdAndLastXDays(String accountId, String lastXDays);
}
