package com.swiftcryptollc.commons.adabounties.es.repository;

import com.swiftcryptollc.commons.adabounties.es.data.UserMetric;
import java.util.Set;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
@Repository
public interface UserMetricRepository extends ElasticsearchRepository<UserMetric, String> {

    public Set<UserMetric> findByAccountId(String accountId);   
}
