package com.swiftcryptollc.commons.adabounties.data;

import com.swiftcryptollc.commons.adabounties.utilities.data.ActionType;
import com.swiftcryptollc.commons.adabounties.es.data.Bounty;
import com.swiftcryptollc.commons.adabounties.es.data.Claim;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
public class AfterActionData {

    protected ActionType actionType;
    protected Bounty bounty;
    protected Claim claim;

    public AfterActionData() {

    }

    public AfterActionData(ActionType actionType, Bounty bounty) {
        this.actionType = actionType;
        this.bounty = bounty;

    }

    public AfterActionData(ActionType actionType, Claim claim) {
        this.actionType = actionType;
        this.claim = claim;
    }

    /**
     * @return the actionType
     */
    public ActionType getActionType() {
        return actionType;
    }

    /**
     * @param actionType the actionType to set
     */
    public void setActionType(ActionType actionType) {
        this.actionType = actionType;
    }

    /**
     * @return the bounty
     */
    public Bounty getBounty() {
        return bounty;
    }

    /**
     * @param bounty the bounty to set
     */
    public void setBounty(Bounty bounty) {
        this.bounty = bounty;
    }

    /**
     * @return the claim
     */
    public Claim getClaim() {
        return claim;
    }

    /**
     * @param claim the claim to set
     */
    public void setClaim(Claim claim) {
        this.claim = claim;
    }
}
