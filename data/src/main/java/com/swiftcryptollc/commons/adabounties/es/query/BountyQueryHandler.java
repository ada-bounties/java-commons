package com.swiftcryptollc.commons.adabounties.es.query;

import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.elasticsearch._types.FieldSort;
import co.elastic.clients.elasticsearch._types.FieldValue;
import co.elastic.clients.elasticsearch._types.SortOptions;
import co.elastic.clients.elasticsearch._types.SortOrder;
import co.elastic.clients.elasticsearch._types.aggregations.Aggregate;
import co.elastic.clients.elasticsearch._types.aggregations.MultiBucketBase;
import co.elastic.clients.elasticsearch._types.aggregations.StringTermsBucket;
import co.elastic.clients.elasticsearch._types.query_dsl.BoolQuery;
import co.elastic.clients.elasticsearch._types.query_dsl.MatchQuery;
import co.elastic.clients.elasticsearch._types.query_dsl.IdsQuery;
import co.elastic.clients.elasticsearch._types.query_dsl.Operator;
import co.elastic.clients.elasticsearch._types.query_dsl.Query;
import co.elastic.clients.elasticsearch._types.query_dsl.RangeQuery;
import co.elastic.clients.elasticsearch.core.SearchResponse;
import co.elastic.clients.elasticsearch.core.search.Hit;
import co.elastic.clients.elasticsearch.core.search.HitsMetadata;
import co.elastic.clients.json.JsonData;
import com.swiftcryptollc.commons.adabounties.data.BountyStatus;
import com.swiftcryptollc.commons.adabounties.data.ReactionSummary;
import com.swiftcryptollc.commons.adabounties.data.SearchData;
import com.swiftcryptollc.commons.adabounties.es.ElasticsearchClientConfig;
import com.swiftcryptollc.commons.adabounties.es.ElasticsearchClientListener;
import com.swiftcryptollc.commons.adabounties.es.data.Bookmark;
import com.swiftcryptollc.commons.adabounties.es.data.Bounty;
import com.swiftcryptollc.commons.adabounties.es.data.Reaction;
import com.swiftcryptollc.commons.adabounties.es.data.SystemMetric;
import com.swiftcryptollc.commons.adabounties.es.data.UserMetric;
import com.swiftcryptollc.commons.adabounties.es.repository.BountyRepository;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
@Service
public final class BountyQueryHandler implements ElasticsearchClientListener {

    @Autowired
    protected BountyRepository bountyRepo;
    @Autowired
    protected UserMetricQueryHandler userMetricQuery;

    @Autowired
    protected ElasticsearchClientConfig esClient;
    protected ElasticsearchClient client;
    protected ReactionSummary sumDown;
    protected ReactionSummary sumUp;
    protected UUID id;
    protected final SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
    protected final static Logger logger = LoggerFactory.getLogger(BountyQueryHandler.class);

    /**
     *
     */
    public BountyQueryHandler() {
    }

    /**
     *
     * @param client
     */
    public BountyQueryHandler(ElasticsearchClient client) {
        this.client = client;
    }

    /**
     * @return the id
     */
    public UUID getId() {
        if (id == null) {
            id = UUID.randomUUID();
        }
        return id;
    }

    @Override
    public void setClient() {
        if (esClient != null) {
            client = esClient.elasticSearchClient();
        }
    }

    /**
     *
     */
    @PostConstruct
    public void init() {
        if (client == null) {
            ElasticsearchClientConfig.subscribe(this);
        }
        sumDown = new ReactionSummary();
        sumDown.setCount(0L);
        sumDown.setType("arrow_down");
        sumUp = new ReactionSummary();
        sumUp.setCount(0L);
        sumUp.setType("arrow_up");
    }

    /**
     *
     * @param category
     * @param tags
     * @return
     */
    public Set<Bounty> findBountiesByTags(String category, List<String> tags) {
        Set<Bounty> bounties = new HashSet<>();
        List<Query> queryList = new ArrayList<>();

        queryList.add(MatchQuery.of(fn -> fn.field("category").query(category))._toQuery());
        for (String tag : tags) {
            queryList.add(MatchQuery.of(fn -> fn.field("lcTags").query(tag.toLowerCase()))._toQuery());
        }
        Query bQuery = BoolQuery.of(fn -> fn.must(queryList))._toQuery();

        try {
            SearchResponse<Bounty> response = client.search(t -> t.index(Bounty.INDEX_NAME).query(bQuery).size(500), Bounty.class);
            HitsMetadata<Bounty> searchHits = response.hits();

            for (Hit<Bounty> hit : searchHits.hits()) {
                Bounty bounty = hit.source();
                bounty.setSearchScore(hit.score());
                bounties.add(bounty);
            }
        } catch (Exception ex) {
            logger.error("Exception during bounty search! [" + ex.getMessage() + "]", ex);
        }

        return bounties;
    }

    /**
     *
     * @param status
     * @param category
     * @param tags
     * @return
     */
    public Set<Bounty> findBountiesByTypeAndTags(String status, String category, List<String> tags) {
        return findBountiesByTypeAndTags(null, status, category, tags, PageRequest.of(0, 100));
    }

    /**
     *
     * @param status
     * @param bookmarks
     * @param pageable
     * @return
     */
    public Set<Bounty> findBookmarkedBounties(String status, Set<Bookmark> bookmarks, PageRequest pageable) {
        Set<Bounty> bounties = new HashSet<>();
        List<Query> queryList = new ArrayList<>();
        if (bookmarks.size() == 0) {
            throw new IllegalArgumentException("Bookmark size must be > 0!");
        }
        if ((status != null) && (!status.equals("All"))) {
            queryList.add(MatchQuery.of(fn -> fn.field("status").query(status))._toQuery());
        }
        List<String> ids = new ArrayList<>();
        for (Bookmark bookmark : bookmarks) {
            ids.add(bookmark.getBountyId());
        }

        queryList.add(IdsQuery.of(fn -> fn.values(ids))._toQuery());
        Query bQuery = BoolQuery.of(fn -> fn.must(queryList))._toQuery();
        SortOptions sortOptions = SortOptions.of(fn -> fn.field(FieldSort.of(fn2 -> fn2.field("postedMs").order(SortOrder.Desc))));

        try {
            SearchResponse<Bounty> response = client.search(t -> t.index(Bounty.INDEX_NAME)
                    .query(bQuery)
                    .sort(sortOptions)
                    .from((pageable.getPageNumber() - 1) * pageable.getPageSize())
                    .size(pageable.getPageSize()), Bounty.class);
            HitsMetadata<Bounty> searchHits = response.hits();

            for (Hit<Bounty> hit : searchHits.hits()) {
                Bounty bounty = hit.source();
                bounty.setLcTags(null);
                //bounty.setSearchScore(hit.score());
                bounties.add(bounty);
            }
        } catch (Exception ex) {
            logger.error("Exception during bounty search! [" + ex.getMessage() + "]", ex);
        }

        return bounties;
    }

    /**
     *
     * @param cutoffMs
     * @param pageable
     * @return
     */
    public Set<Bounty> findPendingBounties(Long cutoffMs, PageRequest pageable) {
        Set<Bounty> bounties = new HashSet<>();
        List<Query> queryList = new ArrayList<>();
        queryList.add(MatchQuery.of(fn -> fn.field("status").query(BountyStatus.PENDING.label))._toQuery());
        queryList.add(RangeQuery.of(fn -> fn.field("updatedMs").lte(JsonData.of(cutoffMs)))._toQuery());

        Query bQuery = BoolQuery.of(fn -> fn.must(queryList))._toQuery();

        try {
            SearchResponse<Bounty> response = client.search(t -> t.index(Bounty.INDEX_NAME)
                    .query(bQuery)
                    .from((pageable.getPageNumber() - 1) * pageable.getPageSize())
                    .size(pageable.getPageSize()), Bounty.class);
            HitsMetadata<Bounty> searchHits = response.hits();

            for (Hit<Bounty> hit : searchHits.hits()) {
                Bounty bounty = hit.source();
                //bounty.setSearchScore(hit.score());
                bounties.add(bounty);
            }
        } catch (Exception ex) {
            logger.error("Exception during bounty search! [" + ex.getMessage() + "]", ex);
        }

        return bounties;
    }

    /**
     *
     * @param accountId
     * @param status
     * @param category
     * @param tags
     * @return
     */
    public Set<Bounty> findBountiesByTypeAndTags(String accountId, String status, String category, List<String> tags, PageRequest pageable) {
        Set<Bounty> bounties = new HashSet<>();
        List<Query> queryList = new ArrayList<>();
        List<Query> notQueryList = new ArrayList<>();
        if ((accountId == null) && (status == null) && (category == null) && (tags.size() == 0)) {
            throw new IllegalArgumentException("At least one parameter must be present!");
        }
        if (accountId != null) {
            queryList.add(MatchQuery.of(fn -> fn.field("accountId").query(accountId))._toQuery());
        } else {
            notQueryList.add(MatchQuery.of(fn -> fn.field("status").query(BountyStatus.DRAFT.label))._toQuery());
        }
        if ((status != null) && (!status.equals("All"))) {
            queryList.add(MatchQuery.of(fn -> fn.field("status").query(status))._toQuery());
        }
        if ((category != null) && (!category.equals("All"))) {
            queryList.add(MatchQuery.of(fn -> fn.field("category").query(category))._toQuery());
        }
        for (String tag : tags) {
            queryList.add(MatchQuery.of(fn -> fn.field("lcTags").query(tag.toLowerCase()))._toQuery());
        }
        Query bQuery;
        if (notQueryList.isEmpty()) {
            bQuery = BoolQuery.of(fn -> fn.must(queryList))._toQuery();
        } else {
            bQuery = BoolQuery.of(fn -> fn.must(queryList).mustNot(notQueryList))._toQuery();
        }
        //logger.info("Executing [" + bQuery.toString() + "]");
        SortOptions sortOptions = SortOptions.of(fn -> fn.field(FieldSort.of(fn2 -> fn2.field("expireMs").order(SortOrder.Desc))));

        try {
            SearchResponse<Bounty> response = client.search(t -> t.index(Bounty.INDEX_NAME)
                    .query(bQuery)
                    .sort(sortOptions)
                    .from((pageable.getPageNumber() - 1) * pageable.getPageSize())
                    .size(pageable.getPageSize()), Bounty.class);
            HitsMetadata<Bounty> searchHits = response.hits();

            for (Hit<Bounty> hit : searchHits.hits()) {
                Bounty bounty = hit.source();
                bounty.setLcTags(null);
                //bounty.setSearchScore(hit.score());
                bounties.add(bounty);
            }
        } catch (Exception ex) {
            logger.error("Exception during bounty search! [" + ex.getMessage() + "]", ex);
        }

        return bounties;
    }

    /**
     *
     * @param searchData
     * @param pageable
     * @return
     */
    public Set<Bounty> searchBounties(SearchData searchData, PageRequest pageable) {
        Set<Bounty> bounties = new HashSet<>();
        List<Query> queryList = new ArrayList<>();
        List<Query> notList = new ArrayList<>();
        final String accountId = searchData.getAccountId();
        final String status = searchData.getStatus();
        final String token = searchData.getToken();
        final String tokenSubject = searchData.getTokenSubject();
        final Double tknAmt = searchData.getTknAmt();
        final Boolean gte = searchData.getGte();
        final String category = searchData.getCategory();
        final List<String> tags = searchData.getTagList();
        final String txt = searchData.getTxt();
        final Integer perClmdBty = searchData.getPerClmdBty();
        final Long totalBty = searchData.getTotalBty();

        boolean sortByScore = false;
        if ((perClmdBty != null) || (totalBty != null)) {
            Set<UserMetric> userMetrics = userMetricQuery.searchOwnersForTotalBtyAndPerClmdBty(totalBty, perClmdBty);
            List<Query> accountList = new ArrayList<>();
            for (UserMetric userMetric : userMetrics) {
                Query query = MatchQuery.of(fn -> fn.field("accountId").query(userMetric.getAccountId()))._toQuery();
                accountList.add(query);
            }
            if (!accountList.isEmpty()) {
                queryList.add(BoolQuery.of(fn -> fn.should(accountList))._toQuery());
            }
        } else if (accountId != null) {
            queryList.add(MatchQuery.of(fn -> fn.field("accountId").query(accountId))._toQuery());
        }
        if (token != null) {
            if (token.equalsIgnoreCase("ADA")) {
                queryList.add(MatchQuery.of(fn -> fn.field("token").query(token))._toQuery());
            } else {
                queryList.add(MatchQuery.of(fn -> fn.field("tokenSubject").query(tokenSubject))._toQuery());
            }
            if (tknAmt != null) {
                if (gte) {
                    queryList.add(RangeQuery.of(fn -> fn.field("amount").gte(JsonData.of(tknAmt)))._toQuery());
                } else {
                    queryList.add(RangeQuery.of(fn -> fn.field("amount").lte(JsonData.of(tknAmt)))._toQuery());
                }
            }
        }
        if ((status != null) && (!status.equals("All"))) {
            queryList.add(MatchQuery.of(fn -> fn.field("status").query(status))._toQuery());
        }
        if ((category != null) && (!category.equals("All"))) {
            queryList.add(MatchQuery.of(fn -> fn.field("category").query(category))._toQuery());
        }
        if ((tags != null) && (!tags.isEmpty())) {
            for (String tag : tags) {
                queryList.add(MatchQuery.of(fn -> fn.field("lcTags.keyword").query(tag.toLowerCase()))._toQuery());
            }
            sortByScore = true;
        }
        notList.add(MatchQuery.of(fn -> fn.field("status").query("Draft"))._toQuery());
        Query textQuery = null;
        if ((txt != null) && (!txt.isBlank())) {
            Query descQ = MatchQuery.of(fn -> fn.field("desc").query(txt))._toQuery();
            Query titleQ = MatchQuery.of(fn -> fn.field("title").query(txt))._toQuery();
            Query tokenDisplay = MatchQuery.of(fn -> fn.field("tokenDisplay").query(txt))._toQuery();
            Query criteriaQ = MatchQuery.of(fn -> fn.field("criteria").query(txt))._toQuery();
            Query claimQ = MatchQuery.of(fn -> fn.field("claiming").query(txt))._toQuery();
            textQuery = BoolQuery.of(fn -> fn.should(descQ).should(titleQ).should(criteriaQ).should(claimQ).should(tokenDisplay))._toQuery();
            sortByScore = true;
        }
        Query bQuery = null;
        if (!queryList.isEmpty()) {
            if (textQuery != null) {
                final Query finalTextQuery = textQuery;
                bQuery = BoolQuery.of(fn -> fn.must(queryList).must(finalTextQuery).mustNot(notList))._toQuery();
            } else {
                bQuery = BoolQuery.of(fn -> fn.must(queryList).mustNot(notList))._toQuery();
            }
        } else if (textQuery != null) {
            final Query finalTextQuery = textQuery;
            bQuery = BoolQuery.of(fn -> fn.must(finalTextQuery).mustNot(notList))._toQuery();
        } else {
            logger.warn("NOTHING TO DO!");
        }

        if (bQuery != null) {
            logger.info(bQuery.toString());
            final Query finalBQuery = bQuery;
            SortOptions sortOptions;
            if (sortByScore) {
                //logger.info("Sorting search by _score");
                sortOptions = SortOptions.of(fn -> fn.field(FieldSort.of(fn2 -> fn2.field("_score").order(SortOrder.Desc))));
            } else {
                //logger.info("Sorting search by postedMs");
                sortOptions = SortOptions.of(fn -> fn.field(FieldSort.of(fn2 -> fn2.field("postedMs").order(SortOrder.Desc))));
            }
            final Boolean finalSortByScore = sortByScore;
            try {
                SearchResponse<Bounty> response = client.search(t -> t.index(Bounty.INDEX_NAME)
                        .query(finalBQuery)
                        .trackScores(finalSortByScore)
                        .sort(sortOptions)
                        .from((pageable.getPageNumber() - 1) * pageable.getPageSize())
                        .size(pageable.getPageSize()), Bounty.class);
                HitsMetadata<Bounty> searchHits = response.hits();

                for (Hit<Bounty> hit : searchHits.hits()) {
                    Bounty bounty = hit.source();
                    if (finalSortByScore) {
                        bounty.setSearchScore(hit.score());
                    }
                    bounties.add(bounty);
                }
            } catch (Exception ex) {
                logger.error("Exception during bounty search! [" + ex.getMessage() + "]", ex);
            }
        } else {
            Page<Bounty> bountyPage = bountyRepo.findAll(pageable);
            for (Bounty bounty : bountyPage.getContent()) {
                bounties.add(bounty);
            }
        }

        return bounties;
    }

    /**
     *
     * @param bountyId
     * @return
     */
    public List<ReactionSummary> getReactionCountListForBounty(String bountyId) {
        Map<String, ReactionSummary> sumMap = new HashMap<>();
        sumMap.put("arrow_down", sumDown);
        sumMap.put("arrow_up", sumUp);

        Aggregate aggregate = aggregateReactionsForBounty(bountyId);
        if (aggregate != null) {
            List<StringTermsBucket> buckets = aggregate.sterms().buckets().array();

            Map<FieldValue, Long> mapAggsDirector = buckets.stream().collect(Collectors.toMap(StringTermsBucket::key, MultiBucketBase::docCount));

            for (Map.Entry<FieldValue, Long> entry : mapAggsDirector.entrySet()) {
                ReactionSummary summary = new ReactionSummary();
                summary.setType(entry.getKey().stringValue());
                summary.setCount(entry.getValue());
                sumMap.put(summary.getType(), summary);
            }
        }
        return new ArrayList<>(sumMap.values());
    }

    /**
     *
     * @param bountyId
     * @return
     */
    protected Aggregate aggregateReactionsForBounty(String bountyId) {
        return aggregateReactionsForBountyComment(bountyId, "null");
    }

    /**
     *
     * @param bountyId
     * @param commentId
     * @return
     */
    protected Aggregate aggregateReactionsForBountyComment(String bountyId, String commentId) {
        Aggregate aggregate = null;
        List<Query> queryList = new ArrayList<>();

        queryList.add(MatchQuery.of(fn -> fn.field("bountyId").query(bountyId))._toQuery());
        queryList.add(MatchQuery.of(fn -> fn.field("commentId").query(commentId))._toQuery());
        Query bQuery = BoolQuery.of(fn -> fn.must(queryList))._toQuery();

        try {
            SearchResponse<Object> response = client.search(t
                    -> t.index(Reaction.INDEX_NAME).query(bQuery)
                            .aggregations("type", a -> a
                            .terms(h -> h
                            .field("type"))),
                    Object.class);
            aggregate = response.aggregations().get("type");
        } catch (Exception ex) {
            logger.error("Exception during aggregateReactionsForBounty! [" + ex.getMessage() + "]");
        }

        return aggregate;
    }

    /**
     *
     * @param accountId
     * @param oldestTimeMs
     * @param userMetric
     */
    public void getStatusCountForBountyByAccount(String accountId, Long oldestTimeMs, UserMetric userMetric) {
        Aggregate aggregate = aggregateStatesForBountyByAccount(accountId, oldestTimeMs);
        userMetric.setClaimedBounties(0L);
        userMetric.setOpenBounties(0L);
        userMetric.setClosedBounties(0L);
        userMetric.setExpiredBounties(0L);
        userMetric.setStaleBounties(0L);
        userMetric.setTotalBounties(0L);
        userMetric.setPerClmdBty(0d);
        if (aggregate != null) {
            Long totalBty = 0L;
            List<StringTermsBucket> buckets = aggregate.sterms().buckets().array();

            Map<FieldValue, Long> mapAggsDirector = buckets.stream().collect(Collectors.toMap(StringTermsBucket::key, MultiBucketBase::docCount));
            for (Map.Entry<FieldValue, Long> entry : mapAggsDirector.entrySet()) {
                String status = entry.getKey().stringValue();
                Long count = entry.getValue();
                if (count != null) {
                    switch (status) {
                        case "Claimed":
                            userMetric.setClaimedBounties(count);
                            totalBty += count;
                            //logger.info("Account [" + accountId + "] Claimed Bounty [" + count + "]");
                            break;
                        case "Open":
                            userMetric.setOpenBounties(count);
                            totalBty += count;
                            //logger.info("Account [" + accountId + "] Open Bounty [" + count + "]");
                            break;
                        case "Closed":
                            userMetric.setClosedBounties(count);
                            totalBty += count;
                            //logger.info("Account [" + accountId + "] Closed Bounty [" + count + "]");
                            break;
                        case "Expired":
                            userMetric.setExpiredBounties(count);
                            totalBty += count;
                            //logger.info("Account [" + accountId + "] Expired Bounty [" + count + "]");
                            break;
                        case "Stale":
                            userMetric.setStaleBounties(count);
                            totalBty += count;
                            //logger.info("Account [" + accountId + "] Stale Bounty [" + count + "]");
                            break;
                    }
                }
            }
            if (totalBty > 0L) {
                userMetric.setTotalBounties(totalBty);
                Long numClaimed = userMetric.getClaimedBounties();
                if (numClaimed > 0L) {
                    if (numClaimed <= totalBty) {
                        userMetric.setPerClmdBty((double) (numClaimed / totalBty) * 100d);
                    } else {
                        logger.error("Error encountered for Account [" + accountId
                                + "] Num Claimed Bty [" + numClaimed + "] > total bty [" + totalBty + "]!");
                        userMetric.setPerClmdBty(100d);
                    }
                }
            }
        }
    }

    /**
     *
     * @param accountId
     * @param fromTimeMs
     * @param status
     * @return
     */
    public Long getTotalForBountyStateByAccount(String accountId, Long fromTimeMs, String status) {
        Long totalBounties = 0L;
        List<Query> queryList = new ArrayList<>();
        queryList.add(MatchQuery.of(fn -> fn.field("accountId").query(accountId))._toQuery());
        if (status != null) {
            queryList.add(MatchQuery.of(fn -> fn.field("status").query(status))._toQuery());
        }
        if (fromTimeMs > 0L) {
            queryList.add(RangeQuery.of(fn -> fn.field("postedMs").gte(JsonData.of(fromTimeMs)))._toQuery());
        }
        Query bQuery = BoolQuery.of(fn -> fn.must(queryList))._toQuery();

        try {
            SearchResponse<Bounty> response = client.search(t
                    -> t.index(Bounty.INDEX_NAME).query(bQuery)
                            .size(0),
                    Bounty.class);
            totalBounties = response.hits().total().value();
        } catch (Exception ex) {
            logger.error("Exception during getTotalForBountyStateByAccount! [" + ex.getMessage() + "]");
        }

        return totalBounties;
    }

    /**
     *
     * @param accountId
     * @param oldestTimeMs
     * @return
     */
    protected Aggregate aggregateStatesForBountyByAccount(String accountId, Long oldestTimeMs) {
        Aggregate aggregate = null;
        List<Query> queryList = new ArrayList<>();

        queryList.add(MatchQuery.of(fn -> fn.field("accountId").query(accountId))._toQuery());
        if (oldestTimeMs > 0L) {
            queryList.add(RangeQuery.of(fn -> fn.field("postedMs").gte(JsonData.of(oldestTimeMs)))._toQuery());
        }
        Query bQuery = BoolQuery.of(fn -> fn.must(queryList))._toQuery();
        //logger.info("Executing query [" + bQuery.toString() + "]");

        try {
            SearchResponse<Object> response = client.search(t
                    -> t.index(Bounty.INDEX_NAME).query(bQuery)
                            .aggregations("status", a -> a
                            .terms(h -> h
                            .field("status"))),
                    Object.class);
            aggregate = response.aggregations().get("status");
        } catch (Exception ex) {
            logger.error("Exception during aggregateStatesForBountyByAccount! [" + ex.getMessage() + "]");
        }

        return aggregate;
    }

    /**
     *
     * @param lessThanTimeMs
     * @param greaterThanTimeMs
     * @param timeframeMs
     * @param systemMetric
     */
    public synchronized void getStatusCountForBounty(Long lessThanTimeMs, Long greaterThanTimeMs, Long timeframeMs, SystemMetric systemMetric) {
        Aggregate aggregate = aggregateStatesForBounty(lessThanTimeMs, greaterThanTimeMs);
        systemMetric.setClaimedBounties(0L);
        systemMetric.setOpenBounties(0L);
        systemMetric.setClosedBounties(0L);
        systemMetric.setExpiredBounties(0L);
        systemMetric.setStaleBounties(0L);
        systemMetric.setTotalBounties(0L);
        systemMetric.setPerClmdBty(0d);
        String from = dateTimeFormat.format(new Date(greaterThanTimeMs));
        String to = dateTimeFormat.format(new Date(lessThanTimeMs));
        if (aggregate != null) {
            Long totalBty = 0L;
            List<StringTermsBucket> buckets = aggregate.sterms().buckets().array();

            Map<FieldValue, Long> mapAggsDirector = buckets.stream().collect(Collectors.toMap(StringTermsBucket::key, MultiBucketBase::docCount));
            for (Map.Entry<FieldValue, Long> entry : mapAggsDirector.entrySet()) {
                String status = entry.getKey().stringValue();
                Long count = entry.getValue();
                if (count != null) {
                    switch (status) {
                        case "Claimed":
                            systemMetric.setClaimedBounties(count);
                            logger.info("[" + systemMetric.getLastXDays() + "] [" + from + "] to [" + to + "] Claimed Bounties [" + count + "]");
                            totalBty += count;
                            break;
                        case "Open":
                            systemMetric.setOpenBounties(count);
                            logger.info("[" + systemMetric.getLastXDays() + "] [" + from + "] to [" + to + "] Open Bounties [" + count + "]");
                            totalBty += count;
                            break;
                        case "Closed":
                            systemMetric.setClosedBounties(count);
                            logger.info("[" + systemMetric.getLastXDays() + "] [" + from + "] to [" + to + "] Closed Bounties [" + count + "]");
                            totalBty += count;
                            break;
                        case "Expired":
                            systemMetric.setExpiredBounties(count);
                            logger.info("[" + systemMetric.getLastXDays() + "] [" + from + "] to [" + to + "] Expired Bounties [" + count + "]");
                            totalBty += count;
                            break;
                        case "Stale":
                            systemMetric.setStaleBounties(count);
                            logger.info("[" + systemMetric.getLastXDays() + "] [" + from + "] to [" + to + "] Stale Bounties [" + count + "]");
                            totalBty += count;
                            break;
                    }
                }
            }
            if (totalBty > 0L) {
                systemMetric.setTotalBounties(totalBty);
                Long numClaimed = systemMetric.getClaimedBounties();
                if (numClaimed > 0L) {
                    if (numClaimed <= totalBty) {
                        systemMetric.setPerClmdBty((double) (numClaimed / totalBty) * 100d);
                    } else {
                        logger.error("[" + systemMetric.getLastXDays() + "] Error encountered for Num Claimed Bty [" + numClaimed + "] > total bty [" + totalBty + "]!");
                        systemMetric.setPerClmdBty(100d);
                    }
                }
            }

            if (timeframeMs > 0) {
                logger.info("[" + systemMetric.getLastXDays() + "] Calculate the previous timeframe for percent increase/decrease");
                Long newOldestTimeMs = greaterThanTimeMs - timeframeMs;
                from = dateTimeFormat.format(new Date(newOldestTimeMs));
                to = dateTimeFormat.format(new Date(greaterThanTimeMs));
                aggregate = aggregateStatesForBounty(greaterThanTimeMs, newOldestTimeMs);
                if (aggregate != null) {
                    Long totalBty2 = 0L;
                    List<StringTermsBucket> buckets2 = aggregate.sterms().buckets().array();

                    Map<FieldValue, Long> mapAggsDirector2 = buckets2.stream().collect(Collectors.toMap(StringTermsBucket::key, MultiBucketBase::docCount));
                    if (!mapAggsDirector2.isEmpty()) {
                        for (Map.Entry<FieldValue, Long> entry : mapAggsDirector2.entrySet()) {
                            String status = entry.getKey().stringValue();
                            Long count = entry.getValue();
                            if (count != null) {
                                switch (status) {
                                    case "Claimed":
                                        Long currentClaimed = systemMetric.getClaimedBounties();
                                        logger.info("[" + systemMetric.getLastXDays() + "] [" + from + "] to [" + to + "] Claimed Bounties [" + count + "]");
                                        totalBty2 += count;
                                        if (count > 0L) {
                                            Double perGrowth = ((currentClaimed / count) - 1d) * 100d;
                                            systemMetric.setGrowthClaimedBounties(perGrowth);
                                            logger.info("[" + systemMetric.getLastXDays() + "] Per growth claimed bounties [" + perGrowth + "%]");
                                        } else {
                                            Double perGrowth = currentClaimed * 100d;
                                            systemMetric.setGrowthClaimedBounties(perGrowth);
                                            logger.info("[" + systemMetric.getLastXDays() + "] Per growth claimed bounties [" + perGrowth + "%]");
                                        }
                                        break;
                                    case "Open":
                                        Long currentOpen = systemMetric.getOpenBounties();
                                        logger.info("[" + systemMetric.getLastXDays() + "] [" + from + "] to [" + to + "] Open Bounties [" + count + "]");
                                        totalBty2 += count;
                                        if (count > 0L) {
                                            Double perGrowth = ((currentOpen / count) - 1d) * 100d;
                                            systemMetric.setGrowthOpenBounties(perGrowth);
                                            logger.info("[" + systemMetric.getLastXDays() + "] Per growth open bounties [" + perGrowth + "%]");
                                        } else {
                                            Double perGrowth = currentOpen * 100d;
                                            systemMetric.setGrowthOpenBounties(perGrowth);
                                            logger.info("[" + systemMetric.getLastXDays() + "] Per growth open bounties [" + perGrowth + "%]");
                                        }
                                        break;
                                    case "Closed":
                                        Long currentClosed = systemMetric.getClosedBounties();
                                        logger.info("[" + systemMetric.getLastXDays() + "] [" + from + "] to [" + to + "] Closed Bounties [" + count + "]");
                                        totalBty2 += count;
                                        if (count > 0L) {
                                            Double perGrowth = ((currentClosed / count) - 1d) * 100d;
                                            systemMetric.setGrowthClosedBounties(perGrowth);
                                            logger.info("[" + systemMetric.getLastXDays() + "] Per growth closed bounties [" + perGrowth + "%]");
                                        } else {
                                            Double perGrowth = currentClosed * 100d;
                                            systemMetric.setGrowthClosedBounties(perGrowth);
                                            logger.info("[" + systemMetric.getLastXDays() + "] Per growth closed bounties [" + perGrowth + "%]");
                                        }
                                        break;
                                    case "Expired":
                                        Long currentExpired = systemMetric.getExpiredBounties();
                                        logger.info("[" + systemMetric.getLastXDays() + "] [" + from + "] to [" + to + "] Expired Bounties [" + count + "]");
                                        totalBty2 += count;
                                        if (count > 0L) {
                                            Double perGrowth = ((currentExpired / count) - 1d) * 100d;
                                            systemMetric.setGrowthExpiredBounties(perGrowth);
                                            logger.info("[" + systemMetric.getLastXDays() + "] Per growth expired bounties [" + perGrowth + "%]");
                                        } else {
                                            Double perGrowth = currentExpired * 100d;
                                            systemMetric.setGrowthExpiredBounties(perGrowth);
                                            logger.info("[" + systemMetric.getLastXDays() + "] Per growth expired bounties [" + perGrowth + "%]");
                                        }
                                        break;
                                    case "Stale":
                                        Long currentStale = systemMetric.getStaleBounties();
                                        logger.info("[" + from + "] to [" + to + "] Stale Bounties [" + count + "]");
                                        totalBty2 += count;
                                        if (count > 0L) {
                                            Double perGrowth = ((currentStale / count) - 1d) * 100d;
                                            systemMetric.setGrowthStaleBounties(perGrowth);
                                            logger.info("[" + systemMetric.getLastXDays() + "] Per growth stale bounties [" + perGrowth + "%]");
                                        } else {
                                            Double perGrowth = currentStale * 100d;
                                            systemMetric.setGrowthStaleBounties(perGrowth);
                                            logger.info("[" + systemMetric.getLastXDays() + "] Per growth stale bounties [" + perGrowth + "%]");
                                        }
                                        break;
                                }
                            }
                        }
                    } else {
                        Long currentClaimed = systemMetric.getClaimedBounties();
                        Double perGrowthClaimed = currentClaimed * 100d;
                        systemMetric.setGrowthClaimedBounties(perGrowthClaimed);
                        logger.info("[" + systemMetric.getLastXDays() + "] Per growth claimed bounties [" + perGrowthClaimed + "%]");
                        Long currentOpen = systemMetric.getOpenBounties();
                        Double perGrowthOpen = currentOpen * 100d;
                        systemMetric.setGrowthOpenBounties(perGrowthOpen);
                        logger.info("[" + systemMetric.getLastXDays() + "] Per growth open bounties [" + perGrowthOpen + "%]");
                        Long currentClosed = systemMetric.getClosedBounties();
                        Double perGrowthClosed = currentClosed * 100d;
                        systemMetric.setGrowthClosedBounties(perGrowthClosed);
                        logger.info("[" + systemMetric.getLastXDays() + "] Per growth closed bounties [" + perGrowthClosed + "%]");
                        Long currentExpired = systemMetric.getExpiredBounties();
                        Double perGrowthExpired = currentExpired * 100d;
                        systemMetric.setGrowthExpiredBounties(perGrowthExpired);
                        logger.info("[" + systemMetric.getLastXDays() + "] Per growth expired bounties [" + perGrowthExpired + "%]");
                        Long currentStale = systemMetric.getStaleBounties();
                        Double perGrowthStale = currentStale * 100d;
                        systemMetric.setGrowthStaleBounties(perGrowthStale);
                        logger.info("[" + systemMetric.getLastXDays() + "] Per growth stale bounties [" + perGrowthStale + "%]");
                    }
                    Long currentTotal = systemMetric.getTotalBounties();
                    if (totalBty2 > 0L) {
                        Double perGrowth = ((currentTotal / totalBty2) - 1d) * 100d;
                        systemMetric.setGrowthTotalBounties(perGrowth);
                        logger.info("[" + systemMetric.getLastXDays() + "] Per growth total bounties [" + perGrowth + "%]");
                    } else {
                        Double perGrowth = currentTotal * 100d;
                        systemMetric.setGrowthTotalBounties(perGrowth);
                        logger.info("[" + systemMetric.getLastXDays() + "] Per growth total bounties [" + perGrowth + "%]");
                    }
                }
            }
        }
    }

    /**
     * @param lessThanTimeMs
     * @param greaterThanTimeMs
     * @return
     */
    protected synchronized Aggregate aggregateStatesForBounty(Long lessThanTimeMs, Long greaterThanTimeMs) {
        Aggregate aggregate = null;
        if (greaterThanTimeMs > 0L) {
            List<Query> queryList = new ArrayList<>();
            queryList.add(RangeQuery.of(fn -> fn.field("postedMs").lt(JsonData.of(lessThanTimeMs)))._toQuery());
            queryList.add(RangeQuery.of(fn -> fn.field("postedMs").gte(JsonData.of(greaterThanTimeMs)))._toQuery());
            Query bQuery = BoolQuery.of(fn -> fn.must(queryList))._toQuery();

            try {
                SearchResponse<Object> response = client.search(t
                        -> t.index(Bounty.INDEX_NAME).query(bQuery)
                                .aggregations("status", a -> a
                                .terms(h -> h
                                .field("status"))),
                        Object.class);
                aggregate = response.aggregations().get("status");
            } catch (Exception ex) {
                logger.error("Exception during aggregateStatesForBounty! [" + ex.getMessage() + "]");
            }
        } else {
            try {
                SearchResponse<Object> response = client.search(t
                        -> t.index(Bounty.INDEX_NAME)
                                .aggregations("status", a -> a
                                .terms(h -> h
                                .field("status"))),
                        Object.class);
                aggregate = response.aggregations().get("status");
            } catch (Exception ex) {
                logger.error("Exception during aggregateStatesForBounty! [" + ex.getMessage() + "]");
            }
        }

        return aggregate;
    }

    /**
     *
     * @param accountId
     * @return
     */
    public Set<String> findMyOpenBountyIds(String accountId) {
        List<Query> queryList = new ArrayList<>();
        List<Query> nestedList = new ArrayList<>();
        Set<String> bountyIds = new HashSet<>();
        queryList.add(MatchQuery.of(fn -> fn.field("accountId").query(accountId))._toQuery());
        queryList.add(MatchQuery.of(fn -> fn.field("status").query(BountyStatus.OPEN.label))._toQuery());
        Query bQuery = BoolQuery.of(fn -> fn.must(queryList))._toQuery();

        try {
            SearchResponse<Bounty> response = client.search(t
                    -> t.index(Bounty.INDEX_NAME)
                            .query(bQuery).size(500),
                    Bounty.class);

            HitsMetadata<Bounty> searchHits = response.hits();

            for (Hit<Bounty> hit : searchHits.hits()) {
                String bountyId = hit.source().getId();
                bountyIds.add(bountyId.toString());
            }

        } catch (Exception ex) {
            logger.error("Exception during findMyOpenBountyIds! [" + ex.getMessage() + "]");
        }
        return bountyIds;
    }

    /**
     *
     * @param ids
     * @return
     */
    public Set<Bounty> findAllById(Set<String> ids) {
        Set<Bounty> bounties = new HashSet<>();
        List<Query> idQueryList = new ArrayList<>();
        if (ids != null) {
            for (String id : ids) {
                idQueryList.add(MatchQuery.of(fn -> fn.field("id").query(id).operator(Operator.Or))._toQuery());
            }
        }
        Query bQuery = BoolQuery.of(fn -> fn.should(idQueryList))._toQuery();
        try {
            SearchResponse<Bounty> response = client.search(t
                    -> t.index(Bounty.INDEX_NAME)
                            .query(bQuery)
                            .size(ids.size()),
                    Bounty.class);

            HitsMetadata<Bounty> searchHits = response.hits();

            for (Hit<Bounty> hit : searchHits.hits()) {
                bounties.add(hit.source());
            }
            //logger.info("Returning [" + bounties.size() + "] bounties from [" + ids.size() + "] bounty ID list");
        } catch (Exception ex) {
            logger.error("Exception during findAllById! [" + ex.getMessage() + "]");
        }
        return bounties;
    }

    /**
     * @param esClient the esClient to set
     */
    public void setEsClient(ElasticsearchClientConfig esClient) {
        this.esClient = esClient;
    }

    /**
     * @param client the client to set
     */
    public void setClient(ElasticsearchClient client) {
        this.client = client;
    }
}
