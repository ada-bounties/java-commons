package com.swiftcryptollc.commons.adabounties.data;

import com.swiftcryptollc.commons.adabounties.es.data.Claim;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
public class ClaimList {

    protected Integer page;
    protected Long count;
    protected Set<Claim> claims = new HashSet<>();

    public ClaimList() {

    }

    /**
     * @return the page
     */
    public Integer getPage() {
        return page;
    }

    /**
     * @param page the page to set
     */
    public void setPage(Integer page) {
        this.page = page;
    }

    /**
     * @return the count
     */
    public Long getCount() {
        return count;
    }

    /**
     * @param count the count to set
     */
    public void setCount(Long count) {
        this.count = count;
    }

    /**
     * @return the claims
     */
    public Set<Claim> getClaims() {
        return claims;
    }

    /**
     * @param claims the claims to set
     */
    public void setClaims(Set<Claim> claims) {
        this.claims = claims;
    }

    public void addClaims(Set<Claim> claims) {
        if (this.claims == null) {
            this.claims = new HashSet<>();
        }

        this.claims.addAll(claims);
    }
}
