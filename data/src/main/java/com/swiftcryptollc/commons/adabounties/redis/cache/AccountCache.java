package com.swiftcryptollc.commons.adabounties.redis.cache;

import com.swiftcryptollc.commons.adabounties.es.data.Account;
import com.swiftcryptollc.commons.adabounties.es.repository.AccountRepository;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
@Service
public class AccountCache {

    @Autowired
    protected AccountRepository accountRepo;
    protected final static Logger logger = LoggerFactory.getLogger(AccountCache.class);

    public AccountCache() {

    }

    /**
     *
     * @param account
     * @return
     */
    @CachePut(value = "account", key = "#account.id")
    public Account updateAccount(Account account) {
        return accountRepo.save(account);
    }

    /**
     *
     * @param id
     * @return
     */
    @Cacheable(value = "account", key = "#id")
    public Account getAccount(String id) {
        if (id == null) {
            throw new IllegalArgumentException("Id cannot be null");
        }
        Optional<Account> accountOpt = accountRepo.findById(id);
        if (accountOpt.isPresent()) {
            return accountOpt.get();
        } else {
            logger.warn("Account [" + id + "] does not exist");
        }
        return null;
    }

}
