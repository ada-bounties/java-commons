
package com.swiftcryptollc.commons.adabounties.data.cardano.tokens;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Signature {

    @SerializedName("signature")
    @Expose
    private String signature;
    @SerializedName("publicKey")
    @Expose
    private String publicKey;

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(String publicKey) {
        this.publicKey = publicKey;
    }

}
