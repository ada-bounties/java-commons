package com.swiftcryptollc.commons.adabounties.es.query;

import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.elasticsearch._types.FieldSort;
import co.elastic.clients.elasticsearch._types.FieldValue;
import co.elastic.clients.elasticsearch._types.SortOptions;
import co.elastic.clients.elasticsearch._types.SortOrder;
import co.elastic.clients.elasticsearch._types.aggregations.Aggregate;
import co.elastic.clients.elasticsearch._types.aggregations.MultiBucketBase;
import co.elastic.clients.elasticsearch._types.aggregations.StringTermsBucket;
import co.elastic.clients.elasticsearch._types.query_dsl.BoolQuery;
import co.elastic.clients.elasticsearch._types.query_dsl.MatchQuery;
import co.elastic.clients.elasticsearch._types.query_dsl.Query;
import co.elastic.clients.elasticsearch._types.query_dsl.RangeQuery;
import co.elastic.clients.elasticsearch.core.BulkRequest;
import co.elastic.clients.elasticsearch.core.BulkResponse;
import co.elastic.clients.elasticsearch.core.SearchResponse;
import co.elastic.clients.elasticsearch.core.bulk.BulkResponseItem;
import co.elastic.clients.elasticsearch.core.search.Hit;
import co.elastic.clients.elasticsearch.core.search.HitsMetadata;
import co.elastic.clients.json.JsonData;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.swiftcryptollc.commons.adabounties.aws.email.data.NotificationSummary;
import com.swiftcryptollc.commons.adabounties.es.ElasticsearchClientConfig;
import com.swiftcryptollc.commons.adabounties.es.ElasticsearchClientListener;
import com.swiftcryptollc.commons.adabounties.es.data.Notification;
import com.swiftcryptollc.commons.adabounties.es.repository.NotificationRepository;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
@Service
public final class NotificationQueryHandler implements ElasticsearchClientListener {

    @Autowired
    protected NotificationRepository notificationRepo;

    @Autowired
    protected ElasticsearchClientConfig esClient;
    protected ElasticsearchClient client;
    protected UUID id;
    protected final static Logger logger = LoggerFactory.getLogger(NotificationQueryHandler.class);

    public NotificationQueryHandler() {
    }

    /**
     *
     * @param client
     */
    public NotificationQueryHandler(ElasticsearchClient client) {
        this.client = client;
    }

    /**
     * @return the id
     */
    @Override
    public UUID getId() {
        if (id == null) {
            id = UUID.randomUUID();
        }
        return id;
    }

    @Override
    public void setClient() {
        if (esClient != null) {
            client = esClient.elasticSearchClient();
        }
    }

    /**
     *
     */
    @PostConstruct
    public void init() {
        if (client == null) {
            ElasticsearchClientConfig.subscribe(this);
        }
    }

    /**
     *
     * @param accountId
     * @param read
     * @param pageable
     * @return
     */
    public Set<Notification> findNotifications(String accountId, Boolean read, Pageable pageable) {
        return findNotifications(accountId, read, null, pageable);
    }

    /**
     *
     * @param accountId
     * @param read
     * @param startTimeMs
     * @param pageable
     * @return
     */
    public Set<Notification> findNotifications(String accountId, Boolean read, Long startTimeMs, Pageable pageable) {
        Set<Notification> notifications = new HashSet<>();
        List<Query> queryList = new ArrayList<>();
        queryList.add(MatchQuery.of(fn -> fn.field("accountId").query(accountId))._toQuery());
        if (read != null) {
            queryList.add(MatchQuery.of(fn -> fn.field("read").query(read))._toQuery());
        }
        if (startTimeMs != null) {
            queryList.add(RangeQuery.of(fn -> fn.field("updateMs").gte(JsonData.of(startTimeMs)))._toQuery());
        }
        Query bQuery = BoolQuery.of(fn -> fn.must(queryList))._toQuery();
        SortOptions sortOptions = SortOptions.of(fn -> fn.field(FieldSort.of(fn2 -> fn2.field("updateMs").order(SortOrder.Desc))));
        try {
            SearchResponse<Notification> response = client.search(t
                    -> t.index(Notification.INDEX_NAME)
                            .query(bQuery)
                            .sort(sortOptions)
                            .from((pageable.getPageNumber() - 1) * pageable.getPageSize())
                            .size(pageable.getPageSize()), Notification.class);
            HitsMetadata<Notification> searchHits = response.hits();

            for (Hit<Notification> hit : searchHits.hits()) {
                Notification notification = hit.source();
                notifications.add(notification);
            }
        } catch (Exception ex) {
            logger.error("Exception during notificaton search! [" + ex.getMessage() + "]", ex);
        }

        return notifications;
    }

    /**
     *
     * @param accountId
     * @param ids
     * @param read
     *
     * @return
     */
    public boolean markNotificationsRead(String accountId, List<String> ids, Boolean read) {
        ObjectMapper mapper = new ObjectMapper();
        boolean success = false;
        Query bQuery;
        List<Query> queryList = new ArrayList<>();
        queryList.add(MatchQuery.of(fn -> fn.field("accountId").query(accountId))._toQuery());
        queryList.add(MatchQuery.of(fn -> fn.field("read").query((!read)))._toQuery());
        if ((ids != null) && (ids.size() > 1)) {
            List<Query> idQueryList = new ArrayList<>();
            for (String id : ids) {
                idQueryList.add(MatchQuery.of(fn -> fn.field("id").query(id))._toQuery());
            }
            Query idQuery = BoolQuery.of(fn -> fn.should(idQueryList))._toQuery();
            bQuery = BoolQuery.of(fn -> fn.must(queryList).must(idQuery))._toQuery();
        } else {
            queryList.add(MatchQuery.of(fn -> fn.field("id").query(ids.get(0)))._toQuery());
            bQuery = BoolQuery.of(fn -> fn.must(queryList))._toQuery();
        }
        do {
            try {
                BulkRequest.Builder br = new BulkRequest.Builder();
                SearchResponse<Notification> response = client.search(t
                        -> t.index(Notification.INDEX_NAME).query(bQuery).size(500), Notification.class);
                HitsMetadata<Notification> searchHits = response.hits();
                if (!searchHits.hits().isEmpty()) {
                    for (Hit<Notification> hit : searchHits.hits()) {
                        Notification notify = hit.source();
                        notify.setRead(read);
                        ObjectNode node = mapper.convertValue(notify, ObjectNode.class);
                        node.put("_class", "com.swiftcryptollc.commons.adabounties.es.data.Notification");
                        br.operations(op -> op
                                .index(idx -> idx
                                .index(Notification.INDEX_NAME)
                                .id(notify.getId())
                                .document(node)
                                )
                        );
                    }
                    BulkResponse result = client.bulk(br.build());
                    if (result.errors()) {
                        logger.error("Bulk update errors encountered");
                        for (BulkResponseItem item : result.items()) {
                            if (item.error() != null) {
                                logger.error(item.error().reason());
                            }
                        }
                    }
                    success = true;
                } else {
                    break;
                }
            } catch (Exception ex) {
                logger.error("Exception while marking notifications read/unread! [" + ex.getMessage() + "]", ex);
                success = false;
                break;
            }
        } while (true);
        return success;
    }

    /**
     *
     * @param accountId
     * @param fromTimeMs
     * @param notSummary
     */
    public void getStatusCountForNotificationsByAccount(String accountId, Long fromTimeMs, NotificationSummary notSummary) {
        Aggregate aggregate = aggregateStatesForUnreadNotificationsByAccount(accountId, fromTimeMs);
        Long totalNotifications = 0L;
        if (aggregate != null) {
            List<StringTermsBucket> buckets = aggregate.sterms().buckets().array();

            Map<FieldValue, Long> mapAggsDirector = buckets.stream().collect(Collectors.toMap(StringTermsBucket::key, MultiBucketBase::docCount));
            for (Map.Entry<FieldValue, Long> entry : mapAggsDirector.entrySet()) {
                String status = entry.getKey().stringValue();
                Long count = entry.getValue();
                totalNotifications += count;
                notSummary.addToStatusMap(status, count);
            }
        }
        notSummary.setNumNotifications(totalNotifications);
    }

    /**
     *
     * @param accountId
     * @param fromTimeMs
     * @return
     */
    protected Aggregate aggregateStatesForUnreadNotificationsByAccount(String accountId, Long fromTimeMs) {
        Aggregate aggregate = null;
        List<Query> queryList = new ArrayList<>();

        queryList.add(MatchQuery.of(fn -> fn.field("accountId").query(accountId))._toQuery());
        if (fromTimeMs > 0L) {
            queryList.add(RangeQuery.of(fn -> fn.field("updateMs").gte(JsonData.of(fromTimeMs)))._toQuery());
        }
        queryList.add(MatchQuery.of(fn -> fn.field("read").query(Boolean.FALSE))._toQuery());
        Query bQuery = BoolQuery.of(fn -> fn.must(queryList))._toQuery();
        //logger.info("Executing query [" + bQuery.toString() + "]");

        try {
            SearchResponse<Object> response = client.search(t
                    -> t.index(Notification.INDEX_NAME).query(bQuery)
                            .aggregations("action", a -> a
                            .terms(h -> h
                            .field("action"))),
                    Object.class);
            aggregate = response.aggregations().get("action");
        } catch (Exception ex) {
            logger.error("Exception during aggregateStatesForUnreadNotificationsByAccount! [" + ex.getMessage() + "]");
        }

        return aggregate;
    }

}
