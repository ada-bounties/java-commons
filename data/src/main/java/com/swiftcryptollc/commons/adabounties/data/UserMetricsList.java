package com.swiftcryptollc.commons.adabounties.data;

import com.swiftcryptollc.commons.adabounties.es.data.UserMetric;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
public class UserMetricsList {

    protected String lastXDays;
    protected String accountId;
    protected Integer page;
    protected Long count;
    protected Set<UserMetric> userMetrics = new HashSet<>();

    public UserMetricsList() {

    }

    /**
     * @return the page
     */
    public Integer getPage() {
        return page;
    }

    /**
     * @param page the page to set
     */
    public void setPage(Integer page) {
        this.page = page;
    }

    /**
     * @return the userMetrics
     */
    public Set<UserMetric> getUserMetrics() {
        return userMetrics;
    }

    /**
     * @param userMetrics the userMetrics to set
     */
    public void setUserMetrics(Set<UserMetric> userMetrics) {
        this.userMetrics = userMetrics;
    }

    /**
     * @return the count
     */
    public Long getCount() {
        return count;
    }

    /**
     * @param count the count to set
     */
    public void setCount(Long count) {
        this.count = count;
    }

    /**
     * @return the accountId
     */
    public String getAccountId() {
        return accountId;
    }

    /**
     * @param accountId the accountId to set
     */
    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    /**
     * @return the lastXDays
     */
    public String getLastXDays() {
        return lastXDays;
    }

    /**
     * @param lastXDays the lastXDays to set
     */
    public void setLastXDays(String lastXDays) {
        this.lastXDays = lastXDays;
    }
}
