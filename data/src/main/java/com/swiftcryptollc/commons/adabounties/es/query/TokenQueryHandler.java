package com.swiftcryptollc.commons.adabounties.es.query;

import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.elasticsearch._types.FieldSort;
import co.elastic.clients.elasticsearch._types.SortOptions;
import co.elastic.clients.elasticsearch._types.SortOrder;
import co.elastic.clients.elasticsearch._types.query_dsl.Query;
import co.elastic.clients.elasticsearch._types.query_dsl.WildcardQuery;
import co.elastic.clients.elasticsearch.core.SearchResponse;
import co.elastic.clients.elasticsearch.core.search.Hit;
import co.elastic.clients.elasticsearch.core.search.HitsMetadata;
import com.swiftcryptollc.commons.adabounties.es.ElasticsearchClientConfig;
import com.swiftcryptollc.commons.adabounties.es.ElasticsearchClientListener;
import com.swiftcryptollc.commons.adabounties.es.data.Token;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import javax.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
@Service
public final class TokenQueryHandler implements ElasticsearchClientListener {

    @Autowired
    protected ElasticsearchClientConfig esClient;
    protected ElasticsearchClient client;
    protected UUID id;
    protected final static Logger logger = LoggerFactory.getLogger(TokenQueryHandler.class);

    public TokenQueryHandler() {
    }

    /**
     * @return the id
     */
    @Override
    public UUID getId() {
        if (id == null) {
            id = UUID.randomUUID();
        }
        return id;
    }

    @Override
    public void setClient() {
        client = esClient.elasticSearchClient();
    }

    /**
     * @param esClient the esClient to set
     */
    public void setEsClient(ElasticsearchClientConfig esClient) {
        this.esClient = esClient;
    }

    /**
     * @param client the client to set
     */
    public void setClient(ElasticsearchClient client) {
        this.client = client;
    }

    /**
     *
     */
    @PostConstruct
    public void init() {
        if (client == null) {
            ElasticsearchClientConfig.subscribe(this);
        }
    }

    /**
     *
     * @param fuzzy
     * @return
     */
    public Set<Token> autocompleteSearch(String fuzzy) {
        Set<Token> tokens = new HashSet<>();
        final String finalFuzzy = fuzzy.concat("*");
        SearchResponse<Token> response;
        Query wcQuery = WildcardQuery.of(fn -> fn.field("ticker").value(finalFuzzy).caseInsensitive(Boolean.TRUE))._toQuery();
        SortOptions sortOptions = SortOptions.of(fn -> fn.field(FieldSort.of(fn2 -> fn2.field("_score").order(SortOrder.Desc))));

        try {
            response = client.search(t -> t.index(Token.INDEX_NAME)
                    .query(wcQuery)
                    .sort(sortOptions)
                    .size(50), Token.class);
            HitsMetadata<Token> searchHits = response.hits();
            for (Hit<Token> hit : searchHits.hits()) {
                Token token = hit.source();
                tokens.add(token);
            }
        } catch (Exception ex) {
            logger.error("Exception during autocomplete token search! [" + ex.getMessage() + "]", ex);
        }
        return tokens;
    }
}
