package com.swiftcryptollc.commons.adabounties.data;

import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
public class BountyStatusList {

    protected Set<String> bountyStatusList = new HashSet<>();

    public BountyStatusList() {
        for (BountyStatus status : BountyStatus.values()) {
            bountyStatusList.add(status.label);
        }
    }

    /**
     * @return the bountyStatusList
     */
    public Set<String> getBountyStatusList() {
        return bountyStatusList;
    }

    /**
     * @param bountyStatusList the bountyStatusList to set
     */
    public void setBountyStatusList(Set<String> bountyStatusList) {
        this.bountyStatusList = bountyStatusList;
    }
}
