package com.swiftcryptollc.commons.adabounties.es.data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.swiftcryptollc.commons.adabounties.utilities.data.Base64Id;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
@Document(indexName = "account")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Account implements Serializable {

    @Transient
    private static final long serialVersionUID = 100001L;

    @Id
    protected String id;
    @Field
    private String first;
    @Field
    private String last;
    @Field
    private String email;
    @Field
    protected String webUrl;
    @Field
    protected String lInUrl;
    @Field
    protected String gitHUrl;
    @Field
    protected String gitLUrl;
    @Field
    protected String twtUrl;
    @Field
    protected String about;
    @Field
    protected Long sinceMS;
    @Field
    protected Boolean notify;
    @Field
    protected Boolean verified;
    @Field
    protected transient String challenge;
    @Transient
    private String error;
    @Transient
    private String jwt;
    @Transient
    protected Alias alias;
    @Transient
    protected Reputation rep;
    @Transient
    protected transient Map<String, Bookmark> bookmarkMap = new HashMap<>();
    @Transient
    public final transient static String INDEX_NAME = "account";

    public Account() {
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the first
     */
    public String getFirst() {
        return first;
    }

    /**
     * @param first the first to set
     */
    public void setFirst(String first) {
        this.first = first;
    }

    /**
     * @return the last
     */
    public String getLast() {
        return last;
    }

    /**
     * @param last the last to set
     */
    public void setLast(String last) {
        this.last = last;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the error
     */
    public String getError() {
        return error;
    }

    /**
     * @param error the error to set
     */
    public void setError(String error) {
        this.error = error;
    }

    /**
     * @return the jwt
     */
    public String getJwt() {
        return jwt;
    }

    /**
     * @param jwt the jwt to set
     */
    public void setJwt(String jwt) {
        this.jwt = jwt;
    }

    /**
     * @return the alias
     */
    public Alias getAlias() {
        return alias;
    }

    /**
     * @param alias the alias to set
     */
    public void setAlias(Alias alias) {
        this.alias = alias;
    }

    /**
     * @return the sinceMS
     */
    public Long getSinceMS() {
        return sinceMS;
    }

    /**
     * @param sinceMS the sinceMS to set
     */
    public void setSinceMS(Long sinceMS) {
        this.sinceMS = sinceMS;
    }

    /**
     * @return the webUrl
     */
    public String getWebUrl() {
        return webUrl;
    }

    /**
     * @param webUrl the webUrl to set
     */
    public void setWebUrl(String webUrl) {
        this.webUrl = webUrl;
    }

    /**
     * @return the challenge
     */
    public String getChallenge() {
        if (challenge == null) {
            this.resetChallenge();
        }
        return challenge;
    }

    /**
     * @param challenge the challenge to set
     */
    public void setChallenge(String challenge) {
        this.challenge = challenge;
    }

    /**
     *
     */
    public void resetChallenge() {
        this.setChallenge(Base64Id.getNewId());
    }

    /**
     * @return the notify
     */
    public Boolean getNotify() {
        if (notify == null) {
            notify = Boolean.FALSE;
        }
        return notify;
    }

    /**
     * @param notify the notify to set
     */
    public void setNotify(Boolean notify) {
        this.notify = notify;
    }

    /**
     * @return the rep
     */
    public Reputation getRep() {
        return rep;
    }

    /**
     * @param rep the rep to set
     */
    public void setRep(Reputation rep) {
        this.rep = rep;
    }

    /**
     * @return the bookmarkMap
     */
    public Map<String, Bookmark> getBookmarkMap() {
        if (bookmarkMap == null) {
            bookmarkMap = new HashMap<>();
        }
        return bookmarkMap;
    }

    /**
     * @param bookmarkMap the bookmarkMap to set
     */
    public void setBookmarkMap(Map<String, Bookmark> bookmarkMap) {
        this.bookmarkMap = bookmarkMap;
    }

    /**
     *
     * @param bookmarks
     */
    public void setBookmarks(Set<Bookmark> bookmarks) {
        if (bookmarkMap == null) {
            bookmarkMap = new HashMap<>();
        }
        for (Bookmark bookmark : bookmarks) {
            this.bookmarkMap.put(bookmark.getBountyId(), bookmark);
        }
    }

    /**
     *
     * @param bookmark
     */
    public void addBookmark(Bookmark bookmark) {
        if (bookmarkMap == null) {
            bookmarkMap = new HashMap<>();
        }
        this.bookmarkMap.put(bookmark.getBountyId(), bookmark);
    }

    /**
     *
     * @param bookmark
     */
    public void deleteBookmark(Bookmark bookmark) {
        if (bookmarkMap == null) {
            bookmarkMap = new HashMap<>();
        }
        this.bookmarkMap.remove(bookmark.getBountyId());

    }

    /**
     * @return the lInUrl
     */
    public String getlInUrl() {
        return lInUrl;
    }

    /**
     * @param lInUrl the lInUrl to set
     */
    public void setlInUrl(String lInUrl) {
        this.lInUrl = lInUrl;
    }

    /**
     * @return the gitHUrl
     */
    public String getGitHUrl() {
        return gitHUrl;
    }

    /**
     * @param gitHUrl the gitHUrl to set
     */
    public void setGitHUrl(String gitHUrl) {
        this.gitHUrl = gitHUrl;
    }

    /**
     * @return the twtUrl
     */
    public String getTwtUrl() {
        return twtUrl;
    }

    /**
     * @param twtUrl the twtUrl to set
     */
    public void setTwtUrl(String twtUrl) {
        this.twtUrl = twtUrl;
    }

    /**
     * @return the about
     */
    public String getAbout() {
        return about;
    }

    /**
     * @param about the about to set
     */
    public void setAbout(String about) {
        this.about = about;
    }

    /**
     * @return the verified
     */
    public Boolean getVerified() {
        if (verified == null) {
            verified = false;
        }
        return verified;
    }

    /**
     * @param verified the verified to set
     */
    public void setVerified(Boolean verified) {
        this.verified = verified;
    }
}
