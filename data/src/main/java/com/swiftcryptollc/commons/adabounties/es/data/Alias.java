package com.swiftcryptollc.commons.adabounties.es.data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.swiftcryptollc.commons.adabounties.utilities.data.Base64Id;
import java.io.Serializable;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;
import org.springframework.data.elasticsearch.annotations.Setting;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
@Document(indexName = "alias")
@Setting(settingPath = "es-config/wc.json")
@JsonIgnoreProperties({"_class", "_id"})
public class Alias implements Serializable {

    @Transient
    private static final long serialVersionUID = 100008L;

    @Id
    private String id;
    @Field(type = FieldType.Wildcard)
    protected String name;
    @Field
    protected String imgSrc;
    @Transient
    public final static transient String INDEX_NAME = "alias";

    public Alias() {
    }

    /**
     * @return the id
     */
    public String getId() {
        if (id == null) {
            id = Base64Id.getNewId();
        }

        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the imgSrc
     */
    public String getImgSrc() {
        return imgSrc;
    }

    /**
     * @param imgSrc the imgSrc to set
     */
    public void setImgSrc(String imgSrc) {
        this.imgSrc = imgSrc;
    }
}
