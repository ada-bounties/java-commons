package com.swiftcryptollc.commons.adabounties.data;

import com.swiftcryptollc.commons.adabounties.es.data.Token;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
public class TokenList {

    private Set<Token> tokens = new HashSet<>();

    public TokenList() {

    }

    public TokenList(Set<Token> tokens) {
        this.tokens = tokens;
    }

    /**
     *
     * @param token
     */
    public void addToken(Token token) {
        for (Token checkToken : tokens) {
            if (checkToken.getSubject().equals(token.getSubject())) {
                return;
            }
        }
        tokens.add(token);

    }

    /**
     *
     * @param tokens
     */
    public void removeTokens(Set<Token> tokens) {
        for (Token token : tokens) {
            this.removeToken(token);
        }
    }

    /**
     *
     * @param tag
     */
    public void removeToken(Token token) {
        Token tokenToRemove = null;
        for (Token checkToken : tokens) {
            if (checkToken.getSubject().equals(token.getSubject())) {
                tokenToRemove = checkToken;
                break;
            }
        }
        if (tokenToRemove != null) {
            tokens.remove(tokenToRemove);
        }
    }

    /**
     * @return the tags
     */
    public Set<Token> getTokens() {
        return tokens;
    }

    /**
     * @param tokens the tags to set
     */
    public void setTokens(Set<Token> tokens) {
        this.tokens = tokens;
    }
}
