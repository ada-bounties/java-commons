package com.swiftcryptollc.commons.adabounties.es.data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.swiftcryptollc.commons.adabounties.utilities.data.Base64Id;
import java.io.Serializable;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
@Document(indexName = "notification")
@JsonIgnoreProperties({"_class", "_id"})
public class Notification implements Serializable {

    @Transient
    private static final long serialVersionUID = 100015L;

    @Id
    protected String id;
    @Field(type = FieldType.Keyword)
    protected String srcId;
    @Field(type = FieldType.Keyword)
    protected String bountyId;
    @Field(type = FieldType.Keyword)
    protected String claimId;
    @Field(type = FieldType.Keyword)
    protected String accountId;
    @Field(type = FieldType.Keyword)
    protected String srcAccountId;
    @Field(type = FieldType.Keyword)
    protected String commentId;
    @Field(type = FieldType.Keyword)
    protected String action;
    @Field
    protected Long updateMs;
    @Field
    protected Boolean read;
    @Transient
    protected Alias alias;
    @Transient
    protected Integer page;
    @Transient
    protected Long startMs;
    @Transient
    protected Long endMs;
    @Transient
    public final transient static String INDEX_NAME = "notification";

    public Notification() {
        id = Base64Id.getNewId();
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the bountyId
     */
    public String getBountyId() {
        return bountyId;
    }

    /**
     * @param bountyId the bountyId to set
     */
    public void setBountyId(String bountyId) {
        this.bountyId = bountyId;
    }

    /**
     * @return the commentId
     */
    public String getCommentId() {
        return commentId;
    }

    /**
     * @param commentId the commentId to set
     */
    public void setCommentId(String commentId) {
        this.commentId = commentId;
    }

    /**
     * @return the alias
     */
    public Alias getAlias() {
        return alias;
    }

    /**
     * @param alias the alias to set
     */
    public void setAlias(Alias alias) {
        this.alias = alias;
    }

    /**
     * @return the updateMs
     */
    public Long getUpdateMs() {
        return updateMs;
    }

    /**
     * @param updateMs the updateMs to set
     */
    public void setUpdateMs(Long updateMs) {
        this.updateMs = updateMs;
    }

    /**
     * @return the action
     */
    public String getAction() {
        return action;
    }

    /**
     * @param action the action to set
     */
    public void setAction(String action) {
        this.action = action;
    }

    /**
     * @return the accountId
     */
    public String getAccountId() {
        return accountId;
    }

    /**
     * @param accountId the accountId to set
     */
    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    /**
     * @return the srcAccountId
     */
    public String getSrcAccountId() {
        return srcAccountId;
    }

    /**
     * @param srcAccountId the srcAccountId to set
     */
    public void setSrcAccountId(String srcAccountId) {
        this.srcAccountId = srcAccountId;
    }

    /**
     * @return the read
     */
    public Boolean getRead() {
        return read;
    }

    /**
     * @param read the read to set
     */
    public void setRead(Boolean read) {
        this.read = read;
    }

    /**
     * @return the page
     */
    public Integer getPage() {
        return page;
    }

    /**
     * @param page the page to set
     */
    public void setPage(Integer page) {
        this.page = page;
    }

    /**
     * @return the endMs
     */
    public Long getEndMs() {
        return endMs;
    }

    /**
     * @param endMs the endMs to set
     */
    public void setEndMs(Long endMs) {
        this.endMs = endMs;
    }

    /**
     * @return the startMs
     */
    public Long getStartMs() {
        return startMs;
    }

    /**
     * @param startMs the startMs to set
     */
    public void setStartMs(Long startMs) {
        this.startMs = startMs;
    }

    /**
     * @return the claimId
     */
    public String getClaimId() {
        return claimId;
    }

    /**
     * @param claimId the claimId to set
     */
    public void setClaimId(String claimId) {
        this.claimId = claimId;
    }

    /**
     * @return the srcId
     */
    public String getSrcId() {
        return srcId;
    }

    /**
     * @param srcId the srcId to set
     */
    public void setSrcId(String srcId) {
        this.srcId = srcId;
    }
}
