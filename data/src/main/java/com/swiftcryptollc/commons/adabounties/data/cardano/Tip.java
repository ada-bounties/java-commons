package com.swiftcryptollc.commons.adabounties.data.cardano;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
public class Tip {

    @SerializedName("time")
    @Expose
    private Integer time;
    @SerializedName("height")
    @Expose
    private Integer height;
    @SerializedName("hash")
    @Expose
    private String hash;
    @SerializedName("slot")
    @Expose
    private Integer slot;
    @SerializedName("epoch")
    @Expose
    private Integer epoch;
    @SerializedName("epoch_slot")
    @Expose
    private Integer epochSlot;
    @SerializedName("slot_leader")
    @Expose
    private String slotLeader;
    @SerializedName("size")
    @Expose
    private Integer size;
    @SerializedName("tx_count")
    @Expose
    private Integer txCount;
    @SerializedName("output")
    @Expose
    private String output;
    @SerializedName("fees")
    @Expose
    private String fees;
    @SerializedName("block_vrf")
    @Expose
    private String blockVrf;
    @SerializedName("op_cert")
    @Expose
    private String opCert;
    @SerializedName("op_cert_counter")
    @Expose
    private String opCertCounter;
    @SerializedName("previous_block")
    @Expose
    private String previousBlock;
    @SerializedName("next_block")
    @Expose
    private String nextBlock;
    @SerializedName("confirmations")
    @Expose
    private Integer confirmations;

    public Tip() {

    }

    public Integer getTime() {
        return time;
    }

    public void setTime(Integer time) {
        this.time = time;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public Integer getSlot() {
        return slot;
    }

    public void setSlot(Integer slot) {
        this.slot = slot;
    }

    public Integer getEpoch() {
        return epoch;
    }

    public void setEpoch(Integer epoch) {
        this.epoch = epoch;
    }

    public Integer getEpochSlot() {
        return epochSlot;
    }

    public void setEpochSlot(Integer epochSlot) {
        this.epochSlot = epochSlot;
    }

    public String getSlotLeader() {
        return slotLeader;
    }

    public void setSlotLeader(String slotLeader) {
        this.slotLeader = slotLeader;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public Integer getTxCount() {
        return txCount;
    }

    public void setTxCount(Integer txCount) {
        this.txCount = txCount;
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public String getFees() {
        return fees;
    }

    public void setFees(String fees) {
        this.fees = fees;
    }

    public String getBlockVrf() {
        return blockVrf;
    }

    public void setBlockVrf(String blockVrf) {
        this.blockVrf = blockVrf;
    }

    public String getOpCert() {
        return opCert;
    }

    public void setOpCert(String opCert) {
        this.opCert = opCert;
    }

    public String getOpCertCounter() {
        return opCertCounter;
    }

    public void setOpCertCounter(String opCertCounter) {
        this.opCertCounter = opCertCounter;
    }

    public String getPreviousBlock() {
        return previousBlock;
    }

    public void setPreviousBlock(String previousBlock) {
        this.previousBlock = previousBlock;
    }

    public String getNextBlock() {
        return nextBlock;
    }

    public void setNextBlock(String nextBlock) {
        this.nextBlock = nextBlock;
    }

    public Integer getConfirmations() {
        return confirmations;
    }

    public void setConfirmations(Integer confirmations) {
        this.confirmations = confirmations;
    }

}
