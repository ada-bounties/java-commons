package com.swiftcryptollc.commons.adabounties.es.data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.swiftcryptollc.commons.adabounties.utilities.data.Base64Id;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
@Document(indexName = "leaderboard")
@JsonIgnoreProperties({"_class", "_id"})
public class Leaderboard implements Serializable {

    @Transient
    private static final long serialVersionUID = 100013L;

    @Id
    protected String id;
    @Field
    protected String type;
    @Field
    protected String ymd;
    @Field(type = FieldType.Object, store = true)
    protected Set<LeaderboardEntry> entries;
    @Transient
    public final static transient String INDEX_NAME = "leaderboard";

    public Leaderboard() {
    }

    /**
     * @return the id
     */
    public String getId() {
        if (id == null) {
            id = Base64Id.getNewId();
        }
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the ymd
     */
    public String getYmd() {
        return ymd;
    }

    /**
     * @param ymd the ymd to set
     */
    public void setYmd(String ymd) {
        this.ymd = ymd;
    }

    /**
     *
     * @param lbe
     */
    public LeaderboardEntry addEntry(LeaderboardEntry lbe) {
        if (this.entries == null) {
            this.entries = new HashSet<>();
        }
        this.entries.add(lbe);
        return lbe;
    }

    /**
     * @return the entries
     */
    public Set<LeaderboardEntry> getEntries() {
        if (this.entries == null) {
            this.entries = new HashSet<>();
        }
        return entries;
    }

    /**
     * @param entries the entries to set
     */
    public void setEntries(Set<LeaderboardEntry> entries) {
        this.entries = entries;
    }
}
