package com.swiftcryptollc.commons.adabounties.data;

import com.swiftcryptollc.commons.adabounties.es.data.Accounting;
import java.util.Set;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
public class AccountingHistory {

    protected String accountId;
    protected Set<Accounting> received;
    protected Set<Accounting> paid;

    public AccountingHistory() {

    }

    /**
     * @return the accountId
     */
    public String getAccountId() {
        return accountId;
    }

    /**
     * @param accountId the accountId to set
     */
    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    /**
     * @return the received
     */
    public Set<Accounting> getReceived() {
        return received;
    }

    /**
     * @param received the received to set
     */
    public void setReceived(Set<Accounting> received) {
        this.received = received;
    }

    /**
     * @return the paid
     */
    public Set<Accounting> getPaid() {
        return paid;
    }

    /**
     * @param paid the paid to set
     */
    public void setPaid(Set<Accounting> paid) {
        this.paid = paid;
    }

}
