package com.swiftcryptollc.commons.adabounties.redis.cache;

import com.swiftcryptollc.commons.adabounties.data.OptionList;
import com.swiftcryptollc.commons.adabounties.es.data.Option;
import com.swiftcryptollc.commons.adabounties.es.repository.OptionRepository;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
@Service
public class OptionCache {

    @Autowired
    private OptionRepository optionRepo;

    protected final static Logger logger = LoggerFactory.getLogger(OptionCache.class);

    public OptionCache() {

    }

    @CachePut(value = "optionList", key = "#optionList.accountId")
    public OptionList updateOptions(OptionList optionList) {
        OptionList existingOptions = getOptionList(optionList.getAccountId());
        List<Option> existingList = existingOptions.getOptions();
        List<Option> newList = optionList.getOptions();
        for (int i = 0; i < existingList.size(); ++i) {
            for (int j = 0; j < newList.size(); ++j) {
                if (existingList.get(i).getKey().equals(newList.get(j).getKey())) {
                    existingList.get(i).setValue(newList.get(j).getValue());
                    break;
                }
            }
        }

        optionRepo.saveAll(existingList);
        existingOptions.setMap();
        return existingOptions;
    }

    /**
     *
     * @param accountId
     * @return
     */
    @Cacheable(value = "optionList", key = "#accountId")
    public OptionList getOptionList(String accountId) {
        if (accountId == null) {
            throw new IllegalArgumentException("accountId cannot be null");
        }
        OptionList optionList = new OptionList();
        optionList.setAccountId(accountId);
        List<Option> existingOptions = optionRepo.findByAccountId(accountId);
        optionList.setOptions(existingOptions);
        optionList.setMap();
        return optionList;
    }

    /**
     *
     * @param accountId
     * @param optionKey
     * @return
     */
    public String getOptionValue(String accountId, String optionKey) {
        OptionList oList = getOptionList(accountId);
        return oList.getOptionValue(optionKey);
    }
}
