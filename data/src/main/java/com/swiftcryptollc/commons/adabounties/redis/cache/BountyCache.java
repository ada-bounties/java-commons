package com.swiftcryptollc.commons.adabounties.redis.cache;

import com.swiftcryptollc.commons.adabounties.es.data.Bounty;
import com.swiftcryptollc.commons.adabounties.es.query.BountyQueryHandler;
import com.swiftcryptollc.commons.adabounties.es.repository.BountyRepository;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
@Service
public class BountyCache {

    @Autowired
    protected BountyRepository bountyRepo;
    @Autowired
    protected BountyQueryHandler bountyQueryHandler;
    protected final static Logger logger = LoggerFactory.getLogger(BountyCache.class);

    public BountyCache() {

    }

    /**
     *
     * @param id
     * @return
     */
    @Cacheable(value = "bounty", key = "#id")
    public Bounty getBounty(String id) {
        if (id == null) {
            throw new IllegalArgumentException("Id cannot be null");
        }
        Optional<Bounty> bountyOpt = bountyRepo.findById(id);
        if (bountyOpt.isPresent()) {
            return bountyOpt.get();
        } else {
            logger.warn("Bounty [" + id + "] does not exist");
        }
        return null;
    }

    /**
     *
     * @param bounty
     * @return
     */
    @CachePut(value = "bounty", key = "#bounty.id")
    public Bounty updateCachedBounty(Bounty bounty) {
        bounty = bountyRepo.save(bounty);
        updateCachedBountyTitle(bounty.getId(), bounty.getTitle());
        updateCachedBountyAccountId(bounty.getId(), bounty.getAccountId());
        return bounty;
    }

    /**
     *
     * @param id
     * @param title
     * @return
     */
    @CachePut(value = "bountyTitle", key = "#id")
    private String updateCachedBountyTitle(String id, String title) {
        return title;
    }

    /**
     *
     * @param id
     * @return
     */
    @Cacheable(value = "bountyTitle", key = "#id")
    public String getBountyTitle(String id) {
        if (id == null) {
            throw new IllegalArgumentException("Id cannot be null");
        }
        Optional<Bounty> bountyOpt = bountyRepo.findById(id);
        if (bountyOpt.isPresent()) {
            return bountyOpt.get().getTitle();
        }
        return null;
    }

    /**
     *
     * @param id
     * @param accountId
     * @return
     */
    @CachePut(value = "bountyAccountId", key = "#bounty.id")
    private String updateCachedBountyAccountId(String id, String accountId) {
        return accountId;
    }

    /**
     *
     * @param id
     * @return
     */
    @Cacheable(value = "bountyAccountId", key = "#id")
    public String getBountyAccountId(String id) {
        if (id == null) {
            throw new IllegalArgumentException("Id cannot be null");
        }
        Optional<Bounty> bountyOpt = bountyRepo.findById(id);
        if (bountyOpt.isPresent()) {
            return bountyOpt.get().getAccountId();
        }
        return null;
    }
}
