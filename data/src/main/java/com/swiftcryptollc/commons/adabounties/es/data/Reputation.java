package com.swiftcryptollc.commons.adabounties.es.data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
@Document(indexName = "reputation")
@JsonIgnoreProperties({"_class", "_id"})
public class Reputation implements Serializable {

    @Transient
    private static final long serialVersionUID = 100017L;

    @Id
    protected String id;
    // Total number of bounty up votes
    @Field
    protected Integer btyUp;
    // Total number of bounty down votes
    @Field
    protected Integer btyDown;
    // Total number of comment up votes
    @Field
    protected Integer cmtUp;
    // Total number of comment down votes
    @Field
    protected Integer cmtDown;
    // Total number of bounties ever created
    @Field
    protected Integer numBty;
    // Total number of comments on their bounties
    @Field
    protected Integer numCmtsOnBty;
    // Total number of replies to their comments
    @Field
    protected Integer numRplysToCmts;
    // Total number of bounty comments they made
    @Field
    protected Integer numCmt;
    // Total number of claims created
    @Field
    protected Integer numClm;
    // Total number of claims on their bounties
    @Field
    protected Integer numClmOnBtys;
    // Total number of their claims that were accepted
    @Field
    protected Integer numAccClm;
    // Total number of their claims that were rejected
    @Field
    protected Integer numRejClm;
    // Totla number of bounties they paid out
    @Field
    protected Integer numPaidBty;
    @Field
    protected Double score;
    @Transient
    protected Double btyScore;
    @Transient
    protected Double cmtScore;
    @Transient
    protected Double clmScore;
    @Transient
    protected List<TokenClaimTotal> tctList = new ArrayList<>();
    @Transient
    protected List<TokenBountyTotal> tbtList = new ArrayList<>();
    @Transient
    protected Alias alias;
    @Transient
    protected Integer page;
    @Transient
    protected String sortField;
    @Transient
    public final transient static String INDEX_NAME = "reputation";

    public Reputation() {
    }

    /**
     *
     */
    public void reset() {
        btyUp = 0;
        btyDown = 0;
        cmtUp = 0;
        cmtDown = 0;
        numBty = 0;
        numClm = 0;
        numCmt = 0;
        numAccClm = 0;
        numRejClm = 0;
        numPaidBty = 0;
        score = 0d;
        btyScore = 0d;
        cmtScore = 0d;
        clmScore = 0d;
        numRplysToCmts = 0;
        numClmOnBtys = 0;
    }

    /**
     *
     */
    public Reputation clearPartialScores() {
        btyScore = null;
        cmtScore = null;
        clmScore = null;
        return this;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the btyUp
     */
    public Integer getBtyUp() {
        if (btyUp == null) {
            btyUp = 0;
        }
        return btyUp;
    }

    /**
     * @param btyUp the btyUp to set
     */
    public void setBtyUp(Integer btyUp) {
        this.btyUp = btyUp;
    }

    /**
     * @return the btyDown
     */
    public Integer getBtyDown() {
        if (btyDown == null) {
            btyDown = 0;
        }
        return btyDown;
    }

    /**
     * @param btyDown the btyDown to set
     */
    public void setBtyDown(Integer btyDown) {
        this.btyDown = btyDown;
    }

    /**
     * @return the cmtUp
     */
    public Integer getCmtUp() {
        if (cmtUp == null) {
            cmtUp = 0;
        }
        return cmtUp;
    }

    /**
     * @param cmtUp the cmtUp to set
     */
    public void setCmtUp(Integer cmtUp) {
        this.cmtUp = cmtUp;
    }

    /**
     * @return the cmtDown
     */
    public Integer getCmtDown() {
        if (cmtDown == null) {
            cmtDown = 0;
        }
        return cmtDown;
    }

    /**
     * @param cmtDown the cmtDown to set
     */
    public void setCmtDown(Integer cmtDown) {
        this.cmtDown = cmtDown;
    }

    /**
     * @return the numBty
     */
    public Integer getNumBty() {
        if (numBty == null) {
            numBty = 0;
        }
        return numBty;
    }

    /**
     * @param numBty the numBty to set
     */
    public void setNumBty(Integer numBty) {
        this.numBty = numBty;
    }

    /**
     * @return the numCmt
     */
    public Integer getNumCmt() {
        if (numCmt == null) {
            numCmt = 0;
        }
        return numCmt;
    }

    /**
     * @param numCmt the numCmt to set
     */
    public void setNumCmt(Integer numCmt) {
        this.numCmt = numCmt;
    }

    /**
     * @return the numClm
     */
    public Integer getNumClm() {
        if (numClm == null) {
            numClm = 0;
        }
        return numClm;
    }

    /**
     * @param numClm the numClm to set
     */
    public void setNumClm(Integer numClm) {
        this.numClm = numClm;
    }

    /**
     * @return the numAccClm
     */
    public Integer getNumAccClm() {
        if (numAccClm == null) {
            numAccClm = 0;
        }
        return numAccClm;
    }

    /**
     * @param numAccClm the numAccClm to set
     */
    public void setNumAccClm(Integer numAccClm) {
        this.numAccClm = numAccClm;
    }

    /**
     * @return the numRejClm
     */
    public Integer getNumRejClm() {
        if (numRejClm == null) {
            numRejClm = 0;
        }
        return numRejClm;
    }

    /**
     * @param numRejClm the numRejClm to set
     */
    public void setNumRejClm(Integer numRejClm) {
        this.numRejClm = numRejClm;
    }

    /**
     * @return the numPaidBty
     */
    public Integer getNumPaidBty() {
        if (numPaidBty == null) {
            numPaidBty = 0;
        }
        return numPaidBty;
    }

    /**
     * @param numPaidBty the numPaidBty to set
     */
    public void setNumPaidBty(Integer numPaidBty) {
        this.numPaidBty = numPaidBty;
    }

    /**
     * @return the tctList
     */
    public List<TokenClaimTotal> getTctList() {
        return tctList;
    }

    /**
     * @param tctList the tctList to set
     */
    public void setTctList(List<TokenClaimTotal> tctList) {
        this.tctList = tctList;
    }

    /**
     * @return the tbtList
     */
    public List<TokenBountyTotal> getTbtList() {
        return tbtList;
    }

    /**
     * @param tbtList the tbtList to set
     */
    public void setTbtList(List<TokenBountyTotal> tbtList) {
        this.tbtList = tbtList;
    }

    /**
     * @return the score
     */
    public Double getScore() {
        if (score == null) {
            score = 0d;
        }
        return score;
    }

    /**
     * @param score the score to set
     */
    public void setScore(Double score) {
        this.score = score;
    }

    /**
     * @return the btyScore
     */
    public Double getBtyScore() {
        return btyScore;
    }

    /**
     * @param btyScore the btyScore to set
     */
    public void setBtyScore(Double btyScore) {
        this.btyScore = btyScore;
    }

    /**
     * @return the cmtScore
     */
    public Double getCmtScore() {
        return cmtScore;
    }

    /**
     * @param cmtScore the cmtScore to set
     */
    public void setCmtScore(Double cmtScore) {
        this.cmtScore = cmtScore;
    }

    /**
     * @return the clmScore
     */
    public Double getClmScore() {
        return clmScore;
    }

    /**
     * @param clmScore the clmScore to set
     */
    public void setClmScore(Double clmScore) {
        this.clmScore = clmScore;
    }

    /**
     * @return the numCmtsOnBty
     */
    public Integer getNumCmtsOnBty() {
        if (numCmtsOnBty == null) {
            numCmtsOnBty = 0;
        }
        return numCmtsOnBty;
    }

    /**
     * @param numCmtsOnBty the numCmtsOnBty to set
     */
    public void setNumCmtsOnBty(Integer numCmtsOnBty) {
        this.numCmtsOnBty = numCmtsOnBty;
    }

    /**
     * @return the numRplysToCmts
     */
    public Integer getNumRplysToCmts() {
        if (numRplysToCmts == null) {
            numRplysToCmts = 0;
        }
        return numRplysToCmts;
    }

    /**
     * @param numRplysToCmts the numRplysToCmts to set
     */
    public void setNumRplysToCmts(Integer numRplysToCmts) {
        this.numRplysToCmts = numRplysToCmts;
    }

    /**
     * @return the numClmOnBtys
     */
    public Integer getNumClmOnBtys() {
        if (numClmOnBtys == null) {
            numClmOnBtys = 0;
        }
        return numClmOnBtys;
    }

    /**
     * @param numClmOnBtys the numClmOnBtys to set
     */
    public void setNumClmOnBtys(Integer numClmOnBtys) {
        this.numClmOnBtys = numClmOnBtys;
    }

    /**
     * @return the alias
     */
    public Alias getAlias() {
        return alias;
    }

    /**
     * @param alias the alias to set
     */
    public void setAlias(Alias alias) {
        this.alias = alias;
    }

    /**
     * @return the page
     */
    public Integer getPage() {
        return page;
    }

    /**
     * @param page the page to set
     */
    public void setPage(Integer page) {
        this.page = page;
    }

    /**
     * @return the sortField
     */
    public String getSortField() {
        return sortField;
    }

    /**
     * @param sortField the sortField to set
     */
    public void setSortField(String sortField) {
        this.sortField = sortField;
    }
}
