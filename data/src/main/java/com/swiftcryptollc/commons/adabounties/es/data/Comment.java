package com.swiftcryptollc.commons.adabounties.es.data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.swiftcryptollc.commons.adabounties.data.ReactionSummary;
import com.swiftcryptollc.commons.adabounties.utilities.data.Base64Id;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
@Document(indexName = "comment")
@JsonIgnoreProperties({"_class", "_id"})
public class Comment implements Serializable {

    @Transient
    private static final long serialVersionUID = 100005L;

    @Id
    protected String id;
    @Field(type = FieldType.Keyword)
    protected String bountyId;
    @Field(type = FieldType.Keyword)
    protected String claimId;
    @Field(type = FieldType.Keyword)
    protected String accountId;
    @Field
    protected String msg;
    // If not null, this comment is a reply
    @Field(type = FieldType.Keyword)
    protected String parentId;
    @Transient
    protected String parentMsg;
    @Field
    protected Long postedMs;
    @Field
    protected Long updatedMs;
    @Transient
    protected List<ReactionSummary> reactSumList;
    @Transient
    protected Alias alias;
    @Transient
    protected Integer page;
    @Transient
    public final transient static String INDEX_NAME = "comment";

    public Comment() {
        id = Base64Id.getNewId();
        postedMs = new Date().getTime();
        updatedMs = new Date().getTime();
    }

    /**
     * @return the id
     */
    public String getId() {
        if (id == null) {
            id = Base64Id.getNewId();
        }
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the bountyId
     */
    public String getBountyId() {
        return bountyId;
    }

    /**
     * @param bountyId the bountyId to set
     */
    public void setBountyId(String bountyId) {
        this.bountyId = bountyId;
    }

    /**
     * @return the accountId
     */
    public String getAccountId() {
        return accountId;
    }

    /**
     * @param accountId the accountId to set
     */
    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    /**
     * @return the msg
     */
    public String getMsg() {
        return msg;
    }

    /**
     * @param msg the msg to set
     */
    public void setMsg(String msg) {
        this.msg = msg;
    }

    /**
     * @return the parentId
     */
    public String getParentId() {
        return parentId;
    }

    /**
     * @param parentId the parentId to set
     */
    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    /**
     * @param alias the alias to set
     */
    public void setAlias(Alias alias) {
        this.alias = alias;
    }

    /**
     * @return the postedMs
     */
    public Long getPostedMs() {
        if (postedMs == null) {
            postedMs = new Date().getTime();
        }
        return postedMs;
    }

    /**
     * @param postedMs the postedMs to set
     */
    public void setPostedMs(Long postedMs) {
        this.postedMs = postedMs;
    }

    /**
     * @return the alias
     */
    public Alias getAlias() {
        return alias;
    }

    /**
     *
     * @param summary
     */
    public void addReactionSummary(ReactionSummary summary) {
        if (this.reactSumList == null) {
            this.reactSumList = new ArrayList<>();
        }
        this.reactSumList.add(summary);
    }

    /**
     * @return the reactSumList
     */
    public List<ReactionSummary> getReactSumList() {
        if (this.reactSumList == null) {
            this.reactSumList = new ArrayList<>();
        }
        return reactSumList;
    }

    /**
     * @param reactSumList the reactSumList to set
     */
    public void setReactSumList(List<ReactionSummary> reactSumList) {
        this.reactSumList = reactSumList;
    }

    /**
     * @return the updatedMs
     */
    public Long getUpdatedMs() {
        return updatedMs;
    }

    /**
     * @param updatedMs the updatedMs to set
     */
    public void setUpdatedMs(Long updatedMs) {
        this.updatedMs = updatedMs;
    }

    /**
     * @return the page
     */
    public Integer getPage() {
        return page;
    }

    /**
     * @param page the page to set
     */
    public void setPage(Integer page) {
        this.page = page;
    }

    /**
     * @return the parentMsg
     */
    public String getParentMsg() {
        return parentMsg;
    }

    /**
     * @param parentMsg the parentMsg to set
     */
    public void setParentMsg(String parentMsg) {
        if ((parentMsg != null) && (parentMsg.length() > 500)) {
            parentMsg = parentMsg.substring(0, 500);
        }
        this.parentMsg = parentMsg;
    }

    /**
     * @return the claimId
     */
    public String getClaimId() {
        return claimId;
    }

    /**
     * @param claimId the claimId to set
     */
    public void setClaimId(String claimId) {
        this.claimId = claimId;
    }
}
