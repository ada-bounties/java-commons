package com.swiftcryptollc.commons.adabounties.es.repository;

import com.swiftcryptollc.commons.adabounties.es.data.Bookmark;
import java.util.Optional;
import java.util.Set;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
@Repository
public interface BookmarkRepository extends ElasticsearchRepository<Bookmark, String> {

    public Set<Bookmark> findByAccountId(String accountId);

    public Set<Bookmark> findByBountyId(String bountyId);

    public Optional<Bookmark> findByAccountIdAndBountyId(String accountId, String bountyId);

}
