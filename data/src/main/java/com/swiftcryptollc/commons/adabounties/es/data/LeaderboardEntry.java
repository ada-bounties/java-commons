package com.swiftcryptollc.commons.adabounties.es.data;

import java.io.Serializable;
import org.springframework.data.annotation.Transient;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
public class LeaderboardEntry implements Serializable {

    @Transient
    private static final long serialVersionUID = 100014L;

    protected String accountId;
    protected Integer rank;
    protected Integer prevRank;
    protected Double value;
    @Transient
    protected Alias alias;

    public LeaderboardEntry() {

    }

    /**
     * @return the accountId
     */
    public String getAccountId() {
        return accountId;
    }

    /**
     * @param accountId the accountId to set
     */
    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    /**
     * @return the alias
     */
    public Alias getAlias() {
        return alias;
    }

    /**
     * @param alias the alias to set
     */
    public void setAlias(Alias alias) {
        this.alias = alias;
    }

    /**
     * @return the rank
     */
    public Integer getRank() {
        return rank;
    }

    /**
     * @param rank the rank to set
     */
    public void setRank(Integer rank) {
        this.rank = rank;
    }

    /**
     * @return the prevRank
     */
    public Integer getPrevRank() {
        return prevRank;
    }

    /**
     * @param prevRank the prevRank to set
     */
    public void setPrevRank(Integer prevRank) {
        this.prevRank = prevRank;
    }

    /**
     * @return the value
     */
    public Double getValue() {
        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(Double value) {
        this.value = value;
    }
}
