package com.swiftcryptollc.commons.adabounties.es;

import co.elastic.clients.elasticsearch.ElasticsearchAsyncClient;
import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.json.jackson.JacksonJsonpMapper;
import co.elastic.clients.transport.ElasticsearchTransport;
import co.elastic.clients.transport.rest_client.RestClientTransport;
import com.swiftcryptollc.commons.adabounties.aws.secrets.SecretsManager;
import com.swiftcryptollc.commons.adabounties.aws.secrets.SecretsManagerListener;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.net.ssl.SSLContext;
import org.apache.http.Header;
import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.nio.client.HttpAsyncClientBuilder;
import org.apache.http.message.BasicHeader;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder.HttpClientConfigCallback;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;

/**
 *
 * https://reflectoring.io/spring-boot-elasticsearch/
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
@Configuration
@EnableElasticsearchRepositories(basePackages = "com.swiftcryptollc.commons.adabounties.es.repository")
@ComponentScan(basePackages = {"com.swiftcryptollc.commons.adabounties.es.data"})
@ConfigurationProperties(ignoreUnknownFields = true, prefix = "swiftcryptollc.adabounties.elasticsearch")
public class ElasticsearchClientConfig extends AbstractElasticsearchClientConfig implements SecretsManagerListener {

    private static Map<UUID, ElasticsearchClientListener> listenerMap = new HashMap<>();
    protected UUID id;
    protected Boolean initialized = Boolean.FALSE;
    public final static Object LOCK = new Object();

    public ElasticsearchClientConfig() {
        id = UUID.randomUUID();
    }

    /**
     *
     */
    @PostConstruct
    public void init() {
        SecretsManager.subscribe(this);
        if (elasticUser == null) {
            elasticUser = "elastic";
        }
        if (elasticPassword == null) {
            elasticPassword = System.getProperty("elasticsearch_elasticPassword");
        }
    }

    /**
     *
     * @param secretMap
     */
    @Override
    public void updateSecrets(Map<String, String> secretMap) {
        synchronized (LOCK) {
            updateStaticSecrets(secretMap);
            initialized = Boolean.TRUE;
            logger.info("Notifying ElasticsearchClientListeners of init");
            for (ElasticsearchClientListener listener : listenerMap.values()) {
                listener.setClient();
            }
            listenerMap.clear();
        }
    }

    /**
     *
     */
    @PreDestroy
    public void preDestroy() {
        SecretsManager.unsubscribe(this);
    }

    /**
     *
     * @return
     */
    @Bean
    public ElasticsearchClient elasticSearchClient() {
        ElasticsearchTransport transport = null;
        try {
            if (encodedApiKey != null) {
                logger.info("Creating SSL client");
                SSLContext sslContext;
                if (serverCert != null) {
                    sslContext = getServerSSLContext();
                } else {
                    sslContext = getSSLContext();
                }
                Header[] defaultHeaders
                        = new Header[]{new BasicHeader("Authorization",
                                    "ApiKey " + encodedApiKey)};

                final CredentialsProvider credentialsProvider
                        = new BasicCredentialsProvider();
                credentialsProvider.setCredentials(AuthScope.ANY,
                        new UsernamePasswordCredentials(elasticUser, elasticPassword));
                RestClient restClient = RestClient.builder(
                        new HttpHost(host, port, "https"))
                        .setDefaultHeaders(defaultHeaders)
                        .setHttpClientConfigCallback(new HttpClientConfigCallback() {
                            @Override
                            public HttpAsyncClientBuilder customizeHttpClient(
                                    HttpAsyncClientBuilder httpClientBuilder) {
                                return httpClientBuilder
                                        .setSSLContext(sslContext)
                                        .setDefaultCredentialsProvider(credentialsProvider);
                            }
                        }).build();

                transport = new RestClientTransport(restClient, new JacksonJsonpMapper());
            } else {
                logger.info("Creating client");
                final CredentialsProvider credentialsProvider
                        = new BasicCredentialsProvider();
                credentialsProvider.setCredentials(AuthScope.ANY,
                        new UsernamePasswordCredentials(elasticUser, elasticPassword));
                RestClient restClient = RestClient.builder(
                        new HttpHost(host, port))
                        .setHttpClientConfigCallback(new HttpClientConfigCallback() {
                            @Override
                            public HttpAsyncClientBuilder customizeHttpClient(
                                    HttpAsyncClientBuilder httpClientBuilder) {
                                return httpClientBuilder
                                        .setDefaultCredentialsProvider(credentialsProvider);
                            }
                        }).build();

                transport = new RestClientTransport(restClient, new JacksonJsonpMapper());
            }
        } catch (Exception ex) {
            logger.error("Exception while trying to create the ElasticsearchAsyncClient! [" + ex.getMessage() + "]", ex);
        }
        return new ElasticsearchClient(transport);
    }

    @Bean
    public ElasticsearchAsyncClient elasticSearchAsyncClient() {
        ElasticsearchTransport transport = null;
        try {
            if (encodedApiKey != null) {
                logger.info("Creating SSL Async client");
                SSLContext sslContext;
                if (serverCert != null) {
                    sslContext = getServerSSLContext();
                } else {
                    sslContext = getSSLContext();
                }
                final Header[] defaultHeaders
                        = new Header[]{new BasicHeader("Authorization",
                                    "ApiKey " + encodedApiKey)};
                final CredentialsProvider credentialsProvider
                        = new BasicCredentialsProvider();
                credentialsProvider.setCredentials(AuthScope.ANY,
                        new UsernamePasswordCredentials(elasticUser, elasticPassword));

                RestClient restClient = RestClient.builder(
                        new HttpHost(host, port, "https"))
                        .setDefaultHeaders(defaultHeaders)
                        .setHttpClientConfigCallback(new HttpClientConfigCallback() {
                            @Override
                            public HttpAsyncClientBuilder customizeHttpClient(
                                    HttpAsyncClientBuilder httpClientBuilder) {
                                return httpClientBuilder
                                        .setSSLContext(sslContext)
                                        .setDefaultCredentialsProvider(credentialsProvider);
                            }
                        }).build();
                transport = new RestClientTransport(restClient, new JacksonJsonpMapper());

            } else {
                logger.info("Creating Async client");
                final CredentialsProvider credentialsProvider
                        = new BasicCredentialsProvider();
                credentialsProvider.setCredentials(AuthScope.ANY,
                        new UsernamePasswordCredentials(elasticUser, elasticPassword));
                RestClient restClient = RestClient.builder(
                        new HttpHost(host, port))
                        .setHttpClientConfigCallback(new HttpClientConfigCallback() {
                            @Override
                            public HttpAsyncClientBuilder customizeHttpClient(
                                    HttpAsyncClientBuilder httpClientBuilder) {
                                return httpClientBuilder
                                        .setDefaultCredentialsProvider(credentialsProvider);
                            }
                        }).build();

                transport = new RestClientTransport(
                        restClient, new JacksonJsonpMapper());
            }
        } catch (Exception ex) {
            logger.error("Exception while trying to create the ElasticsearchAsyncClient! [" + ex.getMessage() + "]", ex);
        }
        return new ElasticsearchAsyncClient(transport);
    }

    /**
     * @return the id
     */
    public UUID getId() {
        if (id == null) {
            id = UUID.randomUUID();
        }
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(UUID id) {
        this.id = id;
    }

    /**
     * @return the initialized
     */
    public Boolean getInitialized() {
        return initialized;
    }

    /**
     * @param initialized the initialized to set
     */
    public void setInitialized(Boolean initialized) {
        this.initialized = initialized;
    }

    /**
     *
     * @param listener
     */
    public static void subscribe(ElasticsearchClientListener listener) {
        synchronized (LOCK) {
            if (elasticPassword != null) {
                listener.setClient();
            } else {
                listenerMap.put(listener.getId(), listener);
            }
        }
    }

    /**
     *
     * @param listener
     */
    public static void unsubscribe(ElasticsearchClientListener listener) {
        synchronized (LOCK) {
            listenerMap.remove(listener.getId());
        }
    }

}
