package com.swiftcryptollc.commons.adabounties.es.data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
@Document(indexName = "user-agent")
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserAgent implements Serializable {

    @Transient
    private static final long serialVersionUID = 100031L;

    @Id
    protected String sessionId;
    @Field(type = FieldType.Keyword)
    protected String sourceIp;
    @Field(type = FieldType.Keyword)
    protected String accountId;
    @Field(type = FieldType.Keyword)
    protected String appCodeName;
    @Field(type = FieldType.Keyword)
    protected String appName;
    @Field(type = FieldType.Keyword)
    protected String appVersion;
    @Field(type = FieldType.Keyword)
    protected String language;
    @Field(type = FieldType.Keyword)
    protected List<String> languages;
    @Field(type = FieldType.Keyword)
    protected String platform;
    @Field
    protected String userAgent;
    @Field(type = FieldType.Keyword)
    protected String vendor;
    @Field(type = FieldType.Keyword)
    protected String vendorSub;
    @Field
    protected Long loginTimeMs;

    public UserAgent() {
        loginTimeMs = new Date().getTime();
    }

    /**
     * @return the appCodeName
     */
    public String getAppCodeName() {
        return appCodeName;
    }

    /**
     * @param appCodeName the appCodeName to set
     */
    public void setAppCodeName(String appCodeName) {
        this.appCodeName = appCodeName;
    }

    /**
     * @return the appName
     */
    public String getAppName() {
        return appName;
    }

    /**
     * @param appName the appName to set
     */
    public void setAppName(String appName) {
        this.appName = appName;
    }

    /**
     * @return the appVersion
     */
    public String getAppVersion() {
        return appVersion;
    }

    /**
     * @param appVersion the appVersion to set
     */
    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    /**
     * @return the language
     */
    public String getLanguage() {
        return language;
    }

    /**
     * @param language the language to set
     */
    public void setLanguage(String language) {
        this.language = language;
    }

    /**
     * @return the languages
     */
    public List<String> getLanguages() {
        return languages;
    }

    /**
     * @param languages the languages to set
     */
    public void setLanguages(List<String> languages) {
        this.languages = languages;
    }

    /**
     * @return the platform
     */
    public String getPlatform() {
        return platform;
    }

    /**
     * @param platform the platform to set
     */
    public void setPlatform(String platform) {
        this.platform = platform;
    }

    /**
     * @return the userAgent
     */
    public String getUserAgent() {
        return userAgent;
    }

    /**
     * @param userAgent the userAgent to set
     */
    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    /**
     * @return the vendor
     */
    public String getVendor() {
        return vendor;
    }

    /**
     * @param vendor the vendor to set
     */
    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    /**
     * @return the vendorSub
     */
    public String getVendorSub() {
        return vendorSub;
    }

    /**
     * @param vendorSub the vendorSub to set
     */
    public void setVendorSub(String vendorSub) {
        this.vendorSub = vendorSub;
    }

    /**
     * @return the sessionId
     */
    public String getSessionId() {
        return sessionId;
    }

    /**
     * @param sessionId the sessionId to set
     */
    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    /**
     * @return the loginTimeMs
     */
    public Long getLoginTimeMs() {
        if (loginTimeMs == null) {
            loginTimeMs = new Date().getTime();
        }
        return loginTimeMs;
    }

    /**
     * @param loginTimeMs the loginTimeMs to set
     */
    public void setLoginTimeMs(Long loginTimeMs) {
        this.loginTimeMs = loginTimeMs;
    }

    /**
     * @return the sourceIp
     */
    public String getSourceIp() {
        return sourceIp;
    }

    /**
     * @param sourceIp the sourceIp to set
     */
    public void setSourceIp(String sourceIp) {
        this.sourceIp = sourceIp;
    }
}
