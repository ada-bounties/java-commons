package com.swiftcryptollc.commons.adabounties.es.query;

import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.elasticsearch._types.FieldSort;
import co.elastic.clients.elasticsearch._types.SortOptions;
import co.elastic.clients.elasticsearch._types.SortOrder;

import co.elastic.clients.elasticsearch.core.SearchResponse;
import co.elastic.clients.elasticsearch.core.search.Hit;
import co.elastic.clients.elasticsearch.core.search.HitsMetadata;
import com.swiftcryptollc.commons.adabounties.es.ElasticsearchClientConfig;
import com.swiftcryptollc.commons.adabounties.es.ElasticsearchClientListener;
import com.swiftcryptollc.commons.adabounties.es.data.AccountIdMap;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import javax.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
@Service
public final class AccountIdMapQueryHandler implements ElasticsearchClientListener {

    @Autowired
    protected ElasticsearchClientConfig esClient;
    protected ElasticsearchClient client;
    protected UUID id;
    protected final static Logger logger = LoggerFactory.getLogger(AccountIdMapQueryHandler.class);

    /**
     *
     */
    public AccountIdMapQueryHandler() {
    }

    /**
     *
     * @param client
     */
    public AccountIdMapQueryHandler(ElasticsearchClient client) {
        this.client = client;
    }

    /**
     * @return the id
     */
    @Override
    public UUID getId() {
        if (id == null) {
            id = UUID.randomUUID();
        }
        return id;
    }

    @Override
    public void setClient() {
        if (esClient != null) {
            client = esClient.elasticSearchClient();
        }
    }

    /**
     *
     */
    @PostConstruct
    public void init() {
        if (client == null) {
            ElasticsearchClientConfig.subscribe(this);
        }
    }

    /**
     * Get account Ids
     *
     * @param pageable
     * @return
     */
    public Set<AccountIdMap> findAllAccounts(PageRequest pageable) {
        Set<AccountIdMap> accountIds = new HashSet<>();
        SortOptions sortOptions = SortOptions.of(fn -> 
                fn.field(FieldSort.of(fn2 -> fn2.field("accountId").order(SortOrder.Desc))));
        try {
            SearchResponse<AccountIdMap> response = client.search(t
                    -> t.index(AccountIdMap.INDEX_NAME)
                            .sort(sortOptions)
                            .from((pageable.getPageNumber() - 1) * pageable.getPageSize())
                            .size(pageable.getPageSize()), AccountIdMap.class);

            HitsMetadata<AccountIdMap> searchHits = response.hits();

            for (Hit<AccountIdMap> hit : searchHits.hits()) {
                accountIds.add(hit.source());
            }
        } catch (Exception ex) {
            logger.error("Exception during findAllAccounts! [" + ex.getMessage() + "]");
        }
        return accountIds;
    }

    /**
     * @param esClient the esClient to set
     */
    public void setEsClient(ElasticsearchClientConfig esClient) {
        this.esClient = esClient;
    }

    /**
     * @param client the client to set
     */
    public void setClient(ElasticsearchClient client) {
        this.client = client;
    }

}
