package com.swiftcryptollc.commons.adabounties.es.query;

import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.elasticsearch._types.query_dsl.BoolQuery;
import co.elastic.clients.elasticsearch._types.query_dsl.MatchQuery;
import co.elastic.clients.elasticsearch._types.query_dsl.Query;
import co.elastic.clients.elasticsearch.core.SearchResponse;
import co.elastic.clients.elasticsearch.core.search.Hit;
import co.elastic.clients.elasticsearch.core.search.HitsMetadata;
import com.swiftcryptollc.commons.adabounties.es.ElasticsearchClientConfig;
import com.swiftcryptollc.commons.adabounties.es.ElasticsearchClientListener;
import com.swiftcryptollc.commons.adabounties.redis.cache.AliasCache;
import com.swiftcryptollc.commons.adabounties.es.data.Leaderboard;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import javax.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
@Service
public class LeaderboardQueryHandler implements ElasticsearchClientListener {

    @Autowired
    protected ElasticsearchClientConfig esClient;
    @Autowired
    protected AliasCache aliasCache;
    protected ElasticsearchClient client;
    protected UUID id;

    protected final static Logger logger = LoggerFactory.getLogger(LeaderboardQueryHandler.class);

    public LeaderboardQueryHandler() {

    }

    /**
     *
     * @param client
     */
    public LeaderboardQueryHandler(ElasticsearchClient client) {
        this.client = client;
    }

    /**
     * @return the id
     */
    public UUID getId() {
        if (id == null) {
            id = UUID.randomUUID();
        }
        return id;
    }

    @Override
    public void setClient() {
        if (esClient != null) {
            client = esClient.elasticSearchClient();
        }
    }

    /**
     * @param client the client to set
     */
    public void setClient(ElasticsearchClient client) {
        this.client = client;
    }

    /**
     *
     */
    @PostConstruct
    public void init() {
        if (client == null) {
            ElasticsearchClientConfig.subscribe(this);
        }
    }

    /**
     *
     * @param type
     * @param ymd
     * @return
     */
    public Leaderboard getLeaderboard(String type, String ymd) {
        Leaderboard leaderboard = null;
        List<Query> queryList = new ArrayList<>();
        queryList.add(MatchQuery.of(fn -> fn.field("type").query(type))._toQuery());
        queryList.add(MatchQuery.of(fn -> fn.field("ymd").query(ymd))._toQuery());
        Query bQuery = BoolQuery.of(fn -> fn.must(queryList))._toQuery();

        try {
            // Should only be one
            SearchResponse<Leaderboard> response = client.search(t -> t.index(Leaderboard.INDEX_NAME)
                    .query(bQuery)
                    .size(1), Leaderboard.class);
            HitsMetadata<Leaderboard> searchHits = response.hits();

            for (Hit<Leaderboard> hit : searchHits.hits()) {
                leaderboard = hit.source();
                break;
            }
        } catch (Exception ex) {
            logger.error("Exception during leadership search! [" + ex.getMessage() + "]", ex);
        }

        return leaderboard;
    }

    /**
     *
     * @param ymd
     * @return
     */
    public Set<Leaderboard> getAllLeaderboards(String ymd) {
        Set<Leaderboard> leaderboards = new HashSet<>();
        List<Query> queryList = new ArrayList<>();
        queryList.add(MatchQuery.of(fn -> fn.field("ymd").query(ymd))._toQuery());
        Query bQuery = BoolQuery.of(fn -> fn.must(queryList))._toQuery();

        try {
            // Should only be one
            SearchResponse<Leaderboard> response = client.search(t -> t.index(Leaderboard.INDEX_NAME)
                    .query(bQuery)
                    .size(100), Leaderboard.class);
            HitsMetadata<Leaderboard> searchHits = response.hits();

            for (Hit<Leaderboard> hit : searchHits.hits()) {
                leaderboards.add(hit.source());
            }
        } catch (Exception ex) {
            logger.error("Exception during leadership search! [" + ex.getMessage() + "]", ex);
        }

        return leaderboards;
    }
}
