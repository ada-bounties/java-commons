package com.swiftcryptollc.commons.adabounties.es.data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.swiftcryptollc.commons.adabounties.utilities.data.Base64Id;
import java.io.Serializable;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
@Document(indexName = "system-metric")
@JsonIgnoreProperties({"_class", "_id"})
public class SystemMetric implements Serializable {

    @Transient
    private static final long serialVersionUID = 100018L;

    @Id
    protected String id;
    @Field
    protected Long updateTimeMs;
    @Field
    protected Long openBounties;
    @Field
    protected Long closedBounties;
    @Field
    protected Long claimedBounties;
    @Field
    protected Long expiredBounties;
    @Field
    protected Long staleBounties;
    @Field
    protected Long totalBounties;
    @Field
    protected Double perClmdBty;
    @Field
    protected Long openClaims;
    @Field
    protected Long closedClaims;
    @Field
    protected Long acceptedClaims;
    @Field
    protected Long rejectedClaims;
    @Field
    protected Long totalClaims;
    @Field
    protected Long totalUsers;
    @Field
    protected Double growthOpenBounties;
    @Field
    protected Double growthClosedBounties;
    @Field
    protected Double growthClaimedBounties;
    @Field
    protected Double growthExpiredBounties;
    @Field
    protected Double growthStaleBounties;
    @Field
    protected Double growthTotalBounties;
    @Field
    protected Double growthOpenClaims;
    @Field
    protected Double growthClosedClaims;
    @Field
    protected Double growthAcceptedClaims;
    @Field
    protected Double growthRejectedClaims;
    @Field
    protected Double growthTotalClaims;
    @Field
    protected Double growthTotalUsers;
    @Field(type = FieldType.Keyword)
    protected String lastXDays;
    @Transient
    protected Integer page;
    @Transient
    public final static transient String INDEX_NAME = "system-metric";

    public SystemMetric() {
    }

    /**
     * @return the id
     */
    public String getId() {
        if (id == null) {
            id = Base64Id.getNewId();
        }
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the openBounties
     */
    public Long getOpenBounties() {
        return openBounties;
    }

    /**
     * @param openBounties the openBounties to set
     */
    public void setOpenBounties(Long openBounties) {
        this.openBounties = openBounties;
    }

    /**
     * @return the closedBounties
     */
    public Long getClosedBounties() {
        return closedBounties;
    }

    /**
     * @param closedBounties the closedBounties to set
     */
    public void setClosedBounties(Long closedBounties) {
        this.closedBounties = closedBounties;
    }

    /**
     * @return the claimedBounties
     */
    public Long getClaimedBounties() {
        return claimedBounties;
    }

    /**
     * @param claimedBounties the claimedBounties to set
     */
    public void setClaimedBounties(Long claimedBounties) {
        this.claimedBounties = claimedBounties;
    }

    /**
     * @return the expiredBounties
     */
    public Long getExpiredBounties() {
        return expiredBounties;
    }

    /**
     * @param expiredBounties the expiredBounties to set
     */
    public void setExpiredBounties(Long expiredBounties) {
        this.expiredBounties = expiredBounties;
    }

    /**
     * @return the staleBounties
     */
    public Long getStaleBounties() {
        return staleBounties;
    }

    /**
     * @param staleBounties the staleBounties to set
     */
    public void setStaleBounties(Long staleBounties) {
        this.staleBounties = staleBounties;
    }

    /**
     * @return the openClaims
     */
    public Long getOpenClaims() {
        return openClaims;
    }

    /**
     * @param openClaims the openClaims to set
     */
    public void setOpenClaims(Long openClaims) {
        this.openClaims = openClaims;
    }

    /**
     * @return the closedClaims
     */
    public Long getClosedClaims() {
        return closedClaims;
    }

    /**
     * @param closedClaims the closedClaims to set
     */
    public void setClosedClaims(Long closedClaims) {
        this.closedClaims = closedClaims;
    }

    /**
     * @return the acceptedClaims
     */
    public Long getAcceptedClaims() {
        return acceptedClaims;
    }

    /**
     * @param acceptedClaims the acceptedClaims to set
     */
    public void setAcceptedClaims(Long acceptedClaims) {
        this.acceptedClaims = acceptedClaims;
    }

    /**
     * @return the rejectedClaims
     */
    public Long getRejectedClaims() {
        return rejectedClaims;
    }

    /**
     * @param rejectedClaims the rejectedClaims to set
     */
    public void setRejectedClaims(Long rejectedClaims) {
        this.rejectedClaims = rejectedClaims;
    }

    /**
     * @return the lastXDays
     */
    public String getLastXDays() {
        return lastXDays;
    }

    /**
     * @param lastXDays the lastXDays to set
     */
    public void setLastXDays(String lastXDays) {
        this.lastXDays = lastXDays;
    }

    /**
     * @return the totalBounties
     */
    public Long getTotalBounties() {
        return totalBounties;
    }

    /**
     * @param totalBounties the totalBounties to set
     */
    public void setTotalBounties(Long totalBounties) {
        this.totalBounties = totalBounties;
    }

    /**
     * @return the totalUsers
     */
    public Long getTotalUsers() {
        return totalUsers;
    }

    /**
     * @param totalUsers the totalUsers to set
     */
    public void setTotalUsers(Long totalUsers) {
        this.totalUsers = totalUsers;
    }

    /**
     * @return the totalClaims
     */
    public Long getTotalClaims() {
        return totalClaims;
    }

    /**
     * @param totalClaims the totalClaims to set
     */
    public void setTotalClaims(Long totalClaims) {
        this.totalClaims = totalClaims;
    }

    /**
     * @return the perClmdBty
     */
    public Double getPerClmdBty() {
        return perClmdBty;
    }

    /**
     * @param perClmdBty the perClmdBty to set
     */
    public void setPerClmdBty(Double perClmdBty) {
        this.perClmdBty = perClmdBty;
    }

    /**
     * @return the updateTimeMs
     */
    public Long getUpdateTimeMs() {
        return updateTimeMs;
    }

    /**
     * @param updateTimeMs the updateTimeMs to set
     */
    public void setUpdateTimeMs(Long updateTimeMs) {
        this.updateTimeMs = updateTimeMs;
    }

    /**
     * @return the growthOpenBounties
     */
    public Double getGrowthOpenBounties() {
        return growthOpenBounties;
    }

    /**
     * @param growthOpenBounties the growthOpenBounties to set
     */
    public void setGrowthOpenBounties(Double growthOpenBounties) {
        this.growthOpenBounties = growthOpenBounties;
    }

    /**
     * @return the growthClosedBounties
     */
    public Double getGrowthClosedBounties() {
        return growthClosedBounties;
    }

    /**
     * @param growthClosedBounties the growthClosedBounties to set
     */
    public void setGrowthClosedBounties(Double growthClosedBounties) {
        this.growthClosedBounties = growthClosedBounties;
    }

    /**
     * @return the growthClaimedBounties
     */
    public Double getGrowthClaimedBounties() {
        return growthClaimedBounties;
    }

    /**
     * @param growthClaimedBounties the growthClaimedBounties to set
     */
    public void setGrowthClaimedBounties(Double growthClaimedBounties) {
        this.growthClaimedBounties = growthClaimedBounties;
    }

    /**
     * @return the growthExpiredBounties
     */
    public Double getGrowthExpiredBounties() {
        return growthExpiredBounties;
    }

    /**
     * @param growthExpiredBounties the growthExpiredBounties to set
     */
    public void setGrowthExpiredBounties(Double growthExpiredBounties) {
        this.growthExpiredBounties = growthExpiredBounties;
    }

    /**
     * @return the growthStaleBounties
     */
    public Double getGrowthStaleBounties() {
        return growthStaleBounties;
    }

    /**
     * @param growthStaleBounties the growthStaleBounties to set
     */
    public void setGrowthStaleBounties(Double growthStaleBounties) {
        this.growthStaleBounties = growthStaleBounties;
    }

    /**
     * @return the growthTotalBounties
     */
    public Double getGrowthTotalBounties() {
        return growthTotalBounties;
    }

    /**
     * @param growthTotalBounties the growthTotalBounties to set
     */
    public void setGrowthTotalBounties(Double growthTotalBounties) {
        this.growthTotalBounties = growthTotalBounties;
    }

    /**
     * @return the growthOpenClaims
     */
    public Double getGrowthOpenClaims() {
        return growthOpenClaims;
    }

    /**
     * @param growthOpenClaims the growthOpenClaims to set
     */
    public void setGrowthOpenClaims(Double growthOpenClaims) {
        this.growthOpenClaims = growthOpenClaims;
    }

    /**
     * @return the growthClosedClaims
     */
    public Double getGrowthClosedClaims() {
        return growthClosedClaims;
    }

    /**
     * @param growthClosedClaims the growthClosedClaims to set
     */
    public void setGrowthClosedClaims(Double growthClosedClaims) {
        this.growthClosedClaims = growthClosedClaims;
    }

    /**
     * @return the growthAcceptedClaims
     */
    public Double getGrowthAcceptedClaims() {
        return growthAcceptedClaims;
    }

    /**
     * @param growthAcceptedClaims the growthAcceptedClaims to set
     */
    public void setGrowthAcceptedClaims(Double growthAcceptedClaims) {
        this.growthAcceptedClaims = growthAcceptedClaims;
    }

    /**
     * @return the growthRejectedClaims
     */
    public Double getGrowthRejectedClaims() {
        return growthRejectedClaims;
    }

    /**
     * @param growthRejectedClaims the growthRejectedClaims to set
     */
    public void setGrowthRejectedClaims(Double growthRejectedClaims) {
        this.growthRejectedClaims = growthRejectedClaims;
    }

    /**
     * @return the growthTotalClaims
     */
    public Double getGrowthTotalClaims() {
        return growthTotalClaims;
    }

    /**
     * @param growthTotalClaims the growthTotalClaims to set
     */
    public void setGrowthTotalClaims(Double growthTotalClaims) {
        this.growthTotalClaims = growthTotalClaims;
    }

    /**
     * @return the growthTotalUsers
     */
    public Double getGrowthTotalUsers() {
        return growthTotalUsers;
    }

    /**
     * @param growthTotalUsers the growthTotalUsers to set
     */
    public void setGrowthTotalUsers(Double growthTotalUsers) {
        this.growthTotalUsers = growthTotalUsers;
    }

    /**
     * @return the page
     */
    public Integer getPage() {
        return page;
    }

    /**
     * @param page the page to set
     */
    public void setPage(Integer page) {
        this.page = page;
    }
}
