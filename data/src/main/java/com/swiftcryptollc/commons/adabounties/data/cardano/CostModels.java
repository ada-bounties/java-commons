
package com.swiftcryptollc.commons.adabounties.data.cardano;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("jsonschema2pojo")
public class CostModels {

    @SerializedName("PlutusV1")
    @Expose
    private PlutusV1 plutusV1;
    @SerializedName("PlutusV2")
    @Expose
    private PlutusV2 plutusV2;

    /**
     * No args constructor for use in serialization
     * 
     */
    public CostModels() {
    }

    /**
     * 
     * @param plutusV2
     * @param plutusV1
     */
    public CostModels(PlutusV1 plutusV1, PlutusV2 plutusV2) {
        super();
        this.plutusV1 = plutusV1;
        this.plutusV2 = plutusV2;
    }

    public PlutusV1 getPlutusV1() {
        return plutusV1;
    }

    public void setPlutusV1(PlutusV1 plutusV1) {
        this.plutusV1 = plutusV1;
    }

    public CostModels withPlutusV1(PlutusV1 plutusV1) {
        this.plutusV1 = plutusV1;
        return this;
    }

    public PlutusV2 getPlutusV2() {
        return plutusV2;
    }

    public void setPlutusV2(PlutusV2 plutusV2) {
        this.plutusV2 = plutusV2;
    }

    public CostModels withPlutusV2(PlutusV2 plutusV2) {
        this.plutusV2 = plutusV2;
        return this;
    }

}
