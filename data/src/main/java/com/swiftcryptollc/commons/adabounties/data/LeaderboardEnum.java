package com.swiftcryptollc.commons.adabounties.data;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
public enum LeaderboardEnum {
    // From Reputation
    REPUTATION("rep"),
    BOUNTY_COMMENTS("btycmt"),
    BOUNTY_UPVOTES("btyup"),
    BOUNTY_DOWNVOTES("btydwn"),
    COMMENT_UPVOTES("cmtup"),
    COMMENT_DOWNVOTES("cmtdwn"),
    REPLYS_TO_COMMENTS("replyto"),
    CLAIMS_ON_BOUNTIES("claimson"),
    // From User Metrics
    OPEN_BOUNTIES("openbty"),
    CLAIMED_BOUNTIES("clmdbty"),
    EXPIRED_BOUNTIES("expbty"),
    STALE_BOUNTIES("stalebty"),
    TOTAL_BOUNTIES("totbty"),
    OPEN_CLAIMS("openclm"),
    ACCEPTED_CLAIMS("accclm"),
    REJ_CLAIMS("rejclm"),
    TOTAL_CLAIMS("totclm"),
    // TBT
    TBT_ADA("tbtada"),
    // TCT
    TCT_ADA("tctada");

    public final String label;

    private LeaderboardEnum(String label) {
        this.label = label;
    }

    public static LeaderboardEnum valueOfLabel(String label) {
        for (LeaderboardEnum type : values()) {
            if (type.label.equalsIgnoreCase(label)) {
                return type;
            }
        }
        return null;
    }

    /**
     *
     * @return
     */
    public String toString() {
        return label;
    }
}
