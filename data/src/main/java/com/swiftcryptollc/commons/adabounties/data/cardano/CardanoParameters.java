
package com.swiftcryptollc.commons.adabounties.data.cardano;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("jsonschema2pojo")
public class CardanoParameters {

    @SerializedName("epoch")
    @Expose
    private Integer epoch;
    @SerializedName("min_fee_a")
    @Expose
    private Integer minFeeA;
    @SerializedName("min_fee_b")
    @Expose
    private Integer minFeeB;
    @SerializedName("max_block_size")
    @Expose
    private Integer maxBlockSize;
    @SerializedName("max_tx_size")
    @Expose
    private Integer maxTxSize;
    @SerializedName("max_block_header_size")
    @Expose
    private Integer maxBlockHeaderSize;
    @SerializedName("key_deposit")
    @Expose
    private String keyDeposit;
    @SerializedName("pool_deposit")
    @Expose
    private String poolDeposit;
    @SerializedName("e_max")
    @Expose
    private Integer eMax;
    @SerializedName("n_opt")
    @Expose
    private Integer nOpt;
    @SerializedName("a0")
    @Expose
    private Double a0;
    @SerializedName("rho")
    @Expose
    private Double rho;
    @SerializedName("tau")
    @Expose
    private Double tau;
    @SerializedName("decentralisation_param")
    @Expose
    private Integer decentralisationParam;
    @SerializedName("extra_entropy")
    @Expose
    private Object extraEntropy;
    @SerializedName("protocol_major_ver")
    @Expose
    private Integer protocolMajorVer;
    @SerializedName("protocol_minor_ver")
    @Expose
    private Integer protocolMinorVer;
    @SerializedName("min_utxo")
    @Expose
    private String minUtxo;
    @SerializedName("min_pool_cost")
    @Expose
    private String minPoolCost;
    @SerializedName("nonce")
    @Expose
    private String nonce;
    @SerializedName("cost_models")
    @Expose
    private CostModels costModels;
    @SerializedName("price_mem")
    @Expose
    private Double priceMem;
    @SerializedName("price_step")
    @Expose
    private Double priceStep;
    @SerializedName("max_tx_ex_mem")
    @Expose
    private String maxTxExMem;
    @SerializedName("max_tx_ex_steps")
    @Expose
    private String maxTxExSteps;
    @SerializedName("max_block_ex_mem")
    @Expose
    private String maxBlockExMem;
    @SerializedName("max_block_ex_steps")
    @Expose
    private String maxBlockExSteps;
    @SerializedName("max_val_size")
    @Expose
    private String maxValSize;
    @SerializedName("collateral_percent")
    @Expose
    private Integer collateralPercent;
    @SerializedName("max_collateral_inputs")
    @Expose
    private Integer maxCollateralInputs;
    @SerializedName("coins_per_utxo_size")
    @Expose
    private String coinsPerUtxoSize;
    @SerializedName("coins_per_utxo_word")
    @Expose
    private String coinsPerUtxoWord;

    /**
     * No args constructor for use in serialization
     * 
     */
    public CardanoParameters() {
    }

    /**
     * 
     * @param maxBlockSize
     * @param keyDeposit
     * @param maxCollateralInputs
     * @param maxValSize
     * @param epoch
     * @param protocolMajorVer
     * @param protocolMinorVer
     * @param minFeeB
     * @param minFeeA
     * @param eMax
     * @param maxBlockHeaderSize
     * @param poolDeposit
     * @param maxTxSize
     * @param priceMem
     * @param nOpt
     * @param decentralisationParam
     * @param maxTxExSteps
     * @param maxBlockExSteps
     * @param collateralPercent
     * @param minUtxo
     * @param tau
     * @param costModels
     * @param nonce
     * @param a0
     * @param minPoolCost
     * @param maxTxExMem
     * @param coinsPerUtxoWord
     * @param extraEntropy
     * @param rho
     * @param maxBlockExMem
     * @param coinsPerUtxoSize
     * @param priceStep
     */
    public CardanoParameters(Integer epoch, Integer minFeeA, Integer minFeeB, Integer maxBlockSize, Integer maxTxSize, Integer maxBlockHeaderSize, String keyDeposit, String poolDeposit, Integer eMax, Integer nOpt, Double a0, Double rho, Double tau, Integer decentralisationParam, Object extraEntropy, Integer protocolMajorVer, Integer protocolMinorVer, String minUtxo, String minPoolCost, String nonce, CostModels costModels, Double priceMem, Double priceStep, String maxTxExMem, String maxTxExSteps, String maxBlockExMem, String maxBlockExSteps, String maxValSize, Integer collateralPercent, Integer maxCollateralInputs, String coinsPerUtxoSize, String coinsPerUtxoWord) {
        super();
        this.epoch = epoch;
        this.minFeeA = minFeeA;
        this.minFeeB = minFeeB;
        this.maxBlockSize = maxBlockSize;
        this.maxTxSize = maxTxSize;
        this.maxBlockHeaderSize = maxBlockHeaderSize;
        this.keyDeposit = keyDeposit;
        this.poolDeposit = poolDeposit;
        this.eMax = eMax;
        this.nOpt = nOpt;
        this.a0 = a0;
        this.rho = rho;
        this.tau = tau;
        this.decentralisationParam = decentralisationParam;
        this.extraEntropy = extraEntropy;
        this.protocolMajorVer = protocolMajorVer;
        this.protocolMinorVer = protocolMinorVer;
        this.minUtxo = minUtxo;
        this.minPoolCost = minPoolCost;
        this.nonce = nonce;
        this.costModels = costModels;
        this.priceMem = priceMem;
        this.priceStep = priceStep;
        this.maxTxExMem = maxTxExMem;
        this.maxTxExSteps = maxTxExSteps;
        this.maxBlockExMem = maxBlockExMem;
        this.maxBlockExSteps = maxBlockExSteps;
        this.maxValSize = maxValSize;
        this.collateralPercent = collateralPercent;
        this.maxCollateralInputs = maxCollateralInputs;
        this.coinsPerUtxoSize = coinsPerUtxoSize;
        this.coinsPerUtxoWord = coinsPerUtxoWord;
    }

    public Integer getEpoch() {
        return epoch;
    }

    public void setEpoch(Integer epoch) {
        this.epoch = epoch;
    }

    public CardanoParameters withEpoch(Integer epoch) {
        this.epoch = epoch;
        return this;
    }

    public Integer getMinFeeA() {
        return minFeeA;
    }

    public void setMinFeeA(Integer minFeeA) {
        this.minFeeA = minFeeA;
    }

    public CardanoParameters withMinFeeA(Integer minFeeA) {
        this.minFeeA = minFeeA;
        return this;
    }

    public Integer getMinFeeB() {
        return minFeeB;
    }

    public void setMinFeeB(Integer minFeeB) {
        this.minFeeB = minFeeB;
    }

    public CardanoParameters withMinFeeB(Integer minFeeB) {
        this.minFeeB = minFeeB;
        return this;
    }

    public Integer getMaxBlockSize() {
        return maxBlockSize;
    }

    public void setMaxBlockSize(Integer maxBlockSize) {
        this.maxBlockSize = maxBlockSize;
    }

    public CardanoParameters withMaxBlockSize(Integer maxBlockSize) {
        this.maxBlockSize = maxBlockSize;
        return this;
    }

    public Integer getMaxTxSize() {
        return maxTxSize;
    }

    public void setMaxTxSize(Integer maxTxSize) {
        this.maxTxSize = maxTxSize;
    }

    public CardanoParameters withMaxTxSize(Integer maxTxSize) {
        this.maxTxSize = maxTxSize;
        return this;
    }

    public Integer getMaxBlockHeaderSize() {
        return maxBlockHeaderSize;
    }

    public void setMaxBlockHeaderSize(Integer maxBlockHeaderSize) {
        this.maxBlockHeaderSize = maxBlockHeaderSize;
    }

    public CardanoParameters withMaxBlockHeaderSize(Integer maxBlockHeaderSize) {
        this.maxBlockHeaderSize = maxBlockHeaderSize;
        return this;
    }

    public String getKeyDeposit() {
        return keyDeposit;
    }

    public void setKeyDeposit(String keyDeposit) {
        this.keyDeposit = keyDeposit;
    }

    public CardanoParameters withKeyDeposit(String keyDeposit) {
        this.keyDeposit = keyDeposit;
        return this;
    }

    public String getPoolDeposit() {
        return poolDeposit;
    }

    public void setPoolDeposit(String poolDeposit) {
        this.poolDeposit = poolDeposit;
    }

    public CardanoParameters withPoolDeposit(String poolDeposit) {
        this.poolDeposit = poolDeposit;
        return this;
    }

    public Integer geteMax() {
        return eMax;
    }

    public void seteMax(Integer eMax) {
        this.eMax = eMax;
    }

    public CardanoParameters witheMax(Integer eMax) {
        this.eMax = eMax;
        return this;
    }

    public Integer getnOpt() {
        return nOpt;
    }

    public void setnOpt(Integer nOpt) {
        this.nOpt = nOpt;
    }

    public CardanoParameters withnOpt(Integer nOpt) {
        this.nOpt = nOpt;
        return this;
    }

    public Double getA0() {
        return a0;
    }

    public void setA0(Double a0) {
        this.a0 = a0;
    }

    public CardanoParameters withA0(Double a0) {
        this.a0 = a0;
        return this;
    }

    public Double getRho() {
        return rho;
    }

    public void setRho(Double rho) {
        this.rho = rho;
    }

    public CardanoParameters withRho(Double rho) {
        this.rho = rho;
        return this;
    }

    public Double getTau() {
        return tau;
    }

    public void setTau(Double tau) {
        this.tau = tau;
    }

    public CardanoParameters withTau(Double tau) {
        this.tau = tau;
        return this;
    }

    public Integer getDecentralisationParam() {
        return decentralisationParam;
    }

    public void setDecentralisationParam(Integer decentralisationParam) {
        this.decentralisationParam = decentralisationParam;
    }

    public CardanoParameters withDecentralisationParam(Integer decentralisationParam) {
        this.decentralisationParam = decentralisationParam;
        return this;
    }

    public Object getExtraEntropy() {
        return extraEntropy;
    }

    public void setExtraEntropy(Object extraEntropy) {
        this.extraEntropy = extraEntropy;
    }

    public CardanoParameters withExtraEntropy(Object extraEntropy) {
        this.extraEntropy = extraEntropy;
        return this;
    }

    public Integer getProtocolMajorVer() {
        return protocolMajorVer;
    }

    public void setProtocolMajorVer(Integer protocolMajorVer) {
        this.protocolMajorVer = protocolMajorVer;
    }

    public CardanoParameters withProtocolMajorVer(Integer protocolMajorVer) {
        this.protocolMajorVer = protocolMajorVer;
        return this;
    }

    public Integer getProtocolMinorVer() {
        return protocolMinorVer;
    }

    public void setProtocolMinorVer(Integer protocolMinorVer) {
        this.protocolMinorVer = protocolMinorVer;
    }

    public CardanoParameters withProtocolMinorVer(Integer protocolMinorVer) {
        this.protocolMinorVer = protocolMinorVer;
        return this;
    }

    public String getMinUtxo() {
        return minUtxo;
    }

    public void setMinUtxo(String minUtxo) {
        this.minUtxo = minUtxo;
    }

    public CardanoParameters withMinUtxo(String minUtxo) {
        this.minUtxo = minUtxo;
        return this;
    }

    public String getMinPoolCost() {
        return minPoolCost;
    }

    public void setMinPoolCost(String minPoolCost) {
        this.minPoolCost = minPoolCost;
    }

    public CardanoParameters withMinPoolCost(String minPoolCost) {
        this.minPoolCost = minPoolCost;
        return this;
    }

    public String getNonce() {
        return nonce;
    }

    public void setNonce(String nonce) {
        this.nonce = nonce;
    }

    public CardanoParameters withNonce(String nonce) {
        this.nonce = nonce;
        return this;
    }

    public CostModels getCostModels() {
        return costModels;
    }

    public void setCostModels(CostModels costModels) {
        this.costModels = costModels;
    }

    public CardanoParameters withCostModels(CostModels costModels) {
        this.costModels = costModels;
        return this;
    }

    public Double getPriceMem() {
        return priceMem;
    }

    public void setPriceMem(Double priceMem) {
        this.priceMem = priceMem;
    }

    public CardanoParameters withPriceMem(Double priceMem) {
        this.priceMem = priceMem;
        return this;
    }

    public Double getPriceStep() {
        return priceStep;
    }

    public void setPriceStep(Double priceStep) {
        this.priceStep = priceStep;
    }

    public CardanoParameters withPriceStep(Double priceStep) {
        this.priceStep = priceStep;
        return this;
    }

    public String getMaxTxExMem() {
        return maxTxExMem;
    }

    public void setMaxTxExMem(String maxTxExMem) {
        this.maxTxExMem = maxTxExMem;
    }

    public CardanoParameters withMaxTxExMem(String maxTxExMem) {
        this.maxTxExMem = maxTxExMem;
        return this;
    }

    public String getMaxTxExSteps() {
        return maxTxExSteps;
    }

    public void setMaxTxExSteps(String maxTxExSteps) {
        this.maxTxExSteps = maxTxExSteps;
    }

    public CardanoParameters withMaxTxExSteps(String maxTxExSteps) {
        this.maxTxExSteps = maxTxExSteps;
        return this;
    }

    public String getMaxBlockExMem() {
        return maxBlockExMem;
    }

    public void setMaxBlockExMem(String maxBlockExMem) {
        this.maxBlockExMem = maxBlockExMem;
    }

    public CardanoParameters withMaxBlockExMem(String maxBlockExMem) {
        this.maxBlockExMem = maxBlockExMem;
        return this;
    }

    public String getMaxBlockExSteps() {
        return maxBlockExSteps;
    }

    public void setMaxBlockExSteps(String maxBlockExSteps) {
        this.maxBlockExSteps = maxBlockExSteps;
    }

    public CardanoParameters withMaxBlockExSteps(String maxBlockExSteps) {
        this.maxBlockExSteps = maxBlockExSteps;
        return this;
    }

    public String getMaxValSize() {
        return maxValSize;
    }

    public void setMaxValSize(String maxValSize) {
        this.maxValSize = maxValSize;
    }

    public CardanoParameters withMaxValSize(String maxValSize) {
        this.maxValSize = maxValSize;
        return this;
    }

    public Integer getCollateralPercent() {
        return collateralPercent;
    }

    public void setCollateralPercent(Integer collateralPercent) {
        this.collateralPercent = collateralPercent;
    }

    public CardanoParameters withCollateralPercent(Integer collateralPercent) {
        this.collateralPercent = collateralPercent;
        return this;
    }

    public Integer getMaxCollateralInputs() {
        return maxCollateralInputs;
    }

    public void setMaxCollateralInputs(Integer maxCollateralInputs) {
        this.maxCollateralInputs = maxCollateralInputs;
    }

    public CardanoParameters withMaxCollateralInputs(Integer maxCollateralInputs) {
        this.maxCollateralInputs = maxCollateralInputs;
        return this;
    }

    public String getCoinsPerUtxoSize() {
        return coinsPerUtxoSize;
    }

    public void setCoinsPerUtxoSize(String coinsPerUtxoSize) {
        this.coinsPerUtxoSize = coinsPerUtxoSize;
    }

    public CardanoParameters withCoinsPerUtxoSize(String coinsPerUtxoSize) {
        this.coinsPerUtxoSize = coinsPerUtxoSize;
        return this;
    }

    public String getCoinsPerUtxoWord() {
        return coinsPerUtxoWord;
    }

    public void setCoinsPerUtxoWord(String coinsPerUtxoWord) {
        this.coinsPerUtxoWord = coinsPerUtxoWord;
    }

    public CardanoParameters withCoinsPerUtxoWord(String coinsPerUtxoWord) {
        this.coinsPerUtxoWord = coinsPerUtxoWord;
        return this;
    }

}
