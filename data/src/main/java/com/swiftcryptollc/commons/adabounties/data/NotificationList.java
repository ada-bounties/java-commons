package com.swiftcryptollc.commons.adabounties.data;

import com.swiftcryptollc.commons.adabounties.es.data.Notification;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
public class NotificationList {

    protected Integer page;
    protected Long numRead;
    protected Long numUnread;
    protected Boolean read;
    
    protected Set<Notification> notifications = new HashSet<>();
    
    public NotificationList() {
        
    }

    /**
     * @return the page
     */
    public Integer getPage() {
        return page;
    }

    /**
     * @param page the page to set
     */
    public void setPage(Integer page) {
        this.page = page;
    }

    /**
     * @return the notifications
     */
    public Set<Notification> getNotifications() {
        if (notifications == null) {
            notifications = new HashSet<>();
        }
        return notifications;
    }

    /**
     * @param notifications the notifications to set
     */
    public void setNotifications(Set<Notification> notifications) {
        this.notifications = notifications;
    }

    /**
     * @return the numRead
     */
    public Long getNumRead() {
        return numRead;
    }

    /**
     * @param numRead the numRead to set
     */
    public void setNumRead(Long numRead) {
        this.numRead = numRead;
    }

    /**
     * @return the numUnread
     */
    public Long getNumUnread() {
        return numUnread;
    }

    /**
     * @param numUnread the numUnread to set
     */
    public void setNumUnread(Long numUnread) {
        this.numUnread = numUnread;
    }

    /**
     * @return the read
     */
    public Boolean getRead() {
        return read;
    }

    /**
     * @param read the read to set
     */
    public void setRead(Boolean read) {
        this.read = read;
    }
}
