package com.swiftcryptollc.commons.adabounties.es.query;

import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.elasticsearch._types.FieldSort;
import co.elastic.clients.elasticsearch._types.FieldValue;
import co.elastic.clients.elasticsearch._types.SortOptions;
import co.elastic.clients.elasticsearch._types.SortOrder;
import co.elastic.clients.elasticsearch._types.aggregations.Aggregate;
import co.elastic.clients.elasticsearch._types.aggregations.MultiBucketBase;
import co.elastic.clients.elasticsearch._types.aggregations.StringTermsBucket;
import co.elastic.clients.elasticsearch._types.query_dsl.BoolQuery;
import co.elastic.clients.elasticsearch._types.query_dsl.MatchQuery;
import co.elastic.clients.elasticsearch._types.query_dsl.Operator;
import co.elastic.clients.elasticsearch._types.query_dsl.Query;
import co.elastic.clients.elasticsearch._types.query_dsl.RangeQuery;
import co.elastic.clients.elasticsearch.core.CountResponse;
import co.elastic.clients.elasticsearch.core.SearchResponse;
import co.elastic.clients.elasticsearch.core.search.Hit;
import co.elastic.clients.elasticsearch.core.search.HitsMetadata;
import co.elastic.clients.json.JsonData;
import com.swiftcryptollc.commons.adabounties.data.ReactionSummary;
import com.swiftcryptollc.commons.adabounties.es.ElasticsearchClientConfig;
import com.swiftcryptollc.commons.adabounties.es.ElasticsearchClientListener;
import com.swiftcryptollc.commons.adabounties.es.data.Comment;
import com.swiftcryptollc.commons.adabounties.es.data.Reaction;
import com.swiftcryptollc.commons.adabounties.redis.cache.AliasCache;
import com.swiftcryptollc.commons.adabounties.redis.cache.CommentCache;
import com.swiftcryptollc.commons.adabounties.es.data.UserMetric;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
@Service
public final class CommentQueryHandler implements ElasticsearchClientListener {

    @Autowired
    private AliasCache aliasCache;
    @Autowired
    private CommentCache commentCache;

    @Autowired
    protected ElasticsearchClientConfig esClient;
    protected ElasticsearchClient client;
    protected ReactionSummary sumDown;
    protected ReactionSummary sumUp;
    protected UUID id;
    private final static Integer PAGE_SIZE = 500;
    protected final static Logger logger = LoggerFactory.getLogger(CommentQueryHandler.class);

    public CommentQueryHandler() {
    }

    /**
     *
     * @param client
     */
    public CommentQueryHandler(ElasticsearchClient client) {
        this.client = client;
    }

    /**
     * @return the id
     */
    @Override
    public UUID getId() {
        if (id == null) {
            id = UUID.randomUUID();
        }
        return id;
    }

    @Override
    public void setClient() {
        if (esClient != null) {
            client = esClient.elasticSearchClient();
        }
    }

    /**
     *
     */
    @PostConstruct
    public void init() {
        if (client == null) {
            ElasticsearchClientConfig.subscribe(this);
        }
        sumDown = new ReactionSummary();
        sumDown.setCount(0L);
        sumDown.setType("arrow_down");
        sumUp = new ReactionSummary();
        sumUp.setCount(0L);
        sumUp.setType("arrow_up");
    }

    /**
     *
     * @param bountyId
     * @param comments
     * @return
     */
    public Map<String, Map<String, ReactionSummary>> findAllReactionsForComments(String accountId, String bountyId, Set<Comment> comments) {
        Map<String, Map<String, ReactionSummary>> reactionMap = new HashMap<>();
        List<Query> queryList = new ArrayList<>();
        List<Query> commentQueryList = new ArrayList<>();

        queryList.add(MatchQuery.of(fn -> fn.field("bountyId").query(bountyId))._toQuery());

        for (Comment comment : comments) {
            commentQueryList.add(MatchQuery.of(fn -> fn.field("commentId").query(comment.getId()).operator(Operator.Or))._toQuery());
        }

        SortOptions sortOptions = SortOptions.of(fn
                -> fn.field(FieldSort.of(fn2 -> fn2.field("postedMs").order(SortOrder.Asc))));
        Query bQuery = BoolQuery.of(fn -> fn.must(queryList).must(commentQueryList))._toQuery();

        Integer page = 1;
        do {
            try {
                final Integer currentPage = page;
                SearchResponse<Reaction> response = client.search(t -> t.index(Reaction.INDEX_NAME)
                        .query(bQuery)
                        .sort(sortOptions)
                        .from((currentPage - 1) * PAGE_SIZE)
                        .size(PAGE_SIZE), Reaction.class);
                HitsMetadata<Reaction> searchHits = response.hits();
                if (!searchHits.hits().isEmpty()) {
                    for (Hit<Reaction> hit : searchHits.hits()) {
                        Reaction reaction = hit.source();
                        ReactionSummary summary;
                        if (reactionMap.containsKey(reaction.getCommentId())) {
                            Map<String, ReactionSummary> typeMap = reactionMap.get(reaction.getCommentId());
                            if (typeMap.containsKey(reaction.getType())) {
                                summary = typeMap.get(reaction.getType());
                            } else {
                                summary = new ReactionSummary(reaction.getType());
                                typeMap.put(reaction.getType(), summary);
                            }
                        } else {
                            summary = new ReactionSummary(reaction.getType());
                            Map<String, ReactionSummary> typeMap = new HashMap<>();
                            typeMap.put(reaction.getType(), summary);
                        }
                        summary.increment();
                        if (reaction.getAccountId().equals(accountId)) {
                            summary.setUserVoted(true);
                        }
                    }
                    ++page;
                } else {
                    break;
                }
            } catch (Exception ex) {
                if (ex.getMessage().contains("all shards failed")) {
                    logger.error("Exception during bounty reaction search [" + ex.getMessage() + "]");
                } else {
                    logger.error("Exception during bounty reaction search [" + ex.getMessage() + "]", ex);
                }
                break;
            }
        } while (true);

        return reactionMap;
    }

    /**
     *
     * @param bountyId
     * @param pageable
     * @return
     */
    public Map<String, Comment> findBountyComments(String bountyId, PageRequest pageable) {
        List<Query> queryList = new ArrayList<>();
        queryList.add(MatchQuery.of(fn -> fn.field("bountyId").query(bountyId))._toQuery());
        // filter out claim comments
        queryList.add(MatchQuery.of(fn -> fn.field("claimId").query("null"))._toQuery());
        Query bQuery = BoolQuery.of(fn -> fn.must(queryList))._toQuery();
        SortOptions sortOptions = SortOptions.of(fn -> fn.field(FieldSort.of(fn2 -> fn2.field("postedMs").order(SortOrder.Desc))));

        return executeQuery(bQuery, sortOptions, pageable);
    }

    /**
     *
     * @param claimId
     * @param pageable
     * @return
     */
    public Map<String, Comment> findClaimComments(String claimId, PageRequest pageable) {
        List<Query> queryList = new ArrayList<>();
        queryList.add(MatchQuery.of(fn -> fn.field("claimId").query(claimId))._toQuery());
        Query bQuery = BoolQuery.of(fn -> fn.must(queryList))._toQuery();
        SortOptions sortOptions = SortOptions.of(fn -> fn.field(FieldSort.of(fn2 -> fn2.field("postedMs").order(SortOrder.Desc))));
        return executeQuery(bQuery, sortOptions, pageable);
    }

    /**
     *
     * @param bQuery
     * @param sortOptions
     * @param pageable
     * @return
     */
    public Map<String, Comment> executeQuery(Query bQuery, SortOptions sortOptions, PageRequest pageable) {
        Map<String, Comment> commentMap = new HashMap<>();
        try {
            SearchResponse<Comment> response = client.search(t -> t.index(Comment.INDEX_NAME)
                    .query(bQuery)
                    .sort(sortOptions)
                    .from((pageable.getPageNumber() - 1) * pageable.getPageSize())
                    .size(pageable.getPageSize()), Comment.class);
            HitsMetadata<Comment> searchHits = response.hits();

            for (Hit<Comment> hit : searchHits.hits()) {
                Comment comment = hit.source();
                comment.setAlias(aliasCache.getAlias(comment.getAccountId()));
                commentCache.updateCachedCommentMsg(comment);
                if (comment.getParentId() != null) {
                    comment.setParentMsg(commentCache.getCommentMsg(comment.getParentId()));
                }
                //bounty.setSearchScore(hit.score());
                commentMap.put(comment.getId(), comment);
            }
        } catch (Exception ex) {
            logger.error("Exception during bounty comment search! [" + ex.getMessage() + "]", ex);
        }

        return commentMap;

    }

    /**
     *
     * @param bountyId
     * @param comment
     */
    public void setReactionCountListForComment(String bountyId, Comment comment) {
        Map<String, Comment> commentMap = new HashMap<>();
        commentMap.put(comment.getId(), comment);
        this.setReactionCountListForComments(bountyId, commentMap);
    }

    /**
     *
     * @param bountyId
     * @param commentMap
     * @return
     */
    public void setReactionCountListForComments(String bountyId, Map<String, Comment> commentMap) {
        Map<String, Aggregate> aggregateMap = aggregateReactionsForComments(bountyId, commentMap);

        for (Map.Entry<String, Aggregate> entry : aggregateMap.entrySet()) {
            Map<String, ReactionSummary> sumMap = new HashMap<>();
            sumMap.put("arrow_down", sumDown);
            sumMap.put("arrow_up", sumUp);
            Aggregate aggregate = entry.getValue();
            String commentId = entry.getKey();
            if (aggregate != null) {
                List<StringTermsBucket> buckets = aggregate.sterms().buckets().array();

                Map<FieldValue, Long> mapAggsDirector = buckets.stream().collect(Collectors.toMap(StringTermsBucket::key, MultiBucketBase::docCount));

                for (Map.Entry<FieldValue, Long> reactionEntry : mapAggsDirector.entrySet()) {
                    ReactionSummary summary = new ReactionSummary();
                    summary.setType(reactionEntry.getKey().stringValue());
                    summary.setCount(reactionEntry.getValue());
                    sumMap.put(summary.getType(), summary);
                }
                Comment comment = commentMap.get(commentId);
                comment.setReactSumList(new ArrayList(sumMap.values()));
            }
        }
    }

    /**
     *
     * @param bountyId
     * @param commentMap
     * @return
     */
    protected Map<String, Aggregate> aggregateReactionsForComments(String bountyId, Map<String, Comment> commentMap) {
        HashMap<String, Aggregate> aggregateMap = new HashMap<>();

        for (Comment comment : commentMap.values()) {
            Aggregate aggregate = aggregateReactionsForBountyComment(bountyId, comment.getId());
            aggregateMap.put(comment.getId(), aggregate);
        }

        return aggregateMap;
    }

    /**
     *
     * @param bountyId
     * @param commentId
     * @return
     */
    protected Aggregate aggregateReactionsForBountyComment(String bountyId, String commentId) {
        Aggregate aggregate = null;

        List<Query> queryList = new ArrayList<>();

        queryList.add(MatchQuery.of(fn -> fn.field("bountyId").query(bountyId))._toQuery());
        queryList.add(MatchQuery.of(fn -> fn.field("commentId").query(commentId))._toQuery());
        Query bQuery = BoolQuery.of(fn -> fn.must(queryList))._toQuery();

        try {
            SearchResponse<Object> response = client.search(t
                    -> t.index(Reaction.INDEX_NAME).query(bQuery)
                            .aggregations("type", a -> a
                            .terms(h -> h
                            .field("type"))),
                    Object.class);
            aggregate = response.aggregations().get("type");
        } catch (Exception ex) {
            logger.error("Exception during aggregateReactionsForBountyComment! [" + ex.getMessage() + "]");
        }

        return aggregate;
    }

    /**
     *
     * @param accountId
     * @param bountyId
     * @param commentId
     * @param type
     * @return
     */
    public Set<Reaction> findReactionsByAccountBountyComment(String accountId, String bountyId, String commentId, String type) {
        Set<Reaction> reactions = new HashSet<>();
        List<Query> queryList = new ArrayList<>();
        if ((accountId == null) && (bountyId == null) && (commentId == null)) {
            throw new IllegalArgumentException("At least one parameter of account, bounty, comment ID must be present!");
        }
        if (accountId != null) {
            queryList.add(MatchQuery.of(fn -> fn.field("accountId").query(accountId))._toQuery());
        }
        if (bountyId != null) {
            queryList.add(MatchQuery.of(fn -> fn.field("bountyId").query(bountyId))._toQuery());
        }
        if (commentId != null) {
            queryList.add(MatchQuery.of(fn -> fn.field("commentId").query(commentId))._toQuery());
        }
        if (type != null) {
            queryList.add(MatchQuery.of(fn -> fn.field("type").query(type))._toQuery());
        }
        Query bQuery = BoolQuery.of(fn -> fn.must(queryList))._toQuery();

        try {
            SearchResponse<Reaction> response = client.search(t -> t.index(Reaction.INDEX_NAME)
                    .query(bQuery), Reaction.class);
            HitsMetadata<Reaction> searchHits = response.hits();

            for (Hit<Reaction> hit : searchHits.hits()) {
                Reaction reaction = hit.source();
                //bounty.setSearchScore(hit.score());
                reactions.add(reaction);

            }
        } catch (Exception ex) {
            logger.error("Exception during reaction search! [" + ex.getMessage() + "]", ex);
        }

        return reactions;
    }

    /**
     *
     * @param accountId
     * @param oldestTimeMs
     * @param userMetric
     */
    public void getStatusCountsForCommentByAccount(String accountId, Long oldestTimeMs, UserMetric userMetric) {
        userMetric.setBountyComments(getBountyCountForCommentByAccount(accountId, oldestTimeMs));
        userMetric.setClaimComments(getClaimCountForCommentByAccount(accountId, oldestTimeMs));
    }

    /**
     *
     * @param accountId
     * @param oldestTimeMs
     * @return
     */
    protected Long getClaimCountForCommentByAccount(String accountId, Long oldestTimeMs) {
        Long count = 0L;
        List<Query> queryList = new ArrayList<>();
        List<Query> notQueryList = new ArrayList<>();
        queryList.add(MatchQuery.of(fn -> fn.field("accountId").query(accountId))._toQuery());
        notQueryList.add(MatchQuery.of(fn -> fn.field("claimId").query("null"))._toQuery());
        if (oldestTimeMs > 0L) {
            queryList.add(RangeQuery.of(fn -> fn.field("postedMs").gte(JsonData.of(oldestTimeMs)))._toQuery());
        }
        Query bQuery = BoolQuery.of(fn -> fn.must(queryList).mustNot(notQueryList))._toQuery();
        //      logger.info("Executing query [" + bQuery.toString() + "]");
        try {
            CountResponse response = client.count(t
                    -> t.index(Comment.INDEX_NAME)
                            .query(bQuery));
            count = response.count();
        } catch (Exception ex) {
            logger.error("Exception during aggregateStatesForClaimByAccount! [" + ex.getMessage() + "]");
        }

        return count;
    }

    /**
     *
     * @param accountId
     * @param oldestTimeMs
     * @return
     */
    protected Long getBountyCountForCommentByAccount(String accountId, Long oldestTimeMs) {
        Long count = 0L;
        List<Query> queryList = new ArrayList<>();

        queryList.add(MatchQuery.of(fn -> fn.field("accountId").query(accountId))._toQuery());
        queryList.add(MatchQuery.of(fn -> fn.field("claimId").query("null"))._toQuery());
        if (oldestTimeMs > 0L) {
            queryList.add(RangeQuery.of(fn -> fn.field("postedMs").gte(JsonData.of(oldestTimeMs)))._toQuery());
        }
        Query bQuery = BoolQuery.of(fn -> fn.must(queryList))._toQuery();
//        logger.info("Executing query [" + bQuery.toString() + "]");

        try {
            CountResponse response = client.count(t
                    -> t.index(Comment.INDEX_NAME)
                            .query(bQuery));
            count = response.count();
        } catch (Exception ex) {
            logger.error("Exception during getBountyCountForCommentByAccount! [" + ex.getMessage() + "]");
        }

        return count;
    }

    /**
     * @param client the client to set
     */
    public void setClient(ElasticsearchClient client) {
        this.client = client;
    }
}
