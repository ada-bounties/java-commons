package com.swiftcryptollc.commons.adabounties.data;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
public class ClaimToBounty {

    protected String bountyId;
    protected String claimId;

    public ClaimToBounty() {

    }

    /**
     * @return the bountyId
     */
    public String getBountyId() {
        return bountyId;
    }

    /**
     * @param bountyId the bountyId to set
     */
    public void setBountyId(String bountyId) {
        this.bountyId = bountyId;
    }

    /**
     * @return the claimId
     */
    public String getClaimId() {
        return claimId;
    }

    /**
     * @param claimId the claimId to set
     */
    public void setClaimId(String claimId) {
        this.claimId = claimId;
    }
}
