package com.swiftcryptollc.commons.adabounties.es.query;

import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.elasticsearch._types.query_dsl.MatchQuery;
import co.elastic.clients.elasticsearch._types.query_dsl.Query;
import co.elastic.clients.elasticsearch._types.query_dsl.WildcardQuery;
import co.elastic.clients.elasticsearch.core.SearchResponse;
import co.elastic.clients.elasticsearch.core.search.Hit;
import co.elastic.clients.elasticsearch.core.search.HitsMetadata;
import com.swiftcryptollc.commons.adabounties.es.ElasticsearchClientConfig;
import com.swiftcryptollc.commons.adabounties.es.ElasticsearchClientListener;
import com.swiftcryptollc.commons.adabounties.es.data.Alias;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import javax.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
@Service
public final class AliasQueryHandler implements ElasticsearchClientListener {

    @Autowired
    protected ElasticsearchClientConfig esClient;
    protected ElasticsearchClient client;
    protected UUID id;
    protected final static Logger logger = LoggerFactory.getLogger(AliasQueryHandler.class);

    public AliasQueryHandler() {
    }

    /**
     *
     * @param client
     */
    public AliasQueryHandler(ElasticsearchClient client) {
        this.client = client;
    }

    /**
     * @return the id
     */
    public UUID getId() {
        if (id == null) {
            id = UUID.randomUUID();
        }
        return id;
    }

    @Override
    public void setClient() {
        if (esClient != null) {
            client = esClient.elasticSearchClient();
        }
    }

    /**
     * @param client the client to set
     */
    public void setClient(ElasticsearchClient client) {
        this.client = client;
    }

    /**
     *
     */
    @PostConstruct
    public void init() {
        if (client == null) {
            ElasticsearchClientConfig.subscribe(this);
        }
    }

    /**
     *
     * @param fuzzy
     * @return
     */
    public Set<Alias> autocompleteSearch(String fuzzy) {
        Set<Alias> aliasSet = new HashSet<>();
        final String finalFuzzy = "*".concat(fuzzy).concat("*");
        SearchResponse<Alias> response;
        Query wcQuery = WildcardQuery.of(fn -> fn.field("name").value(finalFuzzy).caseInsensitive(Boolean.TRUE))._toQuery();
        try {
            response = client.search(t -> t.index(Alias.INDEX_NAME).query(wcQuery), Alias.class);
            HitsMetadata<Alias> searchHits = response.hits();
            for (Hit<Alias> hit : searchHits.hits()) {
                Alias alias = hit.source();
                if (!alias.getImgSrc().equals("BLOCKED")) {
                    aliasSet.add(alias);
                }
            }
        } catch (Exception ex) {
            logger.error("Exception during autocomplete alias search! [" + ex.getMessage() + "]", ex);
        }
        return aliasSet;
    }

    /**
     *
     * @param accountId
     * @return
     */
    public Alias getAlias(String accountId) {
        Alias alias = null;
        Query bQuery = MatchQuery.of(fn -> fn.field("id").query(accountId))._toQuery();
        try {
            SearchResponse<Alias> response = client.search(t -> t.index(Alias.INDEX_NAME).query(bQuery).size(1), Alias.class);
            HitsMetadata<Alias> searchHits = response.hits();
            // should only be one
            for (Hit<Alias> hit : searchHits.hits()) {
                alias = hit.source();
            }
        } catch (Exception ex) {
            logger.error("Exception during alias search! [" + ex.getMessage() + "]", ex);
        }
        return alias;
    }
}
