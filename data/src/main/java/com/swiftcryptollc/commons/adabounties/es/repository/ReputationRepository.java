package com.swiftcryptollc.commons.adabounties.es.repository;

import com.swiftcryptollc.commons.adabounties.es.data.Reputation;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
@Repository
public interface ReputationRepository extends ElasticsearchRepository<Reputation, String> {

}
