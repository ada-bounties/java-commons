package com.swiftcryptollc.commons.adabounties.redis.cache;

import com.swiftcryptollc.commons.adabounties.es.data.Claim;
import com.swiftcryptollc.commons.adabounties.es.repository.ClaimRepository;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
@Service
public class ClaimCache {

    @Autowired
    protected ClaimRepository claimRepo;
    protected final static Logger logger = LoggerFactory.getLogger(ClaimCache.class);

    public ClaimCache() {

    }

    /**
     *
     * @param id
     * @return
     */
    @Cacheable(value = "claim", key = "#id")
    public Claim getClaim(String id) {
        if (id == null) {
            throw new IllegalArgumentException("Id cannot be null");
        }
        Optional<Claim> claimOpt = claimRepo.findById(id);
        if (claimOpt.isPresent()) {
            return claimOpt.get();
        } else {
            logger.warn("Claim [" + id + "] does not exist");
        }
        return null;
    }

    /**
     *
     * @param claim
     * @return
     */
    @CachePut(value = "claim", key = "#claim.id")
    public Claim updateCachedClaim(Claim claim) {
        return claimRepo.save(claim);
    }
}
