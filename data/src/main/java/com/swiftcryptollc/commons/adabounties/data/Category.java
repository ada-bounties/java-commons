package com.swiftcryptollc.commons.adabounties.data;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
public enum Category {
    ALL("All"),
    AI("AI"),
    AUTOMATION("Automation"),
    WEB("Web"),
    BLOCKCHAIN("Blockchain"),
    MARKETING("Marketing"),
    DATA("Data"),
    TESTING("Testing"),
    SOCIAL_MEDIA("Social Media"),
    GRAPHICS("Graphics & Design"),
    MUSIC("Music & Audio"),
    VIDEOS("Videos & Animations"),
    WRITING("Writing & Translations"),
    OTHER("Other"),
    ADMINISTRATION("Administration"),
    SOFTWARE("Software"),
    INFRASTRUCTURE("Infrastructure"),
    LEGAL("Legal"),
    SECURITY("Security");

    public final String label;

    private Category(String label) {
        this.label = label;
    }

    public static Category valueOfLabel(String label) {
        for (Category type : values()) {
            if (type.label.equalsIgnoreCase(label)) {
                return type;
            }
        }
        return null;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return label;
    }

}
