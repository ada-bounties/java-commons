package com.swiftcryptollc.commons.adabounties.data.cardano.tokens;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TokenRegistration {

    @SerializedName("subject")
    @Expose
    private String subject;
    @SerializedName("url")
    @Expose
    private Url url;
    @SerializedName("name")
    @Expose
    private Name name;
    @SerializedName("ticker")
    @Expose
    private Ticker ticker;
    @SerializedName("policy")
    @Expose
    private String policy;
    @SerializedName("decimals")
    @Expose
    protected Decimals decimals;
    @SerializedName("logo")
    @Expose
    private Logo logo;
    @SerializedName("description")
    @Expose
    private Description description;

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public Url getUrl() {
        if (url == null) {
            url = new Url();
            url.setValue(null);
        }
        return url;
    }

    public void setUrl(Url url) {
        this.url = url;
    }

    public Name getName() {
        if (name == null) {
            name = new Name();
            name.setValue(null);
        }
        return name;
    }

    public void setName(Name name) {
        this.name = name;
    }

    public Ticker getTicker() {
        if (ticker == null) {
            ticker = new Ticker();
            ticker.setValue(null);
        }
        return ticker;
    }

    public void setTicker(Ticker ticker) {
        this.ticker = ticker;
    }

    public String getPolicy() {
        return policy;
    }

    public void setPolicy(String policy) {
        this.policy = policy;
    }

    public Logo getLogo() {
        if (logo == null) {
            logo = new Logo();
            logo.setValue(null);
        }
        return logo;
    }

    public void setLogo(Logo logo) {
        this.logo = logo;
    }

    public Description getDescription() {
        if (description == null) {
            description = new Description();
            description.setValue(null);
        }
        return description;
    }

    public void setDescription(Description description) {
        this.description = description;
    }

    /**
     * @return the decimals
     */
    public Decimals getDecimals() {
        if (decimals == null) {
            decimals = new Decimals();
            decimals.setValue(0);
        }
        return decimals;
    }

    /**
     * @param decimals the decimals to set
     */
    public void setDecimals(Decimals decimals) {
        this.decimals = decimals;
    }

}
