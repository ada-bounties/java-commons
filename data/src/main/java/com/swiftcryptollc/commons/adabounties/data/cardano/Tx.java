package com.swiftcryptollc.commons.adabounties.data.cardano;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
public class Tx {

    protected Object txBytes;
    protected List<String> bountyIds;
    protected String claimId;
    protected String witness;
    protected String finalHash;
    protected String tokenSubject;

    public Tx() {

    }

    public Tx(String hash) {
        this.txBytes = hash;
    }

    /**
     * @return the txBytes
     */
    public Object getTxBytes() {
        return txBytes;
    }

    /**
     * @param txBytes the txBytes to set
     */
    public void setTxBytes(Object txBytes) {
        this.txBytes = txBytes;
    }

    /**
     *
     * @param btyId
     */
    public void addBountyId(String btyId) {
        if (bountyIds == null) {
            bountyIds = new ArrayList<>();
        }
        bountyIds.add(btyId);
    }

    /**
     * @return the bountyIds
     */
    public List<String> getBountyIds() {
        return bountyIds;
    }

    /**
     * @param bountyIds the bountyIds to set
     */
    public void setBountyIds(List<String> bountyIds) {
        this.bountyIds = bountyIds;
    }

    /**
     * @return the finalHash
     */
    public String getFinalHash() {
        return finalHash;
    }

    /**
     * @param finalHash the finalHash to set
     */
    public void setFinalHash(String finalHash) {
        this.finalHash = finalHash;
    }

    /**
     * @return the witness
     */
    public String getWitness() {
        return witness;
    }

    /**
     * @param witness the witness to set
     */
    public void setWitness(String witness) {
        this.witness = witness;
    }

    /**
     * @return the claimId
     */
    public String getClaimId() {
        return claimId;
    }

    /**
     * @param claimId the claimId to set
     */
    public void setClaimId(String claimId) {
        this.claimId = claimId;
    }

    /**
     * @return the tokenSubject
     */
    public String getTokenSubject() {
        return tokenSubject;
    }

    /**
     * @param tokenSubject the tokenSubject to set
     */
    public void setTokenSubject(String tokenSubject) {
        this.tokenSubject = tokenSubject;
    }
}
