package com.swiftcryptollc.commons.adabounties.data;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
public enum ClaimStatus {
    All("All"),
    OPEN("Open"),
    REJECTED("Rejected"),
    ACCEPTED("Accepted"),
    PENDING("Pending"),
    CLOSED("Closed");

    public final String label;

    private ClaimStatus(String label) {
        this.label = label;
    }

    public static ClaimStatus valueOfLabel(String label) {
        for (ClaimStatus type : values()) {
            if (type.label.equalsIgnoreCase(label)) {
                return type;
            }
        }
        return null;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return label;
    }

}
