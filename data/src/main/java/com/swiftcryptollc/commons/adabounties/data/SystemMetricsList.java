package com.swiftcryptollc.commons.adabounties.data;

import com.swiftcryptollc.commons.adabounties.es.data.SystemMetric;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
public class SystemMetricsList {

    protected Long count;
    protected Integer page;
    protected Set<SystemMetric> systemMetrics = new HashSet<>();

    public SystemMetricsList() {

    }

    /**
     * @return the systemMetrics
     */
    public Set<SystemMetric> getSystemMetrics() {
        return systemMetrics;
    }

    /**
     * @param systemMetrics the systemMetrics to set
     */
    public void setSystemMetrics(Set<SystemMetric> systemMetrics) {
        this.systemMetrics = systemMetrics;
    }

    /**
     * @return the count
     */
    public Long getCount() {
        return count;
    }

    /**
     * @param count the count to set
     */
    public void setCount(Long count) {
        this.count = count;
    }

    /**
     * @return the page
     */
    public Integer getPage() {
        return page;
    }

    /**
     * @param page the page to set
     */
    public void setPage(Integer page) {
        this.page = page;
    }
}
