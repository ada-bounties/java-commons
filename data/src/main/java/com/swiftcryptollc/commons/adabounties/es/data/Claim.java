package com.swiftcryptollc.commons.adabounties.es.data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.swiftcryptollc.commons.adabounties.utilities.data.Base64Id;
import java.io.Serializable;
import java.util.Date;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
@Document(indexName = "claim")
@JsonIgnoreProperties({"_class", "_id"})
public class Claim implements Serializable {

    @Transient
    private static final long serialVersionUID = 100004L;

    @Id
    protected String id;
    @Field(type = FieldType.Keyword)
    protected String accountId;
    @Field(type = FieldType.Keyword)
    protected String bountyId;
    @Field(type = FieldType.Keyword)
    protected String btyAccountId;
    @Field
    protected String msg;
    @Field
    protected String recAddr;
    @Field
    protected Long postedMs;
    @Field
    protected Long updatedMs;
    @Field(type = FieldType.Keyword)
    protected String status;
    @Transient
    protected String oldStatus;
    @Field
    protected String txHash;
    @Transient
    protected Alias alias;
    @Transient
    protected Integer page;
    @Transient
    protected String bountyTitle;
    @Transient
    protected Bounty bounty;
    @Transient
    protected Double adaUSDPrice;
    @Transient
    protected Token token;
    @Transient
    public final transient static String INDEX_NAME = "claim";

    public Claim() {
    }

    /**
     * @return the id
     */
    public String getId() {
        if (id == null) {
            id = Base64Id.getNewId();
        }
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the msg
     */
    public String getMsg() {
        return msg;
    }

    /**
     * @param msg the msg to set
     */
    public void setMsg(String msg) {
        this.msg = msg;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the postedMs
     */
    public Long getPostedMs() {
        if (postedMs == null) {
            postedMs = new Date().getTime();
        }
        return postedMs;
    }

    /**
     * @param postedMs the postedMs to set
     */
    public void setPostedMs(Long postedMs) {
        this.postedMs = postedMs;
    }

    /**
     * @return the alias
     */
    public Alias getAlias() {
        return alias;
    }

    /**
     * @param alias the alias to set
     */
    public void setAlias(Alias alias) {
        this.alias = alias;
    }

    /**
     * @return the accountId
     */
    public String getAccountId() {
        return accountId;
    }

    /**
     * @param accountId the accountId to set
     */
    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    /**
     * @return the page
     */
    public Integer getPage() {
        if (page == null) {
            page = 1;
        }
        return page;
    }

    /**
     * @param page the page to set
     */
    public void setPage(Integer page) {
        this.page = page;
    }

    /**
     * @return the txHash
     */
    public String getTxHash() {
        return txHash;
    }

    /**
     * @param txHash the txHash to set
     */
    public void setTxHash(String txHash) {
        this.txHash = txHash;
    }

    /**
     * @return the updatedMs
     */
    public Long getUpdatedMs() {
        return updatedMs;
    }

    /**
     * @param updatedMs the updatedMs to set
     */
    public void setUpdatedMs(Long updatedMs) {
        this.updatedMs = updatedMs;
    }

    /**
     * @return the recAddr
     */
    public String getRecAddr() {
        return recAddr;
    }

    /**
     * @param recAddr the recAddr to set
     */
    public void setRecAddr(String recAddr) {
        this.recAddr = recAddr;
    }

    /**
     * @return the bountyId
     */
    public String getBountyId() {
        return bountyId;
    }

    /**
     * @param bountyId the bountyId to set
     */
    public void setBountyId(String bountyId) {
        this.bountyId = bountyId;
    }

    /**
     * @return the bountyTitle
     */
    public String getBountyTitle() {
        return bountyTitle;
    }

    /**
     * @param bountyTitle the bountyTitle to set
     */
    public void setBountyTitle(String bountyTitle) {
        this.bountyTitle = bountyTitle;
    }

    /**
     * @return the bounty
     */
    public Bounty getBounty() {
        return bounty;
    }

    /**
     * @param bounty the bounty to set
     */
    public void setBounty(Bounty bounty) {
        this.bounty = bounty;
    }

    /**
     * @return the oldStatus
     */
    public String getOldStatus() {
        return oldStatus;
    }

    /**
     * @param oldStatus the oldStatus to set
     */
    public void setOldStatus(String oldStatus) {
        this.oldStatus = oldStatus;
    }

    /**
     * @return the btyAccountId
     */
    public String getBtyAccountId() {
        return btyAccountId;
    }

    /**
     * @param btyAccountId the btyAccountId to set
     */
    public void setBtyAccountId(String btyAccountId) {
        this.btyAccountId = btyAccountId;
    }

    /**
     * @return the adaUSDPrice
     */
    public Double getAdaUSDPrice() {
        return adaUSDPrice;
    }

    /**
     * @param adaUSDPrice the adaUSDPrice to set
     */
    public void setAdaUSDPrice(Double adaUSDPrice) {
        this.adaUSDPrice = adaUSDPrice;
    }

    /**
     * @return the token
     */
    public Token getToken() {
        return token;
    }

    /**
     * @param token the token to set
     */
    public void setToken(Token token) {
        this.token = token;
    }
}
