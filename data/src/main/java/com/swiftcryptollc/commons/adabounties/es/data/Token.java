package com.swiftcryptollc.commons.adabounties.es.data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.swiftcryptollc.commons.adabounties.data.cardano.tokens.TokenRegistration;
import java.io.Serializable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;
import org.springframework.data.elasticsearch.annotations.Setting;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
@Document(indexName = "token")
@Setting(settingPath = "es-config/wc.json")
@JsonIgnoreProperties({"_class", "_id"})
public class Token implements Serializable {
    
    @Transient
    private static final long serialVersionUID = 100021L;
    
    @Id
    protected String subject;
    @Field
    protected String name;
    @Field(type = FieldType.Wildcard)
    protected String ticker;
    @Field
    protected String policy;
    @Field
    protected String desc;
    @Field
    protected String logo;
    @Field
    protected Integer decimals;
    @Field
    protected String url;
    @Transient
    public final static transient String INDEX_NAME = "token";
    @Transient
    protected final static Logger logger = LoggerFactory.getLogger(Token.class);
    
    public Token() {
    }
    
    public final static Token generateToken(TokenRegistration tokenReg) {
        final Token token = new Token();
        try {
            token.setDecimals(tokenReg.getDecimals().getValue());
            token.setDesc(tokenReg.getDescription().getValue());
            token.setLogo(tokenReg.getLogo().getValue());
            token.setName(tokenReg.getName().getValue());
            token.setPolicy(tokenReg.getPolicy());
            token.setSubject(tokenReg.getSubject());
            token.setTicker(tokenReg.getTicker().getValue());
            token.setUrl(tokenReg.getUrl().getValue());
        } catch (Exception ex) {
            logger.error("Exception occured while trying to generate a Token object! [" + ex.getMessage() + "]", ex);
        }
        return token;
    }

    /**
     * @return the subject
     */
    public String getSubject() {
        return subject;
    }

    /**
     * @param subject the subject to set
     */
    public void setSubject(String subject) {
        this.subject = subject;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the ticker
     */
    public String getTicker() {
        return ticker;
    }

    /**
     * @param ticker the ticker to set
     */
    public void setTicker(String ticker) {
        this.ticker = ticker;
    }

    /**
     * @return the policy
     */
    public String getPolicy() {
        return policy;
    }

    /**
     * @param policy the policy to set
     */
    public void setPolicy(String policy) {
        this.policy = policy;
    }

    /**
     * @return the desc
     */
    public String getDesc() {
        return desc;
    }

    /**
     * @param desc the desc to set
     */
    public void setDesc(String desc) {
        this.desc = desc;
    }

    /**
     * @return the logo
     */
    public String getLogo() {
        return logo;
    }

    /**
     * @param logo the logo to set
     */
    public void setLogo(String logo) {
        this.logo = logo;
    }

    /**
     * @return the decimals
     */
    public Integer getDecimals() {
        return decimals;
    }

    /**
     * @param decimals the decimals to set
     */
    public void setDecimals(Integer decimals) {
        this.decimals = decimals;
    }

    /**
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url the url to set
     */
    public void setUrl(String url) {
        this.url = url;
    }
    
}
