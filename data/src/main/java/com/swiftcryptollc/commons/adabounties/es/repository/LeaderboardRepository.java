package com.swiftcryptollc.commons.adabounties.es.repository;

import com.swiftcryptollc.commons.adabounties.es.data.Leaderboard;
import java.util.List;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
@Repository
public interface LeaderboardRepository extends ElasticsearchRepository<Leaderboard, String> {

    public List<Leaderboard> findByTypeAndYmd(String type, String ymd);

    public List<Leaderboard> findByYmd(String ymd);
}
