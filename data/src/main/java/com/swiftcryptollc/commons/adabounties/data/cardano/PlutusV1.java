
package com.swiftcryptollc.commons.adabounties.data.cardano;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("jsonschema2pojo")
public class PlutusV1 {

    @SerializedName("addInteger-cpu-arguments-intercept")
    @Expose
    private Integer addIntegerCpuArgumentsIntercept;
    @SerializedName("addInteger-cpu-arguments-slope")
    @Expose
    private Integer addIntegerCpuArgumentsSlope;
    @SerializedName("addInteger-memory-arguments-intercept")
    @Expose
    private Integer addIntegerMemoryArgumentsIntercept;
    @SerializedName("addInteger-memory-arguments-slope")
    @Expose
    private Integer addIntegerMemoryArgumentsSlope;
    @SerializedName("appendByteString-cpu-arguments-intercept")
    @Expose
    private Integer appendByteStringCpuArgumentsIntercept;
    @SerializedName("appendByteString-cpu-arguments-slope")
    @Expose
    private Integer appendByteStringCpuArgumentsSlope;
    @SerializedName("appendByteString-memory-arguments-intercept")
    @Expose
    private Integer appendByteStringMemoryArgumentsIntercept;
    @SerializedName("appendByteString-memory-arguments-slope")
    @Expose
    private Integer appendByteStringMemoryArgumentsSlope;
    @SerializedName("appendString-cpu-arguments-intercept")
    @Expose
    private Integer appendStringCpuArgumentsIntercept;
    @SerializedName("appendString-cpu-arguments-slope")
    @Expose
    private Integer appendStringCpuArgumentsSlope;
    @SerializedName("appendString-memory-arguments-intercept")
    @Expose
    private Integer appendStringMemoryArgumentsIntercept;
    @SerializedName("appendString-memory-arguments-slope")
    @Expose
    private Integer appendStringMemoryArgumentsSlope;
    @SerializedName("bData-cpu-arguments")
    @Expose
    private Integer bDataCpuArguments;
    @SerializedName("bData-memory-arguments")
    @Expose
    private Integer bDataMemoryArguments;
    @SerializedName("blake2b_256-cpu-arguments-intercept")
    @Expose
    private Integer blake2b256CpuArgumentsIntercept;
    @SerializedName("blake2b_256-cpu-arguments-slope")
    @Expose
    private Integer blake2b256CpuArgumentsSlope;
    @SerializedName("blake2b_256-memory-arguments")
    @Expose
    private Integer blake2b256MemoryArguments;
    @SerializedName("cekApplyCost-exBudgetCPU")
    @Expose
    private Integer cekApplyCostExBudgetCPU;
    @SerializedName("cekApplyCost-exBudgetMemory")
    @Expose
    private Integer cekApplyCostExBudgetMemory;
    @SerializedName("cekBuiltinCost-exBudgetCPU")
    @Expose
    private Integer cekBuiltinCostExBudgetCPU;
    @SerializedName("cekBuiltinCost-exBudgetMemory")
    @Expose
    private Integer cekBuiltinCostExBudgetMemory;
    @SerializedName("cekConstCost-exBudgetCPU")
    @Expose
    private Integer cekConstCostExBudgetCPU;
    @SerializedName("cekConstCost-exBudgetMemory")
    @Expose
    private Integer cekConstCostExBudgetMemory;
    @SerializedName("cekDelayCost-exBudgetCPU")
    @Expose
    private Integer cekDelayCostExBudgetCPU;
    @SerializedName("cekDelayCost-exBudgetMemory")
    @Expose
    private Integer cekDelayCostExBudgetMemory;
    @SerializedName("cekForceCost-exBudgetCPU")
    @Expose
    private Integer cekForceCostExBudgetCPU;
    @SerializedName("cekForceCost-exBudgetMemory")
    @Expose
    private Integer cekForceCostExBudgetMemory;
    @SerializedName("cekLamCost-exBudgetCPU")
    @Expose
    private Integer cekLamCostExBudgetCPU;
    @SerializedName("cekLamCost-exBudgetMemory")
    @Expose
    private Integer cekLamCostExBudgetMemory;
    @SerializedName("cekStartupCost-exBudgetCPU")
    @Expose
    private Integer cekStartupCostExBudgetCPU;
    @SerializedName("cekStartupCost-exBudgetMemory")
    @Expose
    private Integer cekStartupCostExBudgetMemory;
    @SerializedName("cekVarCost-exBudgetCPU")
    @Expose
    private Integer cekVarCostExBudgetCPU;
    @SerializedName("cekVarCost-exBudgetMemory")
    @Expose
    private Integer cekVarCostExBudgetMemory;
    @SerializedName("chooseData-cpu-arguments")
    @Expose
    private Integer chooseDataCpuArguments;
    @SerializedName("chooseData-memory-arguments")
    @Expose
    private Integer chooseDataMemoryArguments;
    @SerializedName("chooseList-cpu-arguments")
    @Expose
    private Integer chooseListCpuArguments;
    @SerializedName("chooseList-memory-arguments")
    @Expose
    private Integer chooseListMemoryArguments;
    @SerializedName("chooseUnit-cpu-arguments")
    @Expose
    private Integer chooseUnitCpuArguments;
    @SerializedName("chooseUnit-memory-arguments")
    @Expose
    private Integer chooseUnitMemoryArguments;
    @SerializedName("consByteString-cpu-arguments-intercept")
    @Expose
    private Integer consByteStringCpuArgumentsIntercept;
    @SerializedName("consByteString-cpu-arguments-slope")
    @Expose
    private Integer consByteStringCpuArgumentsSlope;
    @SerializedName("consByteString-memory-arguments-intercept")
    @Expose
    private Integer consByteStringMemoryArgumentsIntercept;
    @SerializedName("consByteString-memory-arguments-slope")
    @Expose
    private Integer consByteStringMemoryArgumentsSlope;
    @SerializedName("constrData-cpu-arguments")
    @Expose
    private Integer constrDataCpuArguments;
    @SerializedName("constrData-memory-arguments")
    @Expose
    private Integer constrDataMemoryArguments;
    @SerializedName("decodeUtf8-cpu-arguments-intercept")
    @Expose
    private Integer decodeUtf8CpuArgumentsIntercept;
    @SerializedName("decodeUtf8-cpu-arguments-slope")
    @Expose
    private Integer decodeUtf8CpuArgumentsSlope;
    @SerializedName("decodeUtf8-memory-arguments-intercept")
    @Expose
    private Integer decodeUtf8MemoryArgumentsIntercept;
    @SerializedName("decodeUtf8-memory-arguments-slope")
    @Expose
    private Integer decodeUtf8MemoryArgumentsSlope;
    @SerializedName("divideInteger-cpu-arguments-constant")
    @Expose
    private Integer divideIntegerCpuArgumentsConstant;
    @SerializedName("divideInteger-cpu-arguments-model-arguments-intercept")
    @Expose
    private Integer divideIntegerCpuArgumentsModelArgumentsIntercept;
    @SerializedName("divideInteger-cpu-arguments-model-arguments-slope")
    @Expose
    private Integer divideIntegerCpuArgumentsModelArgumentsSlope;
    @SerializedName("divideInteger-memory-arguments-intercept")
    @Expose
    private Integer divideIntegerMemoryArgumentsIntercept;
    @SerializedName("divideInteger-memory-arguments-minimum")
    @Expose
    private Integer divideIntegerMemoryArgumentsMinimum;
    @SerializedName("divideInteger-memory-arguments-slope")
    @Expose
    private Integer divideIntegerMemoryArgumentsSlope;
    @SerializedName("encodeUtf8-cpu-arguments-intercept")
    @Expose
    private Integer encodeUtf8CpuArgumentsIntercept;
    @SerializedName("encodeUtf8-cpu-arguments-slope")
    @Expose
    private Integer encodeUtf8CpuArgumentsSlope;
    @SerializedName("encodeUtf8-memory-arguments-intercept")
    @Expose
    private Integer encodeUtf8MemoryArgumentsIntercept;
    @SerializedName("encodeUtf8-memory-arguments-slope")
    @Expose
    private Integer encodeUtf8MemoryArgumentsSlope;
    @SerializedName("equalsByteString-cpu-arguments-constant")
    @Expose
    private Integer equalsByteStringCpuArgumentsConstant;
    @SerializedName("equalsByteString-cpu-arguments-intercept")
    @Expose
    private Integer equalsByteStringCpuArgumentsIntercept;
    @SerializedName("equalsByteString-cpu-arguments-slope")
    @Expose
    private Integer equalsByteStringCpuArgumentsSlope;
    @SerializedName("equalsByteString-memory-arguments")
    @Expose
    private Integer equalsByteStringMemoryArguments;
    @SerializedName("equalsData-cpu-arguments-intercept")
    @Expose
    private Integer equalsDataCpuArgumentsIntercept;
    @SerializedName("equalsData-cpu-arguments-slope")
    @Expose
    private Integer equalsDataCpuArgumentsSlope;
    @SerializedName("equalsData-memory-arguments")
    @Expose
    private Integer equalsDataMemoryArguments;
    @SerializedName("equalsInteger-cpu-arguments-intercept")
    @Expose
    private Integer equalsIntegerCpuArgumentsIntercept;
    @SerializedName("equalsInteger-cpu-arguments-slope")
    @Expose
    private Integer equalsIntegerCpuArgumentsSlope;
    @SerializedName("equalsInteger-memory-arguments")
    @Expose
    private Integer equalsIntegerMemoryArguments;
    @SerializedName("equalsString-cpu-arguments-constant")
    @Expose
    private Integer equalsStringCpuArgumentsConstant;
    @SerializedName("equalsString-cpu-arguments-intercept")
    @Expose
    private Integer equalsStringCpuArgumentsIntercept;
    @SerializedName("equalsString-cpu-arguments-slope")
    @Expose
    private Integer equalsStringCpuArgumentsSlope;
    @SerializedName("equalsString-memory-arguments")
    @Expose
    private Integer equalsStringMemoryArguments;
    @SerializedName("fstPair-cpu-arguments")
    @Expose
    private Integer fstPairCpuArguments;
    @SerializedName("fstPair-memory-arguments")
    @Expose
    private Integer fstPairMemoryArguments;
    @SerializedName("headList-cpu-arguments")
    @Expose
    private Integer headListCpuArguments;
    @SerializedName("headList-memory-arguments")
    @Expose
    private Integer headListMemoryArguments;
    @SerializedName("iData-cpu-arguments")
    @Expose
    private Integer iDataCpuArguments;
    @SerializedName("iData-memory-arguments")
    @Expose
    private Integer iDataMemoryArguments;
    @SerializedName("ifThenElse-cpu-arguments")
    @Expose
    private Integer ifThenElseCpuArguments;
    @SerializedName("ifThenElse-memory-arguments")
    @Expose
    private Integer ifThenElseMemoryArguments;
    @SerializedName("indexByteString-cpu-arguments")
    @Expose
    private Integer indexByteStringCpuArguments;
    @SerializedName("indexByteString-memory-arguments")
    @Expose
    private Integer indexByteStringMemoryArguments;
    @SerializedName("lengthOfByteString-cpu-arguments")
    @Expose
    private Integer lengthOfByteStringCpuArguments;
    @SerializedName("lengthOfByteString-memory-arguments")
    @Expose
    private Integer lengthOfByteStringMemoryArguments;
    @SerializedName("lessThanByteString-cpu-arguments-intercept")
    @Expose
    private Integer lessThanByteStringCpuArgumentsIntercept;
    @SerializedName("lessThanByteString-cpu-arguments-slope")
    @Expose
    private Integer lessThanByteStringCpuArgumentsSlope;
    @SerializedName("lessThanByteString-memory-arguments")
    @Expose
    private Integer lessThanByteStringMemoryArguments;
    @SerializedName("lessThanEqualsByteString-cpu-arguments-intercept")
    @Expose
    private Integer lessThanEqualsByteStringCpuArgumentsIntercept;
    @SerializedName("lessThanEqualsByteString-cpu-arguments-slope")
    @Expose
    private Integer lessThanEqualsByteStringCpuArgumentsSlope;
    @SerializedName("lessThanEqualsByteString-memory-arguments")
    @Expose
    private Integer lessThanEqualsByteStringMemoryArguments;
    @SerializedName("lessThanEqualsInteger-cpu-arguments-intercept")
    @Expose
    private Integer lessThanEqualsIntegerCpuArgumentsIntercept;
    @SerializedName("lessThanEqualsInteger-cpu-arguments-slope")
    @Expose
    private Integer lessThanEqualsIntegerCpuArgumentsSlope;
    @SerializedName("lessThanEqualsInteger-memory-arguments")
    @Expose
    private Integer lessThanEqualsIntegerMemoryArguments;
    @SerializedName("lessThanInteger-cpu-arguments-intercept")
    @Expose
    private Integer lessThanIntegerCpuArgumentsIntercept;
    @SerializedName("lessThanInteger-cpu-arguments-slope")
    @Expose
    private Integer lessThanIntegerCpuArgumentsSlope;
    @SerializedName("lessThanInteger-memory-arguments")
    @Expose
    private Integer lessThanIntegerMemoryArguments;
    @SerializedName("listData-cpu-arguments")
    @Expose
    private Integer listDataCpuArguments;
    @SerializedName("listData-memory-arguments")
    @Expose
    private Integer listDataMemoryArguments;
    @SerializedName("mapData-cpu-arguments")
    @Expose
    private Integer mapDataCpuArguments;
    @SerializedName("mapData-memory-arguments")
    @Expose
    private Integer mapDataMemoryArguments;
    @SerializedName("mkCons-cpu-arguments")
    @Expose
    private Integer mkConsCpuArguments;
    @SerializedName("mkCons-memory-arguments")
    @Expose
    private Integer mkConsMemoryArguments;
    @SerializedName("mkNilData-cpu-arguments")
    @Expose
    private Integer mkNilDataCpuArguments;
    @SerializedName("mkNilData-memory-arguments")
    @Expose
    private Integer mkNilDataMemoryArguments;
    @SerializedName("mkNilPairData-cpu-arguments")
    @Expose
    private Integer mkNilPairDataCpuArguments;
    @SerializedName("mkNilPairData-memory-arguments")
    @Expose
    private Integer mkNilPairDataMemoryArguments;
    @SerializedName("mkPairData-cpu-arguments")
    @Expose
    private Integer mkPairDataCpuArguments;
    @SerializedName("mkPairData-memory-arguments")
    @Expose
    private Integer mkPairDataMemoryArguments;
    @SerializedName("modInteger-cpu-arguments-constant")
    @Expose
    private Integer modIntegerCpuArgumentsConstant;
    @SerializedName("modInteger-cpu-arguments-model-arguments-intercept")
    @Expose
    private Integer modIntegerCpuArgumentsModelArgumentsIntercept;
    @SerializedName("modInteger-cpu-arguments-model-arguments-slope")
    @Expose
    private Integer modIntegerCpuArgumentsModelArgumentsSlope;
    @SerializedName("modInteger-memory-arguments-intercept")
    @Expose
    private Integer modIntegerMemoryArgumentsIntercept;
    @SerializedName("modInteger-memory-arguments-minimum")
    @Expose
    private Integer modIntegerMemoryArgumentsMinimum;
    @SerializedName("modInteger-memory-arguments-slope")
    @Expose
    private Integer modIntegerMemoryArgumentsSlope;
    @SerializedName("multiplyInteger-cpu-arguments-intercept")
    @Expose
    private Integer multiplyIntegerCpuArgumentsIntercept;
    @SerializedName("multiplyInteger-cpu-arguments-slope")
    @Expose
    private Integer multiplyIntegerCpuArgumentsSlope;
    @SerializedName("multiplyInteger-memory-arguments-intercept")
    @Expose
    private Integer multiplyIntegerMemoryArgumentsIntercept;
    @SerializedName("multiplyInteger-memory-arguments-slope")
    @Expose
    private Integer multiplyIntegerMemoryArgumentsSlope;
    @SerializedName("nullList-cpu-arguments")
    @Expose
    private Integer nullListCpuArguments;
    @SerializedName("nullList-memory-arguments")
    @Expose
    private Integer nullListMemoryArguments;
    @SerializedName("quotientInteger-cpu-arguments-constant")
    @Expose
    private Integer quotientIntegerCpuArgumentsConstant;
    @SerializedName("quotientInteger-cpu-arguments-model-arguments-intercept")
    @Expose
    private Integer quotientIntegerCpuArgumentsModelArgumentsIntercept;
    @SerializedName("quotientInteger-cpu-arguments-model-arguments-slope")
    @Expose
    private Integer quotientIntegerCpuArgumentsModelArgumentsSlope;
    @SerializedName("quotientInteger-memory-arguments-intercept")
    @Expose
    private Integer quotientIntegerMemoryArgumentsIntercept;
    @SerializedName("quotientInteger-memory-arguments-minimum")
    @Expose
    private Integer quotientIntegerMemoryArgumentsMinimum;
    @SerializedName("quotientInteger-memory-arguments-slope")
    @Expose
    private Integer quotientIntegerMemoryArgumentsSlope;
    @SerializedName("remainderInteger-cpu-arguments-constant")
    @Expose
    private Integer remainderIntegerCpuArgumentsConstant;
    @SerializedName("remainderInteger-cpu-arguments-model-arguments-intercept")
    @Expose
    private Integer remainderIntegerCpuArgumentsModelArgumentsIntercept;
    @SerializedName("remainderInteger-cpu-arguments-model-arguments-slope")
    @Expose
    private Integer remainderIntegerCpuArgumentsModelArgumentsSlope;
    @SerializedName("remainderInteger-memory-arguments-intercept")
    @Expose
    private Integer remainderIntegerMemoryArgumentsIntercept;
    @SerializedName("remainderInteger-memory-arguments-minimum")
    @Expose
    private Integer remainderIntegerMemoryArgumentsMinimum;
    @SerializedName("remainderInteger-memory-arguments-slope")
    @Expose
    private Integer remainderIntegerMemoryArgumentsSlope;
    @SerializedName("sha2_256-cpu-arguments-intercept")
    @Expose
    private Integer sha2256CpuArgumentsIntercept;
    @SerializedName("sha2_256-cpu-arguments-slope")
    @Expose
    private Integer sha2256CpuArgumentsSlope;
    @SerializedName("sha2_256-memory-arguments")
    @Expose
    private Integer sha2256MemoryArguments;
    @SerializedName("sha3_256-cpu-arguments-intercept")
    @Expose
    private Integer sha3256CpuArgumentsIntercept;
    @SerializedName("sha3_256-cpu-arguments-slope")
    @Expose
    private Integer sha3256CpuArgumentsSlope;
    @SerializedName("sha3_256-memory-arguments")
    @Expose
    private Integer sha3256MemoryArguments;
    @SerializedName("sliceByteString-cpu-arguments-intercept")
    @Expose
    private Integer sliceByteStringCpuArgumentsIntercept;
    @SerializedName("sliceByteString-cpu-arguments-slope")
    @Expose
    private Integer sliceByteStringCpuArgumentsSlope;
    @SerializedName("sliceByteString-memory-arguments-intercept")
    @Expose
    private Integer sliceByteStringMemoryArgumentsIntercept;
    @SerializedName("sliceByteString-memory-arguments-slope")
    @Expose
    private Integer sliceByteStringMemoryArgumentsSlope;
    @SerializedName("sndPair-cpu-arguments")
    @Expose
    private Integer sndPairCpuArguments;
    @SerializedName("sndPair-memory-arguments")
    @Expose
    private Integer sndPairMemoryArguments;
    @SerializedName("subtractInteger-cpu-arguments-intercept")
    @Expose
    private Integer subtractIntegerCpuArgumentsIntercept;
    @SerializedName("subtractInteger-cpu-arguments-slope")
    @Expose
    private Integer subtractIntegerCpuArgumentsSlope;
    @SerializedName("subtractInteger-memory-arguments-intercept")
    @Expose
    private Integer subtractIntegerMemoryArgumentsIntercept;
    @SerializedName("subtractInteger-memory-arguments-slope")
    @Expose
    private Integer subtractIntegerMemoryArgumentsSlope;
    @SerializedName("tailList-cpu-arguments")
    @Expose
    private Integer tailListCpuArguments;
    @SerializedName("tailList-memory-arguments")
    @Expose
    private Integer tailListMemoryArguments;
    @SerializedName("trace-cpu-arguments")
    @Expose
    private Integer traceCpuArguments;
    @SerializedName("trace-memory-arguments")
    @Expose
    private Integer traceMemoryArguments;
    @SerializedName("unBData-cpu-arguments")
    @Expose
    private Integer unBDataCpuArguments;
    @SerializedName("unBData-memory-arguments")
    @Expose
    private Integer unBDataMemoryArguments;
    @SerializedName("unConstrData-cpu-arguments")
    @Expose
    private Integer unConstrDataCpuArguments;
    @SerializedName("unConstrData-memory-arguments")
    @Expose
    private Integer unConstrDataMemoryArguments;
    @SerializedName("unIData-cpu-arguments")
    @Expose
    private Integer unIDataCpuArguments;
    @SerializedName("unIData-memory-arguments")
    @Expose
    private Integer unIDataMemoryArguments;
    @SerializedName("unListData-cpu-arguments")
    @Expose
    private Integer unListDataCpuArguments;
    @SerializedName("unListData-memory-arguments")
    @Expose
    private Integer unListDataMemoryArguments;
    @SerializedName("unMapData-cpu-arguments")
    @Expose
    private Integer unMapDataCpuArguments;
    @SerializedName("unMapData-memory-arguments")
    @Expose
    private Integer unMapDataMemoryArguments;
    @SerializedName("verifyEd25519Signature-cpu-arguments-intercept")
    @Expose
    private Integer verifyEd25519SignatureCpuArgumentsIntercept;
    @SerializedName("verifyEd25519Signature-cpu-arguments-slope")
    @Expose
    private Integer verifyEd25519SignatureCpuArgumentsSlope;
    @SerializedName("verifyEd25519Signature-memory-arguments")
    @Expose
    private Integer verifyEd25519SignatureMemoryArguments;

    /**
     * No args constructor for use in serialization
     * 
     */
    public PlutusV1() {
    }

    /**
     * 
     * @param iDataMemoryArguments
     * @param lengthOfByteStringMemoryArguments
     * @param lessThanByteStringMemoryArguments
     * @param decodeUtf8CpuArgumentsIntercept
     * @param sha3256CpuArgumentsSlope
     * @param unIDataCpuArguments
     * @param appendStringMemoryArgumentsIntercept
     * @param equalsStringCpuArgumentsSlope
     * @param verifyEd25519SignatureMemoryArguments
     * @param blake2b256MemoryArguments
     * @param iDataCpuArguments
     * @param mkNilPairDataCpuArguments
     * @param cekLamCostExBudgetCPU
     * @param sliceByteStringMemoryArgumentsSlope
     * @param consByteStringMemoryArgumentsSlope
     * @param cekForceCostExBudgetCPU
     * @param addIntegerCpuArgumentsIntercept
     * @param equalsDataMemoryArguments
     * @param lessThanEqualsByteStringMemoryArguments
     * @param addIntegerMemoryArgumentsIntercept
     * @param fstPairMemoryArguments
     * @param multiplyIntegerCpuArgumentsSlope
     * @param lessThanIntegerCpuArgumentsIntercept
     * @param quotientIntegerMemoryArgumentsSlope
     * @param sha3256CpuArgumentsIntercept
     * @param lessThanIntegerMemoryArguments
     * @param addIntegerMemoryArgumentsSlope
     * @param equalsDataCpuArgumentsSlope
     * @param modIntegerMemoryArgumentsMinimum
     * @param quotientIntegerCpuArgumentsModelArgumentsIntercept
     * @param appendByteStringMemoryArgumentsIntercept
     * @param sha2256CpuArgumentsSlope
     * @param sha3256MemoryArguments
     * @param consByteStringMemoryArgumentsIntercept
     * @param remainderIntegerCpuArgumentsModelArgumentsIntercept
     * @param cekBuiltinCostExBudgetMemory
     * @param cekVarCostExBudgetCPU
     * @param sha2256MemoryArguments
     * @param headListMemoryArguments
     * @param divideIntegerMemoryArgumentsMinimum
     * @param equalsIntegerMemoryArguments
     * @param subtractIntegerMemoryArgumentsSlope
     * @param sliceByteStringMemoryArgumentsIntercept
     * @param chooseDataCpuArguments
     * @param mapDataMemoryArguments
     * @param cekLamCostExBudgetMemory
     * @param quotientIntegerMemoryArgumentsMinimum
     * @param bDataMemoryArguments
     * @param remainderIntegerCpuArgumentsModelArgumentsSlope
     * @param cekStartupCostExBudgetMemory
     * @param lessThanEqualsByteStringCpuArgumentsIntercept
     * @param blake2b256CpuArgumentsIntercept
     * @param fstPairCpuArguments
     * @param sliceByteStringCpuArgumentsSlope
     * @param indexByteStringMemoryArguments
     * @param equalsDataCpuArgumentsIntercept
     * @param lessThanEqualsIntegerCpuArgumentsSlope
     * @param divideIntegerMemoryArgumentsIntercept
     * @param cekConstCostExBudgetMemory
     * @param remainderIntegerCpuArgumentsConstant
     * @param tailListCpuArguments
     * @param mkNilDataMemoryArguments
     * @param equalsStringCpuArgumentsConstant
     * @param equalsByteStringCpuArgumentsIntercept
     * @param modIntegerCpuArgumentsConstant
     * @param sliceByteStringCpuArgumentsIntercept
     * @param listDataMemoryArguments
     * @param equalsStringMemoryArguments
     * @param divideIntegerCpuArgumentsConstant
     * @param appendStringCpuArgumentsIntercept
     * @param mkPairDataMemoryArguments
     * @param cekDelayCostExBudgetMemory
     * @param blake2b256CpuArgumentsSlope
     * @param chooseListMemoryArguments
     * @param equalsIntegerCpuArgumentsSlope
     * @param headListCpuArguments
     * @param equalsByteStringCpuArgumentsConstant
     * @param cekDelayCostExBudgetCPU
     * @param consByteStringCpuArgumentsSlope
     * @param lessThanEqualsIntegerCpuArgumentsIntercept
     * @param cekStartupCostExBudgetCPU
     * @param ifThenElseCpuArguments
     * @param mkConsMemoryArguments
     * @param mkNilDataCpuArguments
     * @param divideIntegerMemoryArgumentsSlope
     * @param sha2256CpuArgumentsIntercept
     * @param chooseListCpuArguments
     * @param modIntegerCpuArgumentsModelArgumentsIntercept
     * @param encodeUtf8CpuArgumentsSlope
     * @param appendByteStringCpuArgumentsSlope
     * @param subtractIntegerMemoryArgumentsIntercept
     * @param unConstrDataMemoryArguments
     * @param consByteStringCpuArgumentsIntercept
     * @param modIntegerCpuArgumentsModelArgumentsSlope
     * @param unMapDataMemoryArguments
     * @param cekBuiltinCostExBudgetCPU
     * @param unConstrDataCpuArguments
     * @param verifyEd25519SignatureCpuArgumentsSlope
     * @param decodeUtf8MemoryArgumentsSlope
     * @param equalsByteStringMemoryArguments
     * @param multiplyIntegerMemoryArgumentsSlope
     * @param tailListMemoryArguments
     * @param lessThanByteStringCpuArgumentsIntercept
     * @param bDataCpuArguments
     * @param cekConstCostExBudgetCPU
     * @param chooseDataMemoryArguments
     * @param lessThanEqualsIntegerMemoryArguments
     * @param unBDataMemoryArguments
     * @param multiplyIntegerCpuArgumentsIntercept
     * @param ifThenElseMemoryArguments
     * @param unIDataMemoryArguments
     * @param verifyEd25519SignatureCpuArgumentsIntercept
     * @param lessThanEqualsByteStringCpuArgumentsSlope
     * @param cekVarCostExBudgetMemory
     * @param decodeUtf8CpuArgumentsSlope
     * @param divideIntegerCpuArgumentsModelArgumentsIntercept
     * @param appendStringMemoryArgumentsSlope
     * @param encodeUtf8MemoryArgumentsIntercept
     * @param mkPairDataCpuArguments
     * @param modIntegerMemoryArgumentsSlope
     * @param mkNilPairDataMemoryArguments
     * @param traceCpuArguments
     * @param mapDataCpuArguments
     * @param equalsStringCpuArgumentsIntercept
     * @param remainderIntegerMemoryArgumentsSlope
     * @param lessThanByteStringCpuArgumentsSlope
     * @param constrDataMemoryArguments
     * @param cekApplyCostExBudgetMemory
     * @param appendByteStringMemoryArgumentsSlope
     * @param chooseUnitMemoryArguments
     * @param quotientIntegerCpuArgumentsModelArgumentsSlope
     * @param cekApplyCostExBudgetCPU
     * @param unListDataCpuArguments
     * @param sndPairCpuArguments
     * @param lengthOfByteStringCpuArguments
     * @param lessThanIntegerCpuArgumentsSlope
     * @param mkConsCpuArguments
     * @param quotientIntegerCpuArgumentsConstant
     * @param equalsByteStringCpuArgumentsSlope
     * @param chooseUnitCpuArguments
     * @param sndPairMemoryArguments
     * @param nullListCpuArguments
     * @param remainderIntegerMemoryArgumentsIntercept
     * @param equalsIntegerCpuArgumentsIntercept
     * @param multiplyIntegerMemoryArgumentsIntercept
     * @param nullListMemoryArguments
     * @param unBDataCpuArguments
     * @param quotientIntegerMemoryArgumentsIntercept
     * @param appendStringCpuArgumentsSlope
     * @param decodeUtf8MemoryArgumentsIntercept
     * @param modIntegerMemoryArgumentsIntercept
     * @param traceMemoryArguments
     * @param cekForceCostExBudgetMemory
     * @param divideIntegerCpuArgumentsModelArgumentsSlope
     * @param addIntegerCpuArgumentsSlope
     * @param encodeUtf8MemoryArgumentsSlope
     * @param unListDataMemoryArguments
     * @param indexByteStringCpuArguments
     * @param appendByteStringCpuArgumentsIntercept
     * @param subtractIntegerCpuArgumentsIntercept
     * @param unMapDataCpuArguments
     * @param constrDataCpuArguments
     * @param listDataCpuArguments
     * @param remainderIntegerMemoryArgumentsMinimum
     * @param subtractIntegerCpuArgumentsSlope
     * @param encodeUtf8CpuArgumentsIntercept
     */
    public PlutusV1(Integer addIntegerCpuArgumentsIntercept, Integer addIntegerCpuArgumentsSlope, Integer addIntegerMemoryArgumentsIntercept, Integer addIntegerMemoryArgumentsSlope, Integer appendByteStringCpuArgumentsIntercept, Integer appendByteStringCpuArgumentsSlope, Integer appendByteStringMemoryArgumentsIntercept, Integer appendByteStringMemoryArgumentsSlope, Integer appendStringCpuArgumentsIntercept, Integer appendStringCpuArgumentsSlope, Integer appendStringMemoryArgumentsIntercept, Integer appendStringMemoryArgumentsSlope, Integer bDataCpuArguments, Integer bDataMemoryArguments, Integer blake2b256CpuArgumentsIntercept, Integer blake2b256CpuArgumentsSlope, Integer blake2b256MemoryArguments, Integer cekApplyCostExBudgetCPU, Integer cekApplyCostExBudgetMemory, Integer cekBuiltinCostExBudgetCPU, Integer cekBuiltinCostExBudgetMemory, Integer cekConstCostExBudgetCPU, Integer cekConstCostExBudgetMemory, Integer cekDelayCostExBudgetCPU, Integer cekDelayCostExBudgetMemory, Integer cekForceCostExBudgetCPU, Integer cekForceCostExBudgetMemory, Integer cekLamCostExBudgetCPU, Integer cekLamCostExBudgetMemory, Integer cekStartupCostExBudgetCPU, Integer cekStartupCostExBudgetMemory, Integer cekVarCostExBudgetCPU, Integer cekVarCostExBudgetMemory, Integer chooseDataCpuArguments, Integer chooseDataMemoryArguments, Integer chooseListCpuArguments, Integer chooseListMemoryArguments, Integer chooseUnitCpuArguments, Integer chooseUnitMemoryArguments, Integer consByteStringCpuArgumentsIntercept, Integer consByteStringCpuArgumentsSlope, Integer consByteStringMemoryArgumentsIntercept, Integer consByteStringMemoryArgumentsSlope, Integer constrDataCpuArguments, Integer constrDataMemoryArguments, Integer decodeUtf8CpuArgumentsIntercept, Integer decodeUtf8CpuArgumentsSlope, Integer decodeUtf8MemoryArgumentsIntercept, Integer decodeUtf8MemoryArgumentsSlope, Integer divideIntegerCpuArgumentsConstant, Integer divideIntegerCpuArgumentsModelArgumentsIntercept, Integer divideIntegerCpuArgumentsModelArgumentsSlope, Integer divideIntegerMemoryArgumentsIntercept, Integer divideIntegerMemoryArgumentsMinimum, Integer divideIntegerMemoryArgumentsSlope, Integer encodeUtf8CpuArgumentsIntercept, Integer encodeUtf8CpuArgumentsSlope, Integer encodeUtf8MemoryArgumentsIntercept, Integer encodeUtf8MemoryArgumentsSlope, Integer equalsByteStringCpuArgumentsConstant, Integer equalsByteStringCpuArgumentsIntercept, Integer equalsByteStringCpuArgumentsSlope, Integer equalsByteStringMemoryArguments, Integer equalsDataCpuArgumentsIntercept, Integer equalsDataCpuArgumentsSlope, Integer equalsDataMemoryArguments, Integer equalsIntegerCpuArgumentsIntercept, Integer equalsIntegerCpuArgumentsSlope, Integer equalsIntegerMemoryArguments, Integer equalsStringCpuArgumentsConstant, Integer equalsStringCpuArgumentsIntercept, Integer equalsStringCpuArgumentsSlope, Integer equalsStringMemoryArguments, Integer fstPairCpuArguments, Integer fstPairMemoryArguments, Integer headListCpuArguments, Integer headListMemoryArguments, Integer iDataCpuArguments, Integer iDataMemoryArguments, Integer ifThenElseCpuArguments, Integer ifThenElseMemoryArguments, Integer indexByteStringCpuArguments, Integer indexByteStringMemoryArguments, Integer lengthOfByteStringCpuArguments, Integer lengthOfByteStringMemoryArguments, Integer lessThanByteStringCpuArgumentsIntercept, Integer lessThanByteStringCpuArgumentsSlope, Integer lessThanByteStringMemoryArguments, Integer lessThanEqualsByteStringCpuArgumentsIntercept, Integer lessThanEqualsByteStringCpuArgumentsSlope, Integer lessThanEqualsByteStringMemoryArguments, Integer lessThanEqualsIntegerCpuArgumentsIntercept, Integer lessThanEqualsIntegerCpuArgumentsSlope, Integer lessThanEqualsIntegerMemoryArguments, Integer lessThanIntegerCpuArgumentsIntercept, Integer lessThanIntegerCpuArgumentsSlope, Integer lessThanIntegerMemoryArguments, Integer listDataCpuArguments, Integer listDataMemoryArguments, Integer mapDataCpuArguments, Integer mapDataMemoryArguments, Integer mkConsCpuArguments, Integer mkConsMemoryArguments, Integer mkNilDataCpuArguments, Integer mkNilDataMemoryArguments, Integer mkNilPairDataCpuArguments, Integer mkNilPairDataMemoryArguments, Integer mkPairDataCpuArguments, Integer mkPairDataMemoryArguments, Integer modIntegerCpuArgumentsConstant, Integer modIntegerCpuArgumentsModelArgumentsIntercept, Integer modIntegerCpuArgumentsModelArgumentsSlope, Integer modIntegerMemoryArgumentsIntercept, Integer modIntegerMemoryArgumentsMinimum, Integer modIntegerMemoryArgumentsSlope, Integer multiplyIntegerCpuArgumentsIntercept, Integer multiplyIntegerCpuArgumentsSlope, Integer multiplyIntegerMemoryArgumentsIntercept, Integer multiplyIntegerMemoryArgumentsSlope, Integer nullListCpuArguments, Integer nullListMemoryArguments, Integer quotientIntegerCpuArgumentsConstant, Integer quotientIntegerCpuArgumentsModelArgumentsIntercept, Integer quotientIntegerCpuArgumentsModelArgumentsSlope, Integer quotientIntegerMemoryArgumentsIntercept, Integer quotientIntegerMemoryArgumentsMinimum, Integer quotientIntegerMemoryArgumentsSlope, Integer remainderIntegerCpuArgumentsConstant, Integer remainderIntegerCpuArgumentsModelArgumentsIntercept, Integer remainderIntegerCpuArgumentsModelArgumentsSlope, Integer remainderIntegerMemoryArgumentsIntercept, Integer remainderIntegerMemoryArgumentsMinimum, Integer remainderIntegerMemoryArgumentsSlope, Integer sha2256CpuArgumentsIntercept, Integer sha2256CpuArgumentsSlope, Integer sha2256MemoryArguments, Integer sha3256CpuArgumentsIntercept, Integer sha3256CpuArgumentsSlope, Integer sha3256MemoryArguments, Integer sliceByteStringCpuArgumentsIntercept, Integer sliceByteStringCpuArgumentsSlope, Integer sliceByteStringMemoryArgumentsIntercept, Integer sliceByteStringMemoryArgumentsSlope, Integer sndPairCpuArguments, Integer sndPairMemoryArguments, Integer subtractIntegerCpuArgumentsIntercept, Integer subtractIntegerCpuArgumentsSlope, Integer subtractIntegerMemoryArgumentsIntercept, Integer subtractIntegerMemoryArgumentsSlope, Integer tailListCpuArguments, Integer tailListMemoryArguments, Integer traceCpuArguments, Integer traceMemoryArguments, Integer unBDataCpuArguments, Integer unBDataMemoryArguments, Integer unConstrDataCpuArguments, Integer unConstrDataMemoryArguments, Integer unIDataCpuArguments, Integer unIDataMemoryArguments, Integer unListDataCpuArguments, Integer unListDataMemoryArguments, Integer unMapDataCpuArguments, Integer unMapDataMemoryArguments, Integer verifyEd25519SignatureCpuArgumentsIntercept, Integer verifyEd25519SignatureCpuArgumentsSlope, Integer verifyEd25519SignatureMemoryArguments) {
        super();
        this.addIntegerCpuArgumentsIntercept = addIntegerCpuArgumentsIntercept;
        this.addIntegerCpuArgumentsSlope = addIntegerCpuArgumentsSlope;
        this.addIntegerMemoryArgumentsIntercept = addIntegerMemoryArgumentsIntercept;
        this.addIntegerMemoryArgumentsSlope = addIntegerMemoryArgumentsSlope;
        this.appendByteStringCpuArgumentsIntercept = appendByteStringCpuArgumentsIntercept;
        this.appendByteStringCpuArgumentsSlope = appendByteStringCpuArgumentsSlope;
        this.appendByteStringMemoryArgumentsIntercept = appendByteStringMemoryArgumentsIntercept;
        this.appendByteStringMemoryArgumentsSlope = appendByteStringMemoryArgumentsSlope;
        this.appendStringCpuArgumentsIntercept = appendStringCpuArgumentsIntercept;
        this.appendStringCpuArgumentsSlope = appendStringCpuArgumentsSlope;
        this.appendStringMemoryArgumentsIntercept = appendStringMemoryArgumentsIntercept;
        this.appendStringMemoryArgumentsSlope = appendStringMemoryArgumentsSlope;
        this.bDataCpuArguments = bDataCpuArguments;
        this.bDataMemoryArguments = bDataMemoryArguments;
        this.blake2b256CpuArgumentsIntercept = blake2b256CpuArgumentsIntercept;
        this.blake2b256CpuArgumentsSlope = blake2b256CpuArgumentsSlope;
        this.blake2b256MemoryArguments = blake2b256MemoryArguments;
        this.cekApplyCostExBudgetCPU = cekApplyCostExBudgetCPU;
        this.cekApplyCostExBudgetMemory = cekApplyCostExBudgetMemory;
        this.cekBuiltinCostExBudgetCPU = cekBuiltinCostExBudgetCPU;
        this.cekBuiltinCostExBudgetMemory = cekBuiltinCostExBudgetMemory;
        this.cekConstCostExBudgetCPU = cekConstCostExBudgetCPU;
        this.cekConstCostExBudgetMemory = cekConstCostExBudgetMemory;
        this.cekDelayCostExBudgetCPU = cekDelayCostExBudgetCPU;
        this.cekDelayCostExBudgetMemory = cekDelayCostExBudgetMemory;
        this.cekForceCostExBudgetCPU = cekForceCostExBudgetCPU;
        this.cekForceCostExBudgetMemory = cekForceCostExBudgetMemory;
        this.cekLamCostExBudgetCPU = cekLamCostExBudgetCPU;
        this.cekLamCostExBudgetMemory = cekLamCostExBudgetMemory;
        this.cekStartupCostExBudgetCPU = cekStartupCostExBudgetCPU;
        this.cekStartupCostExBudgetMemory = cekStartupCostExBudgetMemory;
        this.cekVarCostExBudgetCPU = cekVarCostExBudgetCPU;
        this.cekVarCostExBudgetMemory = cekVarCostExBudgetMemory;
        this.chooseDataCpuArguments = chooseDataCpuArguments;
        this.chooseDataMemoryArguments = chooseDataMemoryArguments;
        this.chooseListCpuArguments = chooseListCpuArguments;
        this.chooseListMemoryArguments = chooseListMemoryArguments;
        this.chooseUnitCpuArguments = chooseUnitCpuArguments;
        this.chooseUnitMemoryArguments = chooseUnitMemoryArguments;
        this.consByteStringCpuArgumentsIntercept = consByteStringCpuArgumentsIntercept;
        this.consByteStringCpuArgumentsSlope = consByteStringCpuArgumentsSlope;
        this.consByteStringMemoryArgumentsIntercept = consByteStringMemoryArgumentsIntercept;
        this.consByteStringMemoryArgumentsSlope = consByteStringMemoryArgumentsSlope;
        this.constrDataCpuArguments = constrDataCpuArguments;
        this.constrDataMemoryArguments = constrDataMemoryArguments;
        this.decodeUtf8CpuArgumentsIntercept = decodeUtf8CpuArgumentsIntercept;
        this.decodeUtf8CpuArgumentsSlope = decodeUtf8CpuArgumentsSlope;
        this.decodeUtf8MemoryArgumentsIntercept = decodeUtf8MemoryArgumentsIntercept;
        this.decodeUtf8MemoryArgumentsSlope = decodeUtf8MemoryArgumentsSlope;
        this.divideIntegerCpuArgumentsConstant = divideIntegerCpuArgumentsConstant;
        this.divideIntegerCpuArgumentsModelArgumentsIntercept = divideIntegerCpuArgumentsModelArgumentsIntercept;
        this.divideIntegerCpuArgumentsModelArgumentsSlope = divideIntegerCpuArgumentsModelArgumentsSlope;
        this.divideIntegerMemoryArgumentsIntercept = divideIntegerMemoryArgumentsIntercept;
        this.divideIntegerMemoryArgumentsMinimum = divideIntegerMemoryArgumentsMinimum;
        this.divideIntegerMemoryArgumentsSlope = divideIntegerMemoryArgumentsSlope;
        this.encodeUtf8CpuArgumentsIntercept = encodeUtf8CpuArgumentsIntercept;
        this.encodeUtf8CpuArgumentsSlope = encodeUtf8CpuArgumentsSlope;
        this.encodeUtf8MemoryArgumentsIntercept = encodeUtf8MemoryArgumentsIntercept;
        this.encodeUtf8MemoryArgumentsSlope = encodeUtf8MemoryArgumentsSlope;
        this.equalsByteStringCpuArgumentsConstant = equalsByteStringCpuArgumentsConstant;
        this.equalsByteStringCpuArgumentsIntercept = equalsByteStringCpuArgumentsIntercept;
        this.equalsByteStringCpuArgumentsSlope = equalsByteStringCpuArgumentsSlope;
        this.equalsByteStringMemoryArguments = equalsByteStringMemoryArguments;
        this.equalsDataCpuArgumentsIntercept = equalsDataCpuArgumentsIntercept;
        this.equalsDataCpuArgumentsSlope = equalsDataCpuArgumentsSlope;
        this.equalsDataMemoryArguments = equalsDataMemoryArguments;
        this.equalsIntegerCpuArgumentsIntercept = equalsIntegerCpuArgumentsIntercept;
        this.equalsIntegerCpuArgumentsSlope = equalsIntegerCpuArgumentsSlope;
        this.equalsIntegerMemoryArguments = equalsIntegerMemoryArguments;
        this.equalsStringCpuArgumentsConstant = equalsStringCpuArgumentsConstant;
        this.equalsStringCpuArgumentsIntercept = equalsStringCpuArgumentsIntercept;
        this.equalsStringCpuArgumentsSlope = equalsStringCpuArgumentsSlope;
        this.equalsStringMemoryArguments = equalsStringMemoryArguments;
        this.fstPairCpuArguments = fstPairCpuArguments;
        this.fstPairMemoryArguments = fstPairMemoryArguments;
        this.headListCpuArguments = headListCpuArguments;
        this.headListMemoryArguments = headListMemoryArguments;
        this.iDataCpuArguments = iDataCpuArguments;
        this.iDataMemoryArguments = iDataMemoryArguments;
        this.ifThenElseCpuArguments = ifThenElseCpuArguments;
        this.ifThenElseMemoryArguments = ifThenElseMemoryArguments;
        this.indexByteStringCpuArguments = indexByteStringCpuArguments;
        this.indexByteStringMemoryArguments = indexByteStringMemoryArguments;
        this.lengthOfByteStringCpuArguments = lengthOfByteStringCpuArguments;
        this.lengthOfByteStringMemoryArguments = lengthOfByteStringMemoryArguments;
        this.lessThanByteStringCpuArgumentsIntercept = lessThanByteStringCpuArgumentsIntercept;
        this.lessThanByteStringCpuArgumentsSlope = lessThanByteStringCpuArgumentsSlope;
        this.lessThanByteStringMemoryArguments = lessThanByteStringMemoryArguments;
        this.lessThanEqualsByteStringCpuArgumentsIntercept = lessThanEqualsByteStringCpuArgumentsIntercept;
        this.lessThanEqualsByteStringCpuArgumentsSlope = lessThanEqualsByteStringCpuArgumentsSlope;
        this.lessThanEqualsByteStringMemoryArguments = lessThanEqualsByteStringMemoryArguments;
        this.lessThanEqualsIntegerCpuArgumentsIntercept = lessThanEqualsIntegerCpuArgumentsIntercept;
        this.lessThanEqualsIntegerCpuArgumentsSlope = lessThanEqualsIntegerCpuArgumentsSlope;
        this.lessThanEqualsIntegerMemoryArguments = lessThanEqualsIntegerMemoryArguments;
        this.lessThanIntegerCpuArgumentsIntercept = lessThanIntegerCpuArgumentsIntercept;
        this.lessThanIntegerCpuArgumentsSlope = lessThanIntegerCpuArgumentsSlope;
        this.lessThanIntegerMemoryArguments = lessThanIntegerMemoryArguments;
        this.listDataCpuArguments = listDataCpuArguments;
        this.listDataMemoryArguments = listDataMemoryArguments;
        this.mapDataCpuArguments = mapDataCpuArguments;
        this.mapDataMemoryArguments = mapDataMemoryArguments;
        this.mkConsCpuArguments = mkConsCpuArguments;
        this.mkConsMemoryArguments = mkConsMemoryArguments;
        this.mkNilDataCpuArguments = mkNilDataCpuArguments;
        this.mkNilDataMemoryArguments = mkNilDataMemoryArguments;
        this.mkNilPairDataCpuArguments = mkNilPairDataCpuArguments;
        this.mkNilPairDataMemoryArguments = mkNilPairDataMemoryArguments;
        this.mkPairDataCpuArguments = mkPairDataCpuArguments;
        this.mkPairDataMemoryArguments = mkPairDataMemoryArguments;
        this.modIntegerCpuArgumentsConstant = modIntegerCpuArgumentsConstant;
        this.modIntegerCpuArgumentsModelArgumentsIntercept = modIntegerCpuArgumentsModelArgumentsIntercept;
        this.modIntegerCpuArgumentsModelArgumentsSlope = modIntegerCpuArgumentsModelArgumentsSlope;
        this.modIntegerMemoryArgumentsIntercept = modIntegerMemoryArgumentsIntercept;
        this.modIntegerMemoryArgumentsMinimum = modIntegerMemoryArgumentsMinimum;
        this.modIntegerMemoryArgumentsSlope = modIntegerMemoryArgumentsSlope;
        this.multiplyIntegerCpuArgumentsIntercept = multiplyIntegerCpuArgumentsIntercept;
        this.multiplyIntegerCpuArgumentsSlope = multiplyIntegerCpuArgumentsSlope;
        this.multiplyIntegerMemoryArgumentsIntercept = multiplyIntegerMemoryArgumentsIntercept;
        this.multiplyIntegerMemoryArgumentsSlope = multiplyIntegerMemoryArgumentsSlope;
        this.nullListCpuArguments = nullListCpuArguments;
        this.nullListMemoryArguments = nullListMemoryArguments;
        this.quotientIntegerCpuArgumentsConstant = quotientIntegerCpuArgumentsConstant;
        this.quotientIntegerCpuArgumentsModelArgumentsIntercept = quotientIntegerCpuArgumentsModelArgumentsIntercept;
        this.quotientIntegerCpuArgumentsModelArgumentsSlope = quotientIntegerCpuArgumentsModelArgumentsSlope;
        this.quotientIntegerMemoryArgumentsIntercept = quotientIntegerMemoryArgumentsIntercept;
        this.quotientIntegerMemoryArgumentsMinimum = quotientIntegerMemoryArgumentsMinimum;
        this.quotientIntegerMemoryArgumentsSlope = quotientIntegerMemoryArgumentsSlope;
        this.remainderIntegerCpuArgumentsConstant = remainderIntegerCpuArgumentsConstant;
        this.remainderIntegerCpuArgumentsModelArgumentsIntercept = remainderIntegerCpuArgumentsModelArgumentsIntercept;
        this.remainderIntegerCpuArgumentsModelArgumentsSlope = remainderIntegerCpuArgumentsModelArgumentsSlope;
        this.remainderIntegerMemoryArgumentsIntercept = remainderIntegerMemoryArgumentsIntercept;
        this.remainderIntegerMemoryArgumentsMinimum = remainderIntegerMemoryArgumentsMinimum;
        this.remainderIntegerMemoryArgumentsSlope = remainderIntegerMemoryArgumentsSlope;
        this.sha2256CpuArgumentsIntercept = sha2256CpuArgumentsIntercept;
        this.sha2256CpuArgumentsSlope = sha2256CpuArgumentsSlope;
        this.sha2256MemoryArguments = sha2256MemoryArguments;
        this.sha3256CpuArgumentsIntercept = sha3256CpuArgumentsIntercept;
        this.sha3256CpuArgumentsSlope = sha3256CpuArgumentsSlope;
        this.sha3256MemoryArguments = sha3256MemoryArguments;
        this.sliceByteStringCpuArgumentsIntercept = sliceByteStringCpuArgumentsIntercept;
        this.sliceByteStringCpuArgumentsSlope = sliceByteStringCpuArgumentsSlope;
        this.sliceByteStringMemoryArgumentsIntercept = sliceByteStringMemoryArgumentsIntercept;
        this.sliceByteStringMemoryArgumentsSlope = sliceByteStringMemoryArgumentsSlope;
        this.sndPairCpuArguments = sndPairCpuArguments;
        this.sndPairMemoryArguments = sndPairMemoryArguments;
        this.subtractIntegerCpuArgumentsIntercept = subtractIntegerCpuArgumentsIntercept;
        this.subtractIntegerCpuArgumentsSlope = subtractIntegerCpuArgumentsSlope;
        this.subtractIntegerMemoryArgumentsIntercept = subtractIntegerMemoryArgumentsIntercept;
        this.subtractIntegerMemoryArgumentsSlope = subtractIntegerMemoryArgumentsSlope;
        this.tailListCpuArguments = tailListCpuArguments;
        this.tailListMemoryArguments = tailListMemoryArguments;
        this.traceCpuArguments = traceCpuArguments;
        this.traceMemoryArguments = traceMemoryArguments;
        this.unBDataCpuArguments = unBDataCpuArguments;
        this.unBDataMemoryArguments = unBDataMemoryArguments;
        this.unConstrDataCpuArguments = unConstrDataCpuArguments;
        this.unConstrDataMemoryArguments = unConstrDataMemoryArguments;
        this.unIDataCpuArguments = unIDataCpuArguments;
        this.unIDataMemoryArguments = unIDataMemoryArguments;
        this.unListDataCpuArguments = unListDataCpuArguments;
        this.unListDataMemoryArguments = unListDataMemoryArguments;
        this.unMapDataCpuArguments = unMapDataCpuArguments;
        this.unMapDataMemoryArguments = unMapDataMemoryArguments;
        this.verifyEd25519SignatureCpuArgumentsIntercept = verifyEd25519SignatureCpuArgumentsIntercept;
        this.verifyEd25519SignatureCpuArgumentsSlope = verifyEd25519SignatureCpuArgumentsSlope;
        this.verifyEd25519SignatureMemoryArguments = verifyEd25519SignatureMemoryArguments;
    }

    public Integer getAddIntegerCpuArgumentsIntercept() {
        return addIntegerCpuArgumentsIntercept;
    }

    public void setAddIntegerCpuArgumentsIntercept(Integer addIntegerCpuArgumentsIntercept) {
        this.addIntegerCpuArgumentsIntercept = addIntegerCpuArgumentsIntercept;
    }

    public PlutusV1 withAddIntegerCpuArgumentsIntercept(Integer addIntegerCpuArgumentsIntercept) {
        this.addIntegerCpuArgumentsIntercept = addIntegerCpuArgumentsIntercept;
        return this;
    }

    public Integer getAddIntegerCpuArgumentsSlope() {
        return addIntegerCpuArgumentsSlope;
    }

    public void setAddIntegerCpuArgumentsSlope(Integer addIntegerCpuArgumentsSlope) {
        this.addIntegerCpuArgumentsSlope = addIntegerCpuArgumentsSlope;
    }

    public PlutusV1 withAddIntegerCpuArgumentsSlope(Integer addIntegerCpuArgumentsSlope) {
        this.addIntegerCpuArgumentsSlope = addIntegerCpuArgumentsSlope;
        return this;
    }

    public Integer getAddIntegerMemoryArgumentsIntercept() {
        return addIntegerMemoryArgumentsIntercept;
    }

    public void setAddIntegerMemoryArgumentsIntercept(Integer addIntegerMemoryArgumentsIntercept) {
        this.addIntegerMemoryArgumentsIntercept = addIntegerMemoryArgumentsIntercept;
    }

    public PlutusV1 withAddIntegerMemoryArgumentsIntercept(Integer addIntegerMemoryArgumentsIntercept) {
        this.addIntegerMemoryArgumentsIntercept = addIntegerMemoryArgumentsIntercept;
        return this;
    }

    public Integer getAddIntegerMemoryArgumentsSlope() {
        return addIntegerMemoryArgumentsSlope;
    }

    public void setAddIntegerMemoryArgumentsSlope(Integer addIntegerMemoryArgumentsSlope) {
        this.addIntegerMemoryArgumentsSlope = addIntegerMemoryArgumentsSlope;
    }

    public PlutusV1 withAddIntegerMemoryArgumentsSlope(Integer addIntegerMemoryArgumentsSlope) {
        this.addIntegerMemoryArgumentsSlope = addIntegerMemoryArgumentsSlope;
        return this;
    }

    public Integer getAppendByteStringCpuArgumentsIntercept() {
        return appendByteStringCpuArgumentsIntercept;
    }

    public void setAppendByteStringCpuArgumentsIntercept(Integer appendByteStringCpuArgumentsIntercept) {
        this.appendByteStringCpuArgumentsIntercept = appendByteStringCpuArgumentsIntercept;
    }

    public PlutusV1 withAppendByteStringCpuArgumentsIntercept(Integer appendByteStringCpuArgumentsIntercept) {
        this.appendByteStringCpuArgumentsIntercept = appendByteStringCpuArgumentsIntercept;
        return this;
    }

    public Integer getAppendByteStringCpuArgumentsSlope() {
        return appendByteStringCpuArgumentsSlope;
    }

    public void setAppendByteStringCpuArgumentsSlope(Integer appendByteStringCpuArgumentsSlope) {
        this.appendByteStringCpuArgumentsSlope = appendByteStringCpuArgumentsSlope;
    }

    public PlutusV1 withAppendByteStringCpuArgumentsSlope(Integer appendByteStringCpuArgumentsSlope) {
        this.appendByteStringCpuArgumentsSlope = appendByteStringCpuArgumentsSlope;
        return this;
    }

    public Integer getAppendByteStringMemoryArgumentsIntercept() {
        return appendByteStringMemoryArgumentsIntercept;
    }

    public void setAppendByteStringMemoryArgumentsIntercept(Integer appendByteStringMemoryArgumentsIntercept) {
        this.appendByteStringMemoryArgumentsIntercept = appendByteStringMemoryArgumentsIntercept;
    }

    public PlutusV1 withAppendByteStringMemoryArgumentsIntercept(Integer appendByteStringMemoryArgumentsIntercept) {
        this.appendByteStringMemoryArgumentsIntercept = appendByteStringMemoryArgumentsIntercept;
        return this;
    }

    public Integer getAppendByteStringMemoryArgumentsSlope() {
        return appendByteStringMemoryArgumentsSlope;
    }

    public void setAppendByteStringMemoryArgumentsSlope(Integer appendByteStringMemoryArgumentsSlope) {
        this.appendByteStringMemoryArgumentsSlope = appendByteStringMemoryArgumentsSlope;
    }

    public PlutusV1 withAppendByteStringMemoryArgumentsSlope(Integer appendByteStringMemoryArgumentsSlope) {
        this.appendByteStringMemoryArgumentsSlope = appendByteStringMemoryArgumentsSlope;
        return this;
    }

    public Integer getAppendStringCpuArgumentsIntercept() {
        return appendStringCpuArgumentsIntercept;
    }

    public void setAppendStringCpuArgumentsIntercept(Integer appendStringCpuArgumentsIntercept) {
        this.appendStringCpuArgumentsIntercept = appendStringCpuArgumentsIntercept;
    }

    public PlutusV1 withAppendStringCpuArgumentsIntercept(Integer appendStringCpuArgumentsIntercept) {
        this.appendStringCpuArgumentsIntercept = appendStringCpuArgumentsIntercept;
        return this;
    }

    public Integer getAppendStringCpuArgumentsSlope() {
        return appendStringCpuArgumentsSlope;
    }

    public void setAppendStringCpuArgumentsSlope(Integer appendStringCpuArgumentsSlope) {
        this.appendStringCpuArgumentsSlope = appendStringCpuArgumentsSlope;
    }

    public PlutusV1 withAppendStringCpuArgumentsSlope(Integer appendStringCpuArgumentsSlope) {
        this.appendStringCpuArgumentsSlope = appendStringCpuArgumentsSlope;
        return this;
    }

    public Integer getAppendStringMemoryArgumentsIntercept() {
        return appendStringMemoryArgumentsIntercept;
    }

    public void setAppendStringMemoryArgumentsIntercept(Integer appendStringMemoryArgumentsIntercept) {
        this.appendStringMemoryArgumentsIntercept = appendStringMemoryArgumentsIntercept;
    }

    public PlutusV1 withAppendStringMemoryArgumentsIntercept(Integer appendStringMemoryArgumentsIntercept) {
        this.appendStringMemoryArgumentsIntercept = appendStringMemoryArgumentsIntercept;
        return this;
    }

    public Integer getAppendStringMemoryArgumentsSlope() {
        return appendStringMemoryArgumentsSlope;
    }

    public void setAppendStringMemoryArgumentsSlope(Integer appendStringMemoryArgumentsSlope) {
        this.appendStringMemoryArgumentsSlope = appendStringMemoryArgumentsSlope;
    }

    public PlutusV1 withAppendStringMemoryArgumentsSlope(Integer appendStringMemoryArgumentsSlope) {
        this.appendStringMemoryArgumentsSlope = appendStringMemoryArgumentsSlope;
        return this;
    }

    public Integer getbDataCpuArguments() {
        return bDataCpuArguments;
    }

    public void setbDataCpuArguments(Integer bDataCpuArguments) {
        this.bDataCpuArguments = bDataCpuArguments;
    }

    public PlutusV1 withbDataCpuArguments(Integer bDataCpuArguments) {
        this.bDataCpuArguments = bDataCpuArguments;
        return this;
    }

    public Integer getbDataMemoryArguments() {
        return bDataMemoryArguments;
    }

    public void setbDataMemoryArguments(Integer bDataMemoryArguments) {
        this.bDataMemoryArguments = bDataMemoryArguments;
    }

    public PlutusV1 withbDataMemoryArguments(Integer bDataMemoryArguments) {
        this.bDataMemoryArguments = bDataMemoryArguments;
        return this;
    }

    public Integer getBlake2b256CpuArgumentsIntercept() {
        return blake2b256CpuArgumentsIntercept;
    }

    public void setBlake2b256CpuArgumentsIntercept(Integer blake2b256CpuArgumentsIntercept) {
        this.blake2b256CpuArgumentsIntercept = blake2b256CpuArgumentsIntercept;
    }

    public PlutusV1 withBlake2b256CpuArgumentsIntercept(Integer blake2b256CpuArgumentsIntercept) {
        this.blake2b256CpuArgumentsIntercept = blake2b256CpuArgumentsIntercept;
        return this;
    }

    public Integer getBlake2b256CpuArgumentsSlope() {
        return blake2b256CpuArgumentsSlope;
    }

    public void setBlake2b256CpuArgumentsSlope(Integer blake2b256CpuArgumentsSlope) {
        this.blake2b256CpuArgumentsSlope = blake2b256CpuArgumentsSlope;
    }

    public PlutusV1 withBlake2b256CpuArgumentsSlope(Integer blake2b256CpuArgumentsSlope) {
        this.blake2b256CpuArgumentsSlope = blake2b256CpuArgumentsSlope;
        return this;
    }

    public Integer getBlake2b256MemoryArguments() {
        return blake2b256MemoryArguments;
    }

    public void setBlake2b256MemoryArguments(Integer blake2b256MemoryArguments) {
        this.blake2b256MemoryArguments = blake2b256MemoryArguments;
    }

    public PlutusV1 withBlake2b256MemoryArguments(Integer blake2b256MemoryArguments) {
        this.blake2b256MemoryArguments = blake2b256MemoryArguments;
        return this;
    }

    public Integer getCekApplyCostExBudgetCPU() {
        return cekApplyCostExBudgetCPU;
    }

    public void setCekApplyCostExBudgetCPU(Integer cekApplyCostExBudgetCPU) {
        this.cekApplyCostExBudgetCPU = cekApplyCostExBudgetCPU;
    }

    public PlutusV1 withCekApplyCostExBudgetCPU(Integer cekApplyCostExBudgetCPU) {
        this.cekApplyCostExBudgetCPU = cekApplyCostExBudgetCPU;
        return this;
    }

    public Integer getCekApplyCostExBudgetMemory() {
        return cekApplyCostExBudgetMemory;
    }

    public void setCekApplyCostExBudgetMemory(Integer cekApplyCostExBudgetMemory) {
        this.cekApplyCostExBudgetMemory = cekApplyCostExBudgetMemory;
    }

    public PlutusV1 withCekApplyCostExBudgetMemory(Integer cekApplyCostExBudgetMemory) {
        this.cekApplyCostExBudgetMemory = cekApplyCostExBudgetMemory;
        return this;
    }

    public Integer getCekBuiltinCostExBudgetCPU() {
        return cekBuiltinCostExBudgetCPU;
    }

    public void setCekBuiltinCostExBudgetCPU(Integer cekBuiltinCostExBudgetCPU) {
        this.cekBuiltinCostExBudgetCPU = cekBuiltinCostExBudgetCPU;
    }

    public PlutusV1 withCekBuiltinCostExBudgetCPU(Integer cekBuiltinCostExBudgetCPU) {
        this.cekBuiltinCostExBudgetCPU = cekBuiltinCostExBudgetCPU;
        return this;
    }

    public Integer getCekBuiltinCostExBudgetMemory() {
        return cekBuiltinCostExBudgetMemory;
    }

    public void setCekBuiltinCostExBudgetMemory(Integer cekBuiltinCostExBudgetMemory) {
        this.cekBuiltinCostExBudgetMemory = cekBuiltinCostExBudgetMemory;
    }

    public PlutusV1 withCekBuiltinCostExBudgetMemory(Integer cekBuiltinCostExBudgetMemory) {
        this.cekBuiltinCostExBudgetMemory = cekBuiltinCostExBudgetMemory;
        return this;
    }

    public Integer getCekConstCostExBudgetCPU() {
        return cekConstCostExBudgetCPU;
    }

    public void setCekConstCostExBudgetCPU(Integer cekConstCostExBudgetCPU) {
        this.cekConstCostExBudgetCPU = cekConstCostExBudgetCPU;
    }

    public PlutusV1 withCekConstCostExBudgetCPU(Integer cekConstCostExBudgetCPU) {
        this.cekConstCostExBudgetCPU = cekConstCostExBudgetCPU;
        return this;
    }

    public Integer getCekConstCostExBudgetMemory() {
        return cekConstCostExBudgetMemory;
    }

    public void setCekConstCostExBudgetMemory(Integer cekConstCostExBudgetMemory) {
        this.cekConstCostExBudgetMemory = cekConstCostExBudgetMemory;
    }

    public PlutusV1 withCekConstCostExBudgetMemory(Integer cekConstCostExBudgetMemory) {
        this.cekConstCostExBudgetMemory = cekConstCostExBudgetMemory;
        return this;
    }

    public Integer getCekDelayCostExBudgetCPU() {
        return cekDelayCostExBudgetCPU;
    }

    public void setCekDelayCostExBudgetCPU(Integer cekDelayCostExBudgetCPU) {
        this.cekDelayCostExBudgetCPU = cekDelayCostExBudgetCPU;
    }

    public PlutusV1 withCekDelayCostExBudgetCPU(Integer cekDelayCostExBudgetCPU) {
        this.cekDelayCostExBudgetCPU = cekDelayCostExBudgetCPU;
        return this;
    }

    public Integer getCekDelayCostExBudgetMemory() {
        return cekDelayCostExBudgetMemory;
    }

    public void setCekDelayCostExBudgetMemory(Integer cekDelayCostExBudgetMemory) {
        this.cekDelayCostExBudgetMemory = cekDelayCostExBudgetMemory;
    }

    public PlutusV1 withCekDelayCostExBudgetMemory(Integer cekDelayCostExBudgetMemory) {
        this.cekDelayCostExBudgetMemory = cekDelayCostExBudgetMemory;
        return this;
    }

    public Integer getCekForceCostExBudgetCPU() {
        return cekForceCostExBudgetCPU;
    }

    public void setCekForceCostExBudgetCPU(Integer cekForceCostExBudgetCPU) {
        this.cekForceCostExBudgetCPU = cekForceCostExBudgetCPU;
    }

    public PlutusV1 withCekForceCostExBudgetCPU(Integer cekForceCostExBudgetCPU) {
        this.cekForceCostExBudgetCPU = cekForceCostExBudgetCPU;
        return this;
    }

    public Integer getCekForceCostExBudgetMemory() {
        return cekForceCostExBudgetMemory;
    }

    public void setCekForceCostExBudgetMemory(Integer cekForceCostExBudgetMemory) {
        this.cekForceCostExBudgetMemory = cekForceCostExBudgetMemory;
    }

    public PlutusV1 withCekForceCostExBudgetMemory(Integer cekForceCostExBudgetMemory) {
        this.cekForceCostExBudgetMemory = cekForceCostExBudgetMemory;
        return this;
    }

    public Integer getCekLamCostExBudgetCPU() {
        return cekLamCostExBudgetCPU;
    }

    public void setCekLamCostExBudgetCPU(Integer cekLamCostExBudgetCPU) {
        this.cekLamCostExBudgetCPU = cekLamCostExBudgetCPU;
    }

    public PlutusV1 withCekLamCostExBudgetCPU(Integer cekLamCostExBudgetCPU) {
        this.cekLamCostExBudgetCPU = cekLamCostExBudgetCPU;
        return this;
    }

    public Integer getCekLamCostExBudgetMemory() {
        return cekLamCostExBudgetMemory;
    }

    public void setCekLamCostExBudgetMemory(Integer cekLamCostExBudgetMemory) {
        this.cekLamCostExBudgetMemory = cekLamCostExBudgetMemory;
    }

    public PlutusV1 withCekLamCostExBudgetMemory(Integer cekLamCostExBudgetMemory) {
        this.cekLamCostExBudgetMemory = cekLamCostExBudgetMemory;
        return this;
    }

    public Integer getCekStartupCostExBudgetCPU() {
        return cekStartupCostExBudgetCPU;
    }

    public void setCekStartupCostExBudgetCPU(Integer cekStartupCostExBudgetCPU) {
        this.cekStartupCostExBudgetCPU = cekStartupCostExBudgetCPU;
    }

    public PlutusV1 withCekStartupCostExBudgetCPU(Integer cekStartupCostExBudgetCPU) {
        this.cekStartupCostExBudgetCPU = cekStartupCostExBudgetCPU;
        return this;
    }

    public Integer getCekStartupCostExBudgetMemory() {
        return cekStartupCostExBudgetMemory;
    }

    public void setCekStartupCostExBudgetMemory(Integer cekStartupCostExBudgetMemory) {
        this.cekStartupCostExBudgetMemory = cekStartupCostExBudgetMemory;
    }

    public PlutusV1 withCekStartupCostExBudgetMemory(Integer cekStartupCostExBudgetMemory) {
        this.cekStartupCostExBudgetMemory = cekStartupCostExBudgetMemory;
        return this;
    }

    public Integer getCekVarCostExBudgetCPU() {
        return cekVarCostExBudgetCPU;
    }

    public void setCekVarCostExBudgetCPU(Integer cekVarCostExBudgetCPU) {
        this.cekVarCostExBudgetCPU = cekVarCostExBudgetCPU;
    }

    public PlutusV1 withCekVarCostExBudgetCPU(Integer cekVarCostExBudgetCPU) {
        this.cekVarCostExBudgetCPU = cekVarCostExBudgetCPU;
        return this;
    }

    public Integer getCekVarCostExBudgetMemory() {
        return cekVarCostExBudgetMemory;
    }

    public void setCekVarCostExBudgetMemory(Integer cekVarCostExBudgetMemory) {
        this.cekVarCostExBudgetMemory = cekVarCostExBudgetMemory;
    }

    public PlutusV1 withCekVarCostExBudgetMemory(Integer cekVarCostExBudgetMemory) {
        this.cekVarCostExBudgetMemory = cekVarCostExBudgetMemory;
        return this;
    }

    public Integer getChooseDataCpuArguments() {
        return chooseDataCpuArguments;
    }

    public void setChooseDataCpuArguments(Integer chooseDataCpuArguments) {
        this.chooseDataCpuArguments = chooseDataCpuArguments;
    }

    public PlutusV1 withChooseDataCpuArguments(Integer chooseDataCpuArguments) {
        this.chooseDataCpuArguments = chooseDataCpuArguments;
        return this;
    }

    public Integer getChooseDataMemoryArguments() {
        return chooseDataMemoryArguments;
    }

    public void setChooseDataMemoryArguments(Integer chooseDataMemoryArguments) {
        this.chooseDataMemoryArguments = chooseDataMemoryArguments;
    }

    public PlutusV1 withChooseDataMemoryArguments(Integer chooseDataMemoryArguments) {
        this.chooseDataMemoryArguments = chooseDataMemoryArguments;
        return this;
    }

    public Integer getChooseListCpuArguments() {
        return chooseListCpuArguments;
    }

    public void setChooseListCpuArguments(Integer chooseListCpuArguments) {
        this.chooseListCpuArguments = chooseListCpuArguments;
    }

    public PlutusV1 withChooseListCpuArguments(Integer chooseListCpuArguments) {
        this.chooseListCpuArguments = chooseListCpuArguments;
        return this;
    }

    public Integer getChooseListMemoryArguments() {
        return chooseListMemoryArguments;
    }

    public void setChooseListMemoryArguments(Integer chooseListMemoryArguments) {
        this.chooseListMemoryArguments = chooseListMemoryArguments;
    }

    public PlutusV1 withChooseListMemoryArguments(Integer chooseListMemoryArguments) {
        this.chooseListMemoryArguments = chooseListMemoryArguments;
        return this;
    }

    public Integer getChooseUnitCpuArguments() {
        return chooseUnitCpuArguments;
    }

    public void setChooseUnitCpuArguments(Integer chooseUnitCpuArguments) {
        this.chooseUnitCpuArguments = chooseUnitCpuArguments;
    }

    public PlutusV1 withChooseUnitCpuArguments(Integer chooseUnitCpuArguments) {
        this.chooseUnitCpuArguments = chooseUnitCpuArguments;
        return this;
    }

    public Integer getChooseUnitMemoryArguments() {
        return chooseUnitMemoryArguments;
    }

    public void setChooseUnitMemoryArguments(Integer chooseUnitMemoryArguments) {
        this.chooseUnitMemoryArguments = chooseUnitMemoryArguments;
    }

    public PlutusV1 withChooseUnitMemoryArguments(Integer chooseUnitMemoryArguments) {
        this.chooseUnitMemoryArguments = chooseUnitMemoryArguments;
        return this;
    }

    public Integer getConsByteStringCpuArgumentsIntercept() {
        return consByteStringCpuArgumentsIntercept;
    }

    public void setConsByteStringCpuArgumentsIntercept(Integer consByteStringCpuArgumentsIntercept) {
        this.consByteStringCpuArgumentsIntercept = consByteStringCpuArgumentsIntercept;
    }

    public PlutusV1 withConsByteStringCpuArgumentsIntercept(Integer consByteStringCpuArgumentsIntercept) {
        this.consByteStringCpuArgumentsIntercept = consByteStringCpuArgumentsIntercept;
        return this;
    }

    public Integer getConsByteStringCpuArgumentsSlope() {
        return consByteStringCpuArgumentsSlope;
    }

    public void setConsByteStringCpuArgumentsSlope(Integer consByteStringCpuArgumentsSlope) {
        this.consByteStringCpuArgumentsSlope = consByteStringCpuArgumentsSlope;
    }

    public PlutusV1 withConsByteStringCpuArgumentsSlope(Integer consByteStringCpuArgumentsSlope) {
        this.consByteStringCpuArgumentsSlope = consByteStringCpuArgumentsSlope;
        return this;
    }

    public Integer getConsByteStringMemoryArgumentsIntercept() {
        return consByteStringMemoryArgumentsIntercept;
    }

    public void setConsByteStringMemoryArgumentsIntercept(Integer consByteStringMemoryArgumentsIntercept) {
        this.consByteStringMemoryArgumentsIntercept = consByteStringMemoryArgumentsIntercept;
    }

    public PlutusV1 withConsByteStringMemoryArgumentsIntercept(Integer consByteStringMemoryArgumentsIntercept) {
        this.consByteStringMemoryArgumentsIntercept = consByteStringMemoryArgumentsIntercept;
        return this;
    }

    public Integer getConsByteStringMemoryArgumentsSlope() {
        return consByteStringMemoryArgumentsSlope;
    }

    public void setConsByteStringMemoryArgumentsSlope(Integer consByteStringMemoryArgumentsSlope) {
        this.consByteStringMemoryArgumentsSlope = consByteStringMemoryArgumentsSlope;
    }

    public PlutusV1 withConsByteStringMemoryArgumentsSlope(Integer consByteStringMemoryArgumentsSlope) {
        this.consByteStringMemoryArgumentsSlope = consByteStringMemoryArgumentsSlope;
        return this;
    }

    public Integer getConstrDataCpuArguments() {
        return constrDataCpuArguments;
    }

    public void setConstrDataCpuArguments(Integer constrDataCpuArguments) {
        this.constrDataCpuArguments = constrDataCpuArguments;
    }

    public PlutusV1 withConstrDataCpuArguments(Integer constrDataCpuArguments) {
        this.constrDataCpuArguments = constrDataCpuArguments;
        return this;
    }

    public Integer getConstrDataMemoryArguments() {
        return constrDataMemoryArguments;
    }

    public void setConstrDataMemoryArguments(Integer constrDataMemoryArguments) {
        this.constrDataMemoryArguments = constrDataMemoryArguments;
    }

    public PlutusV1 withConstrDataMemoryArguments(Integer constrDataMemoryArguments) {
        this.constrDataMemoryArguments = constrDataMemoryArguments;
        return this;
    }

    public Integer getDecodeUtf8CpuArgumentsIntercept() {
        return decodeUtf8CpuArgumentsIntercept;
    }

    public void setDecodeUtf8CpuArgumentsIntercept(Integer decodeUtf8CpuArgumentsIntercept) {
        this.decodeUtf8CpuArgumentsIntercept = decodeUtf8CpuArgumentsIntercept;
    }

    public PlutusV1 withDecodeUtf8CpuArgumentsIntercept(Integer decodeUtf8CpuArgumentsIntercept) {
        this.decodeUtf8CpuArgumentsIntercept = decodeUtf8CpuArgumentsIntercept;
        return this;
    }

    public Integer getDecodeUtf8CpuArgumentsSlope() {
        return decodeUtf8CpuArgumentsSlope;
    }

    public void setDecodeUtf8CpuArgumentsSlope(Integer decodeUtf8CpuArgumentsSlope) {
        this.decodeUtf8CpuArgumentsSlope = decodeUtf8CpuArgumentsSlope;
    }

    public PlutusV1 withDecodeUtf8CpuArgumentsSlope(Integer decodeUtf8CpuArgumentsSlope) {
        this.decodeUtf8CpuArgumentsSlope = decodeUtf8CpuArgumentsSlope;
        return this;
    }

    public Integer getDecodeUtf8MemoryArgumentsIntercept() {
        return decodeUtf8MemoryArgumentsIntercept;
    }

    public void setDecodeUtf8MemoryArgumentsIntercept(Integer decodeUtf8MemoryArgumentsIntercept) {
        this.decodeUtf8MemoryArgumentsIntercept = decodeUtf8MemoryArgumentsIntercept;
    }

    public PlutusV1 withDecodeUtf8MemoryArgumentsIntercept(Integer decodeUtf8MemoryArgumentsIntercept) {
        this.decodeUtf8MemoryArgumentsIntercept = decodeUtf8MemoryArgumentsIntercept;
        return this;
    }

    public Integer getDecodeUtf8MemoryArgumentsSlope() {
        return decodeUtf8MemoryArgumentsSlope;
    }

    public void setDecodeUtf8MemoryArgumentsSlope(Integer decodeUtf8MemoryArgumentsSlope) {
        this.decodeUtf8MemoryArgumentsSlope = decodeUtf8MemoryArgumentsSlope;
    }

    public PlutusV1 withDecodeUtf8MemoryArgumentsSlope(Integer decodeUtf8MemoryArgumentsSlope) {
        this.decodeUtf8MemoryArgumentsSlope = decodeUtf8MemoryArgumentsSlope;
        return this;
    }

    public Integer getDivideIntegerCpuArgumentsConstant() {
        return divideIntegerCpuArgumentsConstant;
    }

    public void setDivideIntegerCpuArgumentsConstant(Integer divideIntegerCpuArgumentsConstant) {
        this.divideIntegerCpuArgumentsConstant = divideIntegerCpuArgumentsConstant;
    }

    public PlutusV1 withDivideIntegerCpuArgumentsConstant(Integer divideIntegerCpuArgumentsConstant) {
        this.divideIntegerCpuArgumentsConstant = divideIntegerCpuArgumentsConstant;
        return this;
    }

    public Integer getDivideIntegerCpuArgumentsModelArgumentsIntercept() {
        return divideIntegerCpuArgumentsModelArgumentsIntercept;
    }

    public void setDivideIntegerCpuArgumentsModelArgumentsIntercept(Integer divideIntegerCpuArgumentsModelArgumentsIntercept) {
        this.divideIntegerCpuArgumentsModelArgumentsIntercept = divideIntegerCpuArgumentsModelArgumentsIntercept;
    }

    public PlutusV1 withDivideIntegerCpuArgumentsModelArgumentsIntercept(Integer divideIntegerCpuArgumentsModelArgumentsIntercept) {
        this.divideIntegerCpuArgumentsModelArgumentsIntercept = divideIntegerCpuArgumentsModelArgumentsIntercept;
        return this;
    }

    public Integer getDivideIntegerCpuArgumentsModelArgumentsSlope() {
        return divideIntegerCpuArgumentsModelArgumentsSlope;
    }

    public void setDivideIntegerCpuArgumentsModelArgumentsSlope(Integer divideIntegerCpuArgumentsModelArgumentsSlope) {
        this.divideIntegerCpuArgumentsModelArgumentsSlope = divideIntegerCpuArgumentsModelArgumentsSlope;
    }

    public PlutusV1 withDivideIntegerCpuArgumentsModelArgumentsSlope(Integer divideIntegerCpuArgumentsModelArgumentsSlope) {
        this.divideIntegerCpuArgumentsModelArgumentsSlope = divideIntegerCpuArgumentsModelArgumentsSlope;
        return this;
    }

    public Integer getDivideIntegerMemoryArgumentsIntercept() {
        return divideIntegerMemoryArgumentsIntercept;
    }

    public void setDivideIntegerMemoryArgumentsIntercept(Integer divideIntegerMemoryArgumentsIntercept) {
        this.divideIntegerMemoryArgumentsIntercept = divideIntegerMemoryArgumentsIntercept;
    }

    public PlutusV1 withDivideIntegerMemoryArgumentsIntercept(Integer divideIntegerMemoryArgumentsIntercept) {
        this.divideIntegerMemoryArgumentsIntercept = divideIntegerMemoryArgumentsIntercept;
        return this;
    }

    public Integer getDivideIntegerMemoryArgumentsMinimum() {
        return divideIntegerMemoryArgumentsMinimum;
    }

    public void setDivideIntegerMemoryArgumentsMinimum(Integer divideIntegerMemoryArgumentsMinimum) {
        this.divideIntegerMemoryArgumentsMinimum = divideIntegerMemoryArgumentsMinimum;
    }

    public PlutusV1 withDivideIntegerMemoryArgumentsMinimum(Integer divideIntegerMemoryArgumentsMinimum) {
        this.divideIntegerMemoryArgumentsMinimum = divideIntegerMemoryArgumentsMinimum;
        return this;
    }

    public Integer getDivideIntegerMemoryArgumentsSlope() {
        return divideIntegerMemoryArgumentsSlope;
    }

    public void setDivideIntegerMemoryArgumentsSlope(Integer divideIntegerMemoryArgumentsSlope) {
        this.divideIntegerMemoryArgumentsSlope = divideIntegerMemoryArgumentsSlope;
    }

    public PlutusV1 withDivideIntegerMemoryArgumentsSlope(Integer divideIntegerMemoryArgumentsSlope) {
        this.divideIntegerMemoryArgumentsSlope = divideIntegerMemoryArgumentsSlope;
        return this;
    }

    public Integer getEncodeUtf8CpuArgumentsIntercept() {
        return encodeUtf8CpuArgumentsIntercept;
    }

    public void setEncodeUtf8CpuArgumentsIntercept(Integer encodeUtf8CpuArgumentsIntercept) {
        this.encodeUtf8CpuArgumentsIntercept = encodeUtf8CpuArgumentsIntercept;
    }

    public PlutusV1 withEncodeUtf8CpuArgumentsIntercept(Integer encodeUtf8CpuArgumentsIntercept) {
        this.encodeUtf8CpuArgumentsIntercept = encodeUtf8CpuArgumentsIntercept;
        return this;
    }

    public Integer getEncodeUtf8CpuArgumentsSlope() {
        return encodeUtf8CpuArgumentsSlope;
    }

    public void setEncodeUtf8CpuArgumentsSlope(Integer encodeUtf8CpuArgumentsSlope) {
        this.encodeUtf8CpuArgumentsSlope = encodeUtf8CpuArgumentsSlope;
    }

    public PlutusV1 withEncodeUtf8CpuArgumentsSlope(Integer encodeUtf8CpuArgumentsSlope) {
        this.encodeUtf8CpuArgumentsSlope = encodeUtf8CpuArgumentsSlope;
        return this;
    }

    public Integer getEncodeUtf8MemoryArgumentsIntercept() {
        return encodeUtf8MemoryArgumentsIntercept;
    }

    public void setEncodeUtf8MemoryArgumentsIntercept(Integer encodeUtf8MemoryArgumentsIntercept) {
        this.encodeUtf8MemoryArgumentsIntercept = encodeUtf8MemoryArgumentsIntercept;
    }

    public PlutusV1 withEncodeUtf8MemoryArgumentsIntercept(Integer encodeUtf8MemoryArgumentsIntercept) {
        this.encodeUtf8MemoryArgumentsIntercept = encodeUtf8MemoryArgumentsIntercept;
        return this;
    }

    public Integer getEncodeUtf8MemoryArgumentsSlope() {
        return encodeUtf8MemoryArgumentsSlope;
    }

    public void setEncodeUtf8MemoryArgumentsSlope(Integer encodeUtf8MemoryArgumentsSlope) {
        this.encodeUtf8MemoryArgumentsSlope = encodeUtf8MemoryArgumentsSlope;
    }

    public PlutusV1 withEncodeUtf8MemoryArgumentsSlope(Integer encodeUtf8MemoryArgumentsSlope) {
        this.encodeUtf8MemoryArgumentsSlope = encodeUtf8MemoryArgumentsSlope;
        return this;
    }

    public Integer getEqualsByteStringCpuArgumentsConstant() {
        return equalsByteStringCpuArgumentsConstant;
    }

    public void setEqualsByteStringCpuArgumentsConstant(Integer equalsByteStringCpuArgumentsConstant) {
        this.equalsByteStringCpuArgumentsConstant = equalsByteStringCpuArgumentsConstant;
    }

    public PlutusV1 withEqualsByteStringCpuArgumentsConstant(Integer equalsByteStringCpuArgumentsConstant) {
        this.equalsByteStringCpuArgumentsConstant = equalsByteStringCpuArgumentsConstant;
        return this;
    }

    public Integer getEqualsByteStringCpuArgumentsIntercept() {
        return equalsByteStringCpuArgumentsIntercept;
    }

    public void setEqualsByteStringCpuArgumentsIntercept(Integer equalsByteStringCpuArgumentsIntercept) {
        this.equalsByteStringCpuArgumentsIntercept = equalsByteStringCpuArgumentsIntercept;
    }

    public PlutusV1 withEqualsByteStringCpuArgumentsIntercept(Integer equalsByteStringCpuArgumentsIntercept) {
        this.equalsByteStringCpuArgumentsIntercept = equalsByteStringCpuArgumentsIntercept;
        return this;
    }

    public Integer getEqualsByteStringCpuArgumentsSlope() {
        return equalsByteStringCpuArgumentsSlope;
    }

    public void setEqualsByteStringCpuArgumentsSlope(Integer equalsByteStringCpuArgumentsSlope) {
        this.equalsByteStringCpuArgumentsSlope = equalsByteStringCpuArgumentsSlope;
    }

    public PlutusV1 withEqualsByteStringCpuArgumentsSlope(Integer equalsByteStringCpuArgumentsSlope) {
        this.equalsByteStringCpuArgumentsSlope = equalsByteStringCpuArgumentsSlope;
        return this;
    }

    public Integer getEqualsByteStringMemoryArguments() {
        return equalsByteStringMemoryArguments;
    }

    public void setEqualsByteStringMemoryArguments(Integer equalsByteStringMemoryArguments) {
        this.equalsByteStringMemoryArguments = equalsByteStringMemoryArguments;
    }

    public PlutusV1 withEqualsByteStringMemoryArguments(Integer equalsByteStringMemoryArguments) {
        this.equalsByteStringMemoryArguments = equalsByteStringMemoryArguments;
        return this;
    }

    public Integer getEqualsDataCpuArgumentsIntercept() {
        return equalsDataCpuArgumentsIntercept;
    }

    public void setEqualsDataCpuArgumentsIntercept(Integer equalsDataCpuArgumentsIntercept) {
        this.equalsDataCpuArgumentsIntercept = equalsDataCpuArgumentsIntercept;
    }

    public PlutusV1 withEqualsDataCpuArgumentsIntercept(Integer equalsDataCpuArgumentsIntercept) {
        this.equalsDataCpuArgumentsIntercept = equalsDataCpuArgumentsIntercept;
        return this;
    }

    public Integer getEqualsDataCpuArgumentsSlope() {
        return equalsDataCpuArgumentsSlope;
    }

    public void setEqualsDataCpuArgumentsSlope(Integer equalsDataCpuArgumentsSlope) {
        this.equalsDataCpuArgumentsSlope = equalsDataCpuArgumentsSlope;
    }

    public PlutusV1 withEqualsDataCpuArgumentsSlope(Integer equalsDataCpuArgumentsSlope) {
        this.equalsDataCpuArgumentsSlope = equalsDataCpuArgumentsSlope;
        return this;
    }

    public Integer getEqualsDataMemoryArguments() {
        return equalsDataMemoryArguments;
    }

    public void setEqualsDataMemoryArguments(Integer equalsDataMemoryArguments) {
        this.equalsDataMemoryArguments = equalsDataMemoryArguments;
    }

    public PlutusV1 withEqualsDataMemoryArguments(Integer equalsDataMemoryArguments) {
        this.equalsDataMemoryArguments = equalsDataMemoryArguments;
        return this;
    }

    public Integer getEqualsIntegerCpuArgumentsIntercept() {
        return equalsIntegerCpuArgumentsIntercept;
    }

    public void setEqualsIntegerCpuArgumentsIntercept(Integer equalsIntegerCpuArgumentsIntercept) {
        this.equalsIntegerCpuArgumentsIntercept = equalsIntegerCpuArgumentsIntercept;
    }

    public PlutusV1 withEqualsIntegerCpuArgumentsIntercept(Integer equalsIntegerCpuArgumentsIntercept) {
        this.equalsIntegerCpuArgumentsIntercept = equalsIntegerCpuArgumentsIntercept;
        return this;
    }

    public Integer getEqualsIntegerCpuArgumentsSlope() {
        return equalsIntegerCpuArgumentsSlope;
    }

    public void setEqualsIntegerCpuArgumentsSlope(Integer equalsIntegerCpuArgumentsSlope) {
        this.equalsIntegerCpuArgumentsSlope = equalsIntegerCpuArgumentsSlope;
    }

    public PlutusV1 withEqualsIntegerCpuArgumentsSlope(Integer equalsIntegerCpuArgumentsSlope) {
        this.equalsIntegerCpuArgumentsSlope = equalsIntegerCpuArgumentsSlope;
        return this;
    }

    public Integer getEqualsIntegerMemoryArguments() {
        return equalsIntegerMemoryArguments;
    }

    public void setEqualsIntegerMemoryArguments(Integer equalsIntegerMemoryArguments) {
        this.equalsIntegerMemoryArguments = equalsIntegerMemoryArguments;
    }

    public PlutusV1 withEqualsIntegerMemoryArguments(Integer equalsIntegerMemoryArguments) {
        this.equalsIntegerMemoryArguments = equalsIntegerMemoryArguments;
        return this;
    }

    public Integer getEqualsStringCpuArgumentsConstant() {
        return equalsStringCpuArgumentsConstant;
    }

    public void setEqualsStringCpuArgumentsConstant(Integer equalsStringCpuArgumentsConstant) {
        this.equalsStringCpuArgumentsConstant = equalsStringCpuArgumentsConstant;
    }

    public PlutusV1 withEqualsStringCpuArgumentsConstant(Integer equalsStringCpuArgumentsConstant) {
        this.equalsStringCpuArgumentsConstant = equalsStringCpuArgumentsConstant;
        return this;
    }

    public Integer getEqualsStringCpuArgumentsIntercept() {
        return equalsStringCpuArgumentsIntercept;
    }

    public void setEqualsStringCpuArgumentsIntercept(Integer equalsStringCpuArgumentsIntercept) {
        this.equalsStringCpuArgumentsIntercept = equalsStringCpuArgumentsIntercept;
    }

    public PlutusV1 withEqualsStringCpuArgumentsIntercept(Integer equalsStringCpuArgumentsIntercept) {
        this.equalsStringCpuArgumentsIntercept = equalsStringCpuArgumentsIntercept;
        return this;
    }

    public Integer getEqualsStringCpuArgumentsSlope() {
        return equalsStringCpuArgumentsSlope;
    }

    public void setEqualsStringCpuArgumentsSlope(Integer equalsStringCpuArgumentsSlope) {
        this.equalsStringCpuArgumentsSlope = equalsStringCpuArgumentsSlope;
    }

    public PlutusV1 withEqualsStringCpuArgumentsSlope(Integer equalsStringCpuArgumentsSlope) {
        this.equalsStringCpuArgumentsSlope = equalsStringCpuArgumentsSlope;
        return this;
    }

    public Integer getEqualsStringMemoryArguments() {
        return equalsStringMemoryArguments;
    }

    public void setEqualsStringMemoryArguments(Integer equalsStringMemoryArguments) {
        this.equalsStringMemoryArguments = equalsStringMemoryArguments;
    }

    public PlutusV1 withEqualsStringMemoryArguments(Integer equalsStringMemoryArguments) {
        this.equalsStringMemoryArguments = equalsStringMemoryArguments;
        return this;
    }

    public Integer getFstPairCpuArguments() {
        return fstPairCpuArguments;
    }

    public void setFstPairCpuArguments(Integer fstPairCpuArguments) {
        this.fstPairCpuArguments = fstPairCpuArguments;
    }

    public PlutusV1 withFstPairCpuArguments(Integer fstPairCpuArguments) {
        this.fstPairCpuArguments = fstPairCpuArguments;
        return this;
    }

    public Integer getFstPairMemoryArguments() {
        return fstPairMemoryArguments;
    }

    public void setFstPairMemoryArguments(Integer fstPairMemoryArguments) {
        this.fstPairMemoryArguments = fstPairMemoryArguments;
    }

    public PlutusV1 withFstPairMemoryArguments(Integer fstPairMemoryArguments) {
        this.fstPairMemoryArguments = fstPairMemoryArguments;
        return this;
    }

    public Integer getHeadListCpuArguments() {
        return headListCpuArguments;
    }

    public void setHeadListCpuArguments(Integer headListCpuArguments) {
        this.headListCpuArguments = headListCpuArguments;
    }

    public PlutusV1 withHeadListCpuArguments(Integer headListCpuArguments) {
        this.headListCpuArguments = headListCpuArguments;
        return this;
    }

    public Integer getHeadListMemoryArguments() {
        return headListMemoryArguments;
    }

    public void setHeadListMemoryArguments(Integer headListMemoryArguments) {
        this.headListMemoryArguments = headListMemoryArguments;
    }

    public PlutusV1 withHeadListMemoryArguments(Integer headListMemoryArguments) {
        this.headListMemoryArguments = headListMemoryArguments;
        return this;
    }

    public Integer getiDataCpuArguments() {
        return iDataCpuArguments;
    }

    public void setiDataCpuArguments(Integer iDataCpuArguments) {
        this.iDataCpuArguments = iDataCpuArguments;
    }

    public PlutusV1 withiDataCpuArguments(Integer iDataCpuArguments) {
        this.iDataCpuArguments = iDataCpuArguments;
        return this;
    }

    public Integer getiDataMemoryArguments() {
        return iDataMemoryArguments;
    }

    public void setiDataMemoryArguments(Integer iDataMemoryArguments) {
        this.iDataMemoryArguments = iDataMemoryArguments;
    }

    public PlutusV1 withiDataMemoryArguments(Integer iDataMemoryArguments) {
        this.iDataMemoryArguments = iDataMemoryArguments;
        return this;
    }

    public Integer getIfThenElseCpuArguments() {
        return ifThenElseCpuArguments;
    }

    public void setIfThenElseCpuArguments(Integer ifThenElseCpuArguments) {
        this.ifThenElseCpuArguments = ifThenElseCpuArguments;
    }

    public PlutusV1 withIfThenElseCpuArguments(Integer ifThenElseCpuArguments) {
        this.ifThenElseCpuArguments = ifThenElseCpuArguments;
        return this;
    }

    public Integer getIfThenElseMemoryArguments() {
        return ifThenElseMemoryArguments;
    }

    public void setIfThenElseMemoryArguments(Integer ifThenElseMemoryArguments) {
        this.ifThenElseMemoryArguments = ifThenElseMemoryArguments;
    }

    public PlutusV1 withIfThenElseMemoryArguments(Integer ifThenElseMemoryArguments) {
        this.ifThenElseMemoryArguments = ifThenElseMemoryArguments;
        return this;
    }

    public Integer getIndexByteStringCpuArguments() {
        return indexByteStringCpuArguments;
    }

    public void setIndexByteStringCpuArguments(Integer indexByteStringCpuArguments) {
        this.indexByteStringCpuArguments = indexByteStringCpuArguments;
    }

    public PlutusV1 withIndexByteStringCpuArguments(Integer indexByteStringCpuArguments) {
        this.indexByteStringCpuArguments = indexByteStringCpuArguments;
        return this;
    }

    public Integer getIndexByteStringMemoryArguments() {
        return indexByteStringMemoryArguments;
    }

    public void setIndexByteStringMemoryArguments(Integer indexByteStringMemoryArguments) {
        this.indexByteStringMemoryArguments = indexByteStringMemoryArguments;
    }

    public PlutusV1 withIndexByteStringMemoryArguments(Integer indexByteStringMemoryArguments) {
        this.indexByteStringMemoryArguments = indexByteStringMemoryArguments;
        return this;
    }

    public Integer getLengthOfByteStringCpuArguments() {
        return lengthOfByteStringCpuArguments;
    }

    public void setLengthOfByteStringCpuArguments(Integer lengthOfByteStringCpuArguments) {
        this.lengthOfByteStringCpuArguments = lengthOfByteStringCpuArguments;
    }

    public PlutusV1 withLengthOfByteStringCpuArguments(Integer lengthOfByteStringCpuArguments) {
        this.lengthOfByteStringCpuArguments = lengthOfByteStringCpuArguments;
        return this;
    }

    public Integer getLengthOfByteStringMemoryArguments() {
        return lengthOfByteStringMemoryArguments;
    }

    public void setLengthOfByteStringMemoryArguments(Integer lengthOfByteStringMemoryArguments) {
        this.lengthOfByteStringMemoryArguments = lengthOfByteStringMemoryArguments;
    }

    public PlutusV1 withLengthOfByteStringMemoryArguments(Integer lengthOfByteStringMemoryArguments) {
        this.lengthOfByteStringMemoryArguments = lengthOfByteStringMemoryArguments;
        return this;
    }

    public Integer getLessThanByteStringCpuArgumentsIntercept() {
        return lessThanByteStringCpuArgumentsIntercept;
    }

    public void setLessThanByteStringCpuArgumentsIntercept(Integer lessThanByteStringCpuArgumentsIntercept) {
        this.lessThanByteStringCpuArgumentsIntercept = lessThanByteStringCpuArgumentsIntercept;
    }

    public PlutusV1 withLessThanByteStringCpuArgumentsIntercept(Integer lessThanByteStringCpuArgumentsIntercept) {
        this.lessThanByteStringCpuArgumentsIntercept = lessThanByteStringCpuArgumentsIntercept;
        return this;
    }

    public Integer getLessThanByteStringCpuArgumentsSlope() {
        return lessThanByteStringCpuArgumentsSlope;
    }

    public void setLessThanByteStringCpuArgumentsSlope(Integer lessThanByteStringCpuArgumentsSlope) {
        this.lessThanByteStringCpuArgumentsSlope = lessThanByteStringCpuArgumentsSlope;
    }

    public PlutusV1 withLessThanByteStringCpuArgumentsSlope(Integer lessThanByteStringCpuArgumentsSlope) {
        this.lessThanByteStringCpuArgumentsSlope = lessThanByteStringCpuArgumentsSlope;
        return this;
    }

    public Integer getLessThanByteStringMemoryArguments() {
        return lessThanByteStringMemoryArguments;
    }

    public void setLessThanByteStringMemoryArguments(Integer lessThanByteStringMemoryArguments) {
        this.lessThanByteStringMemoryArguments = lessThanByteStringMemoryArguments;
    }

    public PlutusV1 withLessThanByteStringMemoryArguments(Integer lessThanByteStringMemoryArguments) {
        this.lessThanByteStringMemoryArguments = lessThanByteStringMemoryArguments;
        return this;
    }

    public Integer getLessThanEqualsByteStringCpuArgumentsIntercept() {
        return lessThanEqualsByteStringCpuArgumentsIntercept;
    }

    public void setLessThanEqualsByteStringCpuArgumentsIntercept(Integer lessThanEqualsByteStringCpuArgumentsIntercept) {
        this.lessThanEqualsByteStringCpuArgumentsIntercept = lessThanEqualsByteStringCpuArgumentsIntercept;
    }

    public PlutusV1 withLessThanEqualsByteStringCpuArgumentsIntercept(Integer lessThanEqualsByteStringCpuArgumentsIntercept) {
        this.lessThanEqualsByteStringCpuArgumentsIntercept = lessThanEqualsByteStringCpuArgumentsIntercept;
        return this;
    }

    public Integer getLessThanEqualsByteStringCpuArgumentsSlope() {
        return lessThanEqualsByteStringCpuArgumentsSlope;
    }

    public void setLessThanEqualsByteStringCpuArgumentsSlope(Integer lessThanEqualsByteStringCpuArgumentsSlope) {
        this.lessThanEqualsByteStringCpuArgumentsSlope = lessThanEqualsByteStringCpuArgumentsSlope;
    }

    public PlutusV1 withLessThanEqualsByteStringCpuArgumentsSlope(Integer lessThanEqualsByteStringCpuArgumentsSlope) {
        this.lessThanEqualsByteStringCpuArgumentsSlope = lessThanEqualsByteStringCpuArgumentsSlope;
        return this;
    }

    public Integer getLessThanEqualsByteStringMemoryArguments() {
        return lessThanEqualsByteStringMemoryArguments;
    }

    public void setLessThanEqualsByteStringMemoryArguments(Integer lessThanEqualsByteStringMemoryArguments) {
        this.lessThanEqualsByteStringMemoryArguments = lessThanEqualsByteStringMemoryArguments;
    }

    public PlutusV1 withLessThanEqualsByteStringMemoryArguments(Integer lessThanEqualsByteStringMemoryArguments) {
        this.lessThanEqualsByteStringMemoryArguments = lessThanEqualsByteStringMemoryArguments;
        return this;
    }

    public Integer getLessThanEqualsIntegerCpuArgumentsIntercept() {
        return lessThanEqualsIntegerCpuArgumentsIntercept;
    }

    public void setLessThanEqualsIntegerCpuArgumentsIntercept(Integer lessThanEqualsIntegerCpuArgumentsIntercept) {
        this.lessThanEqualsIntegerCpuArgumentsIntercept = lessThanEqualsIntegerCpuArgumentsIntercept;
    }

    public PlutusV1 withLessThanEqualsIntegerCpuArgumentsIntercept(Integer lessThanEqualsIntegerCpuArgumentsIntercept) {
        this.lessThanEqualsIntegerCpuArgumentsIntercept = lessThanEqualsIntegerCpuArgumentsIntercept;
        return this;
    }

    public Integer getLessThanEqualsIntegerCpuArgumentsSlope() {
        return lessThanEqualsIntegerCpuArgumentsSlope;
    }

    public void setLessThanEqualsIntegerCpuArgumentsSlope(Integer lessThanEqualsIntegerCpuArgumentsSlope) {
        this.lessThanEqualsIntegerCpuArgumentsSlope = lessThanEqualsIntegerCpuArgumentsSlope;
    }

    public PlutusV1 withLessThanEqualsIntegerCpuArgumentsSlope(Integer lessThanEqualsIntegerCpuArgumentsSlope) {
        this.lessThanEqualsIntegerCpuArgumentsSlope = lessThanEqualsIntegerCpuArgumentsSlope;
        return this;
    }

    public Integer getLessThanEqualsIntegerMemoryArguments() {
        return lessThanEqualsIntegerMemoryArguments;
    }

    public void setLessThanEqualsIntegerMemoryArguments(Integer lessThanEqualsIntegerMemoryArguments) {
        this.lessThanEqualsIntegerMemoryArguments = lessThanEqualsIntegerMemoryArguments;
    }

    public PlutusV1 withLessThanEqualsIntegerMemoryArguments(Integer lessThanEqualsIntegerMemoryArguments) {
        this.lessThanEqualsIntegerMemoryArguments = lessThanEqualsIntegerMemoryArguments;
        return this;
    }

    public Integer getLessThanIntegerCpuArgumentsIntercept() {
        return lessThanIntegerCpuArgumentsIntercept;
    }

    public void setLessThanIntegerCpuArgumentsIntercept(Integer lessThanIntegerCpuArgumentsIntercept) {
        this.lessThanIntegerCpuArgumentsIntercept = lessThanIntegerCpuArgumentsIntercept;
    }

    public PlutusV1 withLessThanIntegerCpuArgumentsIntercept(Integer lessThanIntegerCpuArgumentsIntercept) {
        this.lessThanIntegerCpuArgumentsIntercept = lessThanIntegerCpuArgumentsIntercept;
        return this;
    }

    public Integer getLessThanIntegerCpuArgumentsSlope() {
        return lessThanIntegerCpuArgumentsSlope;
    }

    public void setLessThanIntegerCpuArgumentsSlope(Integer lessThanIntegerCpuArgumentsSlope) {
        this.lessThanIntegerCpuArgumentsSlope = lessThanIntegerCpuArgumentsSlope;
    }

    public PlutusV1 withLessThanIntegerCpuArgumentsSlope(Integer lessThanIntegerCpuArgumentsSlope) {
        this.lessThanIntegerCpuArgumentsSlope = lessThanIntegerCpuArgumentsSlope;
        return this;
    }

    public Integer getLessThanIntegerMemoryArguments() {
        return lessThanIntegerMemoryArguments;
    }

    public void setLessThanIntegerMemoryArguments(Integer lessThanIntegerMemoryArguments) {
        this.lessThanIntegerMemoryArguments = lessThanIntegerMemoryArguments;
    }

    public PlutusV1 withLessThanIntegerMemoryArguments(Integer lessThanIntegerMemoryArguments) {
        this.lessThanIntegerMemoryArguments = lessThanIntegerMemoryArguments;
        return this;
    }

    public Integer getListDataCpuArguments() {
        return listDataCpuArguments;
    }

    public void setListDataCpuArguments(Integer listDataCpuArguments) {
        this.listDataCpuArguments = listDataCpuArguments;
    }

    public PlutusV1 withListDataCpuArguments(Integer listDataCpuArguments) {
        this.listDataCpuArguments = listDataCpuArguments;
        return this;
    }

    public Integer getListDataMemoryArguments() {
        return listDataMemoryArguments;
    }

    public void setListDataMemoryArguments(Integer listDataMemoryArguments) {
        this.listDataMemoryArguments = listDataMemoryArguments;
    }

    public PlutusV1 withListDataMemoryArguments(Integer listDataMemoryArguments) {
        this.listDataMemoryArguments = listDataMemoryArguments;
        return this;
    }

    public Integer getMapDataCpuArguments() {
        return mapDataCpuArguments;
    }

    public void setMapDataCpuArguments(Integer mapDataCpuArguments) {
        this.mapDataCpuArguments = mapDataCpuArguments;
    }

    public PlutusV1 withMapDataCpuArguments(Integer mapDataCpuArguments) {
        this.mapDataCpuArguments = mapDataCpuArguments;
        return this;
    }

    public Integer getMapDataMemoryArguments() {
        return mapDataMemoryArguments;
    }

    public void setMapDataMemoryArguments(Integer mapDataMemoryArguments) {
        this.mapDataMemoryArguments = mapDataMemoryArguments;
    }

    public PlutusV1 withMapDataMemoryArguments(Integer mapDataMemoryArguments) {
        this.mapDataMemoryArguments = mapDataMemoryArguments;
        return this;
    }

    public Integer getMkConsCpuArguments() {
        return mkConsCpuArguments;
    }

    public void setMkConsCpuArguments(Integer mkConsCpuArguments) {
        this.mkConsCpuArguments = mkConsCpuArguments;
    }

    public PlutusV1 withMkConsCpuArguments(Integer mkConsCpuArguments) {
        this.mkConsCpuArguments = mkConsCpuArguments;
        return this;
    }

    public Integer getMkConsMemoryArguments() {
        return mkConsMemoryArguments;
    }

    public void setMkConsMemoryArguments(Integer mkConsMemoryArguments) {
        this.mkConsMemoryArguments = mkConsMemoryArguments;
    }

    public PlutusV1 withMkConsMemoryArguments(Integer mkConsMemoryArguments) {
        this.mkConsMemoryArguments = mkConsMemoryArguments;
        return this;
    }

    public Integer getMkNilDataCpuArguments() {
        return mkNilDataCpuArguments;
    }

    public void setMkNilDataCpuArguments(Integer mkNilDataCpuArguments) {
        this.mkNilDataCpuArguments = mkNilDataCpuArguments;
    }

    public PlutusV1 withMkNilDataCpuArguments(Integer mkNilDataCpuArguments) {
        this.mkNilDataCpuArguments = mkNilDataCpuArguments;
        return this;
    }

    public Integer getMkNilDataMemoryArguments() {
        return mkNilDataMemoryArguments;
    }

    public void setMkNilDataMemoryArguments(Integer mkNilDataMemoryArguments) {
        this.mkNilDataMemoryArguments = mkNilDataMemoryArguments;
    }

    public PlutusV1 withMkNilDataMemoryArguments(Integer mkNilDataMemoryArguments) {
        this.mkNilDataMemoryArguments = mkNilDataMemoryArguments;
        return this;
    }

    public Integer getMkNilPairDataCpuArguments() {
        return mkNilPairDataCpuArguments;
    }

    public void setMkNilPairDataCpuArguments(Integer mkNilPairDataCpuArguments) {
        this.mkNilPairDataCpuArguments = mkNilPairDataCpuArguments;
    }

    public PlutusV1 withMkNilPairDataCpuArguments(Integer mkNilPairDataCpuArguments) {
        this.mkNilPairDataCpuArguments = mkNilPairDataCpuArguments;
        return this;
    }

    public Integer getMkNilPairDataMemoryArguments() {
        return mkNilPairDataMemoryArguments;
    }

    public void setMkNilPairDataMemoryArguments(Integer mkNilPairDataMemoryArguments) {
        this.mkNilPairDataMemoryArguments = mkNilPairDataMemoryArguments;
    }

    public PlutusV1 withMkNilPairDataMemoryArguments(Integer mkNilPairDataMemoryArguments) {
        this.mkNilPairDataMemoryArguments = mkNilPairDataMemoryArguments;
        return this;
    }

    public Integer getMkPairDataCpuArguments() {
        return mkPairDataCpuArguments;
    }

    public void setMkPairDataCpuArguments(Integer mkPairDataCpuArguments) {
        this.mkPairDataCpuArguments = mkPairDataCpuArguments;
    }

    public PlutusV1 withMkPairDataCpuArguments(Integer mkPairDataCpuArguments) {
        this.mkPairDataCpuArguments = mkPairDataCpuArguments;
        return this;
    }

    public Integer getMkPairDataMemoryArguments() {
        return mkPairDataMemoryArguments;
    }

    public void setMkPairDataMemoryArguments(Integer mkPairDataMemoryArguments) {
        this.mkPairDataMemoryArguments = mkPairDataMemoryArguments;
    }

    public PlutusV1 withMkPairDataMemoryArguments(Integer mkPairDataMemoryArguments) {
        this.mkPairDataMemoryArguments = mkPairDataMemoryArguments;
        return this;
    }

    public Integer getModIntegerCpuArgumentsConstant() {
        return modIntegerCpuArgumentsConstant;
    }

    public void setModIntegerCpuArgumentsConstant(Integer modIntegerCpuArgumentsConstant) {
        this.modIntegerCpuArgumentsConstant = modIntegerCpuArgumentsConstant;
    }

    public PlutusV1 withModIntegerCpuArgumentsConstant(Integer modIntegerCpuArgumentsConstant) {
        this.modIntegerCpuArgumentsConstant = modIntegerCpuArgumentsConstant;
        return this;
    }

    public Integer getModIntegerCpuArgumentsModelArgumentsIntercept() {
        return modIntegerCpuArgumentsModelArgumentsIntercept;
    }

    public void setModIntegerCpuArgumentsModelArgumentsIntercept(Integer modIntegerCpuArgumentsModelArgumentsIntercept) {
        this.modIntegerCpuArgumentsModelArgumentsIntercept = modIntegerCpuArgumentsModelArgumentsIntercept;
    }

    public PlutusV1 withModIntegerCpuArgumentsModelArgumentsIntercept(Integer modIntegerCpuArgumentsModelArgumentsIntercept) {
        this.modIntegerCpuArgumentsModelArgumentsIntercept = modIntegerCpuArgumentsModelArgumentsIntercept;
        return this;
    }

    public Integer getModIntegerCpuArgumentsModelArgumentsSlope() {
        return modIntegerCpuArgumentsModelArgumentsSlope;
    }

    public void setModIntegerCpuArgumentsModelArgumentsSlope(Integer modIntegerCpuArgumentsModelArgumentsSlope) {
        this.modIntegerCpuArgumentsModelArgumentsSlope = modIntegerCpuArgumentsModelArgumentsSlope;
    }

    public PlutusV1 withModIntegerCpuArgumentsModelArgumentsSlope(Integer modIntegerCpuArgumentsModelArgumentsSlope) {
        this.modIntegerCpuArgumentsModelArgumentsSlope = modIntegerCpuArgumentsModelArgumentsSlope;
        return this;
    }

    public Integer getModIntegerMemoryArgumentsIntercept() {
        return modIntegerMemoryArgumentsIntercept;
    }

    public void setModIntegerMemoryArgumentsIntercept(Integer modIntegerMemoryArgumentsIntercept) {
        this.modIntegerMemoryArgumentsIntercept = modIntegerMemoryArgumentsIntercept;
    }

    public PlutusV1 withModIntegerMemoryArgumentsIntercept(Integer modIntegerMemoryArgumentsIntercept) {
        this.modIntegerMemoryArgumentsIntercept = modIntegerMemoryArgumentsIntercept;
        return this;
    }

    public Integer getModIntegerMemoryArgumentsMinimum() {
        return modIntegerMemoryArgumentsMinimum;
    }

    public void setModIntegerMemoryArgumentsMinimum(Integer modIntegerMemoryArgumentsMinimum) {
        this.modIntegerMemoryArgumentsMinimum = modIntegerMemoryArgumentsMinimum;
    }

    public PlutusV1 withModIntegerMemoryArgumentsMinimum(Integer modIntegerMemoryArgumentsMinimum) {
        this.modIntegerMemoryArgumentsMinimum = modIntegerMemoryArgumentsMinimum;
        return this;
    }

    public Integer getModIntegerMemoryArgumentsSlope() {
        return modIntegerMemoryArgumentsSlope;
    }

    public void setModIntegerMemoryArgumentsSlope(Integer modIntegerMemoryArgumentsSlope) {
        this.modIntegerMemoryArgumentsSlope = modIntegerMemoryArgumentsSlope;
    }

    public PlutusV1 withModIntegerMemoryArgumentsSlope(Integer modIntegerMemoryArgumentsSlope) {
        this.modIntegerMemoryArgumentsSlope = modIntegerMemoryArgumentsSlope;
        return this;
    }

    public Integer getMultiplyIntegerCpuArgumentsIntercept() {
        return multiplyIntegerCpuArgumentsIntercept;
    }

    public void setMultiplyIntegerCpuArgumentsIntercept(Integer multiplyIntegerCpuArgumentsIntercept) {
        this.multiplyIntegerCpuArgumentsIntercept = multiplyIntegerCpuArgumentsIntercept;
    }

    public PlutusV1 withMultiplyIntegerCpuArgumentsIntercept(Integer multiplyIntegerCpuArgumentsIntercept) {
        this.multiplyIntegerCpuArgumentsIntercept = multiplyIntegerCpuArgumentsIntercept;
        return this;
    }

    public Integer getMultiplyIntegerCpuArgumentsSlope() {
        return multiplyIntegerCpuArgumentsSlope;
    }

    public void setMultiplyIntegerCpuArgumentsSlope(Integer multiplyIntegerCpuArgumentsSlope) {
        this.multiplyIntegerCpuArgumentsSlope = multiplyIntegerCpuArgumentsSlope;
    }

    public PlutusV1 withMultiplyIntegerCpuArgumentsSlope(Integer multiplyIntegerCpuArgumentsSlope) {
        this.multiplyIntegerCpuArgumentsSlope = multiplyIntegerCpuArgumentsSlope;
        return this;
    }

    public Integer getMultiplyIntegerMemoryArgumentsIntercept() {
        return multiplyIntegerMemoryArgumentsIntercept;
    }

    public void setMultiplyIntegerMemoryArgumentsIntercept(Integer multiplyIntegerMemoryArgumentsIntercept) {
        this.multiplyIntegerMemoryArgumentsIntercept = multiplyIntegerMemoryArgumentsIntercept;
    }

    public PlutusV1 withMultiplyIntegerMemoryArgumentsIntercept(Integer multiplyIntegerMemoryArgumentsIntercept) {
        this.multiplyIntegerMemoryArgumentsIntercept = multiplyIntegerMemoryArgumentsIntercept;
        return this;
    }

    public Integer getMultiplyIntegerMemoryArgumentsSlope() {
        return multiplyIntegerMemoryArgumentsSlope;
    }

    public void setMultiplyIntegerMemoryArgumentsSlope(Integer multiplyIntegerMemoryArgumentsSlope) {
        this.multiplyIntegerMemoryArgumentsSlope = multiplyIntegerMemoryArgumentsSlope;
    }

    public PlutusV1 withMultiplyIntegerMemoryArgumentsSlope(Integer multiplyIntegerMemoryArgumentsSlope) {
        this.multiplyIntegerMemoryArgumentsSlope = multiplyIntegerMemoryArgumentsSlope;
        return this;
    }

    public Integer getNullListCpuArguments() {
        return nullListCpuArguments;
    }

    public void setNullListCpuArguments(Integer nullListCpuArguments) {
        this.nullListCpuArguments = nullListCpuArguments;
    }

    public PlutusV1 withNullListCpuArguments(Integer nullListCpuArguments) {
        this.nullListCpuArguments = nullListCpuArguments;
        return this;
    }

    public Integer getNullListMemoryArguments() {
        return nullListMemoryArguments;
    }

    public void setNullListMemoryArguments(Integer nullListMemoryArguments) {
        this.nullListMemoryArguments = nullListMemoryArguments;
    }

    public PlutusV1 withNullListMemoryArguments(Integer nullListMemoryArguments) {
        this.nullListMemoryArguments = nullListMemoryArguments;
        return this;
    }

    public Integer getQuotientIntegerCpuArgumentsConstant() {
        return quotientIntegerCpuArgumentsConstant;
    }

    public void setQuotientIntegerCpuArgumentsConstant(Integer quotientIntegerCpuArgumentsConstant) {
        this.quotientIntegerCpuArgumentsConstant = quotientIntegerCpuArgumentsConstant;
    }

    public PlutusV1 withQuotientIntegerCpuArgumentsConstant(Integer quotientIntegerCpuArgumentsConstant) {
        this.quotientIntegerCpuArgumentsConstant = quotientIntegerCpuArgumentsConstant;
        return this;
    }

    public Integer getQuotientIntegerCpuArgumentsModelArgumentsIntercept() {
        return quotientIntegerCpuArgumentsModelArgumentsIntercept;
    }

    public void setQuotientIntegerCpuArgumentsModelArgumentsIntercept(Integer quotientIntegerCpuArgumentsModelArgumentsIntercept) {
        this.quotientIntegerCpuArgumentsModelArgumentsIntercept = quotientIntegerCpuArgumentsModelArgumentsIntercept;
    }

    public PlutusV1 withQuotientIntegerCpuArgumentsModelArgumentsIntercept(Integer quotientIntegerCpuArgumentsModelArgumentsIntercept) {
        this.quotientIntegerCpuArgumentsModelArgumentsIntercept = quotientIntegerCpuArgumentsModelArgumentsIntercept;
        return this;
    }

    public Integer getQuotientIntegerCpuArgumentsModelArgumentsSlope() {
        return quotientIntegerCpuArgumentsModelArgumentsSlope;
    }

    public void setQuotientIntegerCpuArgumentsModelArgumentsSlope(Integer quotientIntegerCpuArgumentsModelArgumentsSlope) {
        this.quotientIntegerCpuArgumentsModelArgumentsSlope = quotientIntegerCpuArgumentsModelArgumentsSlope;
    }

    public PlutusV1 withQuotientIntegerCpuArgumentsModelArgumentsSlope(Integer quotientIntegerCpuArgumentsModelArgumentsSlope) {
        this.quotientIntegerCpuArgumentsModelArgumentsSlope = quotientIntegerCpuArgumentsModelArgumentsSlope;
        return this;
    }

    public Integer getQuotientIntegerMemoryArgumentsIntercept() {
        return quotientIntegerMemoryArgumentsIntercept;
    }

    public void setQuotientIntegerMemoryArgumentsIntercept(Integer quotientIntegerMemoryArgumentsIntercept) {
        this.quotientIntegerMemoryArgumentsIntercept = quotientIntegerMemoryArgumentsIntercept;
    }

    public PlutusV1 withQuotientIntegerMemoryArgumentsIntercept(Integer quotientIntegerMemoryArgumentsIntercept) {
        this.quotientIntegerMemoryArgumentsIntercept = quotientIntegerMemoryArgumentsIntercept;
        return this;
    }

    public Integer getQuotientIntegerMemoryArgumentsMinimum() {
        return quotientIntegerMemoryArgumentsMinimum;
    }

    public void setQuotientIntegerMemoryArgumentsMinimum(Integer quotientIntegerMemoryArgumentsMinimum) {
        this.quotientIntegerMemoryArgumentsMinimum = quotientIntegerMemoryArgumentsMinimum;
    }

    public PlutusV1 withQuotientIntegerMemoryArgumentsMinimum(Integer quotientIntegerMemoryArgumentsMinimum) {
        this.quotientIntegerMemoryArgumentsMinimum = quotientIntegerMemoryArgumentsMinimum;
        return this;
    }

    public Integer getQuotientIntegerMemoryArgumentsSlope() {
        return quotientIntegerMemoryArgumentsSlope;
    }

    public void setQuotientIntegerMemoryArgumentsSlope(Integer quotientIntegerMemoryArgumentsSlope) {
        this.quotientIntegerMemoryArgumentsSlope = quotientIntegerMemoryArgumentsSlope;
    }

    public PlutusV1 withQuotientIntegerMemoryArgumentsSlope(Integer quotientIntegerMemoryArgumentsSlope) {
        this.quotientIntegerMemoryArgumentsSlope = quotientIntegerMemoryArgumentsSlope;
        return this;
    }

    public Integer getRemainderIntegerCpuArgumentsConstant() {
        return remainderIntegerCpuArgumentsConstant;
    }

    public void setRemainderIntegerCpuArgumentsConstant(Integer remainderIntegerCpuArgumentsConstant) {
        this.remainderIntegerCpuArgumentsConstant = remainderIntegerCpuArgumentsConstant;
    }

    public PlutusV1 withRemainderIntegerCpuArgumentsConstant(Integer remainderIntegerCpuArgumentsConstant) {
        this.remainderIntegerCpuArgumentsConstant = remainderIntegerCpuArgumentsConstant;
        return this;
    }

    public Integer getRemainderIntegerCpuArgumentsModelArgumentsIntercept() {
        return remainderIntegerCpuArgumentsModelArgumentsIntercept;
    }

    public void setRemainderIntegerCpuArgumentsModelArgumentsIntercept(Integer remainderIntegerCpuArgumentsModelArgumentsIntercept) {
        this.remainderIntegerCpuArgumentsModelArgumentsIntercept = remainderIntegerCpuArgumentsModelArgumentsIntercept;
    }

    public PlutusV1 withRemainderIntegerCpuArgumentsModelArgumentsIntercept(Integer remainderIntegerCpuArgumentsModelArgumentsIntercept) {
        this.remainderIntegerCpuArgumentsModelArgumentsIntercept = remainderIntegerCpuArgumentsModelArgumentsIntercept;
        return this;
    }

    public Integer getRemainderIntegerCpuArgumentsModelArgumentsSlope() {
        return remainderIntegerCpuArgumentsModelArgumentsSlope;
    }

    public void setRemainderIntegerCpuArgumentsModelArgumentsSlope(Integer remainderIntegerCpuArgumentsModelArgumentsSlope) {
        this.remainderIntegerCpuArgumentsModelArgumentsSlope = remainderIntegerCpuArgumentsModelArgumentsSlope;
    }

    public PlutusV1 withRemainderIntegerCpuArgumentsModelArgumentsSlope(Integer remainderIntegerCpuArgumentsModelArgumentsSlope) {
        this.remainderIntegerCpuArgumentsModelArgumentsSlope = remainderIntegerCpuArgumentsModelArgumentsSlope;
        return this;
    }

    public Integer getRemainderIntegerMemoryArgumentsIntercept() {
        return remainderIntegerMemoryArgumentsIntercept;
    }

    public void setRemainderIntegerMemoryArgumentsIntercept(Integer remainderIntegerMemoryArgumentsIntercept) {
        this.remainderIntegerMemoryArgumentsIntercept = remainderIntegerMemoryArgumentsIntercept;
    }

    public PlutusV1 withRemainderIntegerMemoryArgumentsIntercept(Integer remainderIntegerMemoryArgumentsIntercept) {
        this.remainderIntegerMemoryArgumentsIntercept = remainderIntegerMemoryArgumentsIntercept;
        return this;
    }

    public Integer getRemainderIntegerMemoryArgumentsMinimum() {
        return remainderIntegerMemoryArgumentsMinimum;
    }

    public void setRemainderIntegerMemoryArgumentsMinimum(Integer remainderIntegerMemoryArgumentsMinimum) {
        this.remainderIntegerMemoryArgumentsMinimum = remainderIntegerMemoryArgumentsMinimum;
    }

    public PlutusV1 withRemainderIntegerMemoryArgumentsMinimum(Integer remainderIntegerMemoryArgumentsMinimum) {
        this.remainderIntegerMemoryArgumentsMinimum = remainderIntegerMemoryArgumentsMinimum;
        return this;
    }

    public Integer getRemainderIntegerMemoryArgumentsSlope() {
        return remainderIntegerMemoryArgumentsSlope;
    }

    public void setRemainderIntegerMemoryArgumentsSlope(Integer remainderIntegerMemoryArgumentsSlope) {
        this.remainderIntegerMemoryArgumentsSlope = remainderIntegerMemoryArgumentsSlope;
    }

    public PlutusV1 withRemainderIntegerMemoryArgumentsSlope(Integer remainderIntegerMemoryArgumentsSlope) {
        this.remainderIntegerMemoryArgumentsSlope = remainderIntegerMemoryArgumentsSlope;
        return this;
    }

    public Integer getSha2256CpuArgumentsIntercept() {
        return sha2256CpuArgumentsIntercept;
    }

    public void setSha2256CpuArgumentsIntercept(Integer sha2256CpuArgumentsIntercept) {
        this.sha2256CpuArgumentsIntercept = sha2256CpuArgumentsIntercept;
    }

    public PlutusV1 withSha2256CpuArgumentsIntercept(Integer sha2256CpuArgumentsIntercept) {
        this.sha2256CpuArgumentsIntercept = sha2256CpuArgumentsIntercept;
        return this;
    }

    public Integer getSha2256CpuArgumentsSlope() {
        return sha2256CpuArgumentsSlope;
    }

    public void setSha2256CpuArgumentsSlope(Integer sha2256CpuArgumentsSlope) {
        this.sha2256CpuArgumentsSlope = sha2256CpuArgumentsSlope;
    }

    public PlutusV1 withSha2256CpuArgumentsSlope(Integer sha2256CpuArgumentsSlope) {
        this.sha2256CpuArgumentsSlope = sha2256CpuArgumentsSlope;
        return this;
    }

    public Integer getSha2256MemoryArguments() {
        return sha2256MemoryArguments;
    }

    public void setSha2256MemoryArguments(Integer sha2256MemoryArguments) {
        this.sha2256MemoryArguments = sha2256MemoryArguments;
    }

    public PlutusV1 withSha2256MemoryArguments(Integer sha2256MemoryArguments) {
        this.sha2256MemoryArguments = sha2256MemoryArguments;
        return this;
    }

    public Integer getSha3256CpuArgumentsIntercept() {
        return sha3256CpuArgumentsIntercept;
    }

    public void setSha3256CpuArgumentsIntercept(Integer sha3256CpuArgumentsIntercept) {
        this.sha3256CpuArgumentsIntercept = sha3256CpuArgumentsIntercept;
    }

    public PlutusV1 withSha3256CpuArgumentsIntercept(Integer sha3256CpuArgumentsIntercept) {
        this.sha3256CpuArgumentsIntercept = sha3256CpuArgumentsIntercept;
        return this;
    }

    public Integer getSha3256CpuArgumentsSlope() {
        return sha3256CpuArgumentsSlope;
    }

    public void setSha3256CpuArgumentsSlope(Integer sha3256CpuArgumentsSlope) {
        this.sha3256CpuArgumentsSlope = sha3256CpuArgumentsSlope;
    }

    public PlutusV1 withSha3256CpuArgumentsSlope(Integer sha3256CpuArgumentsSlope) {
        this.sha3256CpuArgumentsSlope = sha3256CpuArgumentsSlope;
        return this;
    }

    public Integer getSha3256MemoryArguments() {
        return sha3256MemoryArguments;
    }

    public void setSha3256MemoryArguments(Integer sha3256MemoryArguments) {
        this.sha3256MemoryArguments = sha3256MemoryArguments;
    }

    public PlutusV1 withSha3256MemoryArguments(Integer sha3256MemoryArguments) {
        this.sha3256MemoryArguments = sha3256MemoryArguments;
        return this;
    }

    public Integer getSliceByteStringCpuArgumentsIntercept() {
        return sliceByteStringCpuArgumentsIntercept;
    }

    public void setSliceByteStringCpuArgumentsIntercept(Integer sliceByteStringCpuArgumentsIntercept) {
        this.sliceByteStringCpuArgumentsIntercept = sliceByteStringCpuArgumentsIntercept;
    }

    public PlutusV1 withSliceByteStringCpuArgumentsIntercept(Integer sliceByteStringCpuArgumentsIntercept) {
        this.sliceByteStringCpuArgumentsIntercept = sliceByteStringCpuArgumentsIntercept;
        return this;
    }

    public Integer getSliceByteStringCpuArgumentsSlope() {
        return sliceByteStringCpuArgumentsSlope;
    }

    public void setSliceByteStringCpuArgumentsSlope(Integer sliceByteStringCpuArgumentsSlope) {
        this.sliceByteStringCpuArgumentsSlope = sliceByteStringCpuArgumentsSlope;
    }

    public PlutusV1 withSliceByteStringCpuArgumentsSlope(Integer sliceByteStringCpuArgumentsSlope) {
        this.sliceByteStringCpuArgumentsSlope = sliceByteStringCpuArgumentsSlope;
        return this;
    }

    public Integer getSliceByteStringMemoryArgumentsIntercept() {
        return sliceByteStringMemoryArgumentsIntercept;
    }

    public void setSliceByteStringMemoryArgumentsIntercept(Integer sliceByteStringMemoryArgumentsIntercept) {
        this.sliceByteStringMemoryArgumentsIntercept = sliceByteStringMemoryArgumentsIntercept;
    }

    public PlutusV1 withSliceByteStringMemoryArgumentsIntercept(Integer sliceByteStringMemoryArgumentsIntercept) {
        this.sliceByteStringMemoryArgumentsIntercept = sliceByteStringMemoryArgumentsIntercept;
        return this;
    }

    public Integer getSliceByteStringMemoryArgumentsSlope() {
        return sliceByteStringMemoryArgumentsSlope;
    }

    public void setSliceByteStringMemoryArgumentsSlope(Integer sliceByteStringMemoryArgumentsSlope) {
        this.sliceByteStringMemoryArgumentsSlope = sliceByteStringMemoryArgumentsSlope;
    }

    public PlutusV1 withSliceByteStringMemoryArgumentsSlope(Integer sliceByteStringMemoryArgumentsSlope) {
        this.sliceByteStringMemoryArgumentsSlope = sliceByteStringMemoryArgumentsSlope;
        return this;
    }

    public Integer getSndPairCpuArguments() {
        return sndPairCpuArguments;
    }

    public void setSndPairCpuArguments(Integer sndPairCpuArguments) {
        this.sndPairCpuArguments = sndPairCpuArguments;
    }

    public PlutusV1 withSndPairCpuArguments(Integer sndPairCpuArguments) {
        this.sndPairCpuArguments = sndPairCpuArguments;
        return this;
    }

    public Integer getSndPairMemoryArguments() {
        return sndPairMemoryArguments;
    }

    public void setSndPairMemoryArguments(Integer sndPairMemoryArguments) {
        this.sndPairMemoryArguments = sndPairMemoryArguments;
    }

    public PlutusV1 withSndPairMemoryArguments(Integer sndPairMemoryArguments) {
        this.sndPairMemoryArguments = sndPairMemoryArguments;
        return this;
    }

    public Integer getSubtractIntegerCpuArgumentsIntercept() {
        return subtractIntegerCpuArgumentsIntercept;
    }

    public void setSubtractIntegerCpuArgumentsIntercept(Integer subtractIntegerCpuArgumentsIntercept) {
        this.subtractIntegerCpuArgumentsIntercept = subtractIntegerCpuArgumentsIntercept;
    }

    public PlutusV1 withSubtractIntegerCpuArgumentsIntercept(Integer subtractIntegerCpuArgumentsIntercept) {
        this.subtractIntegerCpuArgumentsIntercept = subtractIntegerCpuArgumentsIntercept;
        return this;
    }

    public Integer getSubtractIntegerCpuArgumentsSlope() {
        return subtractIntegerCpuArgumentsSlope;
    }

    public void setSubtractIntegerCpuArgumentsSlope(Integer subtractIntegerCpuArgumentsSlope) {
        this.subtractIntegerCpuArgumentsSlope = subtractIntegerCpuArgumentsSlope;
    }

    public PlutusV1 withSubtractIntegerCpuArgumentsSlope(Integer subtractIntegerCpuArgumentsSlope) {
        this.subtractIntegerCpuArgumentsSlope = subtractIntegerCpuArgumentsSlope;
        return this;
    }

    public Integer getSubtractIntegerMemoryArgumentsIntercept() {
        return subtractIntegerMemoryArgumentsIntercept;
    }

    public void setSubtractIntegerMemoryArgumentsIntercept(Integer subtractIntegerMemoryArgumentsIntercept) {
        this.subtractIntegerMemoryArgumentsIntercept = subtractIntegerMemoryArgumentsIntercept;
    }

    public PlutusV1 withSubtractIntegerMemoryArgumentsIntercept(Integer subtractIntegerMemoryArgumentsIntercept) {
        this.subtractIntegerMemoryArgumentsIntercept = subtractIntegerMemoryArgumentsIntercept;
        return this;
    }

    public Integer getSubtractIntegerMemoryArgumentsSlope() {
        return subtractIntegerMemoryArgumentsSlope;
    }

    public void setSubtractIntegerMemoryArgumentsSlope(Integer subtractIntegerMemoryArgumentsSlope) {
        this.subtractIntegerMemoryArgumentsSlope = subtractIntegerMemoryArgumentsSlope;
    }

    public PlutusV1 withSubtractIntegerMemoryArgumentsSlope(Integer subtractIntegerMemoryArgumentsSlope) {
        this.subtractIntegerMemoryArgumentsSlope = subtractIntegerMemoryArgumentsSlope;
        return this;
    }

    public Integer getTailListCpuArguments() {
        return tailListCpuArguments;
    }

    public void setTailListCpuArguments(Integer tailListCpuArguments) {
        this.tailListCpuArguments = tailListCpuArguments;
    }

    public PlutusV1 withTailListCpuArguments(Integer tailListCpuArguments) {
        this.tailListCpuArguments = tailListCpuArguments;
        return this;
    }

    public Integer getTailListMemoryArguments() {
        return tailListMemoryArguments;
    }

    public void setTailListMemoryArguments(Integer tailListMemoryArguments) {
        this.tailListMemoryArguments = tailListMemoryArguments;
    }

    public PlutusV1 withTailListMemoryArguments(Integer tailListMemoryArguments) {
        this.tailListMemoryArguments = tailListMemoryArguments;
        return this;
    }

    public Integer getTraceCpuArguments() {
        return traceCpuArguments;
    }

    public void setTraceCpuArguments(Integer traceCpuArguments) {
        this.traceCpuArguments = traceCpuArguments;
    }

    public PlutusV1 withTraceCpuArguments(Integer traceCpuArguments) {
        this.traceCpuArguments = traceCpuArguments;
        return this;
    }

    public Integer getTraceMemoryArguments() {
        return traceMemoryArguments;
    }

    public void setTraceMemoryArguments(Integer traceMemoryArguments) {
        this.traceMemoryArguments = traceMemoryArguments;
    }

    public PlutusV1 withTraceMemoryArguments(Integer traceMemoryArguments) {
        this.traceMemoryArguments = traceMemoryArguments;
        return this;
    }

    public Integer getUnBDataCpuArguments() {
        return unBDataCpuArguments;
    }

    public void setUnBDataCpuArguments(Integer unBDataCpuArguments) {
        this.unBDataCpuArguments = unBDataCpuArguments;
    }

    public PlutusV1 withUnBDataCpuArguments(Integer unBDataCpuArguments) {
        this.unBDataCpuArguments = unBDataCpuArguments;
        return this;
    }

    public Integer getUnBDataMemoryArguments() {
        return unBDataMemoryArguments;
    }

    public void setUnBDataMemoryArguments(Integer unBDataMemoryArguments) {
        this.unBDataMemoryArguments = unBDataMemoryArguments;
    }

    public PlutusV1 withUnBDataMemoryArguments(Integer unBDataMemoryArguments) {
        this.unBDataMemoryArguments = unBDataMemoryArguments;
        return this;
    }

    public Integer getUnConstrDataCpuArguments() {
        return unConstrDataCpuArguments;
    }

    public void setUnConstrDataCpuArguments(Integer unConstrDataCpuArguments) {
        this.unConstrDataCpuArguments = unConstrDataCpuArguments;
    }

    public PlutusV1 withUnConstrDataCpuArguments(Integer unConstrDataCpuArguments) {
        this.unConstrDataCpuArguments = unConstrDataCpuArguments;
        return this;
    }

    public Integer getUnConstrDataMemoryArguments() {
        return unConstrDataMemoryArguments;
    }

    public void setUnConstrDataMemoryArguments(Integer unConstrDataMemoryArguments) {
        this.unConstrDataMemoryArguments = unConstrDataMemoryArguments;
    }

    public PlutusV1 withUnConstrDataMemoryArguments(Integer unConstrDataMemoryArguments) {
        this.unConstrDataMemoryArguments = unConstrDataMemoryArguments;
        return this;
    }

    public Integer getUnIDataCpuArguments() {
        return unIDataCpuArguments;
    }

    public void setUnIDataCpuArguments(Integer unIDataCpuArguments) {
        this.unIDataCpuArguments = unIDataCpuArguments;
    }

    public PlutusV1 withUnIDataCpuArguments(Integer unIDataCpuArguments) {
        this.unIDataCpuArguments = unIDataCpuArguments;
        return this;
    }

    public Integer getUnIDataMemoryArguments() {
        return unIDataMemoryArguments;
    }

    public void setUnIDataMemoryArguments(Integer unIDataMemoryArguments) {
        this.unIDataMemoryArguments = unIDataMemoryArguments;
    }

    public PlutusV1 withUnIDataMemoryArguments(Integer unIDataMemoryArguments) {
        this.unIDataMemoryArguments = unIDataMemoryArguments;
        return this;
    }

    public Integer getUnListDataCpuArguments() {
        return unListDataCpuArguments;
    }

    public void setUnListDataCpuArguments(Integer unListDataCpuArguments) {
        this.unListDataCpuArguments = unListDataCpuArguments;
    }

    public PlutusV1 withUnListDataCpuArguments(Integer unListDataCpuArguments) {
        this.unListDataCpuArguments = unListDataCpuArguments;
        return this;
    }

    public Integer getUnListDataMemoryArguments() {
        return unListDataMemoryArguments;
    }

    public void setUnListDataMemoryArguments(Integer unListDataMemoryArguments) {
        this.unListDataMemoryArguments = unListDataMemoryArguments;
    }

    public PlutusV1 withUnListDataMemoryArguments(Integer unListDataMemoryArguments) {
        this.unListDataMemoryArguments = unListDataMemoryArguments;
        return this;
    }

    public Integer getUnMapDataCpuArguments() {
        return unMapDataCpuArguments;
    }

    public void setUnMapDataCpuArguments(Integer unMapDataCpuArguments) {
        this.unMapDataCpuArguments = unMapDataCpuArguments;
    }

    public PlutusV1 withUnMapDataCpuArguments(Integer unMapDataCpuArguments) {
        this.unMapDataCpuArguments = unMapDataCpuArguments;
        return this;
    }

    public Integer getUnMapDataMemoryArguments() {
        return unMapDataMemoryArguments;
    }

    public void setUnMapDataMemoryArguments(Integer unMapDataMemoryArguments) {
        this.unMapDataMemoryArguments = unMapDataMemoryArguments;
    }

    public PlutusV1 withUnMapDataMemoryArguments(Integer unMapDataMemoryArguments) {
        this.unMapDataMemoryArguments = unMapDataMemoryArguments;
        return this;
    }

    public Integer getVerifyEd25519SignatureCpuArgumentsIntercept() {
        return verifyEd25519SignatureCpuArgumentsIntercept;
    }

    public void setVerifyEd25519SignatureCpuArgumentsIntercept(Integer verifyEd25519SignatureCpuArgumentsIntercept) {
        this.verifyEd25519SignatureCpuArgumentsIntercept = verifyEd25519SignatureCpuArgumentsIntercept;
    }

    public PlutusV1 withVerifyEd25519SignatureCpuArgumentsIntercept(Integer verifyEd25519SignatureCpuArgumentsIntercept) {
        this.verifyEd25519SignatureCpuArgumentsIntercept = verifyEd25519SignatureCpuArgumentsIntercept;
        return this;
    }

    public Integer getVerifyEd25519SignatureCpuArgumentsSlope() {
        return verifyEd25519SignatureCpuArgumentsSlope;
    }

    public void setVerifyEd25519SignatureCpuArgumentsSlope(Integer verifyEd25519SignatureCpuArgumentsSlope) {
        this.verifyEd25519SignatureCpuArgumentsSlope = verifyEd25519SignatureCpuArgumentsSlope;
    }

    public PlutusV1 withVerifyEd25519SignatureCpuArgumentsSlope(Integer verifyEd25519SignatureCpuArgumentsSlope) {
        this.verifyEd25519SignatureCpuArgumentsSlope = verifyEd25519SignatureCpuArgumentsSlope;
        return this;
    }

    public Integer getVerifyEd25519SignatureMemoryArguments() {
        return verifyEd25519SignatureMemoryArguments;
    }

    public void setVerifyEd25519SignatureMemoryArguments(Integer verifyEd25519SignatureMemoryArguments) {
        this.verifyEd25519SignatureMemoryArguments = verifyEd25519SignatureMemoryArguments;
    }

    public PlutusV1 withVerifyEd25519SignatureMemoryArguments(Integer verifyEd25519SignatureMemoryArguments) {
        this.verifyEd25519SignatureMemoryArguments = verifyEd25519SignatureMemoryArguments;
        return this;
    }

}
