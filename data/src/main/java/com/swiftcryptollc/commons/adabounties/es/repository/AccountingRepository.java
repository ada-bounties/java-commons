package com.swiftcryptollc.commons.adabounties.es.repository;

import com.swiftcryptollc.commons.adabounties.es.data.Accounting;
import java.util.Set;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
@Repository
public interface AccountingRepository extends ElasticsearchRepository<Accounting, String> {

    public Set<Accounting> findBySrcAccountId(String srcAccountId);

    public Set<Accounting> findByDestAccountId(String destAccountId);
}
