package com.swiftcryptollc.commons.adabounties.es.query;

import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.elasticsearch._types.FieldSort;
import co.elastic.clients.elasticsearch._types.SortOptions;
import co.elastic.clients.elasticsearch._types.SortOrder;
import co.elastic.clients.elasticsearch._types.query_dsl.BoolQuery;
import co.elastic.clients.elasticsearch._types.query_dsl.Query;
import co.elastic.clients.elasticsearch._types.query_dsl.RangeQuery;
import co.elastic.clients.elasticsearch.core.SearchResponse;
import co.elastic.clients.elasticsearch.core.search.Hit;
import co.elastic.clients.elasticsearch.core.search.HitsMetadata;
import co.elastic.clients.json.JsonData;
import com.swiftcryptollc.commons.adabounties.es.ElasticsearchClientConfig;
import com.swiftcryptollc.commons.adabounties.es.ElasticsearchClientListener;
import com.swiftcryptollc.commons.adabounties.redis.cache.AliasCache;
import com.swiftcryptollc.commons.adabounties.es.data.Reputation;
import com.swiftcryptollc.commons.adabounties.utilities.data.SyncArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import javax.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
@Service
public class ReputationQueryHandler implements ElasticsearchClientListener {

    @Autowired
    protected ElasticsearchClientConfig esClient;
    @Autowired
    protected AliasCache aliasCache;
    protected ElasticsearchClient client;
    protected UUID id;

    protected final static Logger logger = LoggerFactory.getLogger(ReputationQueryHandler.class);

    public ReputationQueryHandler() {

    }

    /**
     *
     * @param client
     */
    public ReputationQueryHandler(ElasticsearchClient client) {
        this.client = client;
    }

    /**
     * @return the id
     */
    public UUID getId() {
        if (id == null) {
            id = UUID.randomUUID();
        }
        return id;
    }

    @Override
    public void setClient() {
        if (esClient != null) {
            client = esClient.elasticSearchClient();
        }
    }

    /**
     * @param esClient the esClient to set
     */
    public void setEsClient(ElasticsearchClientConfig esClient) {
        this.esClient = esClient;
    }

    /**
     * @param client the client to set
     */
    public void setClient(ElasticsearchClient client) {
        this.client = client;
    }

    /**
     *
     */
    @PostConstruct
    public void init() {
        if (client == null) {
            ElasticsearchClientConfig.subscribe(this);
        }
    }

    /**
     *
     * @param sortField
     * @param pageable
     * @return
     */
    public SyncArrayDeque<Reputation> searchReputations(final String sortField, PageRequest pageable) {
        if (sortField == null) {
            throw new IllegalArgumentException("sortField cannot be null");
        }
        SyncArrayDeque<Reputation> reputations = new SyncArrayDeque<>(pageable.getPageSize());
        List<Query> queryList = new ArrayList<>();
        queryList.add(RangeQuery.of(fn -> fn.field(sortField).gt(JsonData.of("0")))._toQuery());
        Query bQuery = BoolQuery.of(fn -> fn.must(queryList))._toQuery();
        SortOptions sortOptions = SortOptions.of(fn -> fn.field(FieldSort.of(fn2 -> fn2.field(sortField).order(SortOrder.Desc))));
        try {
            SearchResponse<Reputation> response = client.search(t -> t.index(Reputation.INDEX_NAME)
                    .query(bQuery)
                    .sort(sortOptions)
                    .from((pageable.getPageNumber() - 1) * pageable.getPageSize())
                    .size(pageable.getPageSize()), Reputation.class);
            HitsMetadata<Reputation> searchHits = response.hits();

            for (Hit<Reputation> hit : searchHits.hits()) {
                Reputation reputation = hit.source();
                if (aliasCache != null) {
                    reputation.setAlias(aliasCache.getAlias(reputation.getId()));
                }
                reputations.addLast(reputation);
            }
        } catch (Exception ex) {
            logger.error("Exception during reputation search! [" + ex.getMessage() + "]", ex);
        }

        return reputations;
    }

}
