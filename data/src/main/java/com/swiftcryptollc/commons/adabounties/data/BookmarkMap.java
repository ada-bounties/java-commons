package com.swiftcryptollc.commons.adabounties.data;

import com.swiftcryptollc.commons.adabounties.es.data.Bookmark;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
public class BookmarkMap {

    protected String accountId;
    protected Map<String, Bookmark> bookmarkMap = new HashMap<>();

    public BookmarkMap() {

    }

    /**
     * @return the accountId
     */
    public String getAccountId() {
        return accountId;
    }

    /**
     * @param accountId the accountId to set
     */
    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    /**
     * @return the bookmarkMap
     */
    public Map<String, Bookmark> getBookmarkMap() {
        return bookmarkMap;
    }

    /**
     * @param bookmarkMap the bookmarkMap to set
     */
    public void setBookmarkMap(Map<String, Bookmark> bookmarkMap) {
        this.bookmarkMap = bookmarkMap;
    }

    /**
     *
     * @param bookmarks
     */
    public void setBookmarks(Set<Bookmark> bookmarks) {
        for (Bookmark bookmark : bookmarks) {
            this.bookmarkMap.put(bookmark.getBountyId(), bookmark);
        }
    }
}
