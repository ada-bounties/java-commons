package com.swiftcryptollc.commons.adabounties.es.repository;

import com.swiftcryptollc.commons.adabounties.es.data.Notification;
import java.util.List;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
@Repository
public interface NotificationRepository extends ElasticsearchRepository<Notification, String> {

    public void deleteByIdAndAccountId(@Param("id") String id, @Param("accountId") String accountId);

    public void deleteByAccountId(@Param("accountId") String accountId);

    public void deleteAllByIdAndAccountId(@Param("ids") List<String> ids, @Param("accountId") String accountId);

    public Long countByAccountId(String accountId);

    public Long countByAccountIdAndRead(String accountId, Boolean read);

    public List<Notification> findBySrcId(String srcId);
}
