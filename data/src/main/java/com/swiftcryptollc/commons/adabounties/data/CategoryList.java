package com.swiftcryptollc.commons.adabounties.data;

import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
public class CategoryList {

    protected Set<String> categories = new HashSet<>();

    public CategoryList() {
        for (Category cat : Category.values()) {
            categories.add(cat.label);
        }
    }

    /**
     * @return the categories
     */
    public Set<String> getCategories() {
        return categories;
    }

    /**
     * @param categories the categories to set
     */
    public void setCategories(Set<String> categories) {
        this.categories = categories;
    }
}
