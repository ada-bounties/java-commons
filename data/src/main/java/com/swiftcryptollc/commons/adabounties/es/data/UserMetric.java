package com.swiftcryptollc.commons.adabounties.es.data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
@Document(indexName = "user-metric")
@JsonIgnoreProperties({"_class", "_id"})
public class UserMetric implements Serializable {

    @Transient
    private static final long serialVersionUID = 100024L;

    // ID is accountId + timeframe
    @Id
    protected String id;
    @Field(type = FieldType.Keyword)
    protected String accountId;
    @Field
    protected Long openBounties;
    @Field
    protected Long closedBounties;
    @Field
    protected Long claimedBounties;
    @Field
    protected Long expiredBounties;
    @Field
    protected Long staleBounties;
    @Field
    protected Long totalBounties;
    @Field
    protected Double perClmdBty;
    @Field
    protected Long openClaims;
    @Field
    protected Long closedClaims;
    @Field
    protected Long acceptedClaims;
    @Field
    protected Long rejectedClaims;
    @Field
    protected Long totalClaims;
    @Field
    protected Long bountyComments;
    @Field
    protected Long claimComments;
    @Field(type = FieldType.Keyword)
    protected String lastXDays;
    @Field
    protected Long updateTimeMs;
    @Transient
    protected Double searchScore;
    @Transient
    protected Alias alias;
    @Transient
    protected Integer page;
    @Transient
    protected String sortField;
    @Transient
    public final static transient String INDEX_NAME = "user-metric";

    public UserMetric() {
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the accountId
     */
    public String getAccountId() {
        return accountId;
    }

    /**
     * @param accountId the accountId to set
     */
    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    /**
     * @return the openBounties
     */
    public Long getOpenBounties() {
        if (openBounties == null) {
            openBounties = 0L;
        }
        return openBounties;
    }

    /**
     * @param openBounties the openBounties to set
     */
    public void setOpenBounties(Long openBounties) {
        this.openBounties = openBounties;
    }

    /**
     * @return the closedBounties
     */
    public Long getClosedBounties() {
        if (closedBounties == null) {
            closedBounties = 0L;
        }
        return closedBounties;
    }

    /**
     * @param closedBounties the closedBounties to set
     */
    public void setClosedBounties(Long closedBounties) {
        this.closedBounties = closedBounties;
    }

    /**
     * @return the claimedBounties
     */
    public Long getClaimedBounties() {
        if (claimedBounties == null) {
            claimedBounties = 0L;
        }
        return claimedBounties;
    }

    /**
     * @param claimedBounties the claimedBounties to set
     */
    public void setClaimedBounties(Long claimedBounties) {
        this.claimedBounties = claimedBounties;
    }

    /**
     * @return the expiredBounties
     */
    public Long getExpiredBounties() {
        if (expiredBounties == null) {
            expiredBounties = 0L;
        }
        return expiredBounties;
    }

    /**
     * @param expiredBounties the expiredBounties to set
     */
    public void setExpiredBounties(Long expiredBounties) {
        this.expiredBounties = expiredBounties;
    }

    /**
     * @return the staleBounties
     */
    public Long getStaleBounties() {
        if (staleBounties == null) {
            staleBounties = 0L;
        }
        return staleBounties;
    }

    /**
     * @param staleBounties the staleBounties to set
     */
    public void setStaleBounties(Long staleBounties) {
        this.staleBounties = staleBounties;
    }

    /**
     * @return the openClaims
     */
    public Long getOpenClaims() {
        if (openClaims == null) {
            openClaims = 0L;
        }
        return openClaims;
    }

    /**
     * @param openClaims the openClaims to set
     */
    public void setOpenClaims(Long openClaims) {
        this.openClaims = openClaims;
    }

    /**
     * @return the closedClaims
     */
    public Long getClosedClaims() {
        if (closedClaims == null) {
            closedClaims = 0L;
        }
        return closedClaims;
    }

    /**
     * @param closedClaims the closedClaims to set
     */
    public void setClosedClaims(Long closedClaims) {
        this.closedClaims = closedClaims;
    }

    /**
     * @return the acceptedClaims
     */
    public Long getAcceptedClaims() {
        if (acceptedClaims == null) {
            acceptedClaims = 0L;
        }
        return acceptedClaims;
    }

    /**
     * @param acceptedClaims the acceptedClaims to set
     */
    public void setAcceptedClaims(Long acceptedClaims) {
        this.acceptedClaims = acceptedClaims;
    }

    /**
     * @return the rejectedClaims
     */
    public Long getRejectedClaims() {
        if (rejectedClaims == null) {
            rejectedClaims = 0L;
        }
        return rejectedClaims;
    }

    /**
     * @param rejectedClaims the rejectedClaims to set
     */
    public void setRejectedClaims(Long rejectedClaims) {
        this.rejectedClaims = rejectedClaims;
    }

    /**
     * @return the lastXDays
     */
    public String getLastXDays() {
        return lastXDays;
    }

    /**
     * @param lastXDays the lastXDays to set
     */
    public void setLastXDays(String lastXDays) {
        this.lastXDays = lastXDays;
    }

    /**
     * @return the bountyComments
     */
    public Long getBountyComments() {
        if (bountyComments == null) {
            bountyComments = 0L;
        }
        return bountyComments;
    }

    /**
     * @param bountyComments the bountyComments to set
     */
    public void setBountyComments(Long bountyComments) {
        this.bountyComments = bountyComments;
    }

    /**
     * @return the claimComments
     */
    public Long getClaimComments() {
        if (claimComments == null) {
            claimComments = 0L;
        }
        return claimComments;
    }

    /**
     * @param claimComments the claimComments to set
     */
    public void setClaimComments(Long claimComments) {
        this.claimComments = claimComments;
    }

    /**
     * @return the totalBounties
     */
    public Long getTotalBounties() {
        if (totalBounties == null) {
            totalBounties = 0L;
        }
        return totalBounties;
    }

    /**
     * @param totalBounties the totalBounties to set
     */
    public void setTotalBounties(Long totalBounties) {
        this.totalBounties = totalBounties;
    }

    /**
     * @return the perClmdBty
     */
    public Double getPerClmdBty() {
        if (perClmdBty == null) {
            perClmdBty = 0d;
        }
        return perClmdBty;
    }

    /**
     * @param perClmdBty the perClmdBty to set
     */
    public void setPerClmdBty(Double perClmdBty) {
        this.perClmdBty = perClmdBty;
    }

    /**
     * @return the totalClaims
     */
    public Long getTotalClaims() {
        if (totalClaims == null) {
            totalClaims = 0L;
        }
        return totalClaims;
    }

    /**
     * @param totalClaims the totalClaims to set
     */
    public void setTotalClaims(Long totalClaims) {
        this.totalClaims = totalClaims;
    }

    /**
     * @return the searchScore
     */
    public Double getSearchScore() {
        if (searchScore == null) {
            searchScore = 0d;
        }
        return searchScore;
    }

    /**
     * @param searchScore the searchScore to set
     */
    public void setSearchScore(Double searchScore) {
        this.searchScore = searchScore;
    }

    /**
     * @return the alias
     */
    public Alias getAlias() {
        return alias;
    }

    /**
     * @param alias the alias to set
     */
    public void setAlias(Alias alias) {
        this.alias = alias;
    }

    /**
     * @return the page
     */
    public Integer getPage() {
        return page;
    }

    /**
     * @param page the page to set
     */
    public void setPage(Integer page) {
        this.page = page;
    }

    /**
     * @return the sortField
     */
    public String getSortField() {
        return sortField;
    }

    /**
     * @param sortField the sortField to set
     */
    public void setSortField(String sortField) {
        this.sortField = sortField;
    }

    /**
     * @return the updateTimeMs
     */
    public Long getUpdateTimeMs() {
        if (updateTimeMs == null) {
            updateTimeMs = 0L;
        }
        return updateTimeMs;
    }

    /**
     * @param updateTimeMs the updateTimeMs to set
     */
    public void setUpdateTimeMs(Long updateTimeMs) {
        this.updateTimeMs = updateTimeMs;
    }
}
