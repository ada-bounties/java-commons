package com.swiftcryptollc.commons.adabounties.data;

import com.swiftcryptollc.commons.adabounties.es.data.Timeline;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
public class TimelineList {

    protected Integer page;
    protected Set<Timeline> timelines = new HashSet<>();
    
    public TimelineList() {
        
    }

    /**
     * @return the page
     */
    public Integer getPage() {
        return page;
    }

    /**
     * @param page the page to set
     */
    public void setPage(Integer page) {
        this.page = page;
    }

    /**
     * @return the timelines
     */
    public Set<Timeline> getTimelines() {
        return timelines;
    }

    /**
     * @param timelines the timelines to set
     */
    public void setTimelines(Set<Timeline> timelines) {
        this.timelines = timelines;
    }
}
