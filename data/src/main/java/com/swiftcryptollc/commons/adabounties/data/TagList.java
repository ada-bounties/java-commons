package com.swiftcryptollc.commons.adabounties.data;

import com.swiftcryptollc.commons.adabounties.es.data.Tag;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
public class TagList {

    private Set<Tag> tags = new HashSet<>();
    private String category;
    protected String status;
    protected Long maxCount;

    public TagList() {

    }

    public TagList(Set<Tag> tags) {
        this.tags = tags;
    }

    /**
     *
     * @param tag
     */
    public void addTag(Tag tag) {
        tags.add(tag);
    }

    /**
     *
     * @param tag
     * @param noDupe
     */
    public void addTag(Tag tag, Boolean noDupe) {
        if (noDupe) {
            for (Tag checkTag : tags) {
                if (checkTag.getValue().equals(tag.getValue())) {
                    return;
                }
            }
        }
        tags.add(tag);

    }

    /**
     *
     * @param tags
     */
    public void removeTags(Set<Tag> tags) {
        for (Tag tag : tags) {
            this.removeTag(tag);
        }
    }

    /**
     *
     * @param tag
     */
    public void removeTag(Tag tag) {
        Tag tagToRemove = null;
        for (Tag checkTag : tags) {
            if (checkTag.getValue().equals(tag.getValue())) {
                tagToRemove = checkTag;
                break;
            }
        }
        if (tagToRemove != null) {
            tags.remove(tagToRemove);
        }
    }

    /**
     * @return the tags
     */
    public Set<Tag> getTags() {
        return tags;
    }

    /**
     * @param tags the tags to set
     */
    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

    /**
     * @return the category
     */
    public String getCategory() {
        return category;
    }

    /**
     * @param category the category to set
     */
    public void setCategory(String category) {
        this.category = category;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the maxCount
     */
    public Long getMaxCount() {
        return maxCount;
    }

    /**
     * @param maxCount the maxCount to set
     */
    public void setMaxCount(Long maxCount) {
        this.maxCount = maxCount;
    }
}
