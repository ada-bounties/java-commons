package com.swiftcryptollc.commons.adabounties.es;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.SecureRandom;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Map;
import java.util.UUID;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.ssl.SSLContexts;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
public abstract class AbstractElasticsearchClientConfig {

    protected static String host = "127.0.0.1";
    protected static int port = 9200;
    protected static String elasticUser;
    protected static String elasticPassword;
    protected static String encodedApiKey;
    protected static String caCert;
    protected static String serverCert;
    protected static String tlsVersion = "TLSv1.3";
    protected final static Object LOCK = new Object();
    protected final static Logger logger = LoggerFactory.getLogger(AbstractElasticsearchClientConfig.class);

    public AbstractElasticsearchClientConfig() {

    }

    /**
     *
     * @param secretMap
     */
    public static void updateStaticSecrets(Map<String, String> secretMap) {
        logger.info("Updating secrets");
        synchronized (LOCK) {
            try {
                if (secretMap.containsKey("elasticsearch_caCert")) {
                    caCert = secretMap.get("elasticsearch_caCert");
                }
            } catch (Exception ex) {
                logger.error("Could not read the elasticsearch_caCert from the SecretsManager [" + ex.getMessage() + "]");
            }
            try {
                if (secretMap.containsKey("elasticsearch_serverCert")) {
                    serverCert = secretMap.get("elasticsearch_serverCert");
                }
            } catch (Exception ex) {
                logger.error("Could not read the elasticsearch_serverCert from the SecretsManager [" + ex.getMessage() + "]");
            }
            try {
                if (secretMap.containsKey("elasticsearch_encodedApiKey")) {
                    encodedApiKey = secretMap.get("elasticsearch_encodedApiKey");
                }
            } catch (Exception ex) {
                logger.error("Could not read the elasticsearch_encodedApiKey from the SecretsManager [" + ex.getMessage() + "]");
            }
            try {
                if (secretMap.containsKey("elasticsearch_elasticPassword")) {
                    elasticPassword = secretMap.get("elasticsearch_elasticPassword");
                }
            } catch (Exception ex) {
                logger.error("Could not read the elasticPassword from the SecretsManager [" + ex.getMessage() + "]");
            }

            try {
                if (secretMap.containsKey("elasticsearch_elasticUser")) {
                    elasticUser = secretMap.get("elasticsearch_elasticUser");
                }
            } catch (Exception ex) {
                elasticUser = "elastic";
                logger.error("Could not read the elasticUser from the SecretsManager [" + ex.getMessage() + "]");
            }

            try {
                if (secretMap.containsKey("elasticsearch_elasticHost")) {
                    host = secretMap.get("elasticsearch_elasticHost");
                }
            } catch (Exception ex) {
                host = "127.0.0.1";
                logger.error("Could not read the elasticHost from the SecretsManager [" + ex.getMessage() + "]");
            }
            try {
                if (secretMap.containsKey("elasticsearch_elasticPort")) {
                    port = Integer.parseInt(secretMap.get("elasticsearch_elasticPort"));
                }
            } catch (Exception ex) {
                logger.error("Could not read the elasticPort from the SecretsManager [" + ex.getMessage() + "]");
                port = 587;
            }
        }
    }

    /**
     *
     * @return @throws Exception
     */
    public static SSLContext getServerSSLContext() throws Exception {
        KeyStore trustStore = KeyStore.getInstance("JKS");
        trustStore.load(null, null);

        CertificateFactory certFact = CertificateFactory.getInstance("X.509");
        logger.info("Using Server Public Cert [" + serverCert + "]");
        ClassLoader classLoader = AbstractElasticsearchClientConfig.class.getClassLoader();
        InputStream inputStream = classLoader.getResourceAsStream(serverCert);
        BufferedInputStream bis2 = new BufferedInputStream(inputStream);
        KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
        KeyStore keystore = KeyStore.getInstance("PKCS12");
        keystore.load(null, null);
        while (bis2.available() > 0) {
            java.security.cert.Certificate cert = certFact.generateCertificate(bis2);
            logger.info("Adding new Server PKI Cert to In-Memory Truststore");
            String certAlias = UUID.randomUUID().toString();
            trustStore.setCertificateEntry(certAlias, cert);
            keystore.setEntry(certAlias, new KeyStore.TrustedCertificateEntry(cert), null);
        }

        TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
        tmf.init(trustStore);
        TrustManager[] tms = tmf.getTrustManagers();
        kmf.init(keystore, null);
        //       URL resource = getClass().getClassLoader().getResource(serverCert);
        // SSLContext sslContext = TransportUtils.sslContextFromHttpCaCrt(new File(resource.toURI()));
        SSLContext sslContext = SSLContext.getInstance(tlsVersion);
        sslContext.init(kmf.getKeyManagers(), tms, new SecureRandom());
        return sslContext;

    }

    /**
     *
     * @return @throws Exception
     */
    public static SSLContext getSSLContext() throws Exception {

        CertificateFactory certFact = CertificateFactory.getInstance("X.509");
        logger.info("Using CA Cert [" + caCert + "]");

        ClassLoader classLoader = AbstractElasticsearchClientConfig.class.getClassLoader();

        X509Certificate trustedCa;
        try (InputStream is = classLoader.getResourceAsStream(caCert)) {
            trustedCa = (X509Certificate) certFact.generateCertificate(is);
        } catch (Exception ex) {
            logger.error("Could not import the CA! [" + ex.getMessage() + "]");
            trustedCa = null;
        }

        KeyStore trustStore = KeyStore.getInstance("pkcs12");
        trustStore.load(null, null);
        trustStore.setCertificateEntry("ca", trustedCa);
        SSLContextBuilder sslContextBuilder = SSLContexts.custom()
                .loadTrustMaterial(trustStore, null);
        final SSLContext sslContext = sslContextBuilder.build();
        logger.info("Initializing SSL Context");
        TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
        tmf.init(trustStore);
        sslContext.init(null, tmf.getTrustManagers(), new SecureRandom());
        return sslContext;
    }

    /**
     * @return the host
     */
    public static String getHost() {
        return host;
    }

    /**
     * @param aHost the host to set
     */
    public static void setHost(String aHost) {
        host = aHost;
    }

    /**
     * @return the port
     */
    public static int getPort() {
        return port;
    }

    /**
     * @param aPort the port to set
     */
    public static void setPort(int aPort) {
        port = aPort;
    }

    /**
     * @return the elasticUser
     */
    public static String getElasticUser() {
        return elasticUser;
    }

    /**
     * @param aElasticUser the elasticUser to set
     */
    public static void setElasticUser(String aElasticUser) {
        elasticUser = aElasticUser;
    }

    /**
     * @return the elasticPassword
     */
    public static String getElasticPassword() {
        return elasticPassword;
    }

    /**
     * @param aElasticPassword the elasticPassword to set
     */
    public static void setElasticPassword(String aElasticPassword) {
        elasticPassword = aElasticPassword;
    }

    /**
     * @return the tlsVersion
     */
    public static String getTlsVersion() {
        return tlsVersion;
    }

    /**
     * @param aTlsVersion the tlsVersion to set
     */
    public static void setTlsVersion(String aTlsVersion) {
        tlsVersion = aTlsVersion;
    }

    /**
     * @return the encodedApiKey
     */
    public static String getEncodedApiKey() {
        return encodedApiKey;
    }

    /**
     * @param aEncodedApiKey the encodedApiKey to set
     */
    public static void setEncodedApiKey(String aEncodedApiKey) {
        encodedApiKey = aEncodedApiKey;
    }

    /**
     * @return the caCert
     */
    public static String getCaCert() {
        return caCert;
    }

    /**
     * @param aCaCert the caCert to set
     */
    public static void setCaCert(String aCaCert) {
        caCert = aCaCert;
    }

}
