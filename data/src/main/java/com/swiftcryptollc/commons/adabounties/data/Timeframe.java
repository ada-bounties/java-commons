package com.swiftcryptollc.commons.adabounties.data;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
public enum Timeframe {
    DAYS_30("30"),
    DAYS_60("60"),
    DAYS_90("90"),
    DAYS_ALL("-1"),
    DAYS_01("1");

    public final String label;

    private Timeframe(String label) {
        this.label = label;
    }

    public static Timeframe valueOfLabel(String label) {
        for (Timeframe type : values()) {
            if (type.label.equalsIgnoreCase(label)) {
                return type;
            }
        }
        return null;
    }

    /**
     *
     * @return
     */
    public String toString() {
        return label;
    }
}
