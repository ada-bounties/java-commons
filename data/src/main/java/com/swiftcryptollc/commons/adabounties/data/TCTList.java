package com.swiftcryptollc.commons.adabounties.data;

import com.swiftcryptollc.commons.adabounties.es.data.TokenClaimTotal;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
public class TCTList {

    protected String lastXDays;
    protected String accountId;
    protected Integer page;
    protected Long count;
    protected Set<TokenClaimTotal> tcts = new HashSet<>();

    public TCTList() {

    }

    /**
     * @return the page
     */
    public Integer getPage() {
        return page;
    }

    /**
     * @param page the page to set
     */
    public void setPage(Integer page) {
        this.page = page;
    }

    /**
     * @return the count
     */
    public Long getCount() {
        return count;
    }

    /**
     * @param count the count to set
     */
    public void setCount(Long count) {
        this.count = count;
    }

    /**
     * @return the accountId
     */
    public String getAccountId() {
        return accountId;
    }

    /**
     * @param accountId the accountId to set
     */
    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    /**
     * @return the lastXDays
     */
    public String getLastXDays() {
        return lastXDays;
    }

    /**
     * @param lastXDays the lastXDays to set
     */
    public void setLastXDays(String lastXDays) {
        this.lastXDays = lastXDays;
    }

    /**
     * @return the tcts
     */
    public Set<TokenClaimTotal> getTcts() {
        return tcts;
    }

    /**
     * @param tcts the tcts to set
     */
    public void setTcts(Set<TokenClaimTotal> tcts) {
        this.tcts = tcts;
    }
}
