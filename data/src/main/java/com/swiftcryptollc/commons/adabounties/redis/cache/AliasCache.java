package com.swiftcryptollc.commons.adabounties.redis.cache;

import com.swiftcryptollc.commons.adabounties.es.data.Alias;
import com.swiftcryptollc.commons.adabounties.es.repository.AliasRepository;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
@Service
public class AliasCache {

    @Autowired
    private AliasRepository aliasRepo;

    protected final static Logger logger = LoggerFactory.getLogger(AliasCache.class);

    public AliasCache() {

    }

    /**
     *
     * @param alias
     * @return
     */
    @CachePut(value = "alias", key = "#alias.id")
    public Alias updateAlias(Alias alias) {
        // do not act on the blocked aliases
        if (!alias.getId().startsWith("BLOCKED")) {
            return aliasRepo.save(alias);
        }
        return null;
    }

    /**
     *
     * @param id
     * @return
     */
    @Cacheable(value = "alias", key = "#id")
    public Alias getAlias(String id) {
        if ((id == null) || (id.startsWith("BLOCKED"))) {
            throw new IllegalArgumentException("Id cannot be null or one of the BLOCKED aliases");
        }
        Optional<Alias> aliasOpt = aliasRepo.findById(id);
        if (aliasOpt.isPresent()) {
            return aliasOpt.get();
        } else {
            logger.warn("Alias for account [" + id + "] does not exist");
        }

        return null;
    }

}
