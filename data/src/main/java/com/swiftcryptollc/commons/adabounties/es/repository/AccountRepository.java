package com.swiftcryptollc.commons.adabounties.es.repository;

import com.swiftcryptollc.commons.adabounties.es.data.Account;
import java.util.Optional;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
@Repository
public interface AccountRepository extends ElasticsearchRepository<Account, String> {

    public Optional<Account> findByEmail(String email);

    public Optional<Account> findByChallenge(String challenge);

}
