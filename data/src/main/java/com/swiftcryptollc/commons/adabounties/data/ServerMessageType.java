package com.swiftcryptollc.commons.adabounties.data;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
public enum ServerMessageType {
    ACCOUNT("acc"),
    BOUNTY("bty"),
    CLAIM("clm"),
    REACTION("rct"),
    USER_AGENT("agent"),
    TAG("tag"),
    TAG_LIST("tagl"),
    CONTACT_US("cntct"),
    JWT("jwt"),
    OPTION("opt"),
    COMMENT("cmt"),
    CARDANO("ada"),
    TIMELINE("tl"),
    TOKEN("tk"),
    ALIAS("al"),
    SEARCH("srch"),
    BOOKMARK("bkm"),
    USER_METRIC("um"),
    SYSTEM_METRIC("sm"),
    TBT("tbt"),
    TCT("tct"),
    REPUTATION("rep"),
    NOTIFICATION("ntfy"),
    LEADERBOARD("lb"),
    ACCOUNTING("atg");

    public final String label;

    private ServerMessageType(String label) {
        this.label = label;
    }

    public static ServerMessageType valueOfLabel(String label) {
        for (ServerMessageType type : values()) {
            if (type.label.equalsIgnoreCase(label)) {
                return type;
            }
        }
        return null;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return label;
    }
}
