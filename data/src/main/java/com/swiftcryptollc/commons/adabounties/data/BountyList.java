package com.swiftcryptollc.commons.adabounties.data;

import com.swiftcryptollc.commons.adabounties.es.data.Bounty;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
public class BountyList {

    protected Integer page;
    protected Long count;
    protected Set<Bounty> bounties = new HashSet<>();

    public BountyList() {

    }

    /**
     * @return the page
     */
    public Integer getPage() {
        return page;
    }

    /**
     * @param page the page to set
     */
    public void setPage(Integer page) {
        this.page = page;
    }

    /**
     * @return the bounties
     */
    public Set<Bounty> getBounties() {
        return bounties;
    }

    /**
     * @param bounties the bounties to set
     */
    public void setBounties(Set<Bounty> bounties) {
        this.bounties = bounties;
    }

    /**
     * @return the count
     */
    public Long getCount() {
        return count;
    }

    /**
     * @param count the count to set
     */
    public void setCount(Long count) {
        this.count = count;
    }
}
