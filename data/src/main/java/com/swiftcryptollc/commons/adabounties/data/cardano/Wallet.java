package com.swiftcryptollc.commons.adabounties.data.cardano;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.swiftcryptollc.commons.adabounties.es.data.Bounty;
import com.swiftcryptollc.commons.adabounties.es.data.Claim;
import java.util.List;

public class Wallet {

    @SerializedName("network")
    @Expose
    private Integer network;
    @SerializedName("sending_stake_addr")
    @Expose
    private String sendingStakeAddr;
    @SerializedName("bech32_stake_addr")
    @Expose
    private String bech32StakeAddr;
    @SerializedName("change_address")
    @Expose
    protected String changeAddress;
    @SerializedName("inputs")
    @Expose
    private List<Object> inputs = null;
    @SerializedName("sending_wal_addrs")
    @Expose
    private List<String> sendingWalAddrs = null;
    @SerializedName("collateral")
    @Expose
    private List<Object> collateral = null;
    @SerializedName("maskedBalance")
    @Expose
    private String maskedBalance;
    @SerializedName("bounty")
    @Expose
    protected Bounty bounty;
    @SerializedName("claim")
    @Expose
    protected Claim claim;
    

    public Integer getNetwork() {
        return network;
    }

    public void setNetwork(Integer network) {
        this.network = network;
    }

    public Wallet withNetwork(Integer network) {
        this.network = network;
        return this;
    }

    public String getSendingStakeAddr() {
        return sendingStakeAddr;
    }

    public void setSendingStakeAddr(String sendingStakeAddr) {
        this.sendingStakeAddr = sendingStakeAddr;
    }

    public Wallet withSendingStakeAddr(String sendingStakeAddr) {
        this.sendingStakeAddr = sendingStakeAddr;
        return this;
    }

    public String getBech32StakeAddr() {
        return bech32StakeAddr;
    }

    public void setBech32StakeAddr(String bech32StakeAddr) {
        this.bech32StakeAddr = bech32StakeAddr;
    }

    public Wallet withBech32StakeAddr(String bech32StakeAddr) {
        this.bech32StakeAddr = bech32StakeAddr;
        return this;
    }

    public List<Object> getInputs() {
        return inputs;
    }

    public void setInputs(List<Object> inputs) {
        this.inputs = inputs;
    }

    public Wallet withInputs(List<Object> inputs) {
        this.inputs = inputs;
        return this;
    }

    public List<String> getSendingWalAddrs() {
        return sendingWalAddrs;
    }

    public void setSendingWalAddrs(List<String> sendingWalAddrs) {
        this.sendingWalAddrs = sendingWalAddrs;
    }

    public Wallet withSendingWalAddrs(List<String> sendingWalAddrs) {
        this.sendingWalAddrs = sendingWalAddrs;
        return this;
    }

    public List<Object> getCollateral() {
        return collateral;
    }

    public void setCollateral(List<Object> collateral) {
        this.collateral = collateral;
    }

    public Wallet withCollateral(List<Object> collateral) {
        this.collateral = collateral;
        return this;
    }

    public String getMaskedBalance() {
        return maskedBalance;
    }

    public void setMaskedBalance(String maskedBalance) {
        this.maskedBalance = maskedBalance;
    }

    public Wallet withMaskedBalance(String maskedBalance) {
        this.maskedBalance = maskedBalance;
        return this;
    }

    /**
     * @return the changeAddress
     */
    public String getChangeAddress() {
        return changeAddress;
    }

    /**
     * @param changeAddress the changeAddress to set
     */
    public void setChangeAddress(String changeAddress) {
        this.changeAddress = changeAddress;
    }

    /**
     * @return the bounty
     */
    public Bounty getBounty() {
        return bounty;
    }

    /**
     * @param bounty the bounty to set
     */
    public void setBounty(Bounty bounty) {
        this.bounty = bounty;
    }

    /**
     * @return the claim
     */
    public Claim getClaim() {
        return claim;
    }

    /**
     * @param claim the claim to set
     */
    public void setClaim(Claim claim) {
        this.claim = claim;
    }
}
