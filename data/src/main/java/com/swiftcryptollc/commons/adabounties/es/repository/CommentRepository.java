package com.swiftcryptollc.commons.adabounties.es.repository;

import com.swiftcryptollc.commons.adabounties.es.data.Comment;
import java.util.List;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
@Repository
public interface CommentRepository extends ElasticsearchRepository<Comment, String> {

    public List<Comment> findByAccountId(String accountId);

    public List<Comment> findByBountyId(String bountyId);

    public Long countByBountyId(String bountyId);

    public Long countByAccountId(String accountId);

    public Long countByAccountIdAndPostedMsGreaterThan(String accountId, Long timeMs);

    public List<Comment> findByParentId(String bountyId);
}
