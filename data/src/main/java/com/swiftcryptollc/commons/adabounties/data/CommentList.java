package com.swiftcryptollc.commons.adabounties.data;

import com.swiftcryptollc.commons.adabounties.es.data.Comment;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
public class CommentList {

    protected Integer page;
    protected Long count;
    protected Boolean isBounty;
    protected List<Comment> comments = new ArrayList<>();

    public CommentList() {

    }

    /**
     * @return the page
     */
    public Integer getPage() {
        return page;
    }

    /**
     * @param page the page to set
     */
    public void setPage(Integer page) {
        this.page = page;
    }

    /**
     * @return the count
     */
    public Long getCount() {
        return count;
    }

    /**
     * @param count the count to set
     */
    public void setCount(Long count) {
        this.count = count;
    }

    /**
     * @return the comments
     */
    public List<Comment> getComments() {
        return comments;
    }

    /**
     * @param comments the comments to set
     */
    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    /**
     * @return the isBounty
     */
    public Boolean getIsBounty() {
        return isBounty;
    }

    /**
     * @param isBounty the isBounty to set
     */
    public void setIsBounty(Boolean isBounty) {
        this.isBounty = isBounty;
    }
}
