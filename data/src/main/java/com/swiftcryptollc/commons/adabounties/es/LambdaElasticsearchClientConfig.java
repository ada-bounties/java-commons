package com.swiftcryptollc.commons.adabounties.es;

import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.json.jackson.JacksonJsonpMapper;
import co.elastic.clients.transport.ElasticsearchTransport;
import co.elastic.clients.transport.rest_client.RestClientTransport;
import com.swiftcryptollc.commons.adabounties.aws.secrets.SecretsManager;
import java.util.Map;
import javax.net.ssl.SSLContext;
import org.apache.http.Header;
import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.nio.client.HttpAsyncClientBuilder;
import org.apache.http.message.BasicHeader;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder.HttpClientConfigCallback;
import org.springframework.context.annotation.Bean;

/**
 *
 * https://reflectoring.io/spring-boot-elasticsearch/
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
public class LambdaElasticsearchClientConfig extends AbstractElasticsearchClientConfig {

    static {
        SecretsManager sm = new SecretsManager(true);
        Map<String, String> secretMap = SecretsManager.getSecretMap();
        updateStaticSecrets(secretMap);
    }

    /**
     *
     * @return
     */
    @Bean
    public static ElasticsearchClient elasticSearchClient() {
        ElasticsearchTransport transport = null;
        try {
            if (encodedApiKey != null) {
                SSLContext sslContext;
                if (serverCert != null) {
                    sslContext = getServerSSLContext();
                } else {
                    sslContext = getSSLContext();
                }
                Header[] defaultHeaders
                        = new Header[]{new BasicHeader("Authorization",
                                    "ApiKey " + encodedApiKey)};

                final CredentialsProvider credentialsProvider
                        = new BasicCredentialsProvider();
                credentialsProvider.setCredentials(AuthScope.ANY,
                        new UsernamePasswordCredentials(elasticUser, elasticPassword));
                RestClient restClient = RestClient.builder(
                        new HttpHost(host, port, "https"))
                        .setDefaultHeaders(defaultHeaders)
                        .setHttpClientConfigCallback(new HttpClientConfigCallback() {
                            @Override
                            public HttpAsyncClientBuilder customizeHttpClient(
                                    HttpAsyncClientBuilder httpClientBuilder) {
                                return httpClientBuilder
                                        .setSSLContext(sslContext)
                                        .setDefaultCredentialsProvider(credentialsProvider);
                            }
                        }).build();

                transport = new RestClientTransport(restClient, new JacksonJsonpMapper());
            } else {
                final CredentialsProvider credentialsProvider
                        = new BasicCredentialsProvider();
                credentialsProvider.setCredentials(AuthScope.ANY,
                        new UsernamePasswordCredentials(elasticUser, elasticPassword));
                RestClient restClient = RestClient.builder(
                        new HttpHost(host, port))
                        .setHttpClientConfigCallback(new HttpClientConfigCallback() {
                            @Override
                            public HttpAsyncClientBuilder customizeHttpClient(
                                    HttpAsyncClientBuilder httpClientBuilder) {
                                return httpClientBuilder
                                        .setDefaultCredentialsProvider(credentialsProvider);
                            }
                        }).build();

                transport = new RestClientTransport(restClient, new JacksonJsonpMapper());
            }
        } catch (Exception ex) {
            logger.error("Exception while trying to create the ElasticsearchAsyncClient! [" + ex.getMessage() + "]", ex);
        }
        return new ElasticsearchClient(transport);
    }

}
