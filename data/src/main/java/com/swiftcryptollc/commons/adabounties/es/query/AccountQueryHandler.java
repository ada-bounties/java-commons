package com.swiftcryptollc.commons.adabounties.es.query;

import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.elasticsearch._types.FieldSort;
import co.elastic.clients.elasticsearch._types.SortOptions;
import co.elastic.clients.elasticsearch._types.SortOrder;
import co.elastic.clients.elasticsearch._types.query_dsl.BoolQuery;
import co.elastic.clients.elasticsearch._types.query_dsl.Query;
import co.elastic.clients.elasticsearch._types.query_dsl.RangeQuery;

import co.elastic.clients.elasticsearch.core.SearchResponse;
import co.elastic.clients.elasticsearch.core.search.Hit;
import co.elastic.clients.elasticsearch.core.search.HitsMetadata;
import co.elastic.clients.json.JsonData;
import com.swiftcryptollc.commons.adabounties.es.ElasticsearchClientConfig;
import com.swiftcryptollc.commons.adabounties.es.ElasticsearchClientListener;
import com.swiftcryptollc.commons.adabounties.es.data.Account;
import com.swiftcryptollc.commons.adabounties.es.data.SystemMetric;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import javax.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
@Service
public final class AccountQueryHandler implements ElasticsearchClientListener {

    @Autowired
    protected ElasticsearchClientConfig esClient;
    protected ElasticsearchClient client;
    protected UUID id;
    protected final SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
    protected final static Logger logger = LoggerFactory.getLogger(AccountQueryHandler.class);

    /**
     *
     */
    public AccountQueryHandler() {
    }

    /**
     *
     * @param client
     */
    public AccountQueryHandler(ElasticsearchClient client) {
        this.client = client;
    }

    /**
     * @return the id
     */
    @Override
    public UUID getId() {
        if (id == null) {
            id = UUID.randomUUID();
        }
        return id;
    }

    @Override
    public void setClient() {
        if (esClient != null) {
            client = esClient.elasticSearchClient();
        }
    }

    /**
     *
     */
    @PostConstruct
    public void init() {
        if (client == null) {
            ElasticsearchClientConfig.subscribe(this);
        }
    }

    /**
     * Get account Ids
     *
     * @param pageable
     * @return
     */
    public Set<Account> findAllAccounts(PageRequest pageable) {
        Set<Account> accounts = new HashSet<>();
        SortOptions sortOptions = SortOptions.of(fn
                -> fn.field(FieldSort.of(fn2 -> fn2.field("sinceMS").order(SortOrder.Desc))));
        try {
            SearchResponse<Account> response = client.search(t
                    -> t.index(Account.INDEX_NAME)
                            .sort(sortOptions)
                            .from((pageable.getPageNumber() - 1) * pageable.getPageSize())
                            .size(pageable.getPageSize()), Account.class);

            HitsMetadata<Account> searchHits = response.hits();

            for (Hit<Account> hit : searchHits.hits()) {
                accounts.add(hit.source());
            }
        } catch (Exception ex) {
            logger.error("Exception during findAllAccounts! [" + ex.getMessage() + "]");
        }
        return accounts;
    }

    /**
     * @param lessThanTimeMs
     * @param greaterThanTimeMs
     * @param timeframeMs
     * @param systemMetric
     */
    public synchronized void getTotalAccountsFrom(Long lessThanTimeMs, Long greaterThanTimeMs, Long timeframeMs, SystemMetric systemMetric) {
        SearchResponse<Account> response;
        try {
            if (greaterThanTimeMs > 0L) {
                List<Query> queryList = new ArrayList<>();
                queryList.add(RangeQuery.of(fn -> fn.field("sinceMS").lt(JsonData.of(lessThanTimeMs)))._toQuery());
                queryList.add(RangeQuery.of(fn -> fn.field("sinceMS").gte(JsonData.of(greaterThanTimeMs)))._toQuery());
                Query bQuery = BoolQuery.of(fn -> fn.must(queryList))._toQuery();
                response = client.search(t
                        -> t.index(Account.INDEX_NAME).query(bQuery)
                                .size(0),
                        Account.class);
            } else {
                response = client.search(t
                        -> t.index(Account.INDEX_NAME)
                                .size(0),
                        Account.class);
            }
            HitsMetadata<Account> searchHits = response.hits();
            Long totalAccounts = searchHits.total().value();
            systemMetric.setTotalUsers(totalAccounts);
            logger.info("[" + systemMetric.getLastXDays() + "] Total new accounts [" + totalAccounts + "]");

            if (greaterThanTimeMs > 0L) {
                Long newFromTimeMs = greaterThanTimeMs - timeframeMs;
                List<Query> queryList2 = new ArrayList<>();
                queryList2.add(RangeQuery.of(fn -> fn.field("sinceMS").lt(JsonData.of(greaterThanTimeMs)))._toQuery());
                queryList2.add(RangeQuery.of(fn -> fn.field("sinceMS").gte(JsonData.of(newFromTimeMs)))._toQuery());
                Query bQuery2 = BoolQuery.of(fn -> fn.must(queryList2))._toQuery();
                SearchResponse<Account> response2 = client.search(t
                        -> t.index(Account.INDEX_NAME).query(bQuery2)
                                .size(0),
                        Account.class);
                HitsMetadata<Account> searchHits2 = response2.hits();
                Long totalAccounts2 = searchHits2.total().value();
                logger.info("[" + systemMetric.getLastXDays() + "] Prev Total new accounts [" + totalAccounts2 + "]");
                if (totalAccounts2 > 0L) {
                    Double perGrowth = ((totalAccounts / totalAccounts2) - 1d) * 100d;
                    systemMetric.setGrowthTotalUsers(perGrowth);
                    logger.info("[" + systemMetric.getLastXDays() + "] Per growth total accounts [" + perGrowth + "%]");
                } else {
                    Double perGrowth = totalAccounts * 100d;
                    systemMetric.setGrowthTotalUsers(perGrowth);
                    logger.info("[" + systemMetric.getLastXDays() + "] Per growth total accounts [" + perGrowth + "%]");
                }
            }

        } catch (Exception ex) {
            logger.error("Exception during getTotalAccountsFrom! [" + ex.getMessage() + "]");
        }
    }

    /**
     * @param esClient the esClient to set
     */
    public void setEsClient(ElasticsearchClientConfig esClient) {
        this.esClient = esClient;
    }

    /**
     * @param client the client to set
     */
    public void setClient(ElasticsearchClient client) {
        this.client = client;
    }

}
