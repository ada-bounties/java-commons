package com.swiftcryptollc.commons.adabounties.utilities.data;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
public enum ActionType {
    BOUNTY_NEW("btynew"),
    BOUNTY_UPDATE("btyupd"),
    BOOKMARKED_BOUNTY_UPDATE("bmbtyupd"),
    BOUNTY_COMMENT("btycmt"),
    BOOKMARKED_BOUNTY_COMMENT("bmbtycmt"),
    BOUNTY_COMMENT_UPDATE("btycmtupd"),
    BOOKMARKED_BOUNTY_COMMENT_UPDATE("bmbtycmtupd"),
    BOUNTY_COMMENT_REPLY("btycmtrpy"),
    BOUNTY_COMMENT_REACT("btycmtrct"),
    BOUNTY_REACT("btyrct"),
    BOUNTY_FAULT("btyflt"),
    BOOKMARKED_BOUNTY_REACT("bmbtyrct"),
    BOOKMARKED_BOUNTY_CLOSED("bmbtycls"),
    BOOKMARKED_BOUNTY_CLAIMED("bmbtyclm"),
    BOUNTY_CLOSED("btycls"),
    BOUNTY_CLAIMED("btyclm"),
    CLAIM_NEW("clmnew"),
    CLAIM_UPDATE("clmupd"),
    CLAIM_COMMENT("clmcmt"),
    CLAIM_COMMENT_UPDATE("clmcmtupd"),
    CLAIM_COMMENT_REPLY("clmcmtrpy"),
    CLAIM_ACCEPT("clmacpt"),
    CLAIM_REJECT("clmrjt"),
    CLAIM_FAULT("clmflt"),
    CLAIM_CLOSED("clmcls"),
    ACCOUNT_NEW("accnew");

    public final String label;

    private ActionType(String label) {
        this.label = label;
    }

    public static ActionType valueOfLabel(String label) {
        for (ActionType type : values()) {
            if (type.label.equalsIgnoreCase(label)) {
                return type;
            }
        }
        return null;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return label;
    }

}
