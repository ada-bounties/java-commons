package com.swiftcryptollc.commons.adabounties.utilities.system;

import com.swiftcryptollc.commons.adabounties.utilities.data.Base64Id;
import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
public class HostnameUtility {

    public final static String getHostname() {
        String hostname = null;
        try {
            Process hostnameProc = new ProcessBuilder().command("hostname").start();
            InputStreamReader isr = new InputStreamReader(hostnameProc.getInputStream());
            BufferedReader br = new BufferedReader(isr);
            String line = "";
            while (((line = br.readLine()) != null) && (hostname == null)) {
                line = line.trim();
                hostname = line;
            }
        } catch (Exception ex) {
            hostname = Base64Id.getNewId();
        }
        return hostname;
    }
}
