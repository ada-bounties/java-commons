package com.swiftcryptollc.commons.adabounties.utilities.handler;

import com.swiftcryptollc.commons.adabounties.utilities.data.SyncArrayDeque;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Runnable that will process a FIFO queue of Objects T
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
public abstract class ObjectHandler<T> implements Runnable {

    protected final String id = UUID.randomUUID().toString();
    protected Thread myThread;
    protected int checkSleepMS = 20;
    protected boolean enabled = true;
    protected boolean drain = false;
    protected boolean stopped = false;
    protected boolean needsInitialization = false;
    protected SyncArrayDeque<T> objectDeque = new SyncArrayDeque<>(5000);
    protected static Logger logger = LoggerFactory.getLogger(ObjectHandler.class);

    public ObjectHandler() {
        init();
    }

    public ObjectHandler(boolean needsInitialization) {
        this.needsInitialization = needsInitialization;
        init();
    }

    private void init() {
        logger.info("Starting thread for [" + id + "]");
        myThread = new Thread(this);
        myThread.start();
    }

    /**
     *
     */
    @Override
    public void run() {
        logger.info("[" + id + "] Started the Handler");
        while ((enabled) && (needsInitialization)) {
            try {
                Thread.sleep(1000);
            } catch (Exception ex) {

            }
        }
        logger.info("[" + id + "] Ready to process");
        while (enabled || drain) {
            T object = objectDeque.pollFirst();
            if (object != null) {
                try {
                    processObject(object);
                } catch (Exception ex) {
                    logger.error("[" + id + "] Could not process the Object! [" + ex.getMessage() + "]", ex);
                }
            } else if (enabled) {
                try {
                    Thread.sleep(checkSleepMS);
                } catch (Exception ex) {

                }
            } else if (objectDeque.isEmpty()) {
                drain = false;
            }
        }
        stopped = true;
        logger.info("[" + id + "] Stopped the Handler!");
    }

    /**
     *
     * @param object
     */
    public void addToQueue(T object) {
        if (enabled) {
            objectDeque.addLast(object);
        }
    }

    /**
     *
     * @param objects
     */
    public void addAllToQueue(List<T> objects) {
        if (enabled) {
            objectDeque.addAll(objects);
        }
    }

    /**
     *
     * @param objects
     */
    public void addAllToQueue(Set<T> objects) {
        if (enabled) {
            objectDeque.addAll(objects);
        }
    }

    /**
     *
     * @param object
     */
    protected abstract void processObject(T object);

    /**
     *
     * @param drain
     */
    public void stop(boolean drain) {
        this.drain = drain;
        this.enabled = false;
        int sleeps = 0;
        if (this.drain) {
            while ((!stopped) || (++sleeps > 1000)) {
                try {
                    Thread.sleep(200);
                } catch (Exception e) {
                }
            }
        }
    }

    /**
     * @return the enabled
     */
    public boolean isEnabled() {
        return enabled;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     *
     * @param needsInitialization
     */
    public void setNeedsInitialization(boolean needsInitialization) {
        this.needsInitialization = needsInitialization;
    }
}
