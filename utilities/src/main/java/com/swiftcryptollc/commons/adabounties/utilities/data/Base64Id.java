package com.swiftcryptollc.commons.adabounties.utilities.data;

import java.nio.ByteBuffer;
import java.util.UUID;
import org.apache.commons.codec.binary.Base64;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
public class Base64Id {

    /**
     *
     * @return
     */
    public static String getNewId() {
        UUID uuid = UUID.randomUUID();
        Base64 base64 = new Base64();
        ByteBuffer bb = ByteBuffer.wrap(new byte[16]);
        bb.putLong(uuid.getMostSignificantBits());
        bb.putLong(uuid.getLeastSignificantBits());
        return base64.encodeBase64URLSafeString(bb.array()).replace("/", "-").replace("+", "_");
    }

    /**
     * Convert to base64
     *
     * @param uuid
     * @return
     */
    public static String uuidToBase64(UUID uuid) {
        Base64 base64 = new Base64();
        ByteBuffer bb = ByteBuffer.wrap(new byte[16]);
        bb.putLong(uuid.getMostSignificantBits());
        bb.putLong(uuid.getLeastSignificantBits());
        return base64.encodeBase64URLSafeString(bb.array()).replace("/", "-").replace("+", "_");
    }

    /**
     * Convert to UUID
     *
     * @param str
     * @return
     */
    public static UUID uuidFromBase64(String str) {
        str = str.replace("-", "/").replace("_", "+");
        Base64 base64 = new Base64();
        byte[] bytes = base64.decodeBase64(str);
        ByteBuffer bb = ByteBuffer.wrap(bytes);
        UUID uuid = new UUID(bb.getLong(), bb.getLong());
        return uuid;
    }

}
