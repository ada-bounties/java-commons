package com.swiftcryptollc.commons.adabounties.utilities.timers;

import java.time.Clock;
import java.time.Duration;
import java.time.ZonedDateTime;
import java.util.UUID;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
public class ScheduledTask {

    private UUID uuid = null;
    private TimeoutTask task;

    /**
     * Construct a new task with the callback interface
     *
     * @param task
     */
    public ScheduledTask(TimeoutTask task) {
        this.task = task;
        uuid = UUID.randomUUID();
    }

    /**
     *
     * @param task
     * @param uuid
     */
    public ScheduledTask(TimeoutTask task, UUID uuid) {
        this.task = task;
        this.uuid = uuid;
    }

    /**
     * @return the uuid
     */
    public UUID getUuid() {
        return uuid;
    }

    /**
     * @param uuid the uuid to set
     */
    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    /**
     *
     * @param hour
     */
    public ScheduledExecutorService schedule(int hour) throws IllegalArgumentException {
        if ((hour < 0) || (hour > 23)) {
            throw new IllegalArgumentException("Valid hours are only 0 to 23");
        }
        ZonedDateTime now = ZonedDateTime.now(Clock.systemUTC());
        ZonedDateTime nextRun = now.withHour(hour).withMinute(0).withSecond(0);
        if (now.compareTo(nextRun) > 0) {
            nextRun = nextRun.plusDays(1);
        }

        Duration duration = Duration.between(now, nextRun);
        long initialDelay = duration.getSeconds();

        ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
        scheduler.scheduleAtFixedRate(task,
                initialDelay,
                TimeUnit.DAYS.toSeconds(1),
                TimeUnit.SECONDS);
        
        return scheduler;
    }
}
