package com.swiftcryptollc.commons.adabounties.utilities.cardano;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.net.URI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
public class CardanoTxUtils {

    private ObjectMapper mapper = new ObjectMapper();
    public String bfProjectId;
    public String bfTxCheckUrl;
    public String bfUrl;
//    public QuickTxBuilder quickTxBuilder;
//    public BackendService backendService;
    protected final static Logger logger = LoggerFactory.getLogger(CardanoTxUtils.class);

    public CardanoTxUtils(String bfUrl, String bfProjectId, String bfTxCheckUrl) {
        this.bfUrl = bfUrl;
        this.bfProjectId = bfProjectId;
        this.bfTxCheckUrl = bfTxCheckUrl;
        logger.info("Setup CardanoTxUtils with [" + this.bfTxCheckUrl + "] and [" + this.bfProjectId + "]");
//        backendService = new BFBackendService(bfUrl, bfProjectId);
//        quickTxBuilder = new QuickTxBuilder(backendService);
    }

    /**
     *
     * @param txHash
     * @return
     */
    public Boolean checkTx(String txHash) {
        Boolean found = false;
        logger.info("Checking TX for [" + txHash + "]");
        try {
            StringBuilder urlBuilder = new StringBuilder(bfTxCheckUrl);
            if (!bfTxCheckUrl.endsWith("/")) {
                urlBuilder.append("/");
            }
            urlBuilder.append(txHash);
            URI uri = new URI(urlBuilder.toString());
            HttpHeaders headers = new HttpHeaders();
            headers.set("project_id", bfProjectId);
            HttpEntity<String> httpTxEntity = new HttpEntity<>(null, headers);
            RestTemplate restTxTemplate = new RestTemplate();
            ResponseEntity txEntity = restTxTemplate.exchange(uri, HttpMethod.GET, httpTxEntity, String.class);
            String resBody = txEntity.getBody().toString();
            if (resBody.contains("status_code")) {
                logger.warn("TX [" + txHash + "] is not on chain..");
                found = false;
            } else {
                found = true;
                logger.warn("TX [" + txHash + "] found on chain!");
            }
        } catch (Exception e) {
            logger.warn("Could not check TX [" + txHash + "] [" + e.getMessage() + "]");
        }
        return found;
    }

    /**
     * @param priceURL
     * @return
     */
    public Double getADAPrice(String priceURL) {
        Double adaPrice = -1d;
        int tries = 0;
        do {
            try {
                URI uri = new URI(priceURL);
                HttpHeaders headers = new HttpHeaders();
                HttpEntity<String> httpTxEntity = new HttpEntity<>(null, headers);
                RestTemplate restTxTemplate = new RestTemplate();
                ResponseEntity txEntity = restTxTemplate.exchange(uri, HttpMethod.GET, httpTxEntity, String.class);
                String jsonBody = txEntity.getBody().toString();
                JsonNode node = mapper.readTree(jsonBody);
                String adaStringPrice = node.get("data").get("rates").get("USD").toString().replaceAll("\"", "");
                adaPrice = Double.parseDouble(adaStringPrice);
            } catch (Exception e) {
                logger.error("Could not get the price data for ADA [" + e.getMessage() + "]", e);
                try {
                    Thread.sleep(1000);
                } catch (Exception e2) {
                }
            }
        } while ((adaPrice == -1d) && (++tries < 3));
        return adaPrice;
    }
}
