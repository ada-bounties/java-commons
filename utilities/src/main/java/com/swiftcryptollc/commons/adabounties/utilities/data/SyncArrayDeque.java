package com.swiftcryptollc.commons.adabounties.utilities.data;

import java.util.ArrayDeque;
import java.util.Collection;


public class SyncArrayDeque<T> extends ArrayDeque<T> {

    private final int numElements;
    private final Object LOCK = new Object();

    /**
     * Constructor with a set number of elements
     *
     * @param numElements
     */
    public SyncArrayDeque(int numElements) {
        super(numElements);
        this.numElements = numElements;
    }

    /**
     * Poll for an object
     *
     * @return
     */
    @Override
    public T poll() {
        T returnTObj = null;
        synchronized (LOCK) {
            returnTObj = super.poll();
        }
        return returnTObj;
    }

    /**
     * Poll for the first element in the list
     *
     * @return
     */
    @Override
    public T pollFirst() {
        T returnTObj = null;
        synchronized (LOCK) {
            returnTObj = super.pollFirst();
        }
        return returnTObj;
    }

    /**
     * Poll for the last element in the list
     *
     * @return
     */
    @Override
    public T pollLast() {
        T returnTObj = null;
        synchronized (LOCK) {
            returnTObj = super.pollLast();
        }
        return returnTObj;
    }

    /**
     * Push this element into the first position of this deque
     *
     * @param t
     */
    @Override
    public void push(T t) {
        synchronized (LOCK) {
            addFirst(t);
        }
    }

    /**
     * Add this object to the end of the list
     *
     * @param t
     * @return
     */
    @Override
    public boolean add(T t) {
        synchronized (LOCK) {
            addLast(t);
        }
        return true;
    }

    /**
     * Add all elements from this collection to this deque
     *
     * @param c
     * @return
     */
    @Override
    public boolean addAll(Collection<? extends T> c) throws IllegalArgumentException {
        boolean added = false;
        if (c.size() > numElements) {
            throw new IllegalArgumentException("Collection to be added exceeds this deque size!");
        }
        synchronized (LOCK) {
            int totalSize = c.size() + size();
            while (totalSize >= numElements) {
                try {
                    removeLast();
                } catch (Exception ex) {

                }
                --totalSize;
            }
            added = super.addAll(c);
        }
        return added;
    }

    /**
     * Add this object as the first element in this deque
     *
     * @param t
     */
    @Override
    public void addFirst(T t) {
        synchronized (LOCK) {
            if (size() >= numElements) {
                try {
                    removeLast();
                } catch (Exception ex) {

                }
            }
            super.addFirst(t);
        }
    }

    /**
     * Add this element to the last position of this deque
     *
     * If the number of elements in the queue exceeds the queue size, remove the
     * first element before adding this new element
     *
     * @param t
     */
    @Override
    public void addLast(T t) {
        synchronized (LOCK) {
            if (size() >= numElements) {
                try {
                    removeFirst();
                } catch (Exception ex) {

                }
            }
            super.addLast(t);
        }
    }

    /**
     * Peek at the first element in the list
     *
     * @return
     */
    @Override
    public T peekFirst() {
        T returnTObj = null;
        synchronized (LOCK) {
            returnTObj = super.peekFirst();
        }
        return returnTObj;
    }

    /**
     * Remove the first occurance of this object from the deque
     *
     * @param o
     * @return
     */
    @Override
    public boolean remove(Object o) {
        boolean removed = false;
        synchronized (LOCK) {
            removed = super.removeFirstOccurrence(o);
        }
        return removed;
    }

    /**
     * Remove the element in the first position
     *
     * @return
     */
    @Override
    public T removeFirst() {
        T t = null;
        synchronized (LOCK) {
            t = super.pollFirst();// same thing
        }
        return t;
    }

    /**
     * Remove the element in the last position
     *
     * @return
     */
    @Override
    public T removeLast() {
        T t = null;
        synchronized (LOCK) {
            t = super.pollLast();// same thing
        }
        return t;
    }

    /**
     * Get the number of elements in the deque
     *
     * @return
     */
    @Override
    public int size() {
        int size = -1;
        synchronized (LOCK) {
            size = super.size();
        }
        return size;
    }

    /**
     * Remove all elements from the deque
     */
    @Override
    public void clear() {
        synchronized (LOCK) {
            super.clear();
        }
    }

    /**
     * Check if there are no elements in the deque
     *
     * @return
     */
    @Override
    public boolean isEmpty() {
        boolean isEmpty = false;
        synchronized (LOCK) {
            if (peekFirst() == null) {
                try {
                    removeFirst(); //throw it out
                } catch (Exception ex) {
                    //dont care
                }
            }
            isEmpty = super.isEmpty();
        }
        return isEmpty;
    }
}
