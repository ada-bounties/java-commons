package com.swiftcryptollc.commons.adabounties.utilities.timers;

import java.util.TimerTask;
import java.util.UUID;

/**
 * Simple Timeout Task
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
public class TimeoutTask extends TimerTask {

    private UUID uuid = null;
    private TimeoutInterface timeoutProcessor;

    /**
     * Construct a new task with the callback interface
     *
     * @param timeoutProcessor
     */
    public TimeoutTask(TimeoutInterface timeoutProcessor) {
        this.timeoutProcessor = timeoutProcessor;
        uuid = UUID.randomUUID();
    }

    /**
     *
     * @param timeoutProcessor
     * @param uuid
     */
    public TimeoutTask(TimeoutInterface timeoutProcessor, UUID uuid) {
        this.timeoutProcessor = timeoutProcessor;
        this.uuid = uuid;
    }

    /**
     * Called when the timer executes
     */
    @Override
    public void run() {
        timeoutProcessor.processTimeout(uuid);
    }

    /**
     * @return the uuid
     */
    public UUID getUuid() {
        return uuid;
    }

    /**
     * @param uuid the uuid to set
     */
    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }
}
