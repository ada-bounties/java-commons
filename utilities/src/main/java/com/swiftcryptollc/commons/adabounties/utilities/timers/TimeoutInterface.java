package com.swiftcryptollc.commons.adabounties.utilities.timers;

import java.util.UUID;

/**
 * Simple Interface for the Timeout Task to use for callbacks
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
public interface TimeoutInterface {

    public void processTimeout(UUID id);
}
