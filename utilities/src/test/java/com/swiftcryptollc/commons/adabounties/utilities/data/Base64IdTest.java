package com.swiftcryptollc.commons.adabounties.utilities.data;

import org.junit.jupiter.api.Test;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
public class Base64IdTest {

    //  @Test
    //   public void testIds() {
//        for (int i = 0;i < 10000;++i) {
    //          UUID uuid = UUID.randomUUID();
    //        String id = Base64Id.uuidToBase64(uuid);
    //      UUID newUUID= Base64Id.uuidFromBase64(id);
    //     assertTrue(uuid.equals(newUUID), "Doesn't match!");
    //   }
    // }
    @Test
    public void testId() {
        String id = "ywB6HwpXQkK//A7rJr2pUg";
        String newId = id.replace("/", "-").replace("+", "_");
        System.out.println(newId);
    }
}
